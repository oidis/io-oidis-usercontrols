/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { DropDownListItem } from "./DropDownList.js";

export class InlineItem extends DropDownListItem {
    private readonly envelop : GuiCommons;

    constructor() {
        super();
        this.envelop = new GuiCommons();
    }

    public Enabled($value? : boolean) : boolean {
        return this.envelop.Enabled($value);
    }

    protected innerHtml() : string {
        return `
            <button class="InlineItem btn btn-outline-dark btn-sm float-start" type="button" style="margin-right: 4px;margin-bottom: 5px;" data-oidis-bind="${this.envelop}">${this.Text()}
                <svg class="bi bi-x-circle-fill" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 10px;margin-bottom: 3px;" data-oidis-bind="${this}">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"></path>
                </svg>
            </button>`;
    }
}
