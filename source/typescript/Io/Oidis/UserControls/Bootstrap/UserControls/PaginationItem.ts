/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Label } from "./Label.js";

export class PaginationItem extends Label {

    protected innerHtml() : string {
        return `<li class="page-item" data-oidis-bind="${this}"><a class="page-link d-flex justify-content-center h-100" href="#">${this.Text()}</a></li>`;
    }
}
