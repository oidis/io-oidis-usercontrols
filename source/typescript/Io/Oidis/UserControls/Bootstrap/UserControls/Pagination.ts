/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { PaginationItem } from "./PaginationItem.js";

export class Pagination extends GuiCommons {
    private itemsPerPage : number;
    private pagesLimit : number;
    private size : number;
    private currentIndex : number;

    constructor() {
        super();
        this.itemsPerPage = 25;
        this.pagesLimit = 5;
        this.size = 0;
        this.currentIndex = 0;
    }

    public ItemsPerPage($value? : number) : number {
        return this.itemsPerPage = Property.PositiveInteger(this.itemsPerPage, $value, 1);
    }

    public PagesLimit($value? : number) : number {
        return this.pagesLimit = Property.PositiveInteger(this.pagesLimit, $value, 1);
    }

    public Size($value? : number) : number {
        return this.size = Property.PositiveInteger(this.size, $value);
    }

    public getIndex() : number {
        return this.currentIndex;
    }

    public Select($index : number) : void {
        this.currentIndex = Property.PositiveInteger(this.currentIndex, $index);
        const onSelect : any = ($offset : number) : void => {
            this.currentIndex = $offset;
            EventsManager.getInstanceSingleton().FireEvent(<any>this, EventType.ON_SELECT);
            EventsManager.getInstanceSingleton().FireEvent(<any>this, EventType.ON_CHANGE);
        };

        let pagesCount : number = Math.ceil(this.size / this.itemsPerPage);
        let pagesOffset : number = 0;
        let addPrevDots : boolean = false;
        let addNextDots : boolean = false;
        if (pagesCount > this.pagesLimit) {
            pagesCount = this.pagesLimit;
        }
        if (pagesCount * this.itemsPerPage - $index <= 0) {
            addPrevDots = true;
            pagesOffset = Math.floor($index / (this.pagesLimit * this.itemsPerPage));
        }
        if ((pagesOffset + 1) * pagesCount * this.itemsPerPage < this.size) {
            addNextDots = true;
        }

        this.Clear();
        const previousItem : PaginationItem = new PaginationItem();
        previousItem.Text(this.formatItem("«"));
        previousItem.getEvents().setOnClick(async ($args : EventArgs) : Promise<void> => {
            $args.PreventDefault();
            onSelect($index - this.itemsPerPage);
        });
        this.AddChild(previousItem);
        previousItem.Enabled($index !== 0);

        if (addPrevDots) {
            const prevDotsItem : PaginationItem = new PaginationItem();
            prevDotsItem.Text(this.formatItem("..."));
            prevDotsItem.getEvents().setOnClick(async ($args : EventArgs) : Promise<void> => {
                $args.PreventDefault();
                onSelect((pagesOffset - 1) * pagesCount * this.itemsPerPage + (pagesCount - 1) * this.itemsPerPage);
            });
            this.AddChild(prevDotsItem);
        }

        const pages : PaginationItem[] = [];
        const creatPageItem : any = ($offset : number) : void => {
            const item : PaginationItem = new PaginationItem();
            pages.push(item);
            let end : number = $offset + this.itemsPerPage;
            if (end > this.size) {
                end = this.size;
            }
            item.Text(($offset + 1) + "-" + end);
            item.getEvents().setOnClick(async ($args : EventArgs) : Promise<void> => {
                $args.PreventDefault();
                onSelect($offset);
                pages.forEach(($page : PaginationItem) : void => {
                    $page.Active(false);
                });
                item.Active(true);
            });
            this.AddChild(item);
            item.Active($offset === $index);
        };
        for (let index : number = 0; index < pagesCount; index++) {
            const offset : number = pagesOffset * this.pagesLimit * this.itemsPerPage + this.itemsPerPage * index;
            if (offset < this.size) {
                creatPageItem(offset);
            }
        }

        if (addNextDots) {
            const nextDotsItem : PaginationItem = new PaginationItem();
            nextDotsItem.Text(this.formatItem("..."));
            nextDotsItem.getEvents().setOnClick(async ($args : EventArgs) : Promise<void> => {
                $args.PreventDefault();
                onSelect((pagesOffset + 1) * pagesCount * this.itemsPerPage);
            });
            this.AddChild(nextDotsItem);
        }
        const nextItem : PaginationItem = new PaginationItem();
        nextItem.Text(this.formatItem("»"));
        nextItem.getEvents().setOnClick(async ($args : EventArgs) : Promise<void> => {
            $args.PreventDefault();
            onSelect($index + this.itemsPerPage);
        });
        this.AddChild(nextItem);
        nextItem.Enabled($index + this.itemsPerPage < this.size);
    }

    protected innerHtml() : string {
        return `<ul class="pagination"></ul>`;
    }

    private formatItem($text : string) : string {
        return `<span aria-hidden="true" style="margin: auto;">${$text}</span>`;
    }
}
