/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { bootstrap } from "@io-oidis-gui/Io/Oidis/Gui/Utils/EnvironmentHelper.js";

export class Toast extends GuiCommons {
    private text : string;
    private domInstance : any;
    private shown : boolean;
    private type : any;
    private formatter : ($type : ToastType) => string[];

    constructor($text? : string, $type? : ToastType) {
        super();
        this.text = this.Id();
        this.shown = false;
        this.Text($text);
        this.Type($type);
        this.formatter = ($type : ToastType) : string[] => {
            switch ($type) {
            case ToastType.SUCCESS:
                return ["text-white", "bg-success"];
            case ToastType.WARNING:
                return ["text-white", "bg-warning"];
            case ToastType.ERROR:
                return ["text-white", "bg-danger"];
            default:
                return [];
            }
        };
    }

    public Text($value? : string) : string {
        return this.text = Property.String(this.text, $value);
    }

    public Type($value? : ToastType) : ToastType {
        return this.type = Property.EnumType(this.type, $value, ToastType, ToastType.INFO);
    }

    public Visible($value? : boolean) : boolean {
        this.shown = Property.Boolean(this.shown, $value);
        if (ObjectValidator.IsSet($value)) {
            if (ObjectValidator.IsEmptyOrNull(this.domInstance)) {
                this.setAttribute($value, "show");
                this.setAttribute(!$value, "hide");
            } else {
                if ($value) {
                    this.domInstance.show();
                } else {
                    this.domInstance.hide();
                }
            }
        }
        return this.shown;
    }

    public setFormatter($value : ($type : ToastType) => string[]) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.formatter = $value;
        }
    }

    protected innerCode() : string {
        this.getEvents().setOnComplete(() : void => {
            this.domInstance = new bootstrap.Toast(this.InstanceOwner(), {animation: true, autohide: true, delay: 1000});
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        let background : string = this.formatter(this.type).join(" ");
        if (!ObjectValidator.IsEmptyOrNull(background)) {
            background = " " + background;
        }
        const role : string = this.type === ToastType.ERROR ? `role="alert" aria-live="assertive"` : `role="status" aria-live="polite"`;

        return `
<div class="toast align-items-center${background} fade hide" data-oidis-bind="${this}" ${role} aria-atomic="true" data-bs-autohide="true">
    <div class="d-flex">
        <div class="toast-body"><span>${this.Text()}<br /></span></div>
        <button class="btn btn-close me-2 m-auto" type="button" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
</div>`;
    }
}

export class ToastType extends BaseEnum {
    public static readonly INFO : string = "Info";
    public static readonly WARNING : string = "Warning";
    public static readonly SUCCESS : string = "Success";
    public static readonly ERROR : string = "Error";
}
