/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { bootstrap } from "@io-oidis-gui/Io/Oidis/Gui/Utils/EnvironmentHelper.js";

export class Dialog extends GuiCommons {
    private shown : boolean;
    private btModal : any;

    constructor() {
        super();
        this.shown = false;
    }

    public Visible($value? : boolean) : boolean {
        this.shown = Property.Boolean(this.shown, $value);
        if (ObjectValidator.IsSet($value) && (<any>this).loaded) {
            if (ObjectValidator.IsEmptyOrNull(this.btModal)) {
                this.assignBTInstance();
            }
            this.getEvents().FireAsynchronousMethod(() : void => {
                if (this.shown) {
                    this.btModal.show();
                } else {
                    this.btModal.hide();
                }
            }, true, 100);
        }
        return this.shown;
    }

    protected innerCode() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.innerHtml())) {
            this.getEvents().setOnLoad(() : void => {
                this.InstanceOwner(this.InstanceOwner().firstElementChild);
            });
        }
        return super.innerCode();
    }

    private assignBTInstance() : void {
        if (this.InstanceOwner().classList.contains("offcanvas")) {
            this.btModal = new bootstrap.Offcanvas(this.InstanceOwner());
            this.InstanceOwner().addEventListener("shown.bs.offcanvas", () : void => {
                this.shown = true;
            });
            this.InstanceOwner().addEventListener("hidden.bs.offcanvas", () : void => {
                this.shown = false;
            });
        } else {
            this.btModal = new bootstrap.Modal(this.InstanceOwner());
            this.InstanceOwner().addEventListener("shown.bs.modal", () : void => {
                this.shown = true;
            });
            this.InstanceOwner().addEventListener("hidden.bs.modal", () : void => {
                this.shown = false;
            });
        }
    }
}
