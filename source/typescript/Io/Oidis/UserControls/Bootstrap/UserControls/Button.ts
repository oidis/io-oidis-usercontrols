/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class Button extends GuiCommons {
    private value : any;
    private autoBlur : boolean;

    constructor() {
        super();
        this.value = null;
        this.autoBlur = true;
    }

    public Value($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.value = $value;
        }
        return this.value;
    }

    /**
     * @param {boolean} $value Specify automatic blur behavior
     * @returns {string} Returns true if button should automatically lose focus after click event otherwise false.
     */
    public AutoBlur($value? : boolean) : boolean {
        return this.autoBlur = Property.Boolean(this.autoBlur, $value);
    }

    protected innerCode() : string {
        this.getEvents().setOnClick(() : void => {
            if (this.autoBlur) {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    this.InstanceOwner().blur();
                }, 1000);
            }
        });
        return super.innerCode();
    }
}
