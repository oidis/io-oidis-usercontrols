/*! ******************************************************************************************************** *
 *
 * Copyright 2021 NXP
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class CheckBox extends GuiCommons {

    public Value($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().checked = $value;
        }
        return this.getInputDOM().checked;
    }

    public Enabled($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().disabled = !$value;
        }
        return !this.getInputDOM().disabled;
    }
}
