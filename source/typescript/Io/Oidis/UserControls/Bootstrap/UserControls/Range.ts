/*! ******************************************************************************************************** *
 *
 * Copyright 2021 NXP
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class Range extends GuiCommons {
    private descriptor : GuiCommons;

    public Value($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().value = <any>$value;
        }
        this.updateValueDescriptor();
        return StringUtils.ToInteger(<any>this.getInputDOM().value);
    }

    public Min($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().min = <any>$value;
        }
        this.updateValueDescriptor();
        return StringUtils.ToInteger(<any>this.getInputDOM().min);
    }

    public Max($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().max = <any>$value;
        }
        this.updateValueDescriptor();
        return StringUtils.ToInteger(<any>this.getInputDOM().max);
    }

    public Step($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().step = <any>$value;
        }
        this.updateValueDescriptor();
        return StringUtils.ToInteger(<any>this.getInputDOM().step);
    }

    public setDescriptorInstance($value : GuiCommons) : void {
        this.descriptor = $value;
    }

    protected innerCode() : string {
        this.getEvents().setEvent("oninput", () : void => {
            this.updateValueDescriptor();
        });
        return super.innerCode();
    }

    private updateValueDescriptor() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.descriptor)) {
            const range : HTMLInputElement = this.getInputDOM();
            this.descriptor.Content(range.value);
            if (!ObjectValidator.IsEmptyOrNull(this.descriptor.InstanceOwner())) {
                const ratio : number = (StringUtils.ToInteger(range.value) - StringUtils.ToInteger(range.min)) /
                    (StringUtils.ToInteger(range.max) - StringUtils.ToInteger(range.min)) * 100;
                this.descriptor.InstanceOwner().style.left = `${ratio}%`;
            }
        }
    }
}
