/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "./Button.js";
import {
    DropDownList as Parent,
    DropDownListItem
} from "./DropDownList.js";
import { DropDownSelectItem } from "./DropDownMultiselect.js";
import { TextField } from "./TextField.js";

export class DropDownSearch extends Parent {
    protected readonly menuContent : GuiCommons;
    protected readonly searchField : TextField;
    protected readonly search : Button;
    protected readonly clearFilter : Button;
    protected items : DropDownListItem[];
    private itemSelected : boolean;
    private fillInContainer : boolean;

    constructor() {
        super();
        this.menuContent = new GuiCommons();
        this.searchField = new TextField();
        this.search = new Button();
        this.clearFilter = new Button();
        this.selectorCss = " btn-primary";
        this.items = [];
        this.itemSelected = false;
        this.fillInContainer = true;
    }

    public AddItem($text : string) : DropDownListItem {
        const item : DropDownListItem = new DropDownListItem();
        item.Text($text);
        item.getEvents().setOnClick(() : void => {
            this.itemSelected = true;
        });
        this.items.push(item);
        this.menu.AddChild(item);
        return item;
    }

    public Clear() : void {
        super.Clear();
        this.items = [];
    }

    public FillInParent($value? : boolean) : boolean {
        return this.fillInContainer = Property.Boolean(this.fillInContainer, $value);
    }

    protected innerCode() : string {
        let menuSelected : boolean = false;
        this.menuContent.getEvents().setOnClick(() : void => {
            menuSelected = true;
        });
        this.searchField.getEvents().setEvent(EventType.ON_BLUR, () : void => {
            menuSelected = false;
        });
        this.searchField.getEvents().setEvent(EventType.ON_KEY_UP, () : void => {
            this.searchItems();
        });
        this.search.getEvents().setOnClick(() : void => {
            this.searchItems();
        });
        this.clearFilter.getEvents().setOnClick(() : void => {
            this.searchField.Value("");
            this.clearFilter.Visible(false);
            this.items.forEach(($item : DropDownSelectItem) : void => {
                $item.Visible(true);
            });
        });
        this.getEvents().setOnComplete(() : void => {
            this.InstanceOwner().addEventListener("show.bs.dropdown", () : void => {
                menuSelected = false;
                this.itemSelected = false;
            });
            this.InstanceOwner().addEventListener("hide.bs.dropdown", ($event : Event) : void => {
                if (menuSelected && !this.itemSelected) {
                    menuSelected = false;
                    $event.preventDefault();
                }
            });
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<div class="dropdown float-start" style="width: 100%;"><button class="btn ${this.selectorCss}" aria-expanded="false" data-bs-toggle="dropdown" type="button" data-oidis-bind="${this.selector}" style="width: 100%;">${this.Text()}</button>
    <div class="dropdown-menu" data-oidis-bind="${this.menuContent}" style="${this.fillInContainer ? "width: 100%;" : ""}">
        <div class="input-group input-group-sm" style="padding-right: 6px;padding-left: 6px;"><input class="form-control" type="text" data-oidis-bind="${this.searchField}" /><span class="input-group-text" style="margin: 0;padding: 0;width: 0;position: relative;border-width: 0px;"><button class="btn ${GeneralCSS.DNONE}" type="button" style="position: absolute;left: -40px;background-color: transparent;border-width: 0px;z-index: 100;border-radius: 0;" data-oidis-bind="${this.clearFilter}"><svg class="bi bi-x-circle text-primary" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 3px;">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                    </svg></button></span><button class="btn btn-primary" type="button" data-oidis-bind="${this.search}" style="width: 32px;"><svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                </svg></button></div>
        <div class="dropdown-divider"></div>
        <div data-oidis-bind="${this.menu}"></div>
    </div>
</div>`;
    }

    private searchItems() : void {
        const value : string = this.searchField.Value();
        this.clearFilter.Visible(value.length !== 0);
        if (value.length >= 3) {
            this.items.forEach(($item : DropDownSelectItem) : void => {
                /// TODO: highlight text somehow?
                $item.Visible(StringUtils.ContainsIgnoreCase($item.Text(), value));
            });
        } else {
            this.items.forEach(($item : DropDownSelectItem) : void => {
                $item.Visible(true);
            });
        }
    }
}
