/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Label } from "./Label.js";
import { TextField } from "./TextField.js";

export class InlineInputGroup extends Label {
    public readonly field : TextField;

    constructor() {
        super();
        this.field = new TextField();
    }

    public Value($value? : string) : string {
        return this.field.Value($value);
    }

    protected innerHtml() : string {
        return `
            <div class="InlineInputGroup input-group input-group-sm float-start" style="width: 20%;">
                <input class="form-control" type="text" data-oidis-bind="${this.field}" />
                <button class="btn btn-primary" type="button" data-oidis-bind="${this}">${this.Text()}</button>
            </div>`;
    }
}
