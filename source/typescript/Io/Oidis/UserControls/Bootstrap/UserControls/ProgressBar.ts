/*! ******************************************************************************************************** *
 *
 * Copyright 2021 NXP
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";

export class ProgressBar extends GuiCommons {
    private value : number;

    constructor() {
        super();
        this.value = 0;
    }

    public Value($value? : number) : number {
        this.value = Property.Integer(this.value, $value);
        this.InstanceOwner().setAttribute("aria-valuenow", this.value);
        ElementManager.setCssProperty(this.InstanceOwner(), "width", this.value + "%");
        this.Content(this.value + "%");
        return this.value;
    }
}
