/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class Label extends GuiCommons {
    private text : string;

    constructor() {
        super();
        this.text = "";
    }

    public Text($value? : string) : string {
        this.text = Property.String(this.text, $value);
        if (ObjectValidator.IsSet($value)) {
            this.Content(this.text);
        }
        return this.text;
    }
}
