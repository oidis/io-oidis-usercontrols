/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "./Button.js";

export class Accordion extends GuiCommons {
    private items : AccordionItem[];

    constructor() {
        super();
        this.items = [];
    }

    public AddItem($header : string, $content : string, $expanded : boolean = false) : AccordionItem {
        const item : AccordionItem = new AccordionItem(this.items.length, $expanded);
        this.items.push(item);
        this.AddChild(item);
        item.text.Content($header);
        item.content.Content($content);
        return item;
    }

    protected innerHtml() : string {
        return `<div id="accordion-${this.Id()}" class="accordion" role="tablist" data-oidis-bind="${this}"></div>`;
    }
}

export class AccordionItem extends GuiCommons {
    public readonly text : Button;
    public readonly content : GuiCommons;
    private readonly index : number;
    private readonly expanded : boolean;

    constructor($index : number, $expanded : boolean = false) {
        super();
        this.index = $index + 1;
        this.expanded = $expanded;
        this.text = new Button();
        this.content = new GuiCommons();
    }

    public Parent($value? : Accordion) : Accordion {
        return <Accordion>super.Parent($value);
    }

    protected innerHtml() : string {
        const parentId : string = "accordion-" + this.Parent().Id();
        return `
<div data-oidis-bind="${this}" class="accordion-item">
    <h2 class="accordion-header" role="tab">
        <button class="accordion-button${this.expanded ? "" : " collapsed"}" type="button" data-bs-toggle="collapse" data-bs-target="#${parentId} .accordionitem-${this.index}" aria-expanded="false" aria-controls="${parentId} .accordionitem-${this.index}" data-oidis-bind="${this.text}">Item ${this.index}</button>
    </h2>
    <div class="accordion-collapse collapse${this.expanded ? " show" : ""} accordionitem-${this.index}" role="tabpanel" data-bs-parent="#${parentId}">
        <div class="accordion-body" data-oidis-bind="${this.content}"></div>
    </div>
</div>`;
    }
}
