/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { CheckBox } from "./CheckBox.js";
import { Label } from "./Label.js";

export class Switch extends CheckBox {
    private readonly text : Label;

    constructor() {
        super();
        this.text = new Label();
    }

    public Text($value? : string) : string {
        return this.text.Text($value);
    }

    protected innerHtml() : string {
        return `
<div class="form-check form-switch" style="margin-right: 5px;float: left;">
    <input class="form-check-input" type="checkbox" data-oidis-bind="${this}" />
    <label class="form-check-label" data-oidis-bind="${this.text}">${this.text.Text()}</label>
</div>`;
    }
}
