/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { ValidationBinding } from "../../Bindings/ValidationBinding.js";

export class TextField extends GuiCommons {
    private error : boolean;
    private allowHtmlTags : boolean;
    private sanitized : boolean;

    constructor() {
        super();
        this.error = false;
        this.allowHtmlTags = false;
        this.sanitized = false;
    }

    public Value($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().value = $value;
        }
        return this.getInputDOM().value;
    }

    public Error($value? : boolean) : boolean {
        this.error = Property.Boolean(this.error, $value);
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.setAttribute(this.error, "border-danger");
        }
        return this.error;
    }

    public Readonly($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.getInputDOM().readOnly = $value;
        }
        return this.getInputDOM().readOnly;
    }

    public HtmlTagsEnabled($value? : boolean) : boolean {
        return this.allowHtmlTags = Property.Boolean(this.allowHtmlTags, $value);
    }

    protected innerCode() : string {
        EventsManager.getInstanceSingleton().setEvent(<any>this, EventType.ON_COMPLETE, () : void => {
            if (!this.allowHtmlTags && !this.sanitized) {
                ValidationBinding.setOnlyQuerySafeAllowed(this);
                this.sanitized = true;
            }
        });
        return super.innerCode();
    }
}
