/*! ******************************************************************************************************** *
 *
 * Copyright 2021 NXP
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";

export class Image extends GuiCommons {
    private source : string;

    constructor() {
        super();
        this.source = "";
    }

    public Source($value? : string) : string {
        this.source = Property.String(this.source, $value);
        if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
            this.InstanceOwner().src = this.source;
        }
        return this.source;
    }
}
