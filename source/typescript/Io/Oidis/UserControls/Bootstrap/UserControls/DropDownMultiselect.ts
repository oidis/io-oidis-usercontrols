/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Button } from "./Button.js";
import { CheckBox } from "./CheckBox.js";
import { DropDownListItem } from "./DropDownList.js";
import { DropDownSearch } from "./DropDownSearch.js";

export class DropDownMultiselect extends DropDownSearch {
    protected readonly selectAll : Button;
    protected readonly unselectAll : Button;
    protected localization : IDropDownMultiselectLocalization;
    private readonly searchContent : GuiCommons;

    constructor() {
        super();
        this.selectAll = new Button();
        this.unselectAll = new Button();
        this.searchContent = new GuiCommons();
        this.localization = {
            emptySelection: "choose",
            fullSelection : "all",
            selectAll     : "select all",
            unselectAll   : "unselect all"
        };
    }

    public AddItem($text : string) : DropDownSelectItem {
        const item : DropDownSelectItem = new DropDownSelectItem();
        item.Text($text);
        item.getEvents().setOnClick(() : void => {
            this.Select(item, !item.Selected());
        });
        this.items.push(item);
        this.menu.AddChild(item);
        this.processSelection();
        return item;
    }

    public SelectAll($value : boolean) : void {
        this.unselectAll.Visible($value);
        this.selectAll.Visible(!$value);
        this.items.forEach(($item : DropDownSelectItem) : void => {
            $item.Selected($value);
        });
        this.processSelection();
    }

    public Select($item : DropDownSelectItem, $value : boolean = true) : void {
        $item.Selected($value);
        this.processSelection();
    }

    public SearchEnabled($value? : boolean) : boolean {
        return this.searchContent.Visible($value);
    }

    public IsAllSelected() : boolean {
        let allSelected : boolean = true;
        this.items.forEach(($item : DropDownSelectItem) : void => {
            if (!$item.Selected()) {
                allSelected = false;
            }
        });
        return allSelected;
    }

    protected innerCode() : string {
        this.getEvents().setOnComplete(() : void => {
            this.selectAll.Visible(true);
        });
        this.selectAll.getEvents().setOnClick(() : void => {
            this.SelectAll(true);
        });
        this.unselectAll.getEvents().setOnClick(() : void => {
            this.SelectAll(false);
        });
        return super.innerCode();
    }

    protected innerHtml() : string {
        return `
<div class="dropdown float-start" style="width: 100%;" data-oidis-bind="${this}"><button class="btn ${this.selectorCss}" aria-expanded="false" data-bs-toggle="dropdown" data-bs-auto-close="false" type="button" data-oidis-bind="${this.selector}" style="width: 100%;">${this.Text()}</button>
    <div class="dropdown-menu" data-oidis-bind="${this.menuContent}" style="width: 100%;">
        <div class="input-group input-group-sm" style="padding-right: 6px;padding-left: 6px;" data-oidis-bind="${this.searchContent}"><input class="form-control" type="text" data-oidis-bind="${this.searchField}" /><span class="input-group-text" style="margin: 0;padding: 0;width: 0;position: relative;border-width: 0px;"><button class="btn ${GeneralCSS.DNONE}" type="button" style="position: absolute;left: -40px;background-color: transparent;border-width: 0px;z-index: 100;border-radius: 0;" data-oidis-bind="${this.clearFilter}"><svg class="bi bi-x-circle text-primary" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-bottom: 3px;">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                    </svg></button></span><button class="btn btn-primary" type="button" data-oidis-bind="${this.search}" style="width: 32px;"><svg class="bi bi-search" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                </svg></button></div><button class="btn btn-outline-dark btn-sm ${GeneralCSS.DNONE}" type="button" style="width: calc(100% - 12px);margin-left: 6px;margin-top: 6px;height: 31px;" data-oidis-bind="${this.selectAll}"><svg class="bi bi-list-check" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;font-size: 22px;">
                <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"></path>
            </svg>${this.localization.selectAll}</button><button class="btn btn-outline-dark btn-sm ${GeneralCSS.DNONE}" type="button" style="width: calc(100% - 12px);margin-left: 6px;margin-top: 6px;height: 31px;" data-oidis-bind="${this.unselectAll}"><svg class="bi bi-list-task" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-right: 10px;font-size: 22px;">
                <path fill-rule="evenodd" d="M2 2.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5H2zM3 3H2v1h1V3z"></path>
                <path d="M5 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM5.5 7a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 4a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9z"></path>
                <path fill-rule="evenodd" d="M1.5 7a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5V7zM2 7h1v1H2V7zm0 3.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5H2zm1 .5H2v1h1v-1z"></path>
            </svg>${this.localization.unselectAll}</button>
        <div class="dropdown-divider"></div>
        <div data-oidis-bind="${this.menu}"></div>
    </div>
</div>`;
    }

    private processSelection() : void {
        const text : string[] = [];
        const data : any[] = [];
        let allSelected : boolean = true;
        let nonSelected : boolean = true;
        this.items.forEach(($item : DropDownSelectItem) : void => {
            if ($item.Selected()) {
                text.push($item.Text());
                data.push($item.Data());
                nonSelected = false;
            } else {
                allSelected = false;
            }
        });
        if (allSelected) {
            this.Text(this.localization.fullSelection);
            this.selectAll.Visible(false);
            this.unselectAll.Visible(true);
        } else {
            this.unselectAll.Visible(false);
            this.selectAll.Visible(true);
            if (nonSelected) {
                this.Text(this.localization.emptySelection);
            } else {
                this.Text("<span style='width: calc(100% - 15px); overflow: hidden; display: block;'>" + text.join(",") + "</span>");
            }
        }
        this.Data(data);
    }
}

export interface IDropDownMultiselectLocalization {
    selectAll : string;
    unselectAll : string;
    emptySelection : string;
    fullSelection : string;
}

export class DropDownSelectItem extends DropDownListItem {
    private readonly select : CheckBox;

    constructor() {
        super();
        this.select = new CheckBox();
    }

    public Selected($value? : boolean) : boolean {
        return this.select.Value($value);
    }

    protected innerHtml() : string {
        return `<div class="form-check dropdown-item" style="padding-left: 32px;cursor: pointer;"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.select}" style="pointer-events: none;" /><label class="form-check-label" style="cursor: pointer;">${this.Text()}</label></div>`;
    }
}

// generated-code-start
/* eslint-disable */
export const IDropDownMultiselectLocalization = globalThis.RegisterInterface(["selectAll", "unselectAll", "emptySelection", "fullSelection"]);
/* eslint-enable */
// generated-code-end
