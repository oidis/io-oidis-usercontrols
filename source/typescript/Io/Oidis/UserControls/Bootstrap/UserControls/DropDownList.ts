/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { Button } from "./Button.js";
import { Label } from "./Label.js";

export class DropDownList extends Label {
    protected readonly menu : GuiCommons;
    protected readonly selector : Button;
    protected selectorCss : string;
    protected selectorText : string;

    constructor() {
        super();
        this.menu = new GuiCommons();
        this.selector = new Button();
        this.selectorText = this.Id();
        this.selectorCss = " btn-primary btn-sm dropdown-toggle";
    }

    public Text($value? : string) : string {
        this.selectorText = Property.String(this.selectorText, $value);
        if (ObjectValidator.IsSet($value)) {
            this.selector.Content(this.selectorText);
        }
        return this.selectorText;
    }

    public AddItem($text : string) : DropDownListItem {
        const item : DropDownListItem = new DropDownListItem();
        item.Text($text);
        this.menu.AddChild(item);
        return item;
    }

    public Clear() : void {
        this.menu.Clear();
    }

    public setSelectorStyle($value : string) : void {
        this.selectorCss = " " + $value;
    }

    public Enabled($value? : boolean) : boolean {
        return this.selector.Enabled($value);
    }

    protected innerHtml() : string {
        return `
            <div class="dropdown float-start">
                <button class="btn ${this.selectorCss}" aria-expanded="false" data-bs-toggle="dropdown" type="button" data-oidis-bind="${this.selector}" style="border-radius: 25px;">${this.Text()}</button>
                <div data-oidis-bind="${this.menu}" class="dropdown-menu"></div>
            </div>`;
    }
}

export class DropDownListItem extends Button {
    private text : string;

    constructor() {
        super();
        this.text = this.Id();
    }

    public Text($value? : string) : string {
        this.text = Property.String(this.text, $value);
        if (ObjectValidator.IsSet($value)) {
            this.Content(this.text);
        }
        return this.text;
    }

    protected innerHtml() : string {
        return `<button class="dropdown-item" type="button">${this.text}</button>`;
    }
}
