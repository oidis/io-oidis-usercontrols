/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsBinding } from "@io-oidis-gui/Io/Oidis/Gui/Bindings/EventsBinding.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { CheckBox } from "./CheckBox.js";
import { DropDownList, DropDownListItem } from "./DropDownList.js";
import { InlineItem } from "./InlineItem.js";
import { Label } from "./Label.js";
import { Pagination } from "./Pagination.js";
import { ProgressBar } from "./ProgressBar.js";

export class ResponsiveTable<T extends ResponsiveTableItem> extends GuiCommons {
    protected localization : IResponsiveTableLocalization;
    private readonly selectAll : CheckBox;
    private readonly filter : GuiCommons;
    private readonly noDataAlert : GuiCommons;
    private readonly table : GuiCommons;
    private readonly pagination : Pagination;
    private tableColumns : IResponsiveTableColumn[];
    private tableColumnsById : any;
    private persistence : IPersistenceHandler;
    private readonly progress : GuiCommons;
    private readonly progressValue : ProgressBar;
    private items : T[];
    private isFilterSelectorVisible : boolean;
    private itemSelectHandler : ($item : T) => Promise<void>;
    private fetchHandler : ($offset : number, $limit : number) => Promise<IModelListResult>;
    private recordToItemMapper : ($data : any, $item : T, $index : number) => Promise<void>;
    private itemCreateFormatter : ($item : T, $index : number) => Promise<void>;

    constructor() {
        super();
        this.selectAll = new CheckBox();
        this.table = new GuiCommons();
        this.pagination = new Pagination();
        this.noDataAlert = new GuiCommons();
        this.filter = new GuiCommons();
        this.tableColumns = [];
        this.tableColumnsById = {};
        this.items = [];
        this.progress = new GuiCommons();
        this.progressValue = new ProgressBar();
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
        this.isFilterSelectorVisible = true;
        this.itemSelectHandler = async () : Promise<void> => {
            // do nothing
        };
        this.fetchHandler = async () : Promise<IModelListResult> => {
            return {data: [], offset: 0, limit: -1, size: 0};
        };
        this.recordToItemMapper = async () : Promise<void> => {
            // do nothing
        };
        this.itemCreateFormatter = async () : Promise<void> => {
            // do nothing
        };
        this.localization = {
            selectAll: "select all",
            show     : "Show",
            showMore : "Show more",
            noData   : "No data found"
        };
    }

    public Enabled($value? : boolean) : boolean {
        if (!this.table.Enabled($value)) {
            this.noDataAlert.Visible(false);
        }
        return this.pagination.Enabled($value);
    }

    public FilterSelectorVisible($value? : boolean) : boolean {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (this.isFilterSelectorVisible !== $value) {
                this.isFilterSelectorVisible = $value;
                this.generateFilterList();
            }
        }
        return this.isFilterSelectorVisible;
    }

    public ItemsPerPage($value? : number) : number {
        return this.pagination.ItemsPerPage($value);
    }

    public PagesLimit($value? : number) : number {
        return this.pagination.PagesLimit($value);
    }

    public getIndex() : number {
        return this.pagination.getIndex();
    }

    public SelectPage($index : number, $from? : number) : void {
        if ($index >= 0) {
            if ($from > this.ItemsPerPage()) {
                this.pagination.Size($from);
                this.pagination.Select($index);
            }
            this.pagination.Visible($from > this.ItemsPerPage());
            this.pagination.Enabled(true);
        } else {
            this.pagination.Visible(false);
        }
        this.handleSelection();
    }

    public setOnPageSelect($handler : ($index : number) => Promise<void>) : void {
        this.pagination.getEvents().setOnSelect(async () : Promise<void> => {
            await $handler(this.getIndex());
        });
    }

    public setOnAllSelect($handler : ($pageIndex : number, $selected : boolean) => Promise<void>) : void {
        EventsBinding.SingleClick(this.selectAll, async () : Promise<void> => {
            await $handler(this.getIndex(), this.selectAll.Value());
        });
    }

    public setOnItemSelect($handler : ($item : T) => Promise<void>) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.itemSelectHandler = $handler;
        }
    }

    public Filter() : void {
        let rowProcessed : boolean = false;
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            $column.controls.forEach(($control : GuiCommons) : void => {
                $control.Visible($column.enabled && !$column.hidden);
                if (!rowProcessed) {
                    try {
                        const row : T = <T>$control.Parent();
                        if (!ObjectValidator.IsEmptyOrNull(row)) {
                            this.filterRow(row);
                        }
                    } catch (ex) {
                        LogIt.Warning("Failed to filter row. \n" + ex.stack);
                    }
                }
            });
            rowProcessed = true;
        });
    }

    public Init($filter : string[] = null) : void {
        const defaultConfig : IResponsiveTableColumn[] = this.getFilterConfig();
        if (ObjectValidator.IsEmptyOrNull(this.tableColumns)) {
            this.tableColumns = defaultConfig;
        } else {
            this.tableColumns.forEach(($column : IResponsiveTableColumn, $index : number) : void => {
                $column.enabled = defaultConfig[$index].enabled;
            });
        }
        if (!ObjectValidator.IsEmptyOrNull($filter)) {
            this.persistence.Variable("tableFilter", $filter);
        }
        let tableFilter : string[] = this.persistence.Variable("tableFilter");
        if (ObjectValidator.IsEmptyOrNull(tableFilter)) {
            tableFilter = [];
        }
        this.tableColumnsById = {};
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            if (!ObjectValidator.IsEmptyOrNull(tableFilter)) {
                $column.enabled = tableFilter.includes($column.id);
            }
            this.tableColumnsById[$column.id] = $column;
        });
        this.generateFilterList();
    }

    public Clear() : void {
        this.table.Clear();
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            $column.controls = [];
        });
        this.items = [];
        this.progress.Visible(false);
        this.noDataAlert.Visible(true);
        this.selectAll.Value(false);
    }

    public Restore() : void {
        this.items.forEach(($item : T) : void => {
            $item.Visible(false);
            $item.Restore();
        });
    }

    public AddItem() : T {
        const item : T = this.getNewItemInstance();
        EventsBinding.SingleClick(item.select, async () : Promise<void> => {
            await this.itemSelectHandler(item);
            this.handleSelection();
        });
        this.table.AddChild(item);
        this.items.push(item);
        this.setItemToFilterMapping(item);
        item.Visible(false);
        return item;
    }

    public getItem($index : number) : T {
        return this.items[$index];
    }

    public IsEmpty() : boolean {
        return ObjectValidator.IsEmptyOrNull(this.items);
    }

    public BindData($onFetch : ($offset : number, $limit : number) => Promise<IModelListResult>,
                    $onRowMapping : ($data : any, $item : T, $index : number) => Promise<void>,
                    $onRowCreate? : ($item : T, $index : number) => Promise<void>) : void {
        this.fetchHandler = $onFetch;
        this.recordToItemMapper = $onRowMapping;
        if (!ObjectValidator.IsEmptyOrNull($onRowCreate)) {
            this.itemCreateFormatter = $onRowCreate;
        }
        this.setOnPageSelect(async () : Promise<void> => {
            await this.ShowData();
        });
    }

    public async ShowData($resetOffset : boolean = false) : Promise<void> {
        if ($resetOffset) {
            this.SelectPage(0);
        }
        this.Enabled(false);
        if (this.IsEmpty()) {
            this.Init();
            this.progress.Visible(true);
        }
        try {
            const data : IModelListResult = await this.fetchHandler(this.getIndex(), this.ItemsPerPage());
            if (data.size > 0) {
                let index : number = 0;
                if (this.IsEmpty()) {
                    const items : number[] = new Array(data.limit > 0 ? data.limit : data.size);
                    if (!ObjectValidator.IsEmptyOrNull(items)) {
                        for await (const item of items) { // eslint-disable-line @typescript-eslint/no-unused-vars
                            try {
                                await this.itemCreateFormatter(this.AddItem(), index);
                            } catch (ex) {
                                LogIt.Warning("Failed to format table row " + index + ".\n" + ex.stack);
                            }
                            index++;
                        }
                    }
                } else {
                    this.Restore();
                }
                this.progress.Visible(false);
                index = 0;
                for await (const record of data.data) {
                    const item : T = this.getItem(index);
                    if (!ObjectValidator.IsEmptyOrNull(item)) {
                        try {
                            await this.recordToItemMapper(record, item, index);
                            item.Visible(true);
                        } catch (ex) {
                            LogIt.Warning("Failed to map table row to data.\n" + ex.stack);
                        }
                    }
                    index++;
                }
            } else {
                this.Clear();
            }
            this.progress.Visible(false);
            this.Enabled(true);
            this.SelectPage(this.getIndex(), data.size);
        } catch (ex) {
            LogIt.Warning("Failed to process table data.\n" + ex.stack);
            this.Clear();
        }
    }

    protected assignFilterTo($id : string, $control : GuiCommons) : void {
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            if ($column.id === $id) {
                $column.controls.push($control);
            }
        });
    }

    protected getNewItemInstance() : T {
        return (<any>new ResponsiveTableItem());
    }

    protected innerHtml() : string {
        return `
<section class="ResponsiveTable">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-auto align-self-center" style="margin-bottom: 3px;">
            <div class="form-check" style="margin-left: 5px;"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.selectAll}" /><label class="form-check-label">${this.localization.selectAll}</label></div>
        </div>
        <div class="col-auto align-self-center" style="margin-bottom: 3px;"><span>${this.localization.show}:</span></div>
        <div class="col-10 Filter" data-oidis-bind="${this.filter}"></div>
    </div>
    <div class="alert alert-danger text-center" role="alert" data-oidis-bind="${this.noDataAlert}"><span><strong>${this.localization.noData}</strong></span></div>
    <div class="Table" data-oidis-bind="${this.table}" style="margin-bottom: 5px;"></div>
    <div class="progress" data-oidis-bind="${this.progress}" style="height: 40px;margin-top: -14px;margin-bottom: 10px;">
        <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" data-oidis-bind="${this.progressValue}"><span class="visually-hidden">100%</span></div>
    </div>
    <nav class="d-flex justify-content-center">
        <ul class="pagination ${GeneralCSS.DNONE}" data-oidis-bind="${this.pagination}"></ul>
    </nav>
</section>`;
    }

    protected getFilterConfig() : IResponsiveTableColumn[] {
        return [
            {
                id: ResponsiveTableColumIds.ID, text: "ID", icon: `
<svg class="bi bi-hash" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 3px;margin-bottom: 3px;margin-right: 3px;">
    <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"></path>
</svg>`, controls: [], enabled: true, hidden: false
            },
            {
                id: ResponsiveTableColumIds.TEXT, text: "Content", icon: `
<svg class="bi bi-file-text" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 3px;margin-bottom: 3px;margin-right: 3px;">
    <path d="M5 4a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z"></path>
    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"></path>
</svg>`, controls: [], enabled: true, hidden: false
            }
        ];
    }

    protected getFilterById($id : string) : IResponsiveTableColumn {
        if (this.tableColumnsById.hasOwnProperty($id)) {
            return this.tableColumnsById[$id];
        }
        return null;
    }

    protected filterRow($item : T) : void {
        // override this method in case of that custom filtering action is needed
    }

    protected setItemToFilterMapping($item : T) : void {
        this.assignFilterTo("id", $item.idContent);
        this.assignFilterTo("text", $item.textContent);
    }

    protected getItems() : T[] {
        return this.items;
    }

    protected getFilterSelectorInstance() : DropDownList {
        return new DropDownList();
    }

    protected getHeaderItem($column : IResponsiveTableColumn) : InlineItem {
        const item : InlineItem = new InlineItem();
        item.Text($column.icon + $column.text);
        return item;
    }

    private handleSelection() : void {
        let allSelected : boolean = true;
        let noneSelected : boolean = true;
        let itemVisible : boolean = false;
        this.items.forEach(($item : T) : void => {
            if ($item.Visible()) {
                itemVisible = true;
                if (!$item.select.Value()) {
                    allSelected = false;
                } else {
                    noneSelected = false;
                }
            }
        });
        if (itemVisible) {
            this.noDataAlert.Visible(false);
            if (allSelected) {
                this.selectAll.Value(true);
            } else if (noneSelected) {
                this.selectAll.Value(false);
            } else {
                this.selectAll.InstanceOwner().indeterminate = true;
            }
        } else {
            this.progress.Visible(false);
            this.noDataAlert.Visible(true);
            this.selectAll.Value(false);
        }
    }

    private generateFilterList() : void {
        this.filter.Clear();
        const tableFilter : string[] = [];
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            if ($column.enabled && !$column.hidden) {
                const item : InlineItem = this.getHeaderItem($column);
                item.Data($column.id);
                item.getEvents().setOnClick(() : void => {
                    $column.enabled = !$column.enabled;
                    let noneSelected : boolean = true;
                    this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
                        if ($column.enabled && !$column.hidden) {
                            noneSelected = false;
                        }
                    });
                    if (noneSelected) {
                        this.persistence.Destroy("tableFilter");
                        this.Init();
                    } else {
                        this.generateFilterList();
                    }
                    this.Filter();
                });
                this.filter.AddChild(item);
                tableFilter.push($column.id);
            }
        });
        this.persistence.Variable("tableFilter", tableFilter);
        const dropDown : DropDownList = this.getFilterSelectorInstance();
        dropDown.Text(this.localization.showMore);
        let showFiltersSelector : boolean = false;
        this.tableColumns.forEach(($column : IResponsiveTableColumn) : void => {
            if (!$column.enabled && !$column.hidden) {
                const item : DropDownListItem = dropDown.AddItem($column.icon + $column.text);
                item.Data($column.id);
                item.getEvents().setOnClick(() : void => {
                    $column.enabled = true;
                    this.generateFilterList();
                    this.Filter();
                });
                showFiltersSelector = true;
            }
        });
        if (showFiltersSelector && this.isFilterSelectorVisible) {
            this.filter.AddChild(dropDown);
        }
    }
}

class ResponsiveTableColumIds extends BaseEnum {
    public static readonly ID : string = "id";
    public static readonly TEXT : string = "text";
}

export interface IResponsiveTableColumn {
    id : string;
    text : string;
    icon : string;
    controls : GuiCommons[];
    enabled : boolean;
    hidden : boolean;
    modifiers? : string[];
    width? : string;
}

export interface IResponsiveTableLocalization {
    selectAll : string;
    show : string;
    noData : string;
    showMore : string;
}

export abstract class ResponsiveTableItemType extends BaseEnum {
    public static readonly DELETED : string = "Deleted";
    public static readonly ARCHIVED : string = "Archived";
    public static readonly IN_PROGRESS : string = "InProgress";
    public static readonly COMPLETED : string = "Completed";
}

export class ResponsiveTableItem extends GuiCommons {
    public readonly select : CheckBox;
    public readonly itemId : Label;
    public readonly text : Label;
    public readonly idContent : GuiCommons;
    public readonly textContent : GuiCommons;
    protected styleClasses : string[];

    constructor() {
        super();
        this.select = new CheckBox();
        this.itemId = new Label();
        this.text = new Label();
        this.idContent = new GuiCommons();
        this.textContent = new GuiCommons();
        this.styleClasses = [];
    }

    public setState($value : ResponsiveTableItemType | any) : void {
        this.styleClasses = [];
        ResponsiveTableItemType.getProperties().forEach(($class : string) : void => {
            this.setAttribute(false, ResponsiveTableItemType[$class]);
        });
        this.setAttribute(true, $value + "");
        this.styleClasses.push($value + "");
    }

    public Restore() : void {
        this.select.Value(false);
        this.itemId.Text(" ");
        this.text.Text(" ");
    }

    protected innerHtml() : string {
        const modifiers : string = this.styleClasses.join(" ");
        return `
<div class="row g-0 ${modifiers} ResponsiveTableItem" style="border-radius: 6px;border-width: 1px;border-style: solid;margin-bottom: 3px;" data-oidis-bind="${this}">
    <div class="col-auto text-start align-self-center" style="width: 25px;"><input class="form-check-input" type="checkbox" data-oidis-bind="${this.select}" style="margin-left: 4px;" /></div>
    <div class="col">
        <div class="row g-0">
            <div class="col-12 col-md-6 col-lg-auto" data-oidis-bind="${this.idContent}">
                <div style="border-radius: 4px;border: 1px solid rgb(115,115,115);margin: 2px;min-width: 106px;font-weight: bold;"><svg class="bi bi-hash" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 3px;margin-bottom: 3px;margin-right: 3px;">
                        <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"></path>
                    </svg><span data-oidis-bind="${this.itemId}"></span></div>
            </div>
            <div class="col" data-oidis-bind="${this.textContent}">
                <div style="border-radius: 4px;border: 1px solid rgb(115,115,115);margin: 2px;min-width: 110px;"><svg class="bi bi-file-text" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" style="margin-left: 3px;margin-bottom: 3px;margin-right: 3px;">
                        <path d="M5 4a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z"></path>
                        <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"></path>
                    </svg><span data-oidis-bind="${this.text}"></span></div>
            </div>
        </div>
    </div>
</div>`;
    }
}

interface IModelListResult {
    size : number;
    limit : number;
    offset : number;
    data : any[];
}

// generated-code-start
/* eslint-disable */
export const IResponsiveTableColumn = globalThis.RegisterInterface(["id", "text", "icon", "controls", "enabled", "hidden", "modifiers", "width"]);
export const IResponsiveTableLocalization = globalThis.RegisterInterface(["selectAll", "show", "noData", "showMore"]);
/* eslint-enable */
// generated-code-end
