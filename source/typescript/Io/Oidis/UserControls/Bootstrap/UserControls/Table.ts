/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { GeneralCSS } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { Label } from "./Label.js";
import { Pagination } from "./Pagination.js";
import { ProgressBar } from "./ProgressBar.js";

export class Table<T extends TableItem> extends GuiCommons {
    protected localization : ITableLocalization;
    private readonly content : GuiCommons;
    private readonly header : GuiCommons;
    private readonly body : GuiCommons;
    private readonly pagination : Pagination;
    private readonly noData : GuiCommons;
    private readonly progress : GuiCommons;
    private readonly progressValue : ProgressBar;
    private items : T[];
    private fetchHandler : ($offset : number, $limit : number) => Promise<IModelListResult>;
    private recordToItemMapper : ($data : any, $item : T, $index : number) => Promise<void>;
    private itemCreateFormatter : ($item : T, $index : number) => Promise<void>;
    private noDataStyle : string;

    constructor() {
        super();
        this.content = new GuiCommons();
        this.header = new GuiCommons();
        this.body = new GuiCommons();
        this.pagination = new Pagination();
        this.noData = new GuiCommons();
        this.progress = new GuiCommons();
        this.progressValue = new ProgressBar();
        this.localization = {
            noData: "No data"
        };
        this.items = [];
        this.fetchHandler = async () : Promise<IModelListResult> => {
            return {data: [], offset: 0, limit: -1, size: 0};
        };
        this.recordToItemMapper = async () : Promise<void> => {
            // do nothing
        };
        this.itemCreateFormatter = async () : Promise<void> => {
            // do nothing
        };
        this.noDataStyle = "danger";
    }

    public Enabled($value? : boolean) : boolean {
        if (!this.body.Enabled($value)) {
            this.noData.Visible(false);
        }
        return this.pagination.Enabled($value);
    }

    public ItemsPerPage($value? : number) : number {
        return this.pagination.ItemsPerPage($value);
    }

    public PagesLimit($value? : number) : number {
        return this.pagination.PagesLimit($value);
    }

    public getIndex() : number {
        return this.pagination.getIndex();
    }

    public SelectPage($index : number, $from? : number) : void {
        if ($index >= 0) {
            if ($from > this.ItemsPerPage()) {
                this.pagination.Size($from);
                this.pagination.Select($index);
            } else {
                this.pagination.Select(0);
            }
            this.pagination.Visible($from > this.ItemsPerPage());
            this.pagination.Enabled(true);
        } else {
            this.pagination.Visible(false);
            this.pagination.Select(0);
        }
        this.handleSelection();
    }

    public setOnPageSelect($handler : ($index : number) => Promise<void>) : void {
        this.pagination.getEvents().setOnSelect(async () : Promise<void> => {
            await $handler(this.getIndex());
        });
    }

    public Clear() : void {
        this.body.Clear();
        this.items = [];
        this.progress.Visible(false);
        this.noData.Visible(true);
        this.pagination.Select(0);
    }

    public Restore() : void {
        this.items.forEach(($item : T) : void => {
            $item.Visible(false);
            $item.Restore();
        });
    }

    public AddItem() : T {
        const item : T = this.getNewItemInstance();
        this.body.AddChild(item);
        this.items.push(item);
        item.Visible(false);
        return item;
    }

    public getItem($index : number) : T {
        return this.items[$index];
    }

    public IsEmpty() : boolean {
        return ObjectValidator.IsEmptyOrNull(this.items);
    }

    public Size() : number {
        return this.items.length;
    }

    public BindData($onFetch : ($offset : number, $limit : number) => Promise<IModelListResult>,
                    $onRowMapping : ($data : any, $item : T, $index : number) => Promise<void>,
                    $onRowCreate? : ($item : T, $index : number) => Promise<void>) : void {
        this.fetchHandler = $onFetch;
        this.recordToItemMapper = $onRowMapping;
        if (!ObjectValidator.IsEmptyOrNull($onRowCreate)) {
            this.itemCreateFormatter = $onRowCreate;
        }
        this.setOnPageSelect(async () : Promise<void> => {
            await this.ShowData();
        });
    }

    public Init() : void {
        const defaultConfig : ITableColumn[] = this.getHeaderConfig();
        let itemsHtmlCode : string = "";
        defaultConfig.forEach(($item : ITableColumn) : void => {
            $item = JsonUtils.Extend(<ITableColumn>{
                enabled  : true,
                modifiers: [],
                text     : "",
                width    : null
            }, $item);
            itemsHtmlCode += `<th class="${$item.enabled ? "" : "d-none"}${$item.modifiers.join(" ")}" style="${ObjectValidator.IsEmptyOrNull($item.width) ? "" : "width:" + $item.width}">${$item.text}</th>`;
        });
        this.header.Content(`<tr>${itemsHtmlCode}</tr>`);
    }

    public async ShowData($resetOffset : boolean = false) : Promise<void> {
        if ($resetOffset) {
            this.SelectPage(0);
        }
        this.Enabled(false);
        if (this.IsEmpty()) {
            this.Init();
            this.progress.Visible(true);
        }
        try {
            const data : IModelListResult = await this.fetchHandler(this.getIndex(), this.ItemsPerPage());
            if (data.size > 0) {
                let index : number = 0;
                if (this.IsEmpty()) {
                    const items : number[] = new Array(data.limit > 0 ? data.limit : data.size);
                    if (!ObjectValidator.IsEmptyOrNull(items)) {
                        for await (const item of items) { // eslint-disable-line @typescript-eslint/no-unused-vars
                            try {
                                await this.itemCreateFormatter(this.AddItem(), index);
                            } catch (ex) {
                                LogIt.Warning("Failed to format table row " + index + ".\n" + ex.stack);
                            }
                            index++;
                        }
                    }
                } else {
                    this.Restore();
                }
                this.progress.Visible(false);
                index = 0;
                for await (const record of data.data) {
                    const item : T = this.getItem(index);
                    if (!ObjectValidator.IsEmptyOrNull(item)) {
                        try {
                            await this.recordToItemMapper(record, item, index);
                            item.Visible(true);
                        } catch (ex) {
                            LogIt.Warning("Failed to map table row to data.\n" + ex.stack);
                        }
                    }
                    index++;
                }
            } else {
                this.Clear();
            }
            this.progress.Visible(false);
            this.Enabled(true);
            this.SelectPage(this.getIndex(), data.size);
        } catch (ex) {
            LogIt.Warning("Failed to process table data.\n" + ex.stack);
            this.Clear();
        }
    }

    protected getNewItemInstance() : T {
        return (<any>new TableItem());
    }

    protected getItems() : T[] {
        return this.items;
    }

    protected setNoDataStyle($value : string) : void {
        this.noDataStyle = Property.String(this.noDataStyle, $value);
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="Table" data-oidis-bind="${this.content}">
        <table class="table table-striped table-sm">
            <thead data-oidis-bind="${this.header}">
                <tr>
                    <th></th>
                </tr>
            </thead>
            <tbody data-oidis-bind="${this.body}">
                <tr></tr>
            </tbody>
        </table>
    </div>
    <div class="progress" data-oidis-bind="${this.progress}" style="height: 40px;margin-top: -14px;margin-bottom: 10px;">
        <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" data-oidis-bind="${this.progressValue}"><span class="visually-hidden">100%</span></div>
    </div>
    <nav class="d-flex justify-content-center">
        <ul class="pagination ${GeneralCSS.DNONE}" data-oidis-bind="${this.pagination}"></ul>
    </nav>
    <div class="alert alert-${this.noDataStyle} text-center ${GeneralCSS.DNONE}" role="alert" data-oidis-bind="${this.noData}"><span>${this.localization.noData}</span></div>
</section>`;
    }

    protected getHeaderConfig() : ITableColumn[] {
        return [
            {
                text: "Info"
            }
        ];
    }

    private handleSelection() : void {
        let itemVisible : boolean = false;
        this.items.forEach(($item : T) : void => {
            if ($item.Visible()) {
                itemVisible = true;
            }
        });
        if (itemVisible) {
            this.noData.Visible(false);
        } else {
            this.progress.Visible(false);
            this.noData.Visible(true);
            this.pagination.Visible(false);
        }
    }
}

export interface ITableLocalization {
    noData : string;
}

export interface ITableColumn {
    text : string;
    width? : string;
    enabled? : boolean;
    modifiers? : string[];
}

export class TableItem extends GuiCommons {
    public readonly text : Label;
    protected styleClasses : string[];

    constructor() {
        super();
        this.text = new Label();
        this.styleClasses = [];
    }

    public Restore() : void {
        this.text.Text(" ");
    }

    protected innerHtml() : string {
        return `
<tr data-oidis-bind="${this}">
    <td><span data-oidis-bind="${this.text}"></span></td>
</tr>`;
    }
}

interface IModelListResult {
    size : number;
    limit : number;
    offset : number;
    data : any[];
}

// generated-code-start
export const ITableLocalization = globalThis.RegisterInterface(["noData"]);
export const ITableColumn = globalThis.RegisterInterface(["text", "width", "enabled", "modifiers"]);
// generated-code-end
