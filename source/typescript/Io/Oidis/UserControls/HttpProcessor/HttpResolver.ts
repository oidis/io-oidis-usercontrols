/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/HttpResolver.js";
import { EventsManager } from "../Events/EventsManager.js";

export class HttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser && !$context.isProd) {
            await this.registerResolver(async () => (await import("../Index.js")).Index,
                "/", "/index", "/web/");
        }
        return super.getStartupResolvers($context);
    }

    protected getEventsManagerClass() : any {
        return EventsManager;
    }
}
