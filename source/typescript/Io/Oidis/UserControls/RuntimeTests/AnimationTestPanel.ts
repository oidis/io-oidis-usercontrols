/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ThreadPool } from "@io-oidis-commons/Io/Oidis/Commons/Events/ThreadPool.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Alignment } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Alignment.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { FitToParent } from "@io-oidis-gui/Io/Oidis/Gui/Enums/FitToParent.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { AnimationThreadPool } from "@io-oidis-gui/Io/Oidis/Gui/Events/AnimationThreadPool.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IResponsiveElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IResponsiveElement.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { Button } from "../BaseInterface/UserControls/Button.js";
import { CheckBox } from "../BaseInterface/UserControls/CheckBox.js";
import { NumberPicker } from "../BaseInterface/UserControls/NumberPicker.js";
import { FormsObject } from "../Primitives/FormsObject.js";

export class AnimationTestPanel extends BasePanel {
    private runButton : Button;
    private stepPicker : NumberPicker;
    private valuePicker : NumberPicker;
    private throttlePicker : NumberPicker;
    private fallbackCheckBox : CheckBox;

    constructor($id? : string) {
        super($id);
        this.runButton = new Button();
        this.stepPicker = new NumberPicker();
        this.valuePicker = new NumberPicker();
        this.throttlePicker = new NumberPicker();
        this.fallbackCheckBox = new CheckBox();
    }

    public StartAnimation($continue? : boolean) : void {
        ProgressType.getProperties().forEach(($value : string) : void => {
            const progressId : string = this.getProgressId($value);
            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(progressId);
            manipulatorArgs.Owner(progressId);
            if (!$continue) {
                manipulatorArgs.DirectionType(manipulatorArgs.DirectionType() === DirectionType.UP ? DirectionType.DOWN : DirectionType.UP);
            }
            manipulatorArgs.ProgressType(ProgressType[$value]);
            manipulatorArgs.RangeStart(0);
            manipulatorArgs.RangeEnd(100);
            manipulatorArgs.ChangeEventType(EventType.ON_CHANGE);

            this.getEventsManager().setEvent(progressId, EventType.ON_CHANGE,
                ($eventArgs : ValueProgressEventArgs) : void => {
                    let i : number = 0;
                    const value : number = this.throttlePicker.Value();
                    for (i; i < value * 100000; i++) {
                        // empty block
                    }

                    ElementManager.setCssProperty(progressId, "width", $eventArgs.FloatValue() + "%");
                    if ($value === ProgressType.getProperties()[0]) {
                        this.valuePicker.Value($eventArgs.FloatValue());
                    }
                });
            ValueProgressManager.Execute(manipulatorArgs);
        });
    }

    protected innerCode() : IGuiElement {
        [this.stepPicker, this.valuePicker, this.throttlePicker, this.fallbackCheckBox]
            .forEach(($element : FormsObject) : void => {
                $element.IsPersistent(false);
            });

        this.runButton.Text("Run");
        this.stepPicker.ValueStep(.1);
        this.stepPicker.RangeStart(.1);
        this.stepPicker.RangeEnd(2);
        this.stepPicker.DecimalPlaces(1);
        this.fallbackCheckBox.Text("FB rendering");

        this.runButton.getEvents().setOnClick(() : void => {
            this.StartAnimation();
        });

        this.stepPicker.getEvents().setOnChange(() : void => {
            ProgressType.getProperties().forEach(($value : string) : void => {
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.getProgressId($value));
                manipulatorArgs.Step(this.stepPicker.Value());
            });
        });

        this.valuePicker.getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
            if ($manager.IsActive(this.valuePicker)) {
                ProgressType.getProperties().forEach(($value : string) : void => {
                    const progressId : string = this.getProgressId($value);
                    ValueProgressManager.Remove(this.getProgressId($value));
                    const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.getProgressId($value));
                    manipulatorArgs.DirectionType(DirectionType.DOWN);
                    manipulatorArgs.FloatValue(ValueProgressManager
                        .getPctFromTime(this.valuePicker.Value() / 100, ProgressType[$value]) * 100);
                    ElementManager
                        .setCssProperty(progressId, "width", manipulatorArgs.FloatValue() + "%");

                });
            }
        });

        if (!ObjectValidator.IsSet(window.requestAnimationFrame)) {
            this.fallbackCheckBox.Checked(true);
            this.fallbackCheckBox.Enabled(false);
        }

        this.fallbackCheckBox.getEvents().setOnChange(() : void => {
            ThreadPool.Clear();
            AnimationThreadPool.Clear();
            ProgressType.getProperties().forEach(($value : string) : void => {
                const progressId : string = this.getProgressId($value);
                ValueProgressManager.Remove(progressId);
            });
            (<any>ValueProgressManager).getThreadPool = () : any => {
                if (this.fallbackCheckBox.Checked()) {
                    return ThreadPool;
                } else {
                    return AnimationThreadPool;
                }
            };
            this.StartAnimation(true);
        });

        ProgressType.getProperties().forEach(($value : string) : void => {
            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.getProgressId($value));
            manipulatorArgs.DirectionType(DirectionType.DOWN);
        });

        return super.innerCode().Add("<style>.AnimProgress {background-color: #80C4DA; height: 100%}</style>");
    }

    protected innerHtml() : IGuiElement {
        const col : IResponsiveElement = this.addColumn()
            .HeightOfRow("20px", true)
            .WidthOfColumn(() : PropagableNumber => {
                return this.Width() > 600 ? new PropagableNumber("600px") : undefined;
            });

        col.Add("<h3>Animation testing suite</h3>");
        ProgressType.getProperties().forEach(($value : string) : void => {
            col.Add(this.addRow()
                .Add(this.addColumn().WidthOfColumn("125px").Add($value))
                .Add(this.addColumn()
                    .Add(this.addElement(this.getProgressId($value)).StyleClassName("AnimProgress"))))
                .Add(this.addRow().HeightOfRow("4px"));
        });
        col.Add(this.addRow()
            .Add(this.runButton))
            .Add(this.addRow()
                .Add(this.addColumn().WidthOfColumn("125px")
                    .Add("Step (%/10ms)"))
                .Add(this.stepPicker))
            .Add(this.addRow()
                .Add(this.addColumn().WidthOfColumn("125px")
                    .Add("Value (%)"))
                .Add(this.valuePicker))
            .Add(this.addRow()
                .Add(this.addColumn().WidthOfColumn("125px")
                    .Add("Handler throttle"))
                .Add(this.throttlePicker))
            .Add(this.addRow()
                .Add(this.addColumn().WidthOfColumn("125px"))
                .Add(this.fallbackCheckBox));

        return this.addRow()
            .FitToParent(FitToParent.FULL)
            .Alignment(Alignment.CENTER_PROPAGATED)
            .Add(col);
    }

    private getProgressId($type : string) : string {
        return this.Id() + "Progress" + ProgressType[$type];
    }
}

/* dev:end */
