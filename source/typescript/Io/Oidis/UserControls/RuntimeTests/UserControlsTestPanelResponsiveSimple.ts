/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Alignment } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Alignment.js";
import { FitToParent } from "@io-oidis-gui/Io/Oidis/Gui/Enums/FitToParent.js";
import { VisibilityStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Enums/VisibilityStrategy.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ButtonType } from "../BaseInterface/Enums/UserControls/ButtonType.js";
import { IconType } from "../BaseInterface/Enums/UserControls/IconType.js";
import { TextFieldType } from "../BaseInterface/Enums/UserControls/TextFieldType.js";
import { Button } from "../BaseInterface/UserControls/Button.js";
import { TextField } from "../BaseInterface/UserControls/TextField.js";
import { BasePanel } from "../Primitives/BasePanel.js";

export class UserControlsTestPanelResponsiveSimple extends BasePanel {
    public buttonResizeSmall : Button;
    public buttonResizeMedium : Button;
    public buttonResizeLarge : Button;
    public toggleShowField1 : Button;
    public toggleShowField2 : Button;
    public toggleShowButton1 : Button;
    public toggleShowButton2 : Button;
    public toggleShowButton3 : Button;
    public textField1 : TextField;
    public textField2 : TextField;

    constructor($id? : string) {
        super($id);

        this.buttonResizeSmall = new Button(ButtonType.RED);
        this.buttonResizeMedium = new Button(ButtonType.GREEN);
        this.buttonResizeLarge = new Button(ButtonType.BLUE);

        this.toggleShowField1 = new Button(ButtonType.GENERAL);
        this.toggleShowField2 = new Button(ButtonType.GENERAL);

        this.toggleShowButton1 = new Button(ButtonType.GENERAL);
        this.toggleShowButton2 = new Button(ButtonType.GENERAL);
        this.toggleShowButton3 = new Button(ButtonType.GENERAL);

        this.textField1 = new TextField();
        this.textField2 = new TextField(TextFieldType.BLUE);
    }

    protected innerCode() : IGuiElement {
        this.buttonResizeSmall.IconName(IconType.BLUE_SQUARE);
        this.buttonResizeSmall.Text("540px");
        this.buttonResizeSmall.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(540);
        });

        this.buttonResizeMedium.IconName(IconType.BLACK_SQUARE);
        this.buttonResizeMedium.Text("720px");
        this.buttonResizeMedium.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(720);
        });
        this.buttonResizeMedium.Visible(false);

        this.buttonResizeLarge.IconName(IconType.RED_SQUARE);
        this.buttonResizeLarge.Text("1100px");
        this.buttonResizeLarge.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(1100);
        });

        this.toggleShowField1.IconName(IconType.GENERAL);
        this.toggleShowField1.Text("toggle field 1");
        this.toggleShowField1.getEvents().setOnClick(() => {
            this.textField1.Visible(!this.textField1.Visible());
        });

        this.toggleShowField2.IconName(IconType.GENERAL);
        this.toggleShowField2.Text("toggle field 2");
        this.toggleShowField2.getEvents().setOnClick(() => {
            this.textField2.Visible(!this.textField2.Visible());
        });

        this.toggleShowButton1.IconName(IconType.GENERAL);
        this.toggleShowButton1.Text("toggle button 1");
        this.toggleShowButton1.getEvents().setOnClick(() => {
            this.buttonResizeSmall.Visible(!this.buttonResizeSmall.Visible());
        });

        this.toggleShowButton2.IconName(IconType.GENERAL);
        this.toggleShowButton2.Text("toggle button 2");
        this.toggleShowButton2.getEvents().setOnClick(() => {
            this.buttonResizeMedium.Visible(!this.buttonResizeMedium.Visible());
        });

        this.toggleShowButton3.IconName(IconType.GENERAL);
        this.toggleShowButton3.Text("toggle button 3");
        this.toggleShowButton3.getEvents().setOnClick(() => {
            this.buttonResizeLarge.Visible(!this.buttonResizeLarge.Visible());
        });

        this.textField1.Hint("type something here");
        this.textField1.Width(300);
        this.textField1.Visible(false);

        this.textField2.Value("type something here");
        this.textField2.Width(300);

        return super.innerCode().Add("<style>" +
            "p,h4 {margin:8px;}" +
            ".IoOidisUserControlsPrimitivesBasePanel {border : 1px dashed #80c4da; margin:20px;}" +
            ".Row1 {background: lightcyan;}" +
            ".Row2 {background: lightpink;}" +
            "</style>");
    }

    protected innerHtml() : IGuiElement {
        return this.addRow()
            .WidthOfColumn("100%")
            .FitToParent(FitToParent.FULL)
            .Alignment(Alignment.CENTER_PROPAGATED)
            .Add(this.addColumn().WidthOfColumn("8px"))
            .Add(this.addColumn()
                .Add(this.addRow().HeightOfRow("8px"))
                .Add(this.addRow()
                    .HeightOfRow("32px")
                    .WidthOfColumn("25%", true)
                    .Alignment(Alignment.RIGHT_PROPAGATED)
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                    // .Add(this.addColumn())
                    .Add(this.addColumn().WidthOfColumn("40%")
                        .Add(this.buttonResizeSmall)
                    )
                    .Add(this.buttonResizeMedium)
                    .Add(this.buttonResizeLarge)
                )
                .Add(this.addRow().HeightOfRow("8px"))
                .Add(this.addRow()
                    .HeightOfRow("32px")
                    .WidthOfColumn("25%", true)
                    .Alignment(Alignment.RIGHT_PROPAGATED)
                    .Add(this.toggleShowButton1)
                    .Add(this.toggleShowButton2)
                    .Add(this.toggleShowButton3)
                )
                .Add(this.addRow()
                    .HeightOfRow("32px")
                    .WidthOfColumn("25%", true)
                    .Alignment(Alignment.RIGHT_PROPAGATED)
                    .Add(this.toggleShowField1)
                    .Add(this.toggleShowField2)
                )
                .Add(this.addRow().HeightOfRow("8px"))
                .Add(this.addRow()
                    .Add(this.addColumn().VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        .Add(this.addRow()
                            .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                            .StyleClassName("Row1")
                            .Add(this.textField1)
                        )
                        .Add(this.addRow()
                            .StyleClassName("Row2")
                            .Add(this.textField2)
                        ))
                )
                .Add(this.addRow().HeightOfRow("8px"))
            )
            .Add(this.addColumn().WidthOfColumn("8px"));
    }
}
/* dev:end */
