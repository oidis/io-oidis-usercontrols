/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { HorizontalPanelHolderViewer } from "../BaseInterface/Viewers/UserControls/HorizontalPanelHolderViewer.js";
import { BasePanelHolder } from "../Primitives/BasePanelHolder.js";
import { BasePanelHolderViewerArgs } from "../Primitives/BasePanelHolderViewerArgs.js";
import { HorizontalPanelHolderStrategy } from "../Strategies/HorizontalPanelHolderStrategy.js";
import { UserControlsTestPanel } from "./UserControlsTestPanel.js";
import { UserControlsTestPanelViewer } from "./UserControlsTestPanelViewer.js";

export class HorizontalTestPanelHolderViewer extends HorizontalPanelHolderViewer {
    protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
        const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        args.BodyViewerClass(UserControlsTestPanelViewer);
        args.HeaderText("Horizontal panel holder");
        args.DescriptionText("This is description text for horizontal panel holder");
        args.PrioritySize(new PropagableNumber({number: 468, unitType: UnitType.PX}));
        // args.IsOpened(false);
        args.Strategy(new HorizontalPanelHolderStrategy());

        return args;
    }

    public getInstance() : BasePanelHolder {
        return <BasePanelHolder>super.getInstance();
    }

    protected testImplementation() : void {
        const instance : BasePanelHolder = this.getInstance();
        instance.StyleClassName("TestCss");

        if (!ObjectValidator.IsEmptyOrNull(instance.getBody())) {
            const body : UserControlsTestPanel = <UserControlsTestPanel>instance.getBody();
            body.StyleClassName("Body");

            // body.image1.Visible(false);
            // body.image2.Visible(false);
            body.tabs1.Visible(false);
            body.tabs2.Visible(false);
            body.inputLabel1.Visible(false);
            body.inputLabel2.Visible(false);
            body.progressBar1.Visible(false);
            body.progressBar2.Visible(false);
            body.numberPicker1.Visible(false);
            body.numberPicker2.Visible(false);
            body.numberPicker1.Visible(false);
            body.link1.Visible(false);
            body.link2.Visible(false);
            body.imageButton1.Visible(false);
            body.imageButton2.Visible(false);
            body.button1.Visible(false);
            body.button2.Visible(false);
        }

        this.normalImplementation();
    }
}
/* dev:end */
