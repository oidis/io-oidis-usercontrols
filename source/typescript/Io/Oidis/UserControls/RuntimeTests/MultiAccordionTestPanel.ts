/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { FitToParent } from "@io-oidis-gui/Io/Oidis/Gui/Enums/FitToParent.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { AccordionResizeType } from "../BaseInterface/Enums/UserControls/AccordionResizeType.js";
import { AccordionType } from "../BaseInterface/Enums/UserControls/AccordionType.js";
import { Accordion } from "../BaseInterface/UserControls/Accordion.js";
import { Button } from "../BaseInterface/UserControls/Button.js";
import { AccordionViewer } from "../BaseInterface/Viewers/UserControls/AccordionViewer.js";
import { AccordionViewerArgs } from "../BaseInterface/ViewersArgs/UserControls/AccordionViewerArgs.js";
import { BasePanel } from "../Primitives/BasePanel.js";
import { BasePanelHolderViewerArgs } from "../Primitives/BasePanelHolderViewerArgs.js";
import { UserControlsTestPanelViewer } from "./UserControlsTestPanelViewer.js";

export class MultiAccordionTestPanel extends BasePanel {
    public panel1 : Accordion;
    public panel2 : Accordion;
    public panel3 : Accordion;
    public panel4 : Accordion;
    public panel5 : Accordion;
    public panel6 : Accordion;
    public panel7 : Accordion;
    public panel8 : Accordion;
    public toggleButton : Button;

    constructor($id? : string) {
        super($id);

        this.toggleButton = new Button();
        this.toggleButton.Text("Toggle expand/collapse");

        const addArgs : any = ($args : AccordionViewerArgs) => {
            const holderArgs1 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            const holderArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            const holderArgs3 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();

            holderArgs1.HeaderText("Holder 1");
            holderArgs1.DescriptionText("Desc.");
            holderArgs1.PrioritySize(new PropagableNumber({number: 200, unitType: UnitType.PCT}));
            holderArgs1.IsOpened(false);

            holderArgs2.HeaderText("Holder 2");
            holderArgs2.DescriptionText("Description txt");
            holderArgs2.IsOpened(false);
            holderArgs2.BodyViewerClass(UserControlsTestPanelViewer);

            holderArgs3.HeaderText("Holder 3");
            holderArgs3.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PX}));
            holderArgs3.IsOpened(false);

            $args.AddPanelHoldersArgs(holderArgs1);
            $args.AddPanelHoldersArgs(holderArgs2);
            $args.AddPanelHoldersArgs(holderArgs3);
        };

        let args : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.RESPONSIVE);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel1 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.VERTICAL);
        args.ResizeType(AccordionResizeType.RESPONSIVE);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel2 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.VERTICAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel3 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel4 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel5 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel6 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel7 = $child;
            });

        args = new AccordionViewerArgs(AccordionType.HORIZONTAL);
        args.ResizeType(AccordionResizeType.DEFAULT);
        addArgs(args);
        this.addChildPanel(AccordionViewer, args,
            ($parent : MultiAccordionTestPanel, $child : Accordion) : void => {
                $parent.panel8 = $child;
            });
    }

    protected innerCode() : IGuiElement {
        this.FitToWindow(true);

        this.panel1.Visible(true);
        this.panel2.Visible(true);
        this.panel3.Visible(true);
        this.panel4.Visible(true);
        this.panel5.Visible(true);
        this.panel6.Visible(true);
        this.panel7.Visible(true);
        this.panel8.Visible(true);

        let expand : boolean = false;
        this.toggleButton.getEvents().setOnClick(() : void => {
            expand = !expand;
            [
                this.panel1, this.panel2, this.panel3, this.panel4,
                this.panel5, this.panel6, this.panel7, this.panel8
            ].forEach(($accordion : Accordion) : void => {
                if (expand) {
                    $accordion.Expand();
                } else {
                    $accordion.Collapse();
                }
            });
        });

        return super.innerCode().Add(
            "<style>.MultiAccordionTestPanel{position:relative; float: left;} " +
            ".Accordion{position:relative; float: left;} " +
            //  ".Accordion .Panel {display: block !important;height: auto;width: auto;}" +
            "[guiType=\"GuiRow\"] > .Panel {display: table !important;height: 100%;width: 100%;}" +
            ".TestPanelCss .PanelScrollBar .Tracker{background-color: transparent;}</style>");
    }

    protected innerHtml() : IGuiElement {
        return this.addColumn().FitToParent(FitToParent.FULL)
            .Add(this.addRow().HeightOfRow("30px").Add(this.toggleButton))
            .Add(this.addRow()
                .Add(this.addColumn().Add(this.panel1))
                .Add(this.addColumn().Add(this.panel2)))
            .Add(this.addRow()
                .Add(this.addColumn().Add(this.panel3))
                .Add(this.addColumn().Add(this.panel4)))
            .Add(this.addRow()
                .Add(this.addColumn().Add(this.panel5))
                .Add(this.addColumn().Add(this.panel6)))
            .Add(this.addRow()
                .Add(this.addColumn().Add(this.panel7))
                .Add(this.addColumn().Add(this.panel8)));
    }
}
/* dev:end */
