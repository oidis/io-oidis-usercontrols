/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Alignment } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Alignment.js";
import { FitToParent } from "@io-oidis-gui/Io/Oidis/Gui/Enums/FitToParent.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { VisibilityStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Enums/VisibilityStrategy.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { ButtonType } from "../BaseInterface/Enums/UserControls/ButtonType.js";
import { IconType } from "../BaseInterface/Enums/UserControls/IconType.js";
import { ImageButtonType } from "../BaseInterface/Enums/UserControls/ImageButtonType.js";
import { InputLabelType } from "../BaseInterface/Enums/UserControls/InputLabelType.js";
import { TextFieldType } from "../BaseInterface/Enums/UserControls/TextFieldType.js";
import { Button } from "../BaseInterface/UserControls/Button.js";
import { CheckBox } from "../BaseInterface/UserControls/CheckBox.js";
import { DropDownList } from "../BaseInterface/UserControls/DropDownList.js";
import { Image } from "../BaseInterface/UserControls/Image.js";
import { ImageButton } from "../BaseInterface/UserControls/ImageButton.js";
import { InputLabel } from "../BaseInterface/UserControls/InputLabel.js";
import { Label } from "../BaseInterface/UserControls/Label.js";
import { LabelList } from "../BaseInterface/UserControls/LabelList.js";
import { Link } from "../BaseInterface/UserControls/Link.js";
import { NumberPicker } from "../BaseInterface/UserControls/NumberPicker.js";
import { ProgressBar } from "../BaseInterface/UserControls/ProgressBar.js";
import { RadioBox } from "../BaseInterface/UserControls/RadioBox.js";
import { Tabs } from "../BaseInterface/UserControls/Tabs.js";
import { TextArea } from "../BaseInterface/UserControls/TextArea.js";
import { TextField } from "../BaseInterface/UserControls/TextField.js";
import { BasePanel } from "../Primitives/BasePanel.js";

export class UserControlsTestPanelResponsive extends BasePanel {
    public label1 : Label;
    public label2 : Label;
    public labelList1 : LabelList;
    public labelList2 : LabelList;
    public link1 : Link;
    public link2 : Link;
    public imageButton1 : ImageButton;
    public imageButton2 : ImageButton;
    public buttonResizeSmall : Button;
    public buttonResizeMedium : Button;
    public buttonResizeLarge : Button;
    public textField1 : TextField;
    public textField2 : TextField;
    public textArea1 : TextArea;
    public textArea2 : TextArea;
    public checkBox1 : CheckBox;
    public checkBox2 : CheckBox;
    public radioBox1 : RadioBox;
    public radioBox2 : RadioBox;
    public dropDownList1 : DropDownList;
    public dropDownList2 : DropDownList;
    public numberPicker1 : NumberPicker;
    public numberPicker2 : NumberPicker;
    public progressBar1 : ProgressBar;
    public progressBar2 : ProgressBar;
    public image1 : Image;
    public image2 : Image;
    public tabs1 : Tabs;
    public tabs2 : Tabs;
    public inputLabel1 : InputLabel;
    public inputLabel2 : InputLabel;

    constructor($id? : string) {
        super($id);

        this.label1 = new Label("test label: ");
        this.label2 = new Label("test label2");

        this.labelList1 = new LabelList(IconType.BLUE_SQUARE);
        this.labelList2 = new LabelList(IconType.RED_SQUARE);

        this.link1 = new Link();
        this.link2 = new Link();
        this.link1.Visible(false);
        this.link2.Visible(false);

        this.imageButton1 = new ImageButton();
        this.imageButton2 = new ImageButton(ImageButtonType.GREEN);
        this.imageButton1.Visible(false);
        this.imageButton2.Visible(false);

        this.buttonResizeSmall = new Button(ButtonType.RED);
        this.buttonResizeMedium = new Button(ButtonType.GREEN);
        this.buttonResizeLarge = new Button(ButtonType.BLUE);

        this.textField1 = new TextField();
        this.textField2 = new TextField(TextFieldType.BLUE);

        this.textArea1 = new TextArea();
        this.textArea2 = new TextArea();
        this.textArea1.Visible(false);
        this.textArea2.Visible(false);

        this.checkBox1 = new CheckBox();
        this.checkBox2 = new CheckBox();
        this.checkBox1.Visible(false);
        this.checkBox2.Visible(false);

        this.radioBox1 = new RadioBox("testGroup");
        this.radioBox2 = new RadioBox("testGroup");
        this.radioBox1.Visible(false);
        this.radioBox2.Visible(false);

        this.dropDownList1 = new DropDownList();
        this.dropDownList2 = new DropDownList();

        this.numberPicker1 = new NumberPicker();
        this.numberPicker2 = new NumberPicker();

        this.progressBar1 = new ProgressBar();
        this.progressBar2 = new ProgressBar();

        this.image1 = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        this.image2 = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg");
        this.image1.Visible(false);
        this.image2.Visible(false);

        this.tabs1 = new Tabs();
        this.tabs2 = new Tabs();

        this.inputLabel1 = new InputLabel(InputLabelType.GREEN, "Responsive label number 1");
        this.inputLabel2 = new InputLabel(InputLabelType.RED, "Responsive label number 2");
    }

    protected innerCode() : IGuiElement {
        this.labelList1.Clear();
        this.labelList1.Add("list item 1");
        this.labelList1.Add("list item 2");

        this.labelList2.Clear();
        this.labelList2.Add("list item 1");
        this.labelList2.Add("list item 2");
        this.labelList2.Add("list item 4");
        this.labelList2.Enabled(false);

        this.link1.Text("test link");

        this.link2.Text("test link2");
        this.link2.ReloadTo("www.oidis.io");

        this.imageButton1.Title().Text("this is test image button");

        this.buttonResizeSmall.IconName(IconType.BLUE_SQUARE);
        this.buttonResizeSmall.Text("500px");
        this.buttonResizeSmall.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(500);
        });

        this.buttonResizeMedium.IconName(IconType.BLACK_SQUARE);
        this.buttonResizeMedium.Text("750px");
        this.buttonResizeMedium.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(750);
        });

        this.buttonResizeLarge.IconName(IconType.RED_SQUARE);
        this.buttonResizeLarge.Text("1100px");
        this.buttonResizeLarge.getEvents().setOnClick(($eventArgs : EventArgs) => {
            (<BasePanel>(<Button>($eventArgs.Owner())).Parent()).Width(1100);
        });

        this.textField1.Hint("type some text here");
        this.textField1.Width(300);

        this.textField2.Value("type some text here");
        this.textField2.Width(300);

        this.textArea1.Hint("type some text here");
        this.textArea1.Width(300);
        this.textArea1.Height(150);

        this.textArea2.Width(300);
        this.textArea2.Height(100);
        this.textArea2.LengthLimit(500);
        this.textArea2.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);

        this.checkBox2.Text(" ");
        this.checkBox2.Checked(true);
        this.checkBox2.Enabled(false);

        this.radioBox1.Text("radio item 1");
        this.radioBox1.Checked(true);

        this.radioBox2.Text("radio item 2");

        this.dropDownList1.Width(300);
        this.dropDownList1.Clear();
        this.dropDownList1.Add("item 1");
        this.dropDownList1.Add("item 2");
        this.dropDownList1.Add("item 3");
        this.dropDownList1.Add("item 4");
        this.dropDownList1.MaxVisibleItemsCount(2);
        this.dropDownList1.Hint("choose some item");
//            this.dropDownList1.Visible(false);

        this.dropDownList2.Width(300);
        this.dropDownList2.Height(300);
        this.dropDownList2.Clear();
        this.dropDownList2.Add("item 1");
        this.dropDownList2.Add("item 2");
        this.dropDownList2.Select(1);

        this.numberPicker1.Width(300);
        this.numberPicker1.Value(25);

        this.numberPicker2.Width(300);
        this.numberPicker2.Value(50);
        this.numberPicker2.RangeStart(-100);
        this.numberPicker2.RangeEnd(100);
        this.numberPicker2.DecimalPlaces(2);

        this.progressBar1.Width(300);
        this.progressBar1.Value(50);

        this.progressBar2.Width(300);
        this.progressBar2.RangeStart(-100);
        this.progressBar2.RangeEnd(100);
        this.progressBar2.Value(50);

        this.image1.setSize(200, 200);

        this.image2.setSize(300, 300);
        this.image2.Link("www.oidis.io");
        // this.image2.Visible(false);

        this.tabs1.Width(300);
        if (!this.IsCached()) {
            this.tabs1.Clear();
            this.tabs1.Add("Tab 1");
            this.tabs1.Add("Tab 2");
            this.tabs1.Add("Tab 3");
            this.tabs1.Add("Tab 4");
            this.tabs1.Add("Tab 5");
        }
        this.tabs1.Select(4);

        this.tabs2.Width(300);
        if (!this.IsCached()) {
            this.tabs2.Clear();
            this.tabs2.Add("Tab 1");
            this.tabs2.Add("Tab 2");
            this.tabs2.Add("Tab 3");
        }
        this.tabs2.Select(1);

        return super.innerCode().Add("<style>" +
            "p,h4 {margin:8px;}" +
            ".IoOidisUserControlsPrimitivesBasePanel {border : 1px dashed #80c4da; margin:20px;}" +
            "</style>");
    }

    protected innerHtml() : IGuiElement {
        return this.addRow().Add(
            this.addColumn()
                .WidthOfColumn("100%")
                .HeightOfRow("60px", true)
                .FitToParent(FitToParent.FULL)
                .Alignment(Alignment.TOP_PROPAGATED)
                .Add("<h3>Centered header</h3>")
                .Add(this.addRow()
                    .HeightOfRow("32px")
                    .WidthOfColumn(() : PropagableNumber => {
                        if (this.Width() > 750) {
                            return new PropagableNumber({number: 250, unitType: UnitType.PX}, true);
                        }
                    })
                    .Alignment(Alignment.RIGHT_PROPAGATED)
                    .Add(this.buttonResizeSmall)
                    .Add(this.buttonResizeMedium)
                    .Add(this.buttonResizeLarge)
                )
                .Add(this.addRow()
                    .HeightOfRow("16%")
                    .Alignment(Alignment.TOP_LEFT)
                    .FitToParent(FitToParent.HORIZONTAL)
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.textArea1)
                    .Add(this.textArea2)
                )
                .Add(this.addRow()
                    .Alignment(Alignment.CENTER_PROPAGATED)
                    .FitToParent(FitToParent.HORIZONTAL)
                    .Add(this.textField1)
                    .Add(this.textField2)
                )
                .Add(this.addRow()
                    .HeightOfRow("16%")
                    .Alignment(Alignment.LEFT_PROPAGATED)
                    .Add("<p>Short text that is vertically centered.</p>")
                    .Add("<p>Longer text that is vertically centered. " +
                        "Longer text that is vertically centered. Longer text that is vertically centered.</p>")
                    .Add("<h4>Header</h4><p>Some other vertically centered text under header.</p>")
                )
                .Add(this.addRow()
                    .HeightOfRow("140px")
                    .Alignment(Alignment.CENTER)
                    .Add(this.addColumn().WidthOfColumn("12px"))
                    .Add(this.addColumn()
                        .WidthOfColumn(100, UnitType.PX)
                        .HeightOfRow(40, UnitType.PX, true)
                        .FitToParent(FitToParent.NONE)
                        .Alignment(Alignment.LEFT_PROPAGATED)
                        .Add(this.label1)
                        .Add(this.label2)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(6, UnitType.PCT)
                    )
                    .Add(this.addColumn()
                        .WidthOfColumn(() : PropagableNumber => {
                            if (this.Width() > 750) {
                                return new PropagableNumber({number: 600, unitType: UnitType.PX}, true);
                            }
                        })
                        .HeightOfRow(40, UnitType.PX, true)
                        .Alignment(Alignment.LEFT_PROPAGATED)
                        .FitToParent(FitToParent.HORIZONTAL)
                        .Add(this.dropDownList1)
                        .Add(this.dropDownList2)
                    )
                    .Add(this.addColumn().WidthOfColumn("12px"))
                )
                .Add(this.addRow()
                    .Alignment(Alignment.CENTER_PROPAGATED)
                    .Add(this.labelList1.FitToParent(FitToParent.NONE))
                    .Add(this.addColumn()
                        .HeightOfRow(40, UnitType.PX, true)
                        .Add(this.inputLabel1)
                        .Add(this.numberPicker1)
                        .Add(this.inputLabel2)
                        .Add(this.numberPicker2)
                    )
                    .Add(this.labelList2.FitToParent(FitToParent.NONE))
                )
                .Add(this.addRow()
                    .Alignment(Alignment.BOTTOM_PROPAGATED).HeightOfRow(100, UnitType.PX)
                    .Add(this.addColumn()
                        .Add(this.tabs1)
                        .Add(this.progressBar1))
                    .Add(this.addColumn()
                        .WidthOfColumn(6, UnitType.PCT))
                    .Add(this.addColumn()
                        .Add(this.progressBar2)
                        .Add(this.tabs2)
                    )
                )
        );
    }
}
/* dev:end */
