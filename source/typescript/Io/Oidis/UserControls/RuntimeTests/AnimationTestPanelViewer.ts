/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { AnimationTestPanel } from "./AnimationTestPanel.js";

export class AnimationTestPanelViewer extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new AnimationTestPanel());
    }

    public getInstance() : AnimationTestPanel {
        return <AnimationTestPanel>super.getInstance();
    }

    protected normalImplementation() : void {
        const instance : AnimationTestPanel = this.getInstance();
        instance.Scrollable(true);
        instance.FitToWindow(true);
    }
}
/* dev:end */
