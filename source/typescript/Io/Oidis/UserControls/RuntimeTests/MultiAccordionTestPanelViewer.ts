/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { MultiAccordionTestPanel } from "./MultiAccordionTestPanel.js";

export class MultiAccordionTestPanelViewer extends BasePanelViewer {

    protected static getTestViewerArgs() : BasePanelViewerArgs {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        args.AsyncEnabled(true);
        return args;
    }

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new MultiAccordionTestPanel());
    }

    public getInstance() : MultiAccordionTestPanel {
        return <MultiAccordionTestPanel>super.getInstance();
    }

    protected before($instance : MultiAccordionTestPanel) : void {
        $instance.Scrollable(true);
    }
}
/* dev:end */
