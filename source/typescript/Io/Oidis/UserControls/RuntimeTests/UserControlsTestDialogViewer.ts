/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { DialogType } from "../BaseInterface/Enums/UserControls/DialogType.js";
import { Dialog } from "../BaseInterface/UserControls/Dialog.js";
import { DialogViewerArgs } from "../BaseInterface/ViewersArgs/UserControls/DialogViewerArgs.js";
import { UserControlsTestPanelViewer } from "./UserControlsTestPanelViewer.js";

export class UserControlsTestDialogViewer extends BaseViewer {

    protected static getTestViewerArgs() : DialogViewerArgs {
        const args : DialogViewerArgs = new DialogViewerArgs();
        const panelArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
//            panelArgs.AsyncEnabled(true);
        args.PanelArgs(panelArgs);

        args.Visible(true);
        args.HeaderText("Integration test dialog");

        return args;
    }

    constructor($args? : DialogViewerArgs) {
        super($args);

        const instance : Dialog = new Dialog(DialogType.GENERAL);
        instance.PanelViewer(new UserControlsTestPanelViewer());
        instance.Visible(true);
        instance.Modal(true);
        instance.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
        instance.headerText.Visible(true);
        instance.AutoResize(false);

        this.setInstance(instance);
    }

    public getInstance() : Dialog {
        return <Dialog>super.getInstance();
    }

    public ViewerArgs($args? : DialogViewerArgs) : DialogViewerArgs {
        return <DialogViewerArgs>super.ViewerArgs($args);
    }

    protected argsHandler($instance : Dialog, $args : DialogViewerArgs) : void {
        $instance.headerText.Text($args.HeaderText());
        if (!ObjectValidator.IsEmptyOrNull($args.PanelArgs())) {
            $instance.Value($args.PanelArgs());
        }
    }

    protected testImplementation($instance : Dialog) : void {
        this.addTestButton("Open dialog", () : void => {
            Dialog.Open($instance);
        });
    }
}
/* dev:end */
