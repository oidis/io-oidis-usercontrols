/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { UserControlsTestPanel } from "./UserControlsTestPanel.js";

export class UserControlsTestPanelViewer extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new UserControlsTestPanel());
    }

    public getInstance() : UserControlsTestPanel {
        return <UserControlsTestPanel>super.getInstance();
    }

    protected normalImplementation() : void {
        const instance : UserControlsTestPanel = this.getInstance();
        // instance.Visible(false);
        instance.Scrollable(true);
        instance.Width(500);
        instance.Height(400);

        // if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())) {
        //     this.ViewerArgs().AsyncEnabled(false);
        // }
    }

    protected testImplementation() : string {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        args.AsyncEnabled(true);
        this.ViewerArgs(args);
        const instance : UserControlsTestPanel = this.getInstance();
        instance.Scrollable(true);
        instance.Width(500);
        instance.Height(500);
        return "";
    }
}
/* dev:end */
