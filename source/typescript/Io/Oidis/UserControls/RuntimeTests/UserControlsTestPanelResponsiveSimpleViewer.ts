/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { UserControlsTestPanelResponsiveSimple } from "./UserControlsTestPanelResponsiveSimple.js";

export class UserControlsTestPanelResponsiveSimpleViewer extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new UserControlsTestPanelResponsiveSimple());
    }

    public getInstance() : UserControlsTestPanelResponsiveSimple {
        return <UserControlsTestPanelResponsiveSimple>super.getInstance();
    }

    protected normalImplementation() : void {
        const instance : UserControlsTestPanelResponsiveSimple = this.getInstance();
        instance.Scrollable(true);
        instance.Width(800);
        instance.Height(800);
    }

    protected testImplementation() : string {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        args.AsyncEnabled(true);
        this.ViewerArgs(args);
        const instance : UserControlsTestPanelResponsiveSimple = this.getInstance();
        instance.Scrollable(false);
        return "";
    }
}
/* dev:end */
