/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { PerformanceTestPanel } from "./PerformanceTestPanel.js";

export class PerformanceTestPanelViewer extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new PerformanceTestPanel());
    }

    public getInstance() : PerformanceTestPanel {
        return <PerformanceTestPanel>super.getInstance();
    }

    protected normalImplementation() : void {
        const instance : PerformanceTestPanel = this.getInstance();
//            instance.Visible(false);
        instance.Scrollable(true);
        instance.Width(500);
        instance.Height(500);
    }
}
/* dev:end */
