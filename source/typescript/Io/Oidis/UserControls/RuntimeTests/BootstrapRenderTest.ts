/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { DropDownList } from "../Bootstrap/UserControls/DropDownList.js";
import { TableBody } from "../Bootstrap/UserControls/TableBody.js";
import { TextField } from "../Bootstrap/UserControls/TextField.js";

export class BootstrapRenderTest extends BaseViewer {

    constructor() {
        super();
        this.setInstance(new BootstrapTestPage());
    }

    protected testImplementation($instance : BootstrapTestPage) : string {
        this.addTestButton("Add button child", () : void => {
            const button : BootstrapTestComponent = new BootstrapTestComponent();
            button.getEvents().setOnClick(($args : EventArgs) : void => {
                LogIt.Debug("Test button clicked {0}", $args.Owner().Id());
            });
            $instance.panel.AddChild(button);
        });

        this.addTestButton("Add dropdown child", () : void => {
            const dropdown : DropDownList = new DropDownList();
            dropdown.Text("select value");
            dropdown.AddItem("test 1").getEvents().setOnClick(($event : EventArgs) : void => {
                LogIt.Debug("Add item 1");
            });
            dropdown.AddItem("test 2").getEvents().setOnClick(($event : EventArgs) : void => {
                LogIt.Debug("Add item 2");
            });
            dropdown.AddItem("test 3");
            dropdown.AddItem("test 4");
            $instance.panel.AddChild(dropdown);
        });

        this.addTestButton("Fill table content", () : void => {
            const table : TableBody = new TableBody();
            table.AddChild(new BootstrapTestTableRecod());
            table.AddChild(new BootstrapTestTableRecod());
            table.AddChild(new BootstrapTestTableRecod());
            $instance.panel.SwapChild($instance.panel.usersTable, table);
        });

        this.addTestButton("Swap child", () : void => {
            const button : BootstrapTestComponent = new BootstrapTestComponent();
            $instance.panel.AddChild(button);
            button.Content("This should be swapped");
            const dropdown : DropDownList = new DropDownList();
            dropdown.Text("swapped element");
            dropdown.AddItem("test 1");
            dropdown.AddItem("test 2");
            dropdown.AddItem("test 2");
            $instance.panel.SwapChild(button, dropdown);
        });

        this.addTestButton("Add input group", () : void => {
            const group : BootstrapTestInputGroup = new BootstrapTestInputGroup();
            $instance.panel.AddChild(group);
            group.getEvents().setOnClick(() : void => {
                LogIt.Debug(group.Value());
            });
        });

        $instance.searchFilter.Value("test");
        $instance.searchFilter.getEvents().setEvent(EventType.ON_KEY_PRESS, () : void => {
            LogIt.Debug($instance.searchFilter.Value());
        });

        $instance.panel.textField.getEvents().setEvent(EventType.ON_KEY_PRESS, () : void => {
            LogIt.Debug($instance.panel.textField.Value());
        });

        return "<div>Test output</div>";
    }
}

class BootstrapTestPage extends BasePanel {
    public readonly templateDropDown : DropDownList;
    public readonly panel : BootstrapTestPanel;
    public readonly searchFilter : TextField;

    constructor() {
        super();
        this.templateDropDown = new DropDownList();
        this.panel = new BootstrapTestPanel();
        this.searchFilter = new TextField();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="container-fluid">
        <div class="row"><h1>This is Bootstrap render test page</h1></div>
        <div class="row">
            <div class="col">
                <div class="dropdown" data-oidis-bind="${this.templateDropDown}" data-bs-toggle="tooltip" title="tooltip test">
                    <button class="btn btn-outline-primary" aria-expanded="false" data-bs-toggle="dropdown" type="button">test</button>
                    <div class="dropdown-menu" style="width: 100%;">loading ...</div>
                </div>
            </div>
            <div class="col">
                <input type="text" class="form-control" value="loading..." data-oidis-bind="${this.searchFilter}" />
            </div>
        </div>
        <div class="row" data-oidis-bind="${this.panel}"></div>
    </div>
</section>`;
    }
}

class BootstrapTestPanel extends BasePanel {
    public usersTable : GuiCommons;
    public textField : TextField;

    constructor() {
        super();
        this.usersTable = new GuiCommons();
        this.textField = new TextField();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="container-fluid">
        <div class="row"><h2>Sub-panel content</h2></div>
        <div class="row"><input type="text" class="form-control" data-oidis-bind="${this.textField}" /></div>
        <div class="row">
            <div class="table-responsive UsersTable" style="margin-top: 10px;">
                <table class="table table-striped table-sm">
                    <thead class="text-primary">
                        <tr>
                            <th>Name</th>
                            <th>Groups</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody data-oidis-bind="${this.usersTable}">
                        <tr>
                            <td>Loading data...</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>`;
    }
}

class BootstrapTestComponent extends GuiCommons {
    protected innerHtml() : string {
        return `<button class="btn btn-primary" type="button">Test</button>`;
    }
}

class BootstrapTestTableRecod extends GuiCommons {
    protected innerHtml() : string {
        return `<tr><td>New record ${this.Id()}</td><td>groups</td><td>actions</td></tr>`;
    }
}

export class BootstrapTestInputGroup extends GuiCommons {
    private readonly field : TextField;

    constructor() {
        super();
        this.field = new TextField();
    }

    public Value($value? : string) : string {
        return this.field.Value($value);
    }

    protected innerHtml() : string {
        return `
            <div class="input-group input-group-sm float-start" style="width: 20%;">
                <input data-oidis-bind="${this.field}" type="text" class="form-control" style="border-top-left-radius: 25px;border-bottom-left-radius: 25px;" />
                <button data-oidis-bind="${this}" class="btn btn-primary" type="button" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">Add</button>
            </div>`;
    }
}
/* dev:end */
