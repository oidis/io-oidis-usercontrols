/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { CropBoxEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/CropBoxEventArgs.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { CropBox } from "../../BaseInterface/Components/CropBox.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class CropBoxTest extends ViewerTestRunner<AbstractGuiObject> {
    private object : CropBox;

    constructor() {
        super();
        // this.setMethodFilter("ignoreAll");
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.object.Enabled(), true);
        this.object.Enabled(false);
        this.assertEquals(this.object.Enabled(), false);
    }

    public testStyleClassNameAPI() : void {
        this.object.StyleClassName("Style");
        this.assertEquals(this.object.StyleClassName(), "Style");
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.object.Visible(), true);
        this.object.Visible(false);
        this.assertEquals(this.object.Visible(), false);
    }

    public testsetDimensionsAPI() : void {
        this.object.setDimensions(200, 400, 400, 200);
        this.assertDeepEqual((<any>this.object).topOffset.Top(), 400);
        this.assertDeepEqual((<any>this.object).topOffset.Left(), 200);
        this.assertDeepEqual((<any>this.object).bottomOffset.Top(), 200);
        this.assertDeepEqual((<any>this.object).bottomOffset.Left(), 400);
    }

    public testgetArgs() : void {
        this.assertDeepEqual(this.object.getArgs().length, 11);
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.object);
                this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.object.getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.object, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.object.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.object.getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.object.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.object.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.object.getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_OVER, event);
        };
    }

    public __IgnoretestOnResizeStart() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.assertEquals(this.object.getEvents().Exists(EventType.ON_RESIZE_START), false);
            this.object.getEvents().setOnResizeStart(($eventArgs : CropBoxEventArgs) : void => {
                this.assertEquals($eventArgs.OffsetLeft(30), 30);
                this.assertEquals($eventArgs.OffsetTop(20), 20);
                this.assertEquals(this.object.getEvents().Exists(EventType.ON_RESIZE_START), true);
                $done();
            });
        };
    }

    public __IgnoretestEventsChange() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.object.getEvents().setOnResizeChange(($eventArgs : CropBoxEventArgs) : void => {
                this.assertEquals(this.object.getEvents().Exists(EventType.ON_RESIZE), true);
                $done();
            });
        };
    }

    protected setUp() : void {
        this.object.Visible(true);
        this.object.Enabled(true);
    }

    protected before() : string {
        // this.removeInstance();

        this.object = new CropBox();
        this.object.setDimensions(100, 100, 600, 600);
        this.object.setSize(500, 500);

        return this.object.Draw();
    }
}
/* dev:end */
