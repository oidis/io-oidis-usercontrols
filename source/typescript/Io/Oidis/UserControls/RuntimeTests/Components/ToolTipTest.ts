/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ToolTip } from "../../BaseInterface/Components/ToolTip.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class ToolTipTest extends ViewerTestRunner<AbstractGuiObject> {

    public testHideAPI() : void {
        const object : AbstractGuiObject = this.getInstance();
        object.Title().Text("My ToolTip");
        ToolTip.Show(object.Title());
        this.assertEquals(object.Title().Visible(), false);
        ToolTip.Hide(object.Title());
        this.assertEquals(object.Title().Visible(), false);
    }

    public testShowAPI() : void {
        const object : AbstractGuiObject = this.getInstance();
        object.Title().Text("My ToolTip");
        object.Visible(true);
        object.Enabled(true);
        object.getGuiOptions().Add(GuiOptionType.TOOL_TIP);
        this.getEventsManager().FireAsynchronousMethod(() => {
            ToolTip.Show(object.Title());
            this.assertEquals(object.Title().Visible(), false);
            this.getEventsManager().FireAsynchronousMethod(() => {
                this.assertEquals(object.Title().Visible(), false);
            }, 2000);
        }, 5000);
    }

    public testShowNext() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("My ToolTip");
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                ToolTip.Show(object.Title());
                this.getEventsManager().FireAsynchronousMethod(() : void => {

                    this.assertEquals(object.Title().Visible(), false);
                }, 600);
            }, 2000);
            $done();
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public __IgnoretestEvents() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const object : AbstractGuiObject = this.getInstance();
            object.getEvents().setOnMouseMove(() : void => {
                this.assertEquals(object.Title().Visible(), true);
                $done();
            });
            this.getEventsManager().FireEvent(object, EventType.ON_MOUSE_MOVE);
        };
    }

    protected setUp() : void {
        const object : AbstractGuiObject = this.getInstance();
        object.Title().Text("Gui object");
        object.Visible(true);
        object.Enabled(true);
        ToolTip.Show(object.Title());
    }

    protected before() : string {
        const object : AbstractGuiObject = this.getInstance();
        object.Title().Text("test " + StringUtils.NewLine() + StringUtils.NewLine() + "asd");
        object.StyleClassName("testCssClass");

        this.addButton("force show", () : void => {
            ToolTip.Show(object.Title());
        });
        this.addButton("force hide", () : void => {
            object.Title().Visible(false);
        });

        return "<style>.testCssClass {float: left; position: relative; top: 50px; left: 150px;}</style>";
    }
}
/* dev:end */
