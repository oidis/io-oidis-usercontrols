/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { ScrollBarEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/ScrollBarEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ScrollBar } from "../../BaseInterface/Components/ScrollBar.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class ScrollBarTest extends ViewerTestRunner<AbstractGuiObject> {
    private scrollBar1 : ScrollBar;
    private scrollBar2 : ScrollBar;

    constructor() {
        super();
        this.setMethodFilter("ignoreAll");
    }

    public testMoveTo() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() => {
                ElementManager.IsVisible(this.scrollBar1.Id());
                Reflection.getInstance();
                const args : ScrollEventArgs = new ScrollEventArgs();
                args.Position(50);
                ScrollBar.MoveTo(this.scrollBar1, 50);
                this.assertEquals(ElementManager.getCssValue(this.scrollBar1.Id() + "_ButtonEnvelop", "left"), "127px");
                this.assertEquals(args.Position(), 50);
                $done();
            }, 500);
        };
    }

    public testMoveToNext() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                ElementManager.getElement(this.scrollBar2.Id()).click();
                ScrollBar.MoveTo(this.scrollBar2, 50);
                this.assertEquals(ElementManager.getCssValue(this.scrollBar2.Id() + "_ButtonEnvelop", "left"), "127px");
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    document.body.click();
                    $done();
                }, 600);
            }, 300);
        };
    }

    public testOnScroll() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.scrollBar1.getEvents().setOnScroll(($eventArgs : ScrollEventArgs) : void => {
                this.assertEquals(this.scrollBar1.getEvents().Exists(EventType.ON_SCROLL), true);
                $done();
            });
            this.emulateEvent(this.scrollBar1, EventType.ON_SCROLL);
        };
    }

    public testOnArrow() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.scrollBar1.getEvents().setOnArrow(($eventArgs : ScrollEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar1.Id()), GeneralCssNames.ON);
                this.assertEquals(this.scrollBar1.getEvents().Exists(ScrollBarEventType.ON_ARROW), true);
                $done();
            });
            this.emulateEvent(this.scrollBar1, ScrollBarEventType.ON_ARROW);
        };
    }

    public testOnArrowOff() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.scrollBar2.getEvents().setOnButton(($eventArgs : ScrollEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar2.Id() + "_Button"), GeneralCssNames.ON);
                this.assertEquals(this.scrollBar2.getEvents().Exists(ScrollBarEventType.ON_BUTTON), true);
                $done();
            });
            this.emulateEvent(this.scrollBar2, ScrollBarEventType.ON_BUTTON);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.scrollBar2);
                this.assertEquals(ElementManager.getClassName(this.scrollBar2.Id() + "_Button"), GeneralCssNames.OFF);
                this.scrollBar2.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.scrollBar2.getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.scrollBar2, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar1.Id() + "_Button"), GeneralCssNames.OFF);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.scrollBar1.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.scrollBar1.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.scrollBar1, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar2.Id() + "_Button"), GeneralCssNames.OFF);
                this.scrollBar2.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.scrollBar2.getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.scrollBar2, EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar2.Id() + "_Button"), GeneralCssNames.OFF);
                this.scrollBar2.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.scrollBar2.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.scrollBar2, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.scrollBar1.Id() + "_Button"), GeneralCssNames.OFF);
                this.scrollBar1.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.scrollBar1.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.scrollBar1, EventType.ON_MOUSE_OUT);
        };
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.scrollBar1.OrientationType(), OrientationType.VERTICAL);
        this.scrollBar1.OrientationType(OrientationType.HORIZONTAL);
        this.assertEquals(this.scrollBar1.OrientationType(), OrientationType.HORIZONTAL);
        this.scrollBar1.OrientationType(OrientationType.VERTICAL);
    }

    public testSizeAPI() : void {
        this.assertEquals(this.scrollBar1.Size(), 300);
        this.scrollBar1.Size(600);
        this.assertEquals(this.scrollBar1.Size(), 600);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.scrollBar1.Visible(), true);
        this.scrollBar1.Visible(false);
        this.assertEquals(this.scrollBar1.Visible(), false);
        this.assertEquals(this.scrollBar1.Size(), 600);
        this.assertEquals(this.scrollBar1.OrientationType(), OrientationType.VERTICAL);
    }

    protected setUp() : void {
        this.scrollBar1.Visible(true);
        this.scrollBar1.Enabled(true);
    }

    protected before() : string {
        // this.removeInstance();

        this.scrollBar1 = new ScrollBar(OrientationType.VERTICAL, "testScrollBar1");
        this.scrollBar1.StyleClassName("TestClassName");

        this.scrollBar2 = new ScrollBar(OrientationType.HORIZONTAL, "testScrollBar2");
        this.scrollBar2.Size(400);

        this.addButton("move to 50%", () : void => {
            ScrollBar.MoveTo(this.scrollBar1, 50);
        });
        this.addButton("move to 10%", () : void => {
            ScrollBar.MoveTo(this.scrollBar1, 10);
        });
        this.addButton("move to 90%", () : void => {
            ScrollBar.MoveTo(this.scrollBar1, 90);
        });

        return "<div style=\"width: 80%; position: relative; top: 50px; left: 50px; height: 400px;\">" +
            this.scrollBar1.Draw() +
            "<div style=\"float: left; padding-left: 20px;\">" + StringUtils.Space(1) + "</div>" +
            this.scrollBar2.Draw() +
            "</div>" +
            "<div style=\"clear: both;\"></div>";
    }
}
/* dev:end */
