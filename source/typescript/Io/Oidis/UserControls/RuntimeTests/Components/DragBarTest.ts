/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementOffset } from "@io-oidis-gui/Io/Oidis/Gui/Structures/ElementOffset.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { DragBar } from "../../BaseInterface/Components/DragBar.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class DragBarTest extends ViewerTestRunner<AbstractGuiObject> {
    private object : DragBar;

    public testEnabledAPI() : void {
        this.assertEquals(this.object.Enabled(), true);
        this.assertEquals(this.object.Visible(), true);
        this.assertEquals(this.object.IsCached(), false);
    }

    public testParentAPI() : void {
        this.assertEquals(this.object.getGuiOptions().Contains(GuiOptionType.DISABLE), false);
    }

    public testgetSizeAPI() : void {
        this.assertEquals(this.object.getSize().Width(), 300);
        this.assertEquals(this.object.getSize().Height(), 15);
    }

    public testGuiOptions() : void {
        this.assertEquals(this.object.getGuiOptions().Contains(GuiOptionType.TOOL_TIP), false);
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.object);
                this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.object.getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.object, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.object.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.object.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.object.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_OUT);
        };
    }

    public __IgnoretestEvents() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.object.getEvents().setOnDragChange(($eventsArgs : MoveEventArgs) : void => {
                this.assertEquals(this.object.getEvents().Exists(EventType.ON_DRAG_CHANGE), true);
                $done();
            });
        };
    }

    public __IgnoretestOnDragComplete() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = (<any>this).owner.getInstance();
            dragbar.getEvents().setOnDragComplete(($eventsArgs : MoveEventArgs) : void => {
                this.assertEquals(dragbar.getEvents().Exists(EventType.ON_DRAG_COMPLETE), true);
                $done();
            });
        };
    }

    public __IgnoretestOnDragStart() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.object.getEvents().setOnDragStart(($eventsArgs : MoveEventArgs) : void => {
                this.assertEquals(this.object.getEvents().Exists(EventType.ON_DRAG_START), true);
                $done();
            });
        };
    }

    protected before() : string {
        // this.removeInstance();

        let parentOffset : ElementOffset;
        this.object = new DragBar("Test_DragBar");

        this.object.getEvents().setOnDragStart(() : void => {
            const parentId : string = "Test";
            if (ElementManager.Exists(parentId)) {
                parentOffset = ElementManager.getAbsoluteOffset(parentId);
            }
        });

        this.object.getEvents().setOnDragChange(($eventArgs : MoveEventArgs) : void => {
            const parentId : string = "Test";
            if (ElementManager.Exists(parentId)) {
                const parent : HTMLElement = ElementManager.getElement(parentId);
                let top : number = parentOffset.Top() + $eventArgs.getDistanceY();
                let left : number = parentOffset.Left() + $eventArgs.getDistanceX();
                const parentWidth : number = parent.offsetWidth;
                const parentHeight : number = parent.offsetHeight;

                if (left < 50) {
                    left = 50;
                }
                if (top < 50) {
                    top = 50;
                }
                const windowSize : Size = WindowManager.getSize();
                if (parentWidth + left > windowSize.Width() - 50) {
                    left = windowSize.Width() - parentWidth - 50;
                }
                if (parentHeight + top > windowSize.Height() - 50) {
                    top = windowSize.Height() - parentHeight - 50;
                }

                ElementManager.setCssProperty(parentId, "position", "fixed");
                ElementManager.setCssProperty(parentId, "top", top);
                ElementManager.setCssProperty(parentId, "left", left);
            }
        });

        return "<div style=\"float: left;\">" + StringUtils.NewLine(false) +
            "   <div id=\"Test\" style=\"border: 1px solid red; width: 300px; height: 300px; " +
            "position: relative; top: 20px; left: 20px;\">" + StringUtils.NewLine(false) +
            "       " + this.object.Draw() + StringUtils.NewLine(false) +
            "   </div>" + StringUtils.NewLine(false) +
            "</div>";
    }
}
/* dev:end */
