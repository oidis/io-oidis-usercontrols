/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ResizeBar } from "../../BaseInterface/Components/ResizeBar.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class ResizeBarTest extends ViewerTestRunner<AbstractGuiObject> {
    private object : ResizeBar;

    public testResizeTypeAPI() : void {
        this.assertEquals(this.object.ResizeableType(), ResizeableType.HORIZONTAL_AND_VERTICAL);
        this.object.ResizeableType(ResizeableType.VERTICAL);
        this.assertEquals(this.object.ResizeableType(), ResizeableType.VERTICAL);
        this.object.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
    }

    public __IgnoretestPositionAPI() : void {
        this.assertEquals(this.object.getScreenPosition().Left(), 417);
        this.assertEquals(this.object.getScreenPosition().Top(), 576);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.object.Visible(), true);
        this.object.Visible(false);
        this.assertEquals(this.object.Visible(), false);
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.object);
                this.object.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.object.getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.object, EventType.ON_CLICK);
        };
    }

    public __IgnoretestEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any =
                this.object.getEvents().setOnMouseMove(($eventArgs : MouseEventArgs) : void => {
                    this.object.setArg(<IGuiCommonsArg>{
                        name : "Width",
                        type : "Number",
                        value: 300
                    }, true);
                    ElementManager.setCssProperty(this.object.Id(), "_Width", 300);
                    this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                    this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                    this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                    $done();
                });
            this.object.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.object.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.object.getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.object, EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        this.object.Visible(true);
        this.object.Enabled(true);
        this.object.Width(200);
    }

    protected before() : string {
        // this.removeInstance();

        let parentSize : Size;
        this.object = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL, "Test_ResizeBar");

        this.object.getEvents().setOnResizeStart(() : void => {
            const parentId : string = "Test";
            if (ElementManager.Exists(parentId)) {
                parentSize = new Size(parentId, true);
            }
        });

        this.object.getEvents().setOnResizeChange(($eventArgs : ResizeBarEventArgs) : void => {
            const parentId : string = "Test";
            if (ElementManager.Exists(parentId)) {
                let width : number = parentSize.Width() + $eventArgs.getDistanceX();
                let height : number = parentSize.Height() + $eventArgs.getDistanceY();

                if (width < 100) {
                    width = 100;
                }
                if (width > 500) {
                    width = 500;
                }

                if (height < 100) {
                    height = 100;
                }
                if (height > 500) {
                    height = 500;
                }

                ElementManager.setWidth(parentId, width);
                ElementManager.setHeight(parentId, height);
            }
        });

        return "<div style=\"float: left; position: relative; top: 100px; left: 100px;\">" + StringUtils.NewLine(false) +
            "   <div id=\"Test\" style=\"border: 1px solid red; width: 300px; height: 300px;\"></div>" + StringUtils.NewLine(false) +
            "   " + this.object.Draw() + StringUtils.NewLine(false) +
            "</div>";
    }
}
/* dev:end */
