/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { Notification } from "../../BaseInterface/Components/Notification.js";
import { NotificationType } from "../../BaseInterface/Enums/Components/NotificationType.js";
import { AbstractGuiObject } from "../../Primitives/AbstractGuiObject.js";

export class NotificationTest extends ViewerTestRunner<AbstractGuiObject> {

    public testTextAPI() : void {
        this.assertEquals(this.getInstance().Notification().Text(), "Notification text");
        this.getInstance().Notification().Text("New Text");
        this.assertEquals(this.getInstance().Notification().Text(), "New Text");
        Notification.Show(this.getInstance().Notification());
        this.assertEquals(this.getInstance().Notification().Visible(), true);
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.getInstance().Notification().GuiType(), NotificationType.GENERAL);
        this.getInstance().Notification().GuiType(NotificationType.WARNING);
        this.assertEquals(this.getInstance().Notification().GuiType(), NotificationType.WARNING);
        this.getInstance().Notification().GuiType(NotificationType.GENERAL);
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Notification().Width(), 200);
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().Notification().StyleClassName(), "testCssClass");
    }

    public testHideAPI() : void {
        Notification.Hide(this.getInstance().Notification());
        this.getInstance().Notification().Visible(false);
        this.assertEquals(this.getInstance().Notification().Visible(), false);
        this.getInstance().Notification().Visible(true);
    }

    public __IgnoretestEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance().Notification());
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.getInstance().Notification().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().Notification().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance().Notification(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().Notification().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().Notification().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance().Notification(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().Notification().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().Notification().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance().Notification(), EventType.ON_MOUSE_OVER, event);
        };
    }

    public __IgnoretestEvent() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const object : AbstractGuiObject = this.getInstance();
            object.Notification().getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                object.getEvents().FireAsynchronousMethod(() : void => {
                    object.Notification().Visible(true);
                }, 2000);
                $done();
            });
        };
    }

    protected setUp() : void {
        const object : AbstractGuiObject = this.getInstance();
        object.Notification().GuiType(NotificationType.GENERAL);
        object.Notification().Text("Notification text");
    }

    protected before() : string {
        const object : AbstractGuiObject = this.getInstance();
        object.StyleClassName("testHolderCssClass");
        object.Notification().Text("Notification text");
        object.Notification().StyleClassName("testCssClass");
        object.Notification().GuiType(NotificationType.GENERAL);
        object.Notification().Width(200);
        object.getEvents().setOnClick(($args : EventArgs) : void => {
            const object : AbstractGuiObject = <AbstractGuiObject>$args.Owner();
            object.Notification().Visible(true);
        });

        this.addButton("hide", () : void => {
            Notification.Hide(object.Notification());
        });
        this.addButton("show", () : void => {
            Notification.Show(object.Notification());
        });
        this.addButton("general type", () : void => {
            object.Notification().GuiType(NotificationType.GENERAL);
        });
        this.addButton("info type", () : void => {
            object.Notification().GuiType(NotificationType.INFO);
        });
        this.addButton("warning type", () : void => {
            object.Notification().GuiType(NotificationType.WARNING);
        });
        this.addButton("error type", () : void => {
            object.Notification().GuiType(NotificationType.ERROR);
        });
        this.addButton("success type", () : void => {
            object.Notification().GuiType(NotificationType.SUCCESS);
        });
        this.addButton("set text", () : void => {
            object.Notification().Text("runtime notification text");
        });
        this.addButton("set width", () : void => {
            object.Notification().Width(100);
        });

        return StringUtils.NewLine() + StringUtils.NewLine() +
            "<style>.testHolderCssClass {position: relative; top: 20px;}</style>" +
            "<style>.testCssClass {float: left; position: relative; top: -30px; left: 110px;}</style>" +
            object.Notification().Draw() +
            "<div style=\"padding-top: 20px; clear: both;\"></div>" +
            StringUtils.NewLine();
    }
}
/* dev:end */
