/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { VideoEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/VideoEventType.js";
import { MediaEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MediaEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { IMediaSource } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IMediaSource.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { Camera } from "../../BaseInterface/Components/Camera.js";
import { FileUpload } from "../../BaseInterface/Components/FileUpload.js";
import { Video } from "../../BaseInterface/UserControls/Video.js";

export class VideoTest extends ViewerTestRunner<Video> {
    private object : Video;

    constructor() {
        super();
        this.setMethodFilter("IgnoreAll");
    }

    public testStreamAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const stopStream : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.stopStreamAPI();
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        $done();
                    }, 1000);
                }, 5000);
            };
            this.streamAPI();
            stopStream();
        };
    }

    public testExternalSourcesAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.addSourceAPI();
            this.getInstance().Play();
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                $done();
            }, 500);
        };
    }

    public testDurationAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const stopSource : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(this.getInstance().Duration(), 5.568, "Test video has correct duration");
                    this.getInstance().Stop();
                    $done();
                }, 500);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            stopSource();
        };
    }

    public testEndedAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const deleteSource : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(this.getInstance().Ended(), true, "Test video has ended");
                    $done();
                }, 6000);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            deleteSource();
        };
    }

    public testMuteAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const unmute : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.getInstance().Mute(false);
                    this.assertEquals(this.getInstance().Mute(), false, "Video is unmuted");
                    $done();
                }, 3000);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            this.assertEquals(this.getInstance().Mute(), false, "Video is not muted");
            this.getInstance().Mute(true);
            this.assertEquals(this.getInstance().Mute(), true, "Video is muted");
            unmute();
        };
    }

    public testPauseAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const pause : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.addSourceAPI();
                    this.getInstance().Play();
                    this.assertEquals(this.getInstance().Paused(), false, "Video is running");
                }, 1000);
            };
            const deleteSource : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(this.getInstance().Paused(), false, "Video is running again");
                    $done();
                }, 3000);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            this.assertEquals(this.getInstance().Paused(), false, "Video is running");
            pause();
            deleteSource();
        };
    }

    public testFullScreenAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const closeFS : any = () : any => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(WindowManager.IsInFullScreen(), true, "Video is in full screen");
                    this.getInstance().FullScreen(false);
                    this.assertEquals(WindowManager.IsInFullScreen(), false, "Video exited full screen");
                    $done();
                }, 6000);
            };

            this.addSourceAPI();
            this.getInstance().Play(true);
            this.assertEquals(WindowManager.IsInFullScreen(), false, "Video is not in full screen");
            this.getInstance().FullScreen(true);
            closeFS();
        };
    }

    public testLoopedAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const loop : any = () : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(this.getInstance().Looped(), true, "Video is looped");
                    $done();
                }, 6000);
            };

            this.addSourceAPI();
            this.getInstance().Play();
            this.assertEquals(this.getInstance().Looped(), false, "Video is not looped");
            this.getInstance().Play(true);
            loop();
        };
    }

    public testAddExternalSources() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const addHandler : any = ($eventArgs : MediaEventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test source add event owner");
                this.assertEquals($eventArgs.EventMessage(),
                    "Source: " + this.getInstance().Id() + "_InputVideoSource added",
                    "Test add event message");
                this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_PLAY, addHandler);
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    $done();
                }, 500);
            };

            this.getInstance().getEvents().setOnPlay(addHandler);
            this.addSourceAPI();
            this.getInstance().Play();
        };
    }

    public testStartStream() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const startStream : any = ($eventArgs : MediaEventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test start streaming from camera");
                this.assertEquals($eventArgs.EventMessage(),
                    "Stream from camera has been started",
                    "Test start streaming from camera");
                this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_PLAY, startStream);
            };

            this.getInstance().getEvents().setOnPlay(startStream);
            this.streamAPI();
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.stopStreamAPI();
                $done();
            }, 5000);
        };
    }

    public testStopStream() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const stopStream : any = ($eventArgs : MediaEventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test stop streaming from camera event owner");
                    this.assertEquals($eventArgs.EventMessage(),
                        "Streaming video has been stopped.",
                        "Test stop streaming event message");
                    this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_STOP, stopStream);
                    $done();
                }, 9000);
            };

            this.streamAPI();
            this.getInstance().getEvents().setOnStop(stopStream);
            this.stopStreamAPI();
        };
    }

    public testMute() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const muteHandler : any = ($eventArgs : MediaEventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test source mute event owner");
                this.assertEquals($eventArgs.EventMessage(), "Video has been muted", "Test mute event message");
                this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_MUTE, muteHandler);
                $done();
            };
            this.addSourceAPI();
            this.getInstance().Play();
            this.getInstance().getEvents().setOnMute(muteHandler);
            this.getInstance().Mute(true);
        };
    }

    public testLooped() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const loopHandler : any = ($eventArgs : MediaEventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test play source in loop event owner");
                    this.assertEquals($eventArgs.EventMessage(), "Video has been playing in loop", "Test loop event message");
                    this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_PLAY, loopHandler);
                    $done();
                }, 9000);
            };
            this.addSourceAPI();
            this.getInstance().getEvents().setOnPlay(loopHandler);
            this.getInstance().Play(true);
        };
    }

    public testOpenFullScreen() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const FSscreenhandler : any = ($eventArgs : MediaEventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test fullscreen (open) event owner");
                    this.assertEquals($eventArgs.EventMessage(), "This video is in fullscreen now",
                        "Test FS event message");
                    this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_FULLSCREEN_START, FSscreenhandler);
                    this.getInstance().FullScreen(false);
                    this.assertEquals(WindowManager.IsInFullScreen(), false, "Video exited full screen");
                    $done();
                }, 6000);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            this.getInstance().FullScreen(true);
            this.assertEquals(WindowManager.IsInFullScreen(), true, "Video is in full screen");
            this.getInstance().getEvents().setOnFullscreenStart(FSscreenhandler);
        };
    }

    public testCloseFullScreen() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const FSscreenhandler : any = ($eventArgs : MediaEventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance(), "Test fullscreen (close) event owner");
                    this.assertEquals($eventArgs.EventMessage(), "This video out of fullscreen now",
                        "Test FS event message");
                    this.getInstance().getEvents().RemoveHandler(VideoEventType.ON_FULLSCREEN_END, FSscreenhandler);
                    $done();
                }, 6000);
            };
            this.addSourceAPI();
            this.getInstance().Play();
            this.getInstance().FullScreen(true);
            this.assertEquals(WindowManager.IsInFullScreen(), true, "Video is in full screen");
            this.getInstance().getEvents().setOnFullscreenEnd(FSscreenhandler);
            this.getInstance().FullScreen(false);
            this.assertEquals(WindowManager.IsInFullScreen(), false, "Video exited full screen");
        };
    }

    protected setUp() : void {
        this.object.Visible(true);
        this.object.Enabled(true);
    }

    protected before() : string {
        const object : Video = this.getInstance();
        this.object = object;
        this.setUp();

        this.addButton("Change values", () : void => {
            object.Autoplay(false);
            object.WithControls(true);
            object.Mute(false);
        });

        const fileUpload : FileUpload = new FileUpload();
        fileUpload.DisableAsynchronousDraw();
        fileUpload.MultipleSelectEnabled(false);
        fileUpload.setDropZone("fileDrop");

        this.addButton("Play", () : void => {
            object.Play();
        });
        this.addButton("Pause", () : void => {
            object.Pause();
        });
        this.addButton("Stop", () : void => {
            object.Stop();
        });
        this.addButton("Play in loop", () : void => {
            object.Play(true);
        });
        this.addButton("Stop loop", () : void => {
            object.Play(false);
        });
        this.addButton("Mute", () : void => {
            object.Mute(true);
        });
        this.addButton("Unmute", () : void => {
            object.Mute(false);
        });
        this.addButton("Duration", () : void => {
            LogIt.Debug(object.Duration());
        });
        this.addButton("Full screen", () : void => {
            LogIt.Debug(WindowManager.IsInFullScreen());
            object.FullScreen(true);
            setTimeout(() : void => {
                LogIt.Debug(WindowManager.IsInFullScreen());
                object.FullScreen(false);
            }, 2500);
        });
        this.addButton("Resize", () : void => {
            object.setSize(200, 200);
        });
        this.addButton("Add file", () : void => {
            object.Source(<IMediaSource>{
                src : "file:///" + this.getAbsoluteRoot() + "/test/resource/graphics/Io/Oidis/UserControls/small.mp4",
                type: "video/mp4"
            });
            object.Play();
        });
        this.addButton("Add url", () : void => {
            object.Source(<IMediaSource>{
                src : "http://techslides.com/demos/sample-videos/small.mp4",
                type: "video/mp4"
            });
            object.Play();
        });
        this.addButton("Add Camera", () : void => {
            this.streamAPI();
        });

        let isBase64 : boolean = false;
        this.addButton("Base64", () : void => {
            isBase64 = true;
        });
        this.addButton("Chunks", () : void => {
            isBase64 = false;
        });
        fileUpload.getEvents().setOnChange(() : void => {
            if (isBase64) {
                fileUpload.getStream(($data : ArrayList<string>) : void => {
                    object.Source(<IMediaSource>{
                        src : $data.getFirst(),
                        type: "video/mp4"
                    });
                    object.Play();
                });
            } else {
                // TODO: works only with:
                //  https://github.com/nickdesaulniers/netfix/raw/gh-pages/demo/frag_bunny.mp4
                fileUpload.getStream(($data : ArrayList<string>) : void => {
                    const data : ArrayBuffer = Convert.Base64ToArrayBuffer($data.getFirst());

                    const arrayBufferToBase64 : any = ($data : ArrayBuffer) : string => {
                        let binary : string = "";
                        const bytes : Uint8Array = new Uint8Array($data);
                        const length : number = bytes.byteLength;
                        for (let i : number = 0; i < length; i++) {
                            binary += String.fromCharCode(bytes[i]);
                        }
                        return window.btoa(binary);
                    };

                    const numberOfChunks : number = 10;
                    const chunkSize : number = Math.ceil(data.byteLength / numberOfChunks);
                    const createChunkData : any = ($index : number) : void => {
                        if ($index < numberOfChunks) {
                            const startByte : number = chunkSize * $index;
                            const chunk : ArrayBuffer = data.slice(startByte, startByte + chunkSize);
                            object.Source(<IMediaSource>{
                                chunkId: $index,
                                codecs : ["avc1.42C01E", "mp4a.40.2"],
                                src    : arrayBufferToBase64(chunk),
                                type   : "video/mp4"
                            });
                            if ($index === 0) {
                                object.Play();
                            }
                            setTimeout(() : void => {
                                createChunkData($index + 1);
                            }, 100);
                        }
                    };
                    createChunkData(0);
                });
            }
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>" +
            "<div id=\"fileDrop\" style=\"width: 500px; height: 250px; border: 1px solid red;\">Drop file here</div>" +
            fileUpload.Draw();
    }

    protected after() : void {
        this.setUp();
    }

    private addSourceAPI() : void {
        this.object.Source(<IMediaSource>{
            src : "http://techslides.com/demos/sample-videos/small.mp4",
            type: "video/mp4"
        });
    }

    private streamAPI() : void {
        const camera : Camera = new Camera();
        camera.getStream(($stream : MediaStream) : void => {
            this.object.Source($stream);
            this.object.Play();
        });
    }

    private stopStreamAPI() : void {
        this.getInstance().Stop();
    }
}
/* dev:end */
