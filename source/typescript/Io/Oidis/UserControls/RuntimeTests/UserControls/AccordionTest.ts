/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { AccordionType } from "../../BaseInterface/Enums/UserControls/AccordionType.js";
import { Accordion } from "../../BaseInterface/UserControls/Accordion.js";
import { BasePanelHolder } from "../../Primitives/BasePanelHolder.js";
import { UserControlsTestPanel } from "../UserControlsTestPanel.js";

export class AccordionTest extends ViewerTestRunner<Accordion> {

    constructor() {
        super();
        this.setMethodFilter("ignoreAll");
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Width(), 785);
        this.getInstance().Width(500);
        this.assertEquals(this.getInstance().Width(), 500);
        this.getInstance().Width(785);
    }

    public testHeightAPI() : void {
        this.assertEquals(this.getInstance().Height(), 530);
        this.getInstance().Height(600);
        this.assertEquals(this.getInstance().Height(), 600);
        this.getInstance().Height(530);
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "TestCss");
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
        this.getInstance().Enabled(true);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(this.getInstance().StyleClassName(), "TestCss");
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                }, 500);
                $done();
            };
            this.getInstance().getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), null);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), null);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), null);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    public testCollapseAll() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const accordion : Accordion = this.getInstance();
            accordion.Collapse();
            let isCollapsed : boolean = true;
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                (<any>accordion).holders.foreach(($propertyName : string) : void => {
                    if (this.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = <BasePanelHolder>accordion[$propertyName];
                        if (holder.IsOpened()) {
                            isCollapsed = false;
                        }
                    }
                });
                this.assertEquals(isCollapsed, true);
                $done();
            }, 600);
        };
    }

    public testOpenSingle() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const accordion : Accordion = this.getInstance();
            accordion.ExpandExclusive();
            let changeWidth : boolean = false;
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                (<any>accordion).holders.foreach(($propertyName : string) : void => {
                    if (this.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                        if (holder.IsOpened()) {
                            if (holder !== holder) {
                                holder.IsOpened(false);
                                changeWidth = true;
                            }
                        } else {
                            if (holder === holder) {
                                holder.IsOpened(true);
                                changeWidth = true;
                            }
                        }
                        this.assertEquals(changeWidth, true);
                    }
                });
            }, 600);
            $done();
        };
    }

    public testExpandAll() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const accordion : Accordion = this.getInstance();
            accordion.Expand();
            let isExpanded : boolean = true;
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                (<any>accordion).holders.foreach(($propertyName : string) : void => {
                    if (this.hasOwnProperty($propertyName)) {
                        const holder : BasePanelHolder = <BasePanelHolder>accordion[$propertyName];
                        if (!holder.IsOpened()) {
                            holder.IsOpened(true);
                            isExpanded = false;
                        }
                    }
                });
                this.assertEquals(isExpanded, true);
                $done();
            }, 600);
        };
    }

    protected setUp() : void {
        const instance : Accordion = this.getInstance();
        instance.Visible(true);
        instance.Enabled(true);
    }

    protected before() : string {
        const instance : Accordion = this.getInstance();
        instance.StyleClassName("TestCss");
        (<any>this).owner.normalImplementation();

        WindowManager.getEvents().setOnResize(($eventArgs : ResizeEventArgs) : void => {
            instance.Width(Math.ceil(WindowManager.getSize().Width() * 0.66));
            instance.Height(Math.ceil(WindowManager.getSize().Height() * 0.66));
        });

        this.addButton("Collapse all", () : void => {
            instance.Collapse();
        });

        this.addButton("Expand 2", () : void => {
            instance.Expand(0, 2);
        });

        this.addButton("Collapse 2", () : void => {
            instance.Collapse(0, 2);
        });

        this.addButton("Expand mid", () : void => {
            instance.Expand(UserControlsTestPanel);
        });

        this.addButton("Open single", () : void => {
            instance.ExpandExclusive(0);
        });

        this.addButton("400/400", () : void => {
            instance.Width(400);
            instance.Height(400);
        });

        this.addButton("400/800", () : void => {
            instance.Width(400);
            instance.Height(800);
        });

        this.addButton("600/600", () : void => {
            instance.Width(600);
            instance.Height(600);
        });

        let isVertical : boolean = instance.GuiType() === AccordionType.VERTICAL;
        this.addButton("toggle horizontal/vertical", () : void => {
            isVertical = !isVertical;
            instance.GuiType(isVertical ? AccordionType.VERTICAL : AccordionType.HORIZONTAL);
        });

        return "<style>.TestCss {position: relative; top: 40px; left: -10px;}</style>" +
            "<div style=\"clear: both; height: 40px;\"></div>";
    }
}
/* dev:end */
