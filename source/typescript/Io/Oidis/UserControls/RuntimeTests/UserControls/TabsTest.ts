/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ITab } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ITab.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { TabsType } from "../../BaseInterface/Enums/UserControls/TabsType.js";
import { Tabs } from "../../BaseInterface/UserControls/Tabs.js";

export class TabsTest extends ViewerTestRunner<Tabs> {

    public testGuiTypeAPI() : void {

        this.getInstance().GuiType(TabsType.GENERAL);
        this.assertEquals(this.getInstance().GuiType(), TabsType.GENERAL);
        this.getInstance().GuiType(TabsType.GREEN);
        this.assertEquals(this.getInstance().GuiType(), TabsType.GREEN);
        this.getInstance().GuiType(TabsType.BLUE);
        this.assertEquals(this.getInstance().GuiType(), TabsType.BLUE);
    }

    public testTextAPI() : void {
        this.assertEquals(this.getInstance().getItem(1).Text(), "test tab 2");
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
        this.getInstance().StyleClassName("NewClassName");
        this.assertEquals(this.getInstance().StyleClassName(), "NewClassName");
    }

    public testIsCached() : void {
        this.assertEquals(this.getInstance().IsCached(), false);
    }

    public testgetItemAPI() : void {
        this.assertEquals(this.getInstance().getItem(2).Text(), "test tab 3");
    }

    public testgetGuiOptions() : void {
        this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
    }

    public testSelectAPI() : void {
        this.getInstance().Select("testSelectItem");
        this.assertEquals(this.getInstance().IsLoaded(), true);
        this.assertEquals(this.getInstance().IsCompleted(), true);
        this.assertEquals(this.getInstance().IsPrepared(), true);
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Width(), 500);
        this.getInstance().Width(700);
        this.assertEquals(this.getInstance().Width(), 700);
    }

    public __IgnoretestFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.testStyleClassNameAPI();
                    this.testWidthAPI();
                    this.testGuiTypeAPI();
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.OFF);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getInstance().Enabled(false);
            const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.testGuiTypeAPI();
                    this.testWidthAPI();
                    // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.OFF);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                // this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), "");
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.OFF);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ON);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.OFF);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ON);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : Tabs = this.getInstance();
        object.Visible(true);
        object.Enabled(true);
        object.StyleClassName("testCssClass");
        object.Select(0);
        object.Width(500);
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
        object.GuiType(TabsType.GENERAL);
    }

    protected before() : string {
        const object : Tabs = this.getInstance();
        this.setUp();
        if (!object.IsCached()) {
            object.Clear();
            // object.Title().Text("label list title");
            object.Add("test tab 1");
            object.Add("test tab 2");
            object.Add("test tab 3");
            object.Add("test tab 4");
        }

        const item : ITab = object.getItem(2);
        item.Text("test tab 3");
        item.Enabled(false);

        const item2 : ITab = object.getItem(3);
        item2.Text("test tab 4");
        item2.Title().Text("tab 4 title");

        // object.Select(item2.Id());
        // object.Width(300);
        // object.Enabled(false);

        this.addButton("Disable", () : void => {
            object.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            object.Enabled(true);
        });
        this.addButton("Show", () : void => {
            object.Visible(true);
        });
        this.addButton("Hide", () : void => {
            object.Visible(false);
        });
        this.addButton("Set text", () : void => {
            item2.Text("new tab text");
        });
        this.addButton("Select", () : void => {
            object.Select(1);
        });
        this.addButton("Width 300", () : void => {
            object.Width(300);
        });
        this.addButton("Width 700", () : void => {
            object.Width(700);
        });
        this.addButton("Red", () : void => {
            object.GuiType(TabsType.RED);
        });
        this.addButton("Blue", () : void => {
            object.GuiType(TabsType.BLUE);
        });
        this.addButton("Green", () : void => {
            object.GuiType(TabsType.GREEN);
        });
        this.addButton("General", () : void => {
            object.GuiType(TabsType.GENERAL);
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
