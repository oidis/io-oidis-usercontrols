/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { IMediaSource } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IMediaSource.js";
import { FileUpload } from "../../BaseInterface/Components/FileUpload.js";
import { Audio } from "../../BaseInterface/UserControls/Audio.js";

export class AudioTest extends ViewerTestRunner<Audio> {

    protected before() : string {
        const object : Audio = this.getInstance();

        this.addButton("With controls", () : void => {
            object.WithControls(true);
            object.Autoplay(false);
            object.Mute(false);
        });

        const fileUpload : FileUpload = new FileUpload();
        fileUpload.DisableAsynchronousDraw();
        fileUpload.MultipleSelectEnabled(false);
        fileUpload.setDropZone("fileDrop");

        this.addButton("Play", () : void => {
            object.Play();
        });
        this.addButton("Pause", () : void => {
            object.Pause();
        });
        this.addButton("Stop", () : void => {
            object.Stop();
        });
        this.addButton("Play in loop", () : void => {
            object.Play(true);
        });
        this.addButton("Stop loop", () : void => {
            object.Play(false);
        });
        this.addButton("Mute", () : void => {
            object.Mute(true);
        });
        this.addButton("Unmute", () : void => {
            object.Mute(false);
        });
        this.addButton("Duration", () : void => {
            LogIt.Debug(object.Duration());
        });
        this.addButton("Add file", () : void => {
            object.Source(<IMediaSource>{
                src : "file:///" + this.getAbsoluteRoot() + "/test/resource/graphics/Io/Oidis/UserControls/small.mp3",
                type: "audio/mpeg"
            });
            object.Play();
        });
        this.addButton("Add url", () : void => {
            object.Source(<IMediaSource>{
                src : "https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_700KB.mp3",
                type: "audio/mpeg"
            });
            object.Play();
        });

        let isBase64 : boolean = false;
        this.addButton("Base64", () : void => {
            isBase64 = true;
        });
        this.addButton("Chunks", () : void => {
            isBase64 = false;
        });
        fileUpload.getEvents().setOnChange(() : void => {
            if (isBase64) {
                fileUpload.getStream(($data : ArrayList<string>) : void => {
                    object.Source(<IMediaSource>{
                        src : $data.getFirst(),
                        type: "audio/mpeg"
                    });
                    object.Play();
                });
            } else {
                fileUpload.getStream(($data : ArrayList<string>) : void => {
                    const data : ArrayBuffer = Convert.Base64ToArrayBuffer($data.getFirst());

                    const arrayBufferToBase64 : any = ($data : ArrayBuffer) : string => {
                        let binary : string = "";
                        const bytes : Uint8Array = new Uint8Array($data);
                        const length : number = bytes.byteLength;
                        for (let i : number = 0; i < length; i++) {
                            binary += String.fromCharCode(bytes[i]);
                        }
                        return window.btoa(binary);
                    };

                    const numberOfChunks : number = 10;
                    const chunkSize : number = Math.ceil(data.byteLength / numberOfChunks);
                    const createChunkData : any = ($index : number) : void => {
                        if ($index < numberOfChunks) {
                            const startByte : number = chunkSize * $index;
                            const chunk : ArrayBuffer = data.slice(startByte, startByte + chunkSize);
                            object.Source(<IMediaSource>{
                                chunkId: $index,
                                codecs : ["opus"],
                                src    : arrayBufferToBase64(chunk),
                                type   : "audio/mpeg"
                            });
                            if ($index === 0) {
                                object.Play();
                            }
                            setTimeout(() : void => {
                                createChunkData($index + 1);
                            }, 100);
                        }
                    };
                    createChunkData(0);
                });
            }
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>" +
            "<div id=\"fileDrop\" style=\"width: 500px; height: 250px; border: 1px solid red;\">Drop file here</div>" +
            fileUpload.Draw();
    }
}
/* dev:end */
