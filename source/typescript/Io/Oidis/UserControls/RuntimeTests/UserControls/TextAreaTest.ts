/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { TextAreaType } from "../../BaseInterface/Enums/UserControls/TextAreaType.js";
import { TextArea } from "../../BaseInterface/UserControls/TextArea.js";

export class TextAreaTest extends ViewerTestRunner<TextArea> {

    public testTitleAPI() : void {
        this.assertEquals(this.getInstance().Title().Text(), "Text filed tooltip text");
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.getInstance().GuiType(), TextAreaType.GENERAL);
    }

    public testLengthLimitAPI() : void {
        this.assertEquals(this.getInstance().LengthLimit(), 100);
    }

    public testValueAPI() : void {
        this.assertEquals(this.getInstance().Value(), "test value");
        this.getInstance().Value("New Value wrote.");
        this.assertEquals(this.getInstance().Value(), "New Value wrote.");
    }

    public testWidthAPI() : void {
        this.getInstance().Width(400);
        this.getInstance().Height(400);
        this.assertEquals(this.getInstance().Height(), 400);
        this.assertEquals(this.getInstance().Width(), 400);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
    }

    public testHintAPI() : void {
        this.assertEquals(this.getInstance().Hint(), "Write some text here ...");
    }

    public testResizeableTypeAPI() : void {
        this.assertEquals(this.getInstance().ResizeableType(), ResizeableType.HORIZONTAL_AND_VERTICAL);
    }

    public testgetGuiOption() : void {
        const area : TextArea = this.getInstance();
        this.assertEquals(area.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
    }

    public __IgnoretestFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                    this.testValueAPI();
                    this.testWidthAPI();
                    this.getInstance().getEvents().RemoveHandler(GeneralCssNames.ACTIVE, focusHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getInstance().Enabled(false);
            const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.testValueAPI();
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                // this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : TextArea = this.getInstance();
        object.Visible(true);
        object.Enabled(true);
        object.Value("test value");
        object.LengthLimit(100);
        object.GuiType(TextAreaType.GENERAL);
        object.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
        object.Title().Text("Text filed tooltip text");
        object.StyleClassName("testCssClass");
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
        object.Hint("Write some text here ...");
    }

    protected before() : string {
        const object : TextArea = this.getInstance();
        this.setUp();

        if (!object.InstanceOwner().IsCached()) {
            object.Width(300);
            object.Height(100);
            object.counterText.Text("Remains character {0} count.");
        }
        // object.Enabled(false);

        this.addButton("Disable", () : void => {
            object.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            object.Enabled(true);
        });
        this.addButton("Error", () : void => {
            object.Error(true);
        });
        this.addButton("Normal", () : void => {
            object.Error(false);
        });
        this.addButton("General", () : void => {
            object.GuiType(TextAreaType.GENERAL);
        });
        this.addButton("Red", () : void => {
            object.GuiType(TextAreaType.RED);
        });
        this.addButton("Green", () : void => {
            object.GuiType(TextAreaType.GREEN);
        });
        this.addButton("Blue", () : void => {
            object.GuiType(TextAreaType.BLUE);
        });
        this.addButton("Set text", () : void => {
            object.Value("new text filed value");
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
