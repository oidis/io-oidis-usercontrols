/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ImageOutputArgs } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageOutputArgs.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ImageType } from "../../BaseInterface/Enums/UserControls/ImageType.js";
import { Image } from "../../BaseInterface/UserControls/Image.js";
import { EventsManager } from "../../Events/EventsManager.js";

export class ImageTest extends ViewerTestRunner<Image> {

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
    }

    public testSourceAPI() : void {
        this.assertEquals(this.getInstance().Source(), "test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "testCssClass ValidationEnvelop");
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.getInstance().GuiType(), ImageType.GALLERY_PHOTO);
        this.getInstance().GuiType(ImageType.GENERAL);
        this.assertEquals(this.getInstance().GuiType(), ImageType.GENERAL);
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().getWidth(), 800);
        this.assertEquals(this.getInstance().getHeight(), 800);
    }

    public testLinkAPI() : void {
        this.getInstance().Link("/testLink2");
        this.assertEquals(this.getInstance().Link(), "/io-oidis-usercontrols/testLink2");
    }

    public __IgnoretestFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 1000);
            };
            EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public __IgnoretestFocusStateDisableAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getInstance().Enabled(false);
            const focusDisabledApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisabledApiHandler);
                    $done();
                }, 1000);
            };
            EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusDisabledApiHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), GeneralCssNames.ACTIVE);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : Image = this.getInstance();
        object.Enabled(true);
        object.Visible(true);
        object.StyleClassName("testCssClass ValidationEnvelop");
        object.setSize(800, 800);
        object.loadingText.Text("image is loading, please wait ...");
        object.GuiType(ImageType.GALLERY_PHOTO);
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
    }

    protected before() : string {
        const object : Image = this.getInstance();
        this.setUp();
        // object.setSize(1024, 796);
        // object.setSize(796, 1024);
        // object.Link("/testLink");
        // object.Link("/testLink2", false);
        // object.loadingText.Visible(false);
        // object.alternateText.Text("test image");
        // object.alternateText.Visible(false);
        // object.LoadingSpinnerEnabled(false);
        // object.OpacityShowEnabled(false);
        object.Title().Text("title test");
        // object.IsSelected(true);
        // object.Visible(false);

        object.Source("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        // object.Source("test/resource/graphics/Io/Oidis/UserControls/img2.jpg");
        // object.Source("Oidis");
        // object.StyleClassName("Logo");

        const outputArgs : ImageOutputArgs = new ImageOutputArgs();
        outputArgs.FrontEndCacheEnabled(false);
        outputArgs.FillEnabled(false);
        outputArgs.Corners(10, 20, 30, 40);
        outputArgs.CornerOnEnvelop(false);
        outputArgs.GrayscaleEnabled(true);
        // outputArgs.InvertEnabled(true);
        // outputArgs.BlurEnabled(true);
        // outputArgs.Brightness(100);
        // outputArgs.Contrast(-100);
        // outputArgs.Rotation(90);
        // outputArgs.CropDimension(100, 100, 300, 200);
        // outputArgs.Zoom(50);
        // outputArgs.Quality(80);
        outputArgs.WaterMarkSource("test/resource/graphics/Io/Oidis/UserControls/watermark.png");
        outputArgs.WaterMarkEnabled(true);
        object.OutputArgs(outputArgs);

        this.addButton("Disable", () : void => {
            object.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            object.Enabled(true);
        });
        this.addButton("Show", () : void => {
            object.Visible(true);
        });
        this.addButton("Hide", () : void => {
            object.Visible(false);
        });
        this.addButton("set link", () : void => {
            object.Link("/testLink2", false);
        });
        this.addButton("set source", () : void => {
            object.Source("test/resource/graphics/Io/Oidis/UserControls/img2.jpg");
        });
        this.addButton("rotate +5deg", () : void => {
            outputArgs.Rotation(outputArgs.Rotation() + 5);
            object.OutputArgs(outputArgs);
        });

        const object2 : Image = new Image("OID");
        object2.Link("www.oidis.io");
        object2.StyleClassName("Logo");

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;} " +
            ".ValidationEnvelop {border: 1px dashed red; background-color: chartreuse;}" +
            ".testCssClass2 {position: relative; top: 50px; left: 210px; float: left;}</style>" +
            object2.Draw() +
            StringUtils.NewLine() +
            StringUtils.NewLine();
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
