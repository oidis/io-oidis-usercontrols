/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { BasePanelHolder } from "../../Primitives/BasePanelHolder.js";

export class HorizontalPanelHolderTest extends ViewerTestRunner<BasePanelHolder> {
    //
    // public __IgnoretestStyleClassnameAPI() : void {
    //     this.assertEquals(this.getInstance().StyleClassName(), "TestCss");
    //     this.getInstance().StyleClassName("NewClassName");
    //     this.assertEquals(this.getInstance().StyleClassName(), "NewClassName");
    // }
    //
    // public __IgnoretestTitleAPI() : void {
    //     this.assertEquals(this.getInstance().Title().Text(), "Horizontal Panel");
    // }
    //
    // public __IgnoretestWidthAPI() : void {
    //     this.assertEquals(this.getInstance().Width(), 400);
    //     this.getInstance().Width(300);
    //     this.assertEquals(this.getInstance().Width(), 300);
    // }
    //
    // public __IgnoretestHeightAPI() : void {
    //     this.assertEquals(this.getInstance().Height(), 777);
    //     this.getInstance().Height(700);
    //     this.assertEquals(this.getInstance().Height(), 700);
    // }
    //
    // public __IgnoretestEnabledAPI() : void {
    //     this.assertEquals(this.getInstance().Enabled(), true);
    //     this.getInstance().Enabled(false);
    //     this.assertEquals(this.getInstance().Enabled(), false);
    // }
    //
    // public __IgnoretestVisibleAPI() : void {
    //     this.assertEquals(this.getInstance().Visible(), true);
    //     this.getInstance().Visible(false);
    //     this.assertEquals(this.getInstance().Visible(), false);
    // }
    //
    // public __IgnoretestFocusStateAPI() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const focusHandler : any = ($eventArgs : EventArgs) : void => {
    //             this.getEventsManager().FireAsynchronousMethod(() : void => {
    //                 this.assertEquals($eventArgs.Owner(), this.getInstance());
    //                 // this.testStyleClassnameAPI();
    //                 this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), null);
    //                 this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
    //                 $done();
    //             }, 500);
    //         };
    //         this.getInstance().getEvents().setOnFocus(focusHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
    //     };
    // }
    //
    // public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         this.getInstance().Enabled(false);
    //         const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
    //             this.getEventsManager().FireAsynchronousMethod(() : void => {
    //                 // this.testStyleClassnameAPI();
    //                 this.assertEquals(this.getInstance().Enabled(), false);
    //                 this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), null);
    //                 this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
    //                 $done();
    //             }, 500);
    //         };
    //         this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
    //     };
    // }
    //
    // public __IgnoretestEventClickME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const clickHandler : any = ($eventArgs : EventArgs) : void => {
    //             this.assertEquals($eventArgs.Owner(), this.getInstance());
    //             this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnClick(clickHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
    //     };
    // }
    //
    // public __IgnoretestEventMoveME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
    //         const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
    //             this.assertEquals(ElementManager.IsVisible(this.getInstance().Title()), true);
    //             this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), null);
    //             this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
    //             this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
    //     };
    // }
    //
    // public __IgnoretestEventDownME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
    //             this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
    //     };
    // }
    //
    // public __IgnoretestEventUpME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
    //             this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), null);
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
    //     };
    // }
    //
    // public __IgnoretestEventOutME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
    //     };
    // }
    //
    // public __IgnoretestEventOverME() : IViewerTestPromise {
    //     return ($done : () => void) : void => {
    //         const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
    //         const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
    //             this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
    //             this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
    //             this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
    //             $done();
    //         };
    //         this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
    //         this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
    //     };
    // }
    //
    // protected setUp() : void {
    //     const instance : HorizontalPanelHolder = this.getInstance();
    //     instance.Enabled(true);
    //     instance.Visible(true);
    //     instance.StyleClassName("TestCss");
    //     instance.Title().Text("Horizontal Panel");
    //     instance.getGuiOptions().Add(GuiOptionType.DISABLE);
    //     (<any>this).owner.normalImplementation();
    // }
    //
    protected before() : string {
        const instance : BasePanelHolder = this.getInstance();
        this.setUp();
        (<any>this).owner.normalImplementation();

        this.addButton("height : 200", () : void => {
            instance.Height(200);
        });

        this.addButton("height : 400", () : void => {
            instance.Height(400);
        });

        this.addButton("height : 600", () : void => {
            instance.Height(600);
        });

        return "<style>.TestCss {position: relative; top: 20px;}</style>" +
            "<div style=\"clear: both; height: 100px;\"></div>";
    }

    // protected after() : void {
    //     this.setUp();
    // }
}
/* dev:end */
