/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { DropDownListType } from "../../BaseInterface/Enums/UserControls/DropDownListType.js";
import { IconType } from "../../BaseInterface/Enums/UserControls/IconType.js";
import { DropDownList } from "../../BaseInterface/UserControls/DropDownList.js";

export class DropDownListTest extends ViewerTestRunner<DropDownList> {
    private instance2 : DropDownList;

    constructor() {
        super();
        this.setMethodFilter("ignoreAll");
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.getInstance().GuiType(), DropDownListType.GENERAL);
        this.getInstance().GuiType(DropDownListType.GREEN);
        this.assertEquals(this.getInstance().GuiType(), DropDownListType.GREEN);
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Width(), 300);
        this.getInstance().Width(150);
        this.assertEquals(this.getInstance().Width(), 150);
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
        this.getInstance().StyleClassName("DropDownList");
        this.assertEquals(this.getInstance().StyleClassName(), "DropDownList");
    }

    public testHintAPI() : void {
        this.assertEquals(this.getInstance().Hint(), "Write some text here ...");
    }

    public testGuiOptions() : void {
        this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.ACTIVED), false);
    }

    public testMaxVisibleItem() : void {
        this.assertEquals(this.getInstance().MaxVisibleItemsCount(), 3);
    }

    public testEnableAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
        this.testWidthAPI();
        this.testGuiTypeAPI();
        this.assertDeepEqual(this.getInstance().getItemsCount(), 6);
        // this.getInstance().Add("Add the next Item");
        // this.assertDeepEqual(this.getInstance().getItemsCount(), 7);
        this.testStyleClassNameAPI();
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
        this.assertEquals(this.getInstance().Enabled(), true);
        this.testGuiTypeAPI();
        this.testWidthAPI();
        this.testStyleClassNameAPI();
    }

    public testFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.testWidthAPI();
                    this.testGuiTypeAPI();
                    this.testStyleClassNameAPI();
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                    list.getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 500);
            };
            list.getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testOpen() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                ElementManager.getElement(this.getInstance().Id() + "_Field").click();

                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    ElementManager.getElement(this.instance2.Id() + "_Field").click();

                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        document.body.click();
                        $done();
                    }, 600);
                }, 300);
            }, 100);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), list);
                this.assertEquals(list.getEvents().Exists(EventType.ON_CLICK), true);
                list.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            list.getEvents().setOnClick(clickHandler);
            this.emulateEvent(list, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                list.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            list.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(list, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                list.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            list.getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(list, EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                this.assertEquals(list.getEvents().Exists(EventType.ON_MOUSE_UP), true);
                list.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            list.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(list, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                list.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            list.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(list, EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const list : DropDownList = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                list.getEvents().setOnMouseOver(mouseoverHandler);
                $done();
            };
            list.getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(list, EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const instance : DropDownList = this.getInstance();
        instance.Visible(true);
        instance.Enabled(true);
        instance.GuiType(DropDownListType.GENERAL);
        instance.StyleClassName("testCssClass");
        instance.Width(300);
        instance.getGuiOptions().Add(GuiOptionType.DISABLE);
        // instance.Value("test value");
        instance.Hint("Write some text here ...");
    }

    protected before() : string {
        const instance : DropDownList = this.getInstance();
        // instance.Title().Text("Text filed tooltip text");
        this.setUp();

        if (!instance.InstanceOwner().IsCached()) {
            // instance.Width(150);
            instance.Width(300);
        }
        // instance.Height(500);
        // instance.Enabled(false);
        instance.MaxVisibleItemsCount(3);
        // instance.Clear();
        instance.Add("18", 1, IconType.BLUE_SQUARE);
        instance.Add("35", 2, "withSeparator");
        instance.Add("test4", 3);
        instance.Add("test5", 4);
        instance.Add("test7 with long visible text", "string value 2");
        instance.Add("test6", "string value");

        instance.ShowField(false);
        instance.ShowButton(false);

        instance.Select(3);
        instance.Select("test6");

        this.addButton("Disable", () : void => {
            instance.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            instance.Enabled(true);
        });
        this.addButton("Error", () : void => {
            instance.Error(true);
        });
        this.addButton("Normal", () : void => {
            instance.Error(false);
        });
        this.addButton("General", () : void => {
            instance.GuiType(DropDownListType.GENERAL);
        });
        this.addButton("Red", () : void => {
            instance.GuiType(DropDownListType.RED);
        });
        this.addButton("Green", () : void => {
            instance.GuiType(DropDownListType.GREEN);
        });
        this.addButton("Blue", () : void => {
            instance.GuiType(DropDownListType.BLUE);
        });
        this.addButton("Add item", () : void => {
            instance.Add("new text list item");
        });
        this.addButton("Hide button", () : void => {
            instance.ShowButton(false);
            instance.ShowField(true);
        });
        this.addButton("Hide field", () : void => {
            instance.ShowField(false);
            instance.ShowButton(true);
        });
        this.addButton("Set size", () : void => {
            instance.Width(500);
        });
        this.addButton("Select item 2", () : void => {
            instance.Select("35");
        });

        this.instance2 = new DropDownList(DropDownListType.GENERAL, "testDropDownList2");
        this.instance2.StyleClassName("testCssClass2");
        this.instance2.Width(300);
        this.instance2.MaxVisibleItemsCount(3);
        // this.instance2.Clear();
        this.instance2.Add("test 1");
        this.instance2.Add("test 2");
        this.instance2.Add("test 3");
        this.instance2.Add("test 4");

        this.instance2.Select(0);
        // this.instance2.ShowField(false);
        this.instance2.ShowButton(false);

        return "<style>" +
            ".testCssClass {position: relative; top: 50px; left: 200px; float: left;} " +
            ".withSeparator .Separator {border-bottom: 1px solid red;} " +
            ".testCssClass2 {position: relative; top: 50px; left: 210px; float: left;}" +
            "</style>" +
            this.instance2.Draw();
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
