/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ButtonType } from "../../BaseInterface/Enums/UserControls/ButtonType.js";
import { IconType } from "../../BaseInterface/Enums/UserControls/IconType.js";
import { Button } from "../../BaseInterface/UserControls/Button.js";

export class ButtonTest extends ViewerTestRunner<Button> {

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
        this.getInstance().StyleClassName("newCssClass");
        this.assertEquals(this.getInstance().StyleClassName(), "newCssClass");
    }

    public testTextAPI() : void {
        this.assertEquals(this.getInstance().Text(), "Test");
        this.getInstance().Text("Button");
        this.assertEquals(this.getInstance().Text(), "Button");
    }

    public testWidthAPI() : void {
        // this.assertEquals(this.getInstance().Width(), 56);
        this.getInstance().Width(100);
        this.assertEquals(this.getInstance().Width(), 100);
    }

    public testGuiTypeAPI() : void {
        this.assertEquals(this.getInstance().GuiType(), ButtonType.GENERAL);
        this.getInstance().GuiType(ButtonType.RED);
        this.assertEquals(this.getInstance().GuiType(), ButtonType.RED);
    }

    public testDisableAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
        this.testWidthAPI();
        this.testGuiTypeAPI();
    }

    public testErrorAPI() : void {
        this.assertEquals(this.getInstance().Error(), false);
        this.getInstance().Error(true);
        this.assertEquals(this.getInstance().Error(), true);
        this.testTextAPI();
        this.testGuiTypeAPI();
        this.testStyleClassNameAPI();
    }

    public testHide() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testSelect() : void {
        this.assertEquals(this.getInstance().IsSelected(), false);
        this.getInstance().IsSelected(true);
        this.assertEquals(this.getInstance().IsSelected(), true);
        this.testWidthAPI();
        this.testGuiTypeAPI();
    }

    public __IgnoretestFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), button);
                    this.testErrorAPI();
                    this.testWidthAPI();
                    this.assertEquals(ElementManager.getClassName(button.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                    button.getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 500);
            };
            button.getEvents().setOnFocus(focusHandler);
            this.emulateEvent(button, EventType.ON_FOCUS);
        };
    }

    public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            button.Enabled(false);
            const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(button.GuiType(), ButtonType.GENERAL);
                    this.assertEquals(ElementManager.getClassName(button.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                    button.getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                    $done();
                }, 500);
            };
            button.getEvents().setOnFocus(focusDisableApiHandler);
            this.emulateEvent(button, EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), button);
                button.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            button.getEvents().setOnClick(clickHandler);
            this.emulateEvent(button, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(button.Title()), true);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            button.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ACTIVE);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            button.getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            button.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.OFF);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            button.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : Button = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Enabled"), GeneralCssNames.ON);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            button.getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const instance : Button = this.getInstance();
        instance.Visible(true);
        instance.Error(false);
        instance.Enabled(true);
        instance.GuiType(ButtonType.GENERAL);
        instance.Text("Test");
        instance.Title().Text("Button tooltip text");
        instance.IconName(IconType.BLACK_SQUARE);
        instance.StyleClassName("testCssClass");
        instance.getGuiOptions().Add(GuiOptionType.DISABLE);
        instance.getGuiOptions().Add(GuiOptionType.ACTIVED);
        instance.Width(-1);
        instance.IsSelected(false);
    }

    protected before() : string {
        const instance : Button = this.getInstance();
        let clickCounter : number = 0;
        instance.getEvents().setOnClick(() : void => {
            clickCounter++;
            Echo.Printf("button has been clicked " + clickCounter + " times");
        });
        this.setUp();
        instance.GuiType(ButtonType.GREEN);
        instance.Width(250);

        this.addButton("Disable", () : void => {
            instance.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            instance.Enabled(true);
        });
        this.addButton("Error", () : void => {
            instance.Error(true);
        });
        this.addButton("Selected", () : void => {
            instance.IsSelected(true);
        });
        this.addButton("Normal", () : void => {
            instance.IsSelected(false);
            instance.Error(false);
        });
        this.addButton("General", () : void => {
            instance.GuiType(ButtonType.GENERAL);
        });
        this.addButton("Red", () : void => {
            instance.GuiType(ButtonType.RED);
        });
        this.addButton("Blue", () : void => {
            instance.GuiType(ButtonType.BLUE);
        });
        this.addButton("Green", () : void => {
            instance.GuiType(ButtonType.GREEN);
        });
        this.addButton("Set text", () : void => {
            instance.Text("new button text");
        });
        this.addButton("Width 300", () : void => {
            instance.Width(300);
        });
        this.addButton("Width 50", () : void => {
            instance.Width(50);
        });
        this.addButton("Reset Width", () : void => {
            instance.Width(-1);
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
