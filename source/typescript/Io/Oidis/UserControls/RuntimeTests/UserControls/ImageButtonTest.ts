/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { IconType } from "../../BaseInterface/Enums/UserControls/IconType.js";
import { ImageButtonType } from "../../BaseInterface/Enums/UserControls/ImageButtonType.js";
import { ImageButton } from "../../BaseInterface/UserControls/ImageButton.js";

export class ImageButtonTest extends ViewerTestRunner<ImageButton> {

    public testIconNameAPI() : void {
        this.assertEquals(this.getInstance().IconName(), IconType.BLACK_SQUARE);
    }

    public testgetGuiOptionsAPI() : void {
        this.assertEquals(this.getInstance().getGuiOptions().Contains(GuiOptionType.ACTIVED), true);
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
    }

    public testPreventScrollAPI() : void {
        this.assertEquals(this.getInstance().IsPreventingScroll(), true);
    }

    public testErrorAPI() : void {
        this.assertEquals(this.getInstance().Error(), false);
        this.getInstance().Error(true);
        this.assertEquals(this.getInstance().Error(), true);
    }

    public testSelectedAPI() : void {
        this.assertEquals(this.getInstance().IsSelected(), false);
        this.getInstance().IsSelected(true);
        this.assertEquals(this.getInstance().IsSelected(), true);
    }

    public testGuiTypeAPI() : void {
        this.getInstance().GuiType(ImageButtonType.GENERAL);
        this.assertEquals(this.getInstance().GuiType(), ImageButtonType.GENERAL);
        this.getInstance().GuiType(ImageButtonType.GREEN);
        this.assertEquals(this.getInstance().GuiType(), ImageButtonType.GREEN);
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public __IgnoretestFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.testGuiTypeAPI();
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public __IgnoretestFocusStateDisabledAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getInstance().Enabled(false);
            const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), GeneralCssNames.ACTIVE);
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                    $done();
                }, 500);
            };
            this.getInstance().getEvents().setOnFocus(focusDisableApiHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const imagebutton : ImageButton = this.getInstance();
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), GeneralCssNames.OFF);
                this.assertEquals($eventArgs.Owner(), imagebutton);
                imagebutton.getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            imagebutton.getEvents().setOnClick(clickHandler);
            this.emulateEvent(imagebutton, EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : ImageButton = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.IsVisible(button.Title()), true);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), GeneralCssNames.OFF);
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            button.getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : ImageButton = this.getInstance();
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.ACTIVE);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            button.getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : ImageButton = this.getInstance();
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Active"), GeneralCssNames.ON);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            button.getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : ImageButton = this.getInstance();
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.OFF);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            button.getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const button : ImageButton = this.getInstance();
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.assertEquals(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.ON);
                button.getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            button.getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(button, EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : ImageButton = this.getInstance();
        object.Visible(true);
        object.Enabled(true);
        object.Error(false);
        object.StyleClassName("testCssClass");
        object.GuiType(ImageButtonType.GENERAL);
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
        object.getGuiOptions().Add(GuiOptionType.ACTIVED);
    }

    protected before() : string {
        let clickCounter : number = 0;
        const object : ImageButton = this.getInstance();
        object.getEvents().setOnClick(() : void => {
            clickCounter++;
            Echo.Printf("button has been clicked " + clickCounter + " times");
        });
        this.setUp();
        object.Title().Text("Image button tooltip text");
        object.IconName(IconType.BLACK_SQUARE);

        object.GuiType(ImageButtonType.GREEN);

        this.addButton("Disable", () : void => {
            object.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            object.Enabled(true);
        });
        this.addButton("Error", () : void => {
            object.Error(true);
        });
        this.addButton("Selected", () : void => {
            object.IsSelected(true);
        });
        this.addButton("Normal", () : void => {
            object.IsSelected(false);
            object.Error(false);
        });
        this.addButton("General", () : void => {
            object.GuiType(ImageButtonType.GENERAL);
        });
        this.addButton("Red", () : void => {
            object.GuiType(ImageButtonType.RED);
        });
        this.addButton("Blue", () : void => {
            object.GuiType(ImageButtonType.BLUE);
        });
        this.addButton("Green", () : void => {
            object.GuiType(ImageButtonType.GREEN);
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
