/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ProgressBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ProgressBarEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ProgressBarType } from "../../BaseInterface/Enums/UserControls/ProgressBarType.js";
import { Label } from "../../BaseInterface/UserControls/Label.js";
import { ProgressBar } from "../../BaseInterface/UserControls/ProgressBar.js";
import { EventsManager } from "../../Events/EventsManager.js";

export class ProgressBarTest extends ViewerTestRunner<ProgressBar> {

    public testRangeStartAPI() : void {
        this.assertEquals(this.getInstance().RangeStart(), 0);
    }

    public testRangeEndAPI() : void {
        this.assertEquals(this.getInstance().RangeEnd(), 100);
    }

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Width(), 200);
    }

    public testGuiTypeAPI() : void {
        this.getInstance().GuiType(ProgressBarType.RED);
        this.assertEquals(this.getInstance().GuiType(), ProgressBarType.RED);
        this.getInstance().GuiType(ProgressBarType.GENERAL);
        this.assertEquals(this.getInstance().GuiType(), ProgressBarType.GENERAL);
    }

    public testValueAPI() : void {
        this.getInstance().RangeEnd(100);
        this.getInstance().RangeStart(0);
        this.getInstance().Value(50);
        this.assertEquals(this.getInstance().Value(), 50);
        this.getInstance().Value(30);
        this.assertEquals(this.getInstance().Value(), 30);
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
        this.testRangeEndAPI();
        this.testRangeStartAPI();
        this.getInstance().Value(100);
        this.assertEquals(this.getInstance().Value(), 30);
        this.testGuiTypeAPI();
        this.testWidthAPI();
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
        this.assertEquals(this.getInstance().Value(), 30);
        this.testGuiTypeAPI();
        this.testWidthAPI();
    }

    public testStyleClassName() : void {
        const progress : ProgressBar = this.getInstance();
        this.assertEquals(progress.StyleClassName(), "testCssClass");
    }

    public testgetGuiOptions() : void {
        const picker : ProgressBar = this.getInstance();
        this.assertEquals(picker.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
    }

    public testFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
                    $done();
                }, 500);
            };
            EventsManager.getInstanceSingleton().setEvent(this.getInstance(),  EventType.ON_FOCUS, focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testFocusStateDisabledAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            this.getInstance().Enabled(false);
            const focusDisableApiHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals(this.getInstance().GuiType(), ProgressBarType.GENERAL);
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "Disable");
                    this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusDisableApiHandler);
                    $done();
                }, 500);
            };
            EventsManager.getInstanceSingleton().setEvent(this.getInstance(), EventType.ON_FOCUS, focusDisableApiHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Pointer"), null);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : ProgressBar = this.getInstance();
        object.Visible(true);
        object.Enabled(true);
        object.GuiType(ProgressBarType.GENERAL);
        object.RangeStart(0);
        object.RangeEnd(100);
        object.Width(200);
        object.Title().Text("title test");
        object.StyleClassName("testCssClass");
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
    }

    protected before() : string {
        const object : ProgressBar = this.getInstance();
        this.setUp();
        // object.Value(50);
        this.addButton("Disable", () : void => {
            object.Enabled(false);
        });
        this.addButton("Enable", () : void => {
            object.Enabled(true);
        });
        this.addButton("set 0", () : void => {
            object.Value(0);
        });
        this.addButton("set 50", () : void => {
            object.Value(50);
        });
        this.addButton("set 100", () : void => {
            object.Value(100);
        });
        this.addButton("General", () : void => {
            object.GuiType(ProgressBarType.GENERAL);
        });
        this.addButton("Blue", () : void => {
            object.GuiType(ProgressBarType.BLUE);
        });
        this.addButton("Green", () : void => {
            object.GuiType(ProgressBarType.GREEN);
        });
        this.addButton("Red", () : void => {
            object.GuiType(ProgressBarType.RED);
        });
        this.addButton("Set size", () : void => {
            object.Width(400);
        });

        const label : Label = new Label("Progress 0 %", "progressShow");
        object.getEvents().setOnChange(($eventArgs : ProgressBarEventArgs) : void => {
            label.Text("Progress " + $eventArgs.Percentage() + " %");
        });

        return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>" +
            StringUtils.NewLine() +
            StringUtils.NewLine() +
            label.Draw() +
            StringUtils.NewLine();
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
