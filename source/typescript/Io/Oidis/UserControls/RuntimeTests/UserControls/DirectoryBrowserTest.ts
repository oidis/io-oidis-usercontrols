/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BuilderConnector } from "@io-oidis-commons/Io/Oidis/Commons/Connectors/BuilderConnector.js";
import { FileSystemItemType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/FileSystemItemType.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import {
    IFileSystemDirectoryProtocol,
    IFileSystemDriveProtocol, IFileSystemFileProtocol,
    IFileSystemItemProtocol
} from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IFileSystemItemProtocol.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { FileSystemFilter } from "@io-oidis-commons/Io/Oidis/Commons/Utils/FileSystemFilter.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { DirectoryBrowserEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/DirectoryBrowserEventArgs.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IViewerTestPromise, ViewerTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/ViewerTestRunner.js";
import { IDirectoryBrowser } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IDirectoryBrowser.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { DirectoryBrowser } from "../../BaseInterface/UserControls/DirectoryBrowser.js";

export class DirectoryBrowserTest extends ViewerTestRunner<DirectoryBrowser> {
    private fileSystemHandler : any;
    private map : any;

    public testWidthAPI() : void {
        this.assertEquals(this.getInstance().Width(), 500);
        this.getInstance().Width(200);
        this.assertEquals(this.getInstance().Width(), 200);
    }

    public testPathAPI() : void {
        this.assertEquals(this.getInstance().Path(), "C:/boot");
        this.getInstance().Path("C:/architekt");
        this.assertEquals(this.getInstance().Path(), "C:/architekt");
    }

    public testStyleClassNameAPI() : void {
        this.assertEquals(this.getInstance().StyleClassName(), "testCssClass");
        this.getInstance().StyleClassName("ChangeStyle");
        this.assertEquals(this.getInstance().StyleClassName(), "ChangeStyle");
    }

    public testEnabledAPI() : void {
        this.assertEquals(this.getInstance().Enabled(), true);
        this.getInstance().Enabled(false);
        this.assertEquals(this.getInstance().Enabled(), false);
        this.testWidthAPI();
        this.testPathAPI();
    }

    public testVisibleAPI() : void {
        this.assertEquals(this.getInstance().Visible(), true);
        this.getInstance().Visible(false);
        this.assertEquals(this.getInstance().Visible(), false);
    }

    public testFocusStateAPI() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const focusHandler : any = ($eventArgs : EventArgs) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.assertEquals($eventArgs.Owner(), this.getInstance());
                    this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                    this.testWidthAPI();
                    this.testPathAPI();
                    this.testStyleClassNameAPI();
                    $done();
                }, 500);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_FOCUS, focusHandler);
            };
            this.getInstance().getEvents().setOnFocus(focusHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_FOCUS);
        };
    }

    public testFilter() : void {
        const object : DirectoryBrowser = this.getInstance();
        this.assertEquals(object.Filter(FileSystemFilter.DIRECTORIES), (<any>FileSystemFilter).Create(
            FileSystemFilter.FILESYSTEM_GROUPS,
            FileSystemItemType.DRIVE,
            FileSystemItemType.DIRECTORY
        ));
    }

    public testOnCreateDirectoryRequest() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const cDrive : any = this.map[1].map[0].map;
            cDrive.forEach(($item : any) : void => {
                Echo.Printf($item.name);
            });
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
                const object : DirectoryBrowser = this.getInstance();
                object.Filter(FileSystemFilter.DIRECTORIES);
                object.Path("C:/testItem");
                eventArgs.Owner(object);
                eventArgs.Value("C:/testItem");
                this.fileSystemHandler.onCreateDirectoryRequest(eventArgs);
            }, 600);
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                const lastItem : any = cDrive[cDrive.length - 1];
                Echo.Printf(lastItem.name);

                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    cDrive.forEach(($item : any) : void => {
                        Echo.Printf($item.name);
                    });
                    $done();
                }, 600);
            }, 600);
        };
    }

    public testOnRenameRequest() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const cDrive : any = this.map[1].map[0].map;
            cDrive.forEach(($item : any) : void => {
                Echo.Printf($item.name);
            });
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
                    const object : DirectoryBrowser = this.getInstance();
                    object.Path("C:/testItem");
                    eventArgs.Owner(object);
                    eventArgs.Value(object.Path() + "_Renamed");
                    this.fileSystemHandler.onRenameRequest(eventArgs);
                }, 600);
                const lastItem : any = cDrive[cDrive.length - 1];
                Echo.Printf(lastItem.name);
                $done();
            }, 600);
            cDrive.forEach(($item : any) : void => {
                Echo.Printf($item.name);
            });
        };
    }

    public testOnDeleteRequest() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            const object : DirectoryBrowser = this.getInstance();
            const cDrive : any = this.map[1].map[0].map;
            cDrive.forEach(($item : any) : void => {
                Echo.Printf($item.name);
            });
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    object.Path("C:/testItem");
                    eventArgs.Owner(object);
                    eventArgs.Value(object.Path());
                    this.assertEquals(eventArgs.Owner(), object);
                    this.assertEquals(eventArgs.Value(), object.Path());
                    this.fileSystemHandler.onDeleteRequest(eventArgs);
                    $done();
                }, 600);
                const lastItem : any = cDrive[cDrive.length - 1];
                Echo.Printf(lastItem.name);
            }, 600);
            cDrive.forEach(($item : any) : void => {
                Echo.Printf($item.name);
            });
        };
    }

    public testEventClickME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const clickHandler : any = ($eventArgs : EventArgs) : void => {
                this.assertEquals($eventArgs.Owner(), this.getInstance());
                this.getInstance().getEvents().RemoveHandler(EventType.ON_CLICK, clickHandler);
                $done();
            };
            this.getInstance().getEvents().setOnClick(clickHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_CLICK);
        };
    }

    public testEventMoveME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mousemoveHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_MOVE, mousemoveHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseMove(mousemoveHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_MOVE, event);
        };
    }

    public testEventDownME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mousedownHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_DOWN, mousedownHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseDown(mousedownHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_DOWN);
        };
    }

    public testEventUpME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseupHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals(ElementManager.getClassName(this.getInstance().Id() + "_Status"), "");
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_UP, mouseupHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseUp(mouseupHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_UP);
        };
    }

    public testEventOutME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const mouseoutHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OUT, mouseoutHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOut(mouseoutHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OUT);
        };
    }

    public testEventOverME() : IViewerTestPromise {
        return ($done : () => void) : void => {
            const event : any = {button: 2, clientX: 50, clientY: 10, pageX: 50, pageY: 10};
            const mouseoverHandler : any = ($eventArgs : MouseEventArgs) : void => {
                this.assertEquals($eventArgs.NativeEventArgs().clientX, 50);
                this.assertEquals($eventArgs.NativeEventArgs().clientY, 10);
                this.getInstance().getEvents().RemoveHandler(EventType.ON_MOUSE_OVER, mouseoverHandler);
                $done();
            };
            this.getInstance().getEvents().setOnMouseOver(mouseoverHandler);
            this.emulateEvent(this.getInstance(), EventType.ON_MOUSE_OVER, event);
        };
    }

    protected setUp() : void {
        const object : DirectoryBrowser = this.getInstance();
        object.Visible(true);
        object.Enabled(true);
        object.StyleClassName("testCssClass");
        object.Width(500);
        object.Height(350);
        object.Path("C:/boot");
        object.getGuiOptions().Add(GuiOptionType.DISABLE);
    }

    protected before() : void {
        const object : DirectoryBrowser = this.getInstance();
        this.map = <any>[
            {
                name: "Favorites",
                type: FileSystemItemType.FAVORITES,
                map : <IFileSystemItemProtocol[]>[
                    {
                        name      : "Desktop",
                        type      : FileSystemItemType.DIRECTORY,
                        value     : "C:/Users/Public/Desktop",
                        attributes: [],
                        map       : []
                    },
                    {
                        name      : "Downloads",
                        type      : FileSystemItemType.DIRECTORY,
                        value     : "C:/Users/Public/Downloads",
                        attributes: [],
                        map       : []
                    }
                ]
            },
            {
                name: "Computer",
                type: FileSystemItemType.MY_COMPUTER,
                map : <IFileSystemDriveProtocol[]>[
                    {
                        name : "Primary",
                        value: "C:",
                        type : FileSystemItemType.DRIVE,
                        map  : [
                            <IFileSystemDirectoryProtocol>{
                                name      : "boot",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : [
                                    <IFileSystemDirectoryProtocol>{
                                        name      : "macrium",
                                        type      : FileSystemItemType.DIRECTORY,
                                        attributes: [],
                                        map       : []
                                    }
                                ]
                            },
                            <IFileSystemDirectoryProtocol>{
                                name      : "cygwin",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : <IFileSystemDirectoryProtocol[]>[]
                            },
                            <IFileSystemDirectoryProtocol>{
                                name      : "cygwin64",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : <IFileSystemDirectoryProtocol[]>[
                                    {
                                        name      : "Cyrrus logic",
                                        type      : FileSystemItemType.DIRECTORY,
                                        attributes: [],
                                        map       : <IFileSystemDirectoryProtocol[]>[]
                                    }
                                ]
                            },
                            <IFileSystemDirectoryProtocol>{
                                name      : "Drivers",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : <IFileSystemDirectoryProtocol[]>[]
                            },
                            <IFileSystemDirectoryProtocol>{
                                name      : "Driver Cabal",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : <IFileSystemDirectoryProtocol[]>[
                                    {
                                        name      : "Driver Cabal and his dauther",
                                        type      : FileSystemItemType.DIRECTORY,
                                        attributes: [],
                                        map       : <IFileSystemDirectoryProtocol[]>[
                                            {
                                                name      : "Very long text with very short dog, it sleep well on the grass ground " +
                                                    "and Very long text with very short dog, it sleep well on the grass ground",
                                                type      : FileSystemItemType.DIRECTORY,
                                                attributes: [],
                                                map       : <IFileSystemDirectoryProtocol[]>[]
                                            }
                                        ]
                                    }
                                ]
                            },
                            <IFileSystemDirectoryProtocol>{
                                name      : "InstallAnywhere",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [FileSystemItemType.HIDDEN],
                                map       : []
                            },
                            {
                                name      : "Xaver Daniel",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : "***"
                            },
                            <IFileSystemFileProtocol>{
                                name      : ".rnd",
                                type      : FileSystemItemType.FILE,
                                attributes: []
                            },
                            <IFileSystemFileProtocol>{
                                name      : "HaxLogs.txt",
                                type      : FileSystemItemType.FILE,
                                attributes: []
                            }
                        ]
                    },
                    {
                        name : "WORK",
                        value: "D:",
                        type : FileSystemItemType.DRIVE,
                        map  : <IFileSystemDirectoryProtocol[]>[
                            {
                                name      : "__DOKUMENTY__",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : []
                            },
                            {
                                name      : "__PROJECTS__",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : <IFileSystemDirectoryProtocol[]>[
                                    {
                                        name      : "Freescale",
                                        type      : FileSystemItemType.DIRECTORY,
                                        attributes: [],
                                        map       : []
                                    }
                                ]
                            },
                            {
                                name      : "__TEMP__",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : []
                            },
                            {
                                name      : "Android CM",
                                type      : FileSystemItemType.DIRECTORY,
                                attributes: [],
                                map       : []
                            }
                        ]
                    }
                ]
            },
            {
                name: "Network",
                type: FileSystemItemType.NETWORK,
                map : []
            }
        ];
        object.setStructure(this.map);
        // object.Filter(FileSystemFilter.HIDDEN_ALL, "*.log");
        this.setUp();

        object.getEvents().setOnChange(() : void => {
            Echo.Printf("selected: \"{0}\"", StringUtils.Replace(object.Path(), "/", "\\"));
        });

        const getItem : ($path : string) => IFileSystemItemProtocol[] = ($path : string) : IFileSystemItemProtocol[] => {
            let selectedMap : IFileSystemItemProtocol[] = [];
            const dataLookup : ($structure : IFileSystemItemProtocol[], $structurePath : string) => void =
                ($structure : IFileSystemItemProtocol[], $structurePath : string) : void => {
                    if (ObjectValidator.IsNativeArray($structure) && ObjectValidator.IsEmptyOrNull(selectedMap)) {
                        $structure.forEach(($item : IFileSystemItemProtocol) : boolean => {
                            if (ObjectValidator.IsEmptyOrNull(selectedMap)) {
                                let path : string = $structurePath + "/" + $item.name;
                                if ($item.type === FileSystemItemType.DRIVE) {
                                    path = $item.value;
                                }
                                if (($item.type === FileSystemItemType.DRIVE ? path + "/" : path) === $path) {
                                    selectedMap = <IFileSystemItemProtocol[]>$item.map;
                                } else if (ObjectValidator.IsEmptyOrNull(selectedMap) && !ObjectValidator.IsEmptyOrNull($item.map)) {
                                    dataLookup(<IFileSystemItemProtocol[]>$item.map, path);
                                }
                                return ObjectValidator.IsEmptyOrNull(selectedMap);
                            }
                            return false;
                        });
                    }
                };
            dataLookup(this.map, $path);
            return selectedMap;
        };

        this.fileSystemHandler = {
            onCreateDirectoryRequest($eventArgs : DirectoryBrowserEventArgs) : void {
                const selectedMap : IFileSystemItemProtocol[] = getItem($eventArgs.Value());
                let name : string = "New Folder";
                let index : number;
                let counter : number = 2;
                for (index = 0; index < selectedMap.length; index++) {
                    if (selectedMap[index].name === name) {
                        name = "New Folder (" + counter + ")";
                        counter++;
                        index = 0;
                    }
                }
                selectedMap.push(<any>{
                    name,
                    type      : FileSystemItemType.DIRECTORY,
                    attributes: [],
                    map       : []
                });
                $eventArgs.Owner().setStructure(selectedMap, $eventArgs.Value());
            },
            onDeleteRequest($eventArgs : DirectoryBrowserEventArgs) : void {
                const directory : string = StringUtils.Substring(
                    $eventArgs.Value(), 0, StringUtils.IndexOf($eventArgs.Value(), "/", false));
                const item : string = StringUtils.Remove($eventArgs.Value(), directory + "/");
                const selectedMap : IFileSystemItemProtocol[] = getItem(directory);
                const newMap : IFileSystemItemProtocol[] = [];
                selectedMap.forEach(($item : IFileSystemItemProtocol) : void => {
                    if ($item.name !== item) {
                        newMap.push($item);
                    }
                });
                $eventArgs.Owner().setStructure(newMap, directory);
                $eventArgs.Owner().Path(directory);
            },
            onDirectoryRequest($eventArgs : DirectoryBrowserEventArgs) : void {
                const path : string = $eventArgs.Value();
                if (path === "C:/cygwin") {
                    $eventArgs.Owner().setStructure(<any>[
                        <IFileSystemDirectoryProtocol>{
                            name      : "var",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : [
                                <IFileSystemDirectoryProtocol>{
                                    name      : "log",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemFileProtocol[]>[
                                        {
                                            name      : "setup.log",
                                            type      : FileSystemItemType.FILE,
                                            attributes: []
                                        },
                                        {
                                            name      : "setup.log.full",
                                            type      : FileSystemItemType.FILE,
                                            attributes: []
                                        }
                                    ]
                                }
                            ]
                        }
                    ], path);
                } else if (path === "D:/__DOKUMENTY__") {
                    $eventArgs.Owner().setStructure(<any>[
                        <IFileSystemDirectoryProtocol>{
                            name      : "Reports",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : []
                        },
                        <IFileSystemFileProtocol>{
                            name      : "FTF 2014.pod.xml",
                            type      : FileSystemItemType.FILE,
                            attributes: []
                        },
                        <IFileSystemFileProtocol>{
                            name      : "MCAT",
                            type      : FileSystemItemType.LINK,
                            attributes: []
                        }
                    ], path);
                } else if (path === "C:/Xaver Daniel") {
                    $eventArgs.Owner().setStructure(<any>[
                        <IFileSystemDirectoryProtocol>{
                            name      : "Is our hero",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : "***"
                        }
                    ], path);
                } else if (path === "C:/Xaver Daniel/Is our hero") {
                    $eventArgs.Owner().setStructure(<any>[
                        <IFileSystemFileProtocol>{
                            name      : "But nothing else.txt",
                            type      : FileSystemItemType.FILE,
                            attributes: []
                        }
                    ], path);
                } else {
                    $eventArgs.Owner().setStructure([], "unknown");
                }
            },
            onPathRequest($eventArgs : DirectoryBrowserEventArgs) : void {
                $eventArgs.Owner().setStructure([], "unknown");
            },
            onRenameRequest($eventArgs : DirectoryBrowserEventArgs) : void {
                const directory : string = StringUtils.Substring($eventArgs.Owner().Value(),
                    0, StringUtils.IndexOf($eventArgs.Owner().Value(), "/", false));
                const oldValue : string = StringUtils.Remove($eventArgs.Owner().Value(), directory + "/");
                const selectedMap : IFileSystemItemProtocol[] = getItem(directory);
                let name : string = $eventArgs.Value();
                let extension : string = "";
                if (StringUtils.Contains(name, ".")) {
                    extension = StringUtils.Substring(name, StringUtils.IndexOf(name, ".", false) - 1);
                    name = StringUtils.Substring(name, 0, StringUtils.IndexOf(name, extension, false));
                }
                let index : number;
                let counter : number = 2;
                let newName : string = name + extension;
                for (index = 0; index < selectedMap.length; index++) {
                    if (selectedMap[index].name === newName) {
                        newName = name + " (" + counter + ")" + extension;
                        counter++;
                        index = 0;
                    }
                }
                selectedMap.forEach(($item : IFileSystemItemProtocol) : void => {
                    if ($item.name === oldValue) {
                        $item.name = newName;
                    }
                });
                $eventArgs.Owner().setStructure(selectedMap, directory);
            }
        };

        object.getEvents().setOnDirectoryRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
            this.fileSystemHandler.onDirectoryRequest($eventArgs);
        });
        object.getEvents().setOnPathRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
            this.fileSystemHandler.onPathRequest($eventArgs);
        });
        object.getEvents().setOnDeleteRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
            this.fileSystemHandler.onDeleteRequest($eventArgs);
        });
        object.getEvents().setOnCreateDirectoryRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
            this.fileSystemHandler.onCreateDirectoryRequest($eventArgs);
        });
        object.getEvents().setOnRenameRequest(($eventArgs : DirectoryBrowserEventArgs) : void => {
            this.fileSystemHandler.onRenameRequest($eventArgs);
        });

        this.addButton("apply filter", () : void => {
            object.Filter(FileSystemFilter.DIRECTORIES);
        });

        this.addButton("Connect FS", () : void => {
            const connector : BuilderConnector = BuilderConnector.Connect();
            this.fileSystemHandler.onPathRequest = ($eventArgs : DirectoryBrowserEventArgs) : void => {
                connector.Send("FileSystem", {
                    type : "getPath",
                    value: $eventArgs.Value()
                }, ($data : any) : void => {
                    const structure : any = [
                        {
                            map : [],
                            name: "Recent",
                            type: FileSystemItemType.RECENT
                        }
                    ];
                    JSON.parse(ObjectDecoder.Base64($data.data)).forEach(($group : any) : void => {
                        $group.forEach(($item : any) : void => {
                            structure.push($item);
                        });
                    });
                    $eventArgs.Owner().setStructure(structure);
                });
            };

            const getDirectory : ($path : string, $owner : IDirectoryBrowser) => void =
                ($path : string, $owner : IDirectoryBrowser) : void => {
                    connector.Send("FileSystem", {
                        type : "getDirectory",
                        value: $path
                    }, ($data : any) : void => {

                        $data = JSON.parse(ObjectDecoder.Base64($data.data));
                        $owner.setStructure($data[0], $path);
                    });
                };

            this.fileSystemHandler.onDirectoryRequest = ($eventArgs : DirectoryBrowserEventArgs) : void => {
                getDirectory($eventArgs.Value(), $eventArgs.Owner());
            };

            this.fileSystemHandler.onCreateDirectoryRequest = ($eventArgs : DirectoryBrowserEventArgs) : void => {
                connector.Send("FileSystem", {
                    type : "CreateDirectory",
                    value: $eventArgs.Owner().Value() + "/New Folder"
                }, ($status : boolean) : void => {
                    if ($status) {
                        getDirectory($eventArgs.Owner().Value(), $eventArgs.Owner());
                    } else {
                        Echo.Printf("Unable to create directory: {0}", $eventArgs.Owner().Value());
                    }
                });
            };

            this.fileSystemHandler.onDeleteRequest = ($eventArgs : DirectoryBrowserEventArgs) : void => {
                const path : string = $eventArgs.Owner().Value();
                const directory : string = StringUtils.Substring(path, 0, StringUtils.IndexOf(path, "/", false));

                connector.Send("FileSystem", {
                    type : "Delete",
                    value: path
                }, ($status : boolean) : void => {
                    if ($status !== null) {
                        getDirectory(directory, $eventArgs.Owner());
                    } else {
                        Echo.Printf("Unable to delete directory: {0}", $eventArgs.Owner().Value());
                    }
                });
            };

            this.fileSystemHandler.onRenameRequest = ($eventArgs : DirectoryBrowserEventArgs) : void => {
                const oldValue : string = $eventArgs.Owner().Value();
                const newValue : string =
                    StringUtils.Substring(oldValue, 0, StringUtils.IndexOf(oldValue, "/", false)) + "/" + $eventArgs.Value();
                const directory : string = StringUtils.Substring(oldValue, 0, StringUtils.IndexOf(oldValue, "/", false));

                connector.Send("FileSystem", {
                    oldValue,
                    type : "Rename",
                    value: newValue
                }, ($status : boolean) : void => {
                    if ($status !== null) {
                        getDirectory(directory, $eventArgs.Owner());
                    } else {
                        Echo.Printf("Unable to rename: {0}", $eventArgs.Owner().Value());
                    }
                });
            };

            object.setStructure(<any>[
                {
                    map : [],
                    name: "Recent",
                    type: FileSystemItemType.RECENT
                }
            ]);
            object.Path("C:/");
        });

        this.addButton("Delete Selected", () : void => {
            const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            eventArgs.Owner(object);
            eventArgs.Value(object.Path());
            this.fileSystemHandler.onDeleteRequest(eventArgs);
        });

        this.addButton("Create Directory", () : void => {
            const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            eventArgs.Owner(object);
            eventArgs.Value(object.Path());
            this.fileSystemHandler.onCreateDirectoryRequest(eventArgs);
        });

        this.addButton("Rename Selected", () : void => {
            const eventArgs : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            eventArgs.Owner(object);
            eventArgs.Value(object.Path() + "_Renamed");
            this.fileSystemHandler.onRenameRequest(eventArgs);
        });
    }

    protected after() : void {
        this.setUp();
    }
}
/* dev:end */
