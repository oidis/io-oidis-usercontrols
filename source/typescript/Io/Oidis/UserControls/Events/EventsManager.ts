/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { EventsManager as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { DragBar } from "../BaseInterface/Components/DragBar.js";
import { ResizeBar } from "../BaseInterface/Components/ResizeBar.js";
import { ScrollBar } from "../BaseInterface/Components/ScrollBar.js";
import { NumberPicker } from "../BaseInterface/UserControls/NumberPicker.js";
import { TextArea } from "../BaseInterface/UserControls/TextArea.js";
import { TextField } from "../BaseInterface/UserControls/TextField.js";

/**
 * EventsManager class provides handling of all registered custom events.
 */
export class EventsManager extends Parent {

    protected isOnLoadKey($eventArgs : KeyboardEvent) : boolean {
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        return (super.isOnLoadKey($eventArgs) && !manager.IsActive(<IClassName>TextField) && !manager.IsActive(<IClassName>TextArea));
    }

    protected isElementMoveAllowed() : boolean {
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        return !(manager.IsActive(<IClassName>ScrollBar) ||
            manager.IsActive(<IClassName>DragBar) ||
            manager.IsActive(<IClassName>ResizeBar) ||
            manager.IsActive(<IClassName>NumberPicker));
    }
}
