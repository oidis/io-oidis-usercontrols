/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { BaseGuiGroupObjectArgs } from "@io-oidis-gui/Io/Oidis/Gui/Structures/BaseGuiGroupObjectArgs.js";

/**
 * BaseFormInputArgs is structure for handling of FormInput user control.
 */
export abstract class BaseFormInputArgs extends BaseGuiGroupObjectArgs {
    private name : string;
    private isRequired : boolean;
    private requiredContent : string;
    private hint : string;

    constructor() {
        super();

        this.name = "";
        this.isRequired = false;
        this.requiredContent = "*";
        this.hint = "";
    }

    /**
     * @param {string} [$value] Specify field name.
     * @returns {string} Returns field name.
     */
    public Name($value? : string) : string {
        return this.name = Property.String(this.name, $value);
    }

    /**
     * @param {boolean} [$value] Specify, if field should be marked as required.
     * @returns {boolean} Returns true, if field should be marked as required, otherwise false.
     */
    public IsRequired($value? : boolean) : boolean {
        return this.isRequired = Property.Boolean(this.isRequired, $value);
    }

    /**
     * @param {string} [$value] Specify value, which should be used as required mark.
     * @returns {string} Returns required mark value.
     */
    public RequiredContent($value? : string) : string {
        return this.requiredContent = Property.String(this.requiredContent, $value);
    }

    /**
     * @param {string} [$value] Specify value, which should be used as input hint.
     * @returns {string} Returns input hint value.
     */
    public Hint($value? : string) : string {
        return this.hint = Property.String(this.hint, $value);
    }
}
