/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

/**
 * AutocompleteOption class provides structure for autocomplete options.
 */
export class AutocompleteOption extends BaseObject {
    private readonly text : string;
    private readonly value : string | number;
    private readonly data : string;
    private readonly styleClassName : string;

    /**
     * @param {string} $text Specify value for visible option text.
     * @param {string|number} [$value] Specify value for selected option.
     * @param {string} [$lookupData] Specify value for option lookup.
     * @param {string} [$styleClassName] Specify option's css style class.
     */
    constructor($text : string, $value? : string | number, $lookupData? : string, $styleClassName? : string) {
        super();
        this.text = StringUtils.StripTags($text);
        if (ObjectValidator.IsEmptyOrNull($value)) {
            this.value = this.text;
        } else {
            this.value = $value;
        }
        if (ObjectValidator.IsEmptyOrNull($lookupData)) {
            this.data = this.text;
        } else {
            this.data = $lookupData;
        }
        if (ObjectValidator.IsEmptyOrNull($styleClassName)) {
            this.styleClassName = "";
        } else {
            this.styleClassName = $styleClassName;
        }
    }

    /**
     * @returns {string} Returns value, which should be used as visible option text.
     */
    public Text() : string {
        return this.text;
    }

    /**
     * @returns {string|number} Returns value, which should be used as selected option value.
     */
    public Value() : string | number {
        return this.value;
    }

    /**
     * @returns {string} Returns value, which should be used for option lookup.
     */
    public LookupData() : string {
        return this.data;
    }

    /**
     * @returns {string} Returns value, which should be used for option's css style.
     */
    public StyleClassName() : string {
        return this.styleClassName;
    }
}
