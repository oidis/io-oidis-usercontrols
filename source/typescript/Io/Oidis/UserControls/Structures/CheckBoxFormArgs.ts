/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseFormInputArgs } from "./BaseFormInputArgs.js";

/**
 * CheckBoxFormArgs is structure for handling of FormInput with CheckBox user control.
 */
export class CheckBoxFormArgs extends BaseFormInputArgs {

    constructor() {
        super();
        this.Value(false);
    }

    /**
     * @param {boolean} [$value] Specify check box value.
     * @returns {boolean} Returns check box value.
     */
    public Value($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value) && ObjectValidator.IsBoolean($value)) {
            super.Value($value);
        }
        return <boolean>super.Value();
    }
}
