/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { BaseFormInputArgs } from "./BaseFormInputArgs.js";

/**
 * DropDownListFormArgs is structure for handling of FormInput with DropDownList user control.
 */
export class DropDownListFormArgs extends BaseFormInputArgs {
    private readonly namesList : ArrayList<string>;
    private readonly valuesList : ArrayList<string | number>;
    private maxVisibleItemsCount : number;

    constructor() {
        super();

        this.namesList = new ArrayList<string>();
        this.valuesList = new ArrayList<string | number>();
        this.maxVisibleItemsCount = -1;
    }

    /**
     * @param {(string|number)} [$value] Specify text field value.
     * @returns {(string|number)} Returns text field value.
     */
    public Value($value? : string | number) : string | number {
        if (ObjectValidator.IsSet($value) && (ObjectValidator.IsString($value) || ObjectValidator.IsDigit($value))) {
            return <string | number>super.Value($value);
        }
        return <string | number>super.Value();
    }

    /**
     * @param {string} $text Specify DropDownList item's text value, which should be displayed.
     * @param {string|number} [$value] Specify DropDownList item's value, which should be passed as chosen value.
     * If not specified, it will be equal to the $text value.
     * @returns {void}
     */
    public AddItem($text : string, $value? : string | number) : void {
        if (!ObjectValidator.IsEmptyOrNull($text)) {
            this.namesList.Add($text);
            if (!ObjectValidator.IsSet($value)) {
                $value = $text;
            }
            this.valuesList.Add($value);
        }
    }

    /**
     * @returns {ArrayList<string>} Returns DropDownList item names list.
     */
    public getNames() : ArrayList<string> {
        return this.namesList;
    }

    /**
     * @returns {ArrayList<string|number>} Returns DropDownList item values list.
     */
    public getValues() : ArrayList<string | number> {
        return this.valuesList;
    }

    /**
     * Clear all items hold by the args instance.
     * @returns {void}
     */
    public Clear() : void {
        this.valuesList.Clear();
        this.namesList.Clear();
        this.Value("");
    }

    /**
     * @param {number} [$value] Specify maximal number of items, which can be shown in menu content.
     * @returns {number} Returns maximal number of visible items.
     */
    public MaxVisibleItemsCount($value? : number) : number {
        return this.maxVisibleItemsCount = Property.Integer(this.maxVisibleItemsCount, $value);
    }
}
