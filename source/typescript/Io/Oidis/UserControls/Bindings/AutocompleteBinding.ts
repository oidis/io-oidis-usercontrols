/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { bootstrap } from "@io-oidis-gui/Io/Oidis/Gui/Utils/EnvironmentHelper.js";
import { Button } from "../Bootstrap/UserControls/Button.js";
import { DropDownListItem } from "../Bootstrap/UserControls/DropDownList.js";
import { TextField } from "../Bootstrap/UserControls/TextField.js";

export class AutocompleteBinding extends BaseObject {

    public static setAutocomplete($instance : TextField, $value : boolean) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().autocomplete = $value ? "on" : "off";
        }
    }

    public Show($userControls : IAutocompleteUserControls, $value : boolean) : void {
        const autoCompleteUC : any = new bootstrap.Dropdown($userControls.dropdownSelector.InstanceOwner());
        if ($value) {
            autoCompleteUC.show();
            $userControls.field.InstanceOwner().focus();
        } else {
            autoCompleteUC.hide();
        }
    }

    public Bind($userControls : IAutocompleteUserControls, $onRequest : ($query : any) => Promise<IAutocompleteData[]>) : void {
        const autocompleteShow : any = ($value : boolean) : void => {
            this.Show($userControls, $value);
        };
        $userControls.field.getEvents().setOnBlur(() : void => {
            autocompleteShow(false);
        });
        $userControls.field.getEvents().setEvent(EventType.ON_KEY_UP, () : void => {
            $userControls.menu.Data(null);
            $userControls.field.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                const value : string = $userControls.field.Value();
                if (StringUtils.Length(value) >= 3) {
                    $userControls.menu.Enabled(false);
                    const regex : any = {$regex: ".*" + StringUtils.Replace(value, " ", ".*") + ".*", $options: "i"};
                    const data : IAutocompleteData[] = await $onRequest(regex);
                    $userControls.menu.Clear();
                    if (!ObjectValidator.IsEmptyOrNull(data)) {
                        data.forEach(($record : IAutocompleteData) : void => {
                            const item : DropDownListItem = new DropDownListItem();
                            item.Text($record.text.replace(new RegExp("(" + value + ")", "gi"), "<b>$1</b>"));
                            if (!ObjectValidator.IsEmptyOrNull($record.value)) {
                                item.Value($record.value);
                            } else {
                                item.Value($record.text);
                            }
                            $userControls.menu.AddChild(item);
                            item.getEvents().setOnClick(() : void => {
                                $userControls.menu.Data(item.Value());
                                $userControls.field.Value($record.text);
                                autocompleteShow(false);
                            });
                        });
                        autocompleteShow(true);
                    } else {
                        autocompleteShow(false);
                    }
                    $userControls.menu.Enabled(true);
                } else {
                    autocompleteShow(false);
                }
            }, true);
        });
    }
}

export interface IAutocompleteUserControls {
    field : TextField;
    dropdownSelector : Button;
    menu : GuiCommons;
}

export interface IAutocompleteData {
    text : string;
    value? : any;
}

// generated-code-start
export const IAutocompleteUserControls = globalThis.RegisterInterface(["field", "dropdownSelector", "menu"]);
export const IAutocompleteData = globalThis.RegisterInterface(["text", "value"]);
// generated-code-end
