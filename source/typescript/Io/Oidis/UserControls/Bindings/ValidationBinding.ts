/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/GuiCommons.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { KeyEventHandler } from "@io-oidis-gui/Io/Oidis/Gui/Utils/KeyEventHandler.js";
import { Button } from "../Bootstrap/UserControls/Button.js";
import { CheckBox } from "../Bootstrap/UserControls/CheckBox.js";
import { TextField } from "../Bootstrap/UserControls/TextField.js";

export class ValidationBinding extends BaseObject {

    public static IsValidDropDown($instance : Button, $value : boolean) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().classList.remove("btn-outline-success", "btn-outline-danger");
            $instance.InstanceOwner().classList.add($value ? "btn-outline-success" : "btn-outline-danger");
        }
    }

    /// TODO: move directly to UC implementation
    public static IsValidInput($instance : TextField | CheckBox, $value : boolean) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().classList.remove("is-invalid", "is-valid");
            $instance.InstanceOwner().classList.add($value ? "is-valid" : "is-invalid");
        }
    }

    public static RestoreValidMark($instance : GuiCommons) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().classList.remove("is-invalid", "is-valid", "btn-outline-success", "btn-outline-danger");
        }
    }

    public static RequiredFields($map : TextField[]) : boolean {
        let validated = true;
        $map.forEach(($control : TextField) : void => {
            ValidationBinding.IsValidInput($control, !ObjectValidator.IsEmptyOrNull($control.Value()));
            if (ObjectValidator.IsEmptyOrNull($control.Value())) {
                validated = false;
            }
        });
        return validated;
    }

    public static setOnlyQuerySafeAllowed($instance : TextField) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().oninput = () : void => {
                try {
                    const value : string = $instance.Value();
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        $instance.Value(value
                            .replace(/&amp;/g, "##AMP##")
                            .replace(/&lt;/g, "##LT##")
                            .replace(/&gt;/g, "##GT##")
                            .replace(/&/g, "&amp;")
                            .replace(/</g, "&lt;")
                            .replace(/>/g, "&gt;")
                            .replace(/##AMP##/g, "&amp;")
                            .replace(/##LT##/g, "&lt;")
                            .replace(/##GT##/g, "&gt;"));
                    }
                } catch (ex) {
                    LogIt.Warning("Failed to handle safe query.\n" + ex.stack);
                }
            };
        }
    }

    /// TODO: still needs refactoring and updates in KeyEventHandler for an ability to reflect "key" standard
    public static setOnlyDoubleAllowed($instance : TextField) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().setAttribute("inputmode", "number");
            $instance.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) && !KeyEventHandler.IsDouble($eventArgs.NativeEventArgs())) {
                    $eventArgs.PreventDefault();
                }
            });
        }
    }

    public static setOnlyNumbersAllowed($instance : TextField) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().setAttribute("inputmode", "number");
            $instance.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                this.handleNumberKeys($eventArgs, ["+", "."]);
            });
        }
    }

    public static setOnlyPhoneFormat($instance : TextField) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().setAttribute("inputmode", "tel");
            $instance.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                const key : string = $eventArgs.NativeEventArgs().key;
                if (key !== "+" &&
                    this.handleNumberKeys($eventArgs, []) &&
                    StringUtils.Length(StringUtils.Remove($instance.Value(), "+", ".")) >= 12) {
                    $eventArgs.PreventDefault();
                }
            });
        }
    }

    public static setOnlyZIPFormat($instance : TextField) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().setAttribute("inputmode", "number");
            $instance.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                if (this.handleNumberKeys($eventArgs, ["+", "."]) && StringUtils.Length($instance.Value()) >= 5) {
                    $eventArgs.PreventDefault();
                }
            });
        }
    }

    public static setOnlyLowercaseAllowed($instance : TextField, $onChange? : ($status : boolean) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($instance.InstanceOwner())) {
            $instance.InstanceOwner().style.textTransform = "lowercase";
            if (!ObjectValidator.IsEmptyOrNull($onChange)) {
                let capslockState : boolean = false;
                let shiftState : boolean = false;
                $instance.getEvents().setEvent(EventType.ON_KEY_UP, ($eventArgs : KeyEventArgs) : void => {
                    capslockState = $eventArgs.NativeEventArgs().getModifierState("CapsLock");
                    shiftState = $eventArgs.NativeEventArgs().getModifierState("Shift");
                    $onChange(capslockState || shiftState);
                });
                $instance.getEvents().setEvent(EventType.ON_KEY_DOWN, ($eventArgs : KeyEventArgs) : void => {
                    capslockState = $eventArgs.NativeEventArgs().getModifierState("CapsLock");
                    shiftState = $eventArgs.NativeEventArgs().getModifierState("Shift");
                    $onChange(capslockState || shiftState);
                });
            }
        }
    }

    protected static handleNumberKeys($eventArgs : KeyEventArgs, $notAllowed : string[]) : boolean {
        /// TODO: refactor to use key and validated against string array [".",","," ","+"...]
        //        https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_key_values

        const key : string = $eventArgs.NativeEventArgs().key;
        const notAllowed : string[] = ["-", ",", " "].concat($notAllowed);
        if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) &&
            !KeyEventHandler.IsInteger($eventArgs.NativeEventArgs()) ||
            notAllowed.includes(key)) {
            $eventArgs.PreventDefault();
            return false;
        }
        return true;
    }
}
