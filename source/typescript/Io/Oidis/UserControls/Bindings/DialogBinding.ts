/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { Button } from "../Bootstrap/UserControls/Button.js";
import { Dialog } from "../Bootstrap/UserControls/Dialog.js";

export class DialogBinding extends BaseObject {
    private static instances : any = {};

    public Bind($userControls : IDialogUserControls) : void {
        if (!ObjectValidator.IsArray($userControls.close)) {
            $userControls.close = [<Button>$userControls.close];
        }
        if (!ObjectValidator.IsArray($userControls.submit)) {
            $userControls.submit = [<Button>$userControls.submit];
        }
        (<Button[]>$userControls.close).forEach(($button : Button) : void => {
            $button.getEvents().setOnClick(() : void => {
                $userControls.holder.Visible(false);
            });
        });
        if (!DialogBinding.instances.hasOwnProperty($userControls.holder.Id())) {
            DialogBinding.instances[$userControls.holder.Id()] = $userControls;
        }
        WindowManager.getEvents().setOnKeyDown(($events : KeyEventArgs) : void => {
            for (const id in DialogBinding.instances) {
                if (DialogBinding.instances.hasOwnProperty(id)) {
                    const userControls : IDialogUserControls = DialogBinding.instances[id];
                    if (userControls.holder.Visible()) {
                        if ($events.getKeyCode() === KeyMap.ESC) {
                            (<Button[]>userControls.close)[0].InstanceOwner().click();
                        } else if ($events.getKeyCode() === KeyMap.ENTER) {
                            (<Button[]>userControls.submit)[0].InstanceOwner().click();
                        }
                    }
                }
            }
        });
    }
}

export interface IDialogUserControls {
    holder : Dialog;
    close : Button | Button[];
    submit : Button | Button[];
}

// generated-code-start
export const IDialogUserControls = globalThis.RegisterInterface(["holder", "close", "submit"]);
// generated-code-end
