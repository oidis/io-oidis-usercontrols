/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { Button } from "../Bootstrap/UserControls/Button.js";
import { Dialog } from "../Bootstrap/UserControls/Dialog.js";

export class OffCanvasBinding extends BaseObject {
    private static instances : any = {};

    public Bind($userControls : IOffCanvasUserControls) : void {
        if (!ObjectValidator.IsEmptyOrNull($userControls.open)) {
            let elements : Button[] = [];
            if (!ObjectValidator.IsArray($userControls.open)) {
                elements.push(<Button>$userControls.open);
            } else {
                elements = <Button[]>$userControls.open;
            }
            elements.forEach(($element : Button) : void => {
                $element.getEvents().setOnClick(() : void => {
                    this.Open($userControls.holder);
                });
            });
        }
        $userControls.close.getEvents().setOnClick(() : void => {
            this.Close($userControls.holder);
        });
        if (!OffCanvasBinding.instances.hasOwnProperty($userControls.holder.Id())) {
            OffCanvasBinding.instances[$userControls.holder.Id()] = $userControls;
        }
        WindowManager.getEvents().setOnKeyDown(($events : KeyEventArgs) : void => {
            for (const id in OffCanvasBinding.instances) {
                if (OffCanvasBinding.instances.hasOwnProperty(id)) {
                    const userControls : IOffCanvasUserControls = OffCanvasBinding.instances[id];
                    if (!ObjectValidator.IsEmptyOrNull(userControls.holder.InstanceOwner()) &&
                        userControls.holder.InstanceOwner().classList.contains("show")) {
                        if ($events.getKeyCode() === KeyMap.ESC) {
                            this.Close(userControls.holder);
                        }
                    }
                }
            }
        });
    }

    public Open($instance : Dialog) : void {
        $instance.Visible(true);
    }

    public Close($instance : Dialog) : void {
        $instance.Visible(false);
    }
}

export interface IOffCanvasUserControls {
    holder : Dialog;
    close : Button;
    open? : Button | Button[];
}

// generated-code-start
export const IOffCanvasUserControls = globalThis.RegisterInterface(["holder", "close", "open"]);
// generated-code-end
