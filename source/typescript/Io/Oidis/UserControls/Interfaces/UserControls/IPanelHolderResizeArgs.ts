/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";

export interface IPanelHolderResizeArgs extends IBaseObject {
    size? : number;
}

// generated-code-start
export const IPanelHolderResizeArgs = globalThis.RegisterInterface(["size"], <any>IBaseObject);
// generated-code-end
