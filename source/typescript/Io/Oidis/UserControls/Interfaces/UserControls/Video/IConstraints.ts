/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";

export interface IConstraints extends IBaseObject {
    audio? : boolean | IMediaTrackConstraints;
    peerIdentity? : string;
    video? : boolean | IMediaTrackConstraints;
}

export interface IMediaTrackConstraintSet {
    aspectRatio? : number | IRange;
    channelCount? : number | IRange;
    deviceId? : string | string[] | IRange;
    displaySurface? : string | string[];
    echoCancellation? : boolean;
    facingMode? : string | string[];
    frameRate? : number | IRange;
    groupId? : string | string[];
    height? : number | IRange;
    latency? : number | IRange;
    logicalSurface? : boolean;
    sampleRate? : number | IRange;
    sampleSize? : number | IRange;
    volume? : number | IRange;
    width? : number | IRange;
}

export interface IMediaTrackConstraints extends IMediaTrackConstraintSet {
    advanced? : IMediaTrackConstraintSet[];
}

export interface IRange {
    exact? : number | string;
    ideal? : number | string;
}

// generated-code-start
/* eslint-disable */
export const IConstraints = globalThis.RegisterInterface(["audio", "peerIdentity", "video"], <any>IBaseObject);
export const IMediaTrackConstraintSet = globalThis.RegisterInterface(["aspectRatio", "channelCount", "deviceId", "displaySurface", "echoCancellation", "facingMode", "frameRate", "groupId", "height", "latency", "logicalSurface", "sampleRate", "sampleSize", "volume", "width"]);
export const IMediaTrackConstraints = globalThis.RegisterInterface(["advanced"], <any>IMediaTrackConstraintSet);
export const IRange = globalThis.RegisterInterface(["exact", "ideal"]);
/* eslint-enable */
// generated-code-end
