/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IRadioBox } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IRadioBox.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { CheckBox } from "./CheckBox.js";

/**
 * RadioBox class renders switch type of element, which belongs to elements group,
 * where only of the element from the group can be selected.
 */
export class RadioBox extends CheckBox implements IRadioBox {
    private groupName : string;

    /**
     * @param {RadioBox} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static ToggleChecked($element : RadioBox) : void {
        $element.Checked(!$element.Checked());
    }

    /**
     * @param {string} $groupName Specify group name, which should be handled.
     * @param {boolean} $value Switch type of group mode between enabled and disabled.
     * @returns {void}
     */
    public static EnabledGroup($groupName : string, $value : boolean) : void {
        GuiObjectManager.getInstanceSingleton().getType(RadioBox).foreach(($element : RadioBox) : void => {
            if ($element.GroupName() === $groupName) {
                $element.Enabled($value);
            }
        });
    }

    /**
     * @param {string} $groupName Specify group name, which should be handled.
     * @param {boolean} $value Specify, if group is in error status or not.
     * @returns {void}
     */
    public static ErrorGroup($groupName : string, $value : boolean) : void {
        GuiObjectManager.getInstanceSingleton().getType(RadioBox).foreach(($element : RadioBox) : void => {
            if ($element.GroupName() === $groupName) {
                $element.Error($value);
            }
        });
    }

    protected static checked($element : RadioBox) : void {
        let eventArgs : EventArgs;
        const elements : ArrayList<RadioBox> = $element.getGroup();
        elements.foreach(($groupElement : any) : void => {
            if ($groupElement.Id() !== $element.Id()) {
                $groupElement.setChanged();
                $groupElement.Checked(false);

                eventArgs = new EventArgs();
                eventArgs.Owner($groupElement);
                $element.getEventsManager().FireEvent($groupElement, EventType.ON_CHANGE, eventArgs);
                $element.getEventsManager().FireEvent($groupElement.getClassName(), EventType.ON_CHANGE, eventArgs);
            }
        });
        $element.setChanged();
        $element.Checked(true);

        eventArgs = new EventArgs();
        eventArgs.Owner($element);
        $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
        $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_CHANGE, eventArgs);
    }

    protected static onClickEventHandler($eventArgs : MouseEventArgs) : void {
        RadioBox.checked($eventArgs.Owner());
    }

    /**
     * @param {string} $groupName Specify group name, to which the element should belongs to.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($groupName : string, $id? : string) {
        super($id);
        this.GroupName($groupName);
    }

    /**
     * @param {string} [$value] Set text value, which should be displayed as element context.
     * @returns {string} Returns element's text value.
     */
    public GroupName($value? : string) : string {
        return this.groupName = Property.String(this.groupName, $value);
    }

    /**
     * @returns {ArrayList<RadioBox>} Returns list of elements, which belong to the same group as current element.
     */
    public getGroup() : ArrayList<RadioBox> {
        const group : ArrayList<RadioBox> = new ArrayList<RadioBox>();
        const groupName : string = this.GroupName();
        const elements : ArrayList<IGuiCommons> = this.getGuiManager().getType(RadioBox);
        elements.foreach(($element : RadioBox) : void => {
            if ($element.GroupName() === groupName) {
                group.Add($element);
            }
        });
        return group;
    }

    /**
     * @param {boolean} [$value] Specify element's check state.
     * @returns {boolean} Returns element's check state.
     */
    public Checked($value? : boolean) : boolean {
        const checked : boolean = super.Checked($value);
        if (!this.IsCompleted() && !this.IsPersistent() ||
            ElementManager.Exists(this.Id())) {
            if (ObjectValidator.IsSet($value) && checked) {
                this.getGroup().foreach(($element : RadioBox) : void => {
                    if ($element.Id() !== this.Id()) {
                        $element.Checked(false);
                    }
                });
            }
        }
        return checked;
    }
}
