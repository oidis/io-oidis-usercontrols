/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IDropDownListEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IDropDownListEvents.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IDropDownList } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IDropDownList.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { BaseField } from "../../Primitives/BaseField.js";
import { DropDownListItem } from "../../Primitives/DropDownListItem.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { DropDownListType } from "../Enums/UserControls/DropDownListType.js";

/**
 * DropDownList class renders selectable list of items.
 */
export class DropDownList extends BaseField implements IDropDownList {
    protected static minWidth : number = 20;

    private height : number;
    private initWidth : number;
    private fieldWidth : number;
    private showField : boolean;
    private showButton : boolean;
    private blurEnabled : boolean;
    private items : ArrayList<DropDownListItem>;
    private itemOptions : ArrayList<ArrayList<number>>;
    private visibleItemsCount : number;
    private selectedIndex : number;
    private selectedValue : string;
    private selectedItem : DropDownListItem;
    private lastActiveItem : number;
    private listEvents : ElementEventsManager;
    private listHeaderEvents : ElementEventsManager;
    private readonly verticalScrollBar : ScrollBar;

    /**
     * @param {DropDownList} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnActive($element : DropDownList, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        ElementManager.TurnActive($element.Id() + "_Enabled");
    }

    /**
     * @param {DropDownList} $element Specify item owner, which should be handled.
     * @param {number} $itemIndex Specify item index, which should be handled.
     * @returns {void}
     */
    public static ItemTurnActive($element : DropDownList, $itemIndex : number) : void {
        if (!$element.getGuiManager().IsActive(<IClassName>ScrollBar)) {
            if (!ObjectValidator.IsSet($element.lastActiveItem)) {
                $element.lastActiveItem = -1;
            }
            if ($element.lastActiveItem !== -1 && $itemIndex !== $element.lastActiveItem) {
                ElementManager.TurnOff($element.Id() + "_Item_" + $element.lastActiveItem + "_Status");
            }

            ElementManager.TurnActive($element.Id() + "_Item_" + $itemIndex + "_Status");
            $element.lastActiveItem = $itemIndex;
        }
    }

    /**
     * @param {DropDownList} $element Specify item owner, which should be handled.
     * @param {number} $itemIndex Specify item index, which should be handled.
     * @returns {void}
     */
    public static SelectItem($element : DropDownList, $itemIndex : number) : void {
        if (ElementManager.Exists($element.Id() + "_Item_" + $itemIndex)) {
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            if ($element.getSelectedIndex() !== $itemIndex) {
                $element.selectedIndex = $itemIndex;

                const item : DropDownListItem = $element.items.getItem($element.items.getKeys()[$itemIndex]);
                $element.setChanged();
                $element.Value(item.Value());

                DropDownList.forceSetValue($element);

                $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
                $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_CHANGE, eventArgs);
            }

            $element.getEventsManager().FireEvent($element, EventType.ON_SELECT, eventArgs);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_SELECT, eventArgs);

            $element.getGuiManager().setActive($element, true);
            $element.blurEnabled = true;
            DropDownList.Blur();
        }
    }

    /**
     * @param {DropDownList} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Focus($element : DropDownList) : void {
        if (!$element.getGuiManager().IsActive($element)) {
            const id : string = $element.Id();
            ElementManager.getElement(id + "_Input").focus();

            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_FOCUS, eventArgs);
            $element.getEventsManager().FireEvent($element, EventType.ON_FOCUS, eventArgs);

            if ($element.ShowField()) {
                ElementManager.Hide(id + "_Field");
                if ($element.ShowButton()) {
                    ElementManager.Show(id + "_ListButton");
                }
            } else {
                const buttonOffset : number =
                    ElementManager.getElement(id).offsetWidth -
                    ElementManager.getElement(id + "_FieldButton").offsetWidth;
                ElementManager.setCssProperty(id + "_List", "margin-right", buttonOffset);
                ElementManager.setCssProperty(id + "_List", "left", buttonOffset);
            }

            ElementManager.Hide(id + "_FieldButton");
            ElementManager.Show(id + "_List");
            ElementManager.BringToFront(id + "_List");
            ElementManager.Show(id + "_SelectedItem_Separator");

            ElementManager.setCssProperty(id + "_SelectedItem_Text", "position", "fixed");
            ElementManager.ClearCssProperty(id + "_SelectedItem_Text", "width");
            const textWidth : number = ElementManager.getElement(id + "_SelectedItem_Text").offsetWidth +
                ElementManager.getElement(id + "_SelectedItem_Text").offsetLeft;
            ElementManager.setCssProperty(id + "_SelectedItem_Text", "position", "relative");
            ElementManager.setWidth(id + "_SelectedItem_Text", textWidth);
            ElementManager.setWidth(id + "_SelectedItem_ContentEnvelop", $element.Width() + textWidth);

            $element.initWidth = ElementManager.getElement(id + "_List").offsetWidth;

            let info : any = DropDownList.getInfo($element);
            if ($element.fieldWidth >= DropDownList.minWidth && $element.MaxVisibleItemsCount() !== 0) {
                if ($element.ShowField()) {
                    ElementManager.setWidth(id + "_ListTopCenter", info.listWidth);
                    ElementManager.setWidth(id + "_ListMiddleCenter", info.listWidth);
                    ElementManager.setWidth(id + "_ListBottomCenter", info.listWidth);
                    ElementManager.setWidth(id + "_SelectedItem_Content", info.contentWidth - info.headerOffset);
                    ElementManager.setWidth(id + "_Content", info.contentWidth);
                    ElementManager.setWidth(id + "_Available", info.contentWidth);
                    ElementManager.setWidth(id + "_AvailableContent", info.contentWidth);
                } else {
                    ElementManager.setWidth(id + "_ListEnvelop", $element.initWidth);

                    const listCenter : number = $element.initWidth - info.listLeft - info.listRight;
                    ElementManager.setWidth(id + "_ListTopCenter", listCenter);
                    ElementManager.setWidth(id + "_ListMiddleCenter", listCenter);
                    ElementManager.setWidth(id + "_ListBottomCenter", listCenter);

                    ElementManager.setWidth(id + "_SelectedItem_Content",
                        $element.initWidth - info.headerOffset - info.contentOffsetLeft);

                    const contentWidth : number = $element.initWidth - 2 * info.contentOffsetLeft;
                    ElementManager.setWidth(id + "_Content", contentWidth);
                    ElementManager.setWidth(id + "_Available", contentWidth);
                    ElementManager.setWidth(id + "_AvailableContent", info.contentWidth);
                }

                let index : number;
                for (index = 0; index < $element.items.Length(); index++) {
                    const itemHeight : number =
                        ElementManager.getElement(id + "_Item_" + index + "_Text").offsetHeight +
                        2 * ElementManager.getElement(id + "_Item_" + index + "_Text").offsetTop;
                    ElementManager.setHeight(id + "_Item_" + index + "_Status", itemHeight);
                    ElementManager.setHeight(id + "_Item_" + index + "_Envelop", itemHeight);
                    ElementManager.TurnOff(id + "_Item_" + index + "_Status");
                }
                info = DropDownList.getInfo($element);

                const listCenterHeight : number = info.headerHeight - info.listTop - info.listBottom + info.contentOffsetTop;
                ElementManager.setHeight(id + "_ListMiddleLeft", listCenterHeight);
                ElementManager.setHeight(id + "_ListMiddleCenter", listCenterHeight);
                ElementManager.setHeight(id + "_ListMiddleRight", listCenterHeight);

                if ($element.getSelectedIndex() !== -1) {
                    DropDownList.ItemTurnActive($element, $element.selectedIndex);
                }

                $element.getGuiManager().setActive($element, true);
                $element.blurEnabled = true;

                const widthManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(id + "Width");
                widthManipulatorArgs.Owner($element);
                widthManipulatorArgs.DirectionType(DirectionType.UP);
                widthManipulatorArgs.ProgressType(ProgressType.FAST_END);
                widthManipulatorArgs.RangeStart($element.initWidth);
                widthManipulatorArgs.RangeEnd(info.contentWidth);
                widthManipulatorArgs.CurrentValue($element.initWidth);
                widthManipulatorArgs.Step($element.ShowField() ? 20 : 12);
                widthManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
                widthManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Show");

                $element.getEventsManager().setEvent(id + "Width", EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const contentWidth : number = $eventArgs.CurrentValue();
                        ElementManager.setWidth(id + "_ListEnvelop", contentWidth + 2 * info.contentOffsetLeft);

                        const listWidth : number = contentWidth - info.listLeft - info.listRight + 2 * info.contentOffsetLeft;
                        ElementManager.setWidth(id + "_ListTopCenter", listWidth);
                        ElementManager.setWidth(id + "_ListMiddleCenter", listWidth);
                        ElementManager.setWidth(id + "_ListBottomCenter", listWidth);

                        ElementManager.setWidth(id + "_SelectedItem_Content", contentWidth - info.headerOffset);

                        ElementManager.setWidth(id + "_Content", contentWidth);
                        ElementManager.setWidth(id + "_Available", contentWidth);
                        ElementManager.setWidth(id + "_AvailableContent", contentWidth);
                    });

                if (!$element.ShowField()) {
                    if (info.listWidth > info.listHeight) {
                        $element.getEventsManager().setEvent(id + "Width", EventType.ON_COMPLETE + "Show",
                            () : void => {
                                DropDownList.finalizeListShow($element, info);
                            });
                    }
                    ValueProgressManager.Execute(widthManipulatorArgs);
                }

                const heightManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(id + "Height");
                heightManipulatorArgs.Owner($element);
                heightManipulatorArgs.DirectionType(DirectionType.UP);
                heightManipulatorArgs.ProgressType(ProgressType.FAST_END);
                heightManipulatorArgs.RangeStart(info.headerHeight);
                heightManipulatorArgs.RangeEnd(info.contentHeight);
                heightManipulatorArgs.CurrentValue(info.headerHeight);
                heightManipulatorArgs.Step($element.ShowField() ? 20 : 12);
                heightManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
                heightManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Show");
                $element.getEventsManager().setEvent(id + "Height", EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const listHeight : number =
                            Math.max(1, $eventArgs.CurrentValue() - info.listTop - info.listBottom + 2 * info.contentOffsetTop);
                        ElementManager.setHeight(id + "_ListMiddleLeft", listHeight);
                        ElementManager.setHeight(id + "_ListMiddleCenter", listHeight);
                        ElementManager.setHeight(id + "_ListMiddleRight", listHeight);

                        ElementManager.setHeight(id + "_Content", $eventArgs.CurrentValue());

                        const currentAvailableHeight : number = $eventArgs.CurrentValue() - info.headerHeight;
                        ElementManager.setHeight(id + "_Available", currentAvailableHeight);
                        ElementManager.setHeight(id + "_AvailableContent", currentAvailableHeight);
                    });

                if ($element.ShowField() || !$element.ShowField() && info.listWidth <= info.listHeight) {
                    $element.getEventsManager().setEvent(id + "Height", EventType.ON_COMPLETE + "Show",
                        () : void => {
                            DropDownList.finalizeListShow($element, info);
                        });
                }
                ValueProgressManager.Execute(heightManipulatorArgs);
            }
        }
    }

    /**
     * @returns {void}
     */
    public static Blur() : void {
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        const reflection : Reflection = Reflection.getInstance();
        const elements : ArrayList<IGuiCommons> = manager.getType(<IClassName>DropDownList);
        elements.foreach(($element : DropDownList) : void => {
            if ($element.blurEnabled && $element.IsCompleted()) {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                const widthManagerExists : boolean = ValueProgressManager.Exists($element.Id() + "Width");
                const heightManagerExists : boolean = ValueProgressManager.Exists($element.Id() + "Height");
                const isFocused : boolean = heightManagerExists || widthManagerExists;

                if (isFocused) {
                    if (widthManagerExists) {
                        const widthManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id() + "Width");
                        widthManipulatorArgs.Owner($element);
                        widthManipulatorArgs.DirectionType(DirectionType.DOWN);
                        widthManipulatorArgs.Step(8);
                        if (!ObjectValidator.IsSet($element.initWidth)) {
                            $element.initWidth = DropDownList.minWidth;
                        }
                        widthManipulatorArgs.RangeStart($element.initWidth);
                        $element.getEventsManager().setEvent($element.Id() + "Width", EventType.ON_COMPLETE + "Hide",
                            () : void => {
                                ValueProgressManager.Remove($element.Id() + "Width");
                            });
                        if (!$element.ShowField()) {
                            ValueProgressManager.Execute(widthManipulatorArgs);
                        } else {
                            ValueProgressManager.Remove($element.Id() + "Width");
                        }
                    }

                    if (heightManagerExists) {
                        const heightManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id() + "Height");
                        heightManipulatorArgs.Owner($element);
                        heightManipulatorArgs.DirectionType(DirectionType.DOWN);
                        heightManipulatorArgs.Step(12);
                        heightManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Hide");

                        ElementManager.Hide($element.Id() + "_ScrollBarEnvelop");
                        $element.verticalScrollBar.Visible(false);

                        $element.getEventsManager().setEvent($element.Id() + "Height", EventType.ON_COMPLETE + "Hide",
                            () : void => {
                                ValueProgressManager.Remove($element.Id() + "Height");
                                manager.setActive($element, false);

                                ElementManager.SendToBack($element.Id() + "_List");
                                ElementManager.Hide($element.Id() + "_SelectedItem_Separator");
                                ElementManager.Hide($element.Id() + "_List");

                                if ($element.ShowButton()) {
                                    ElementManager.Show($element.Id() + "_FieldButton");
                                }
                                if ($element.ShowField()) {
                                    if ($element.ShowButton()) {
                                        ElementManager.Hide($element.Id() + "_ListButton");
                                    }
                                    ElementManager.Show($element.Id() + "_Field");
                                    DropDownList.resize($element);

                                    ElementManager.setClassName($element.Id() + "_FieldEnvelop",
                                        $element.getSelectedIndex() === -1 ? "" : "Selected");
                                }

                                DropDownList.TurnOff($element, manager, reflection);
                                $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_BLUR, eventArgs);
                                $element.getEventsManager().FireEvent($element, EventType.ON_BLUR, eventArgs);
                            });
                        ValueProgressManager.Execute(heightManipulatorArgs);
                    }
                } else {
                    manager.setActive($element, false);
                }
            }
        });
    }

    protected static forceSetValue($element : DropDownList, $value? : string | number) : void {
        const id : string = $element.Id();
        let item : DropDownListItem;
        if (!ObjectValidator.IsEmptyOrNull($value) && !$element.items.IsEmpty()) {
            let itemIndex : number = 0;
            $element.items.foreach(($item : DropDownListItem) : boolean => {
                if ((!ObjectValidator.IsString($value) && $value === itemIndex) ||
                    $value === $item.Text() || $value === $item.Value()) {
                    $element.selectedItem = $item;
                    $element.selectedIndex = itemIndex;
                    $element.selectedValue = $value.toString();
                    item = $item;
                    return false;
                }
                itemIndex++;
                return true;
            });
        } else {
            if ($element.getSelectedIndex() === -1) {
                item = $element.selectedItem;
            } else {
                item = $element.items.getItem($element.items.getKeys()[$element.selectedIndex]);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(item)) {
            const styleName : string = item.StyleClassName();
            ElementManager.setClassName(id + "_SelectedItem_StyleName", styleName);
            ElementManager.setClassName(id + "_Field_StyleName", styleName);
            ElementManager.setClassName(id + "_DisableField_StyleName", styleName);

            const text : string = item.Text();
            ElementManager.setInnerHtml(id + "_SelectedItem_Text", text);
            ElementManager.setInnerHtml(id + "_Field_Text", text);
            ElementManager.setInnerHtml(id + "_DisableField_Text", text);
        }
    }

    protected static onKeyPressEventHandler($eventArgs : KeyEventArgs) : void {
        // do not handle key press event
    }

    protected static onKeyUpEventHandler($eventArgs : KeyEventArgs) : void {
        // do not handle key up event
    }

    protected static onKeyDownEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>DropDownList);
        elements.foreach(($element : DropDownList) : void => {
            if ($eventArgs.getKeyCode() === KeyMap.ESC) {
                $element.blurEnabled = true;
                DropDownList.Blur();
                $eventArgs.PreventDefault();
            } else {
                if (!ObjectValidator.IsSet($element.lastActiveItem)) {
                    $element.lastActiveItem = -1;
                }
                if ($eventArgs.getKeyCode() === KeyMap.ENTER ||
                    $eventArgs.getKeyCode() === KeyMap.LEFT_ARROW ||
                    $eventArgs.getKeyCode() === KeyMap.RIGHT_ARROW ||
                    $eventArgs.getKeyCode() === KeyMap.SPACE) {
                    DropDownList.SelectItem($element, $element.lastActiveItem);
                } else {
                    let selected : number = $element.lastActiveItem;
                    if ($eventArgs.getKeyCode() === KeyMap.DOWN_ARROW) {
                        selected++;
                        if ($element.items.Length() <= selected) {
                            selected = 0;
                        }

                        DropDownList.ItemTurnActive($element, selected);
                        DropDownList.scrollToItem($element, $element.lastActiveItem);
                        $eventArgs.PreventDefault();
                    } else if ($eventArgs.getKeyCode() === KeyMap.UP_ARROW) {
                        selected--;
                        if (selected < 0) {
                            selected = $element.items.Length() - 1;
                        }

                        DropDownList.ItemTurnActive($element, selected);
                        DropDownList.scrollToItem($element, $element.lastActiveItem);
                        $eventArgs.PreventDefault();
                    } else {
                        const key : string = Convert.UnicodeToString($eventArgs.getKeyCode());
                        if (key !== " ") {
                            const searchIndex : string = StringUtils.ToUpperCase(key);
                            if ($element.itemOptions.KeyExists(StringUtils.ToUpperCase(key))) {
                                let index : number;
                                for (index = 0; index < $element.itemOptions.getItem(searchIndex).Length(); index++) {
                                    const current : number = $element.itemOptions.getItem(searchIndex).getItem(index);
                                    if (selected < current) {
                                        selected = current;
                                        break;
                                    }
                                }
                                if (selected === $element.lastActiveItem) {
                                    selected = $element.itemOptions.getItem(searchIndex).getFirst();
                                }

                                DropDownList.ItemTurnActive($element, selected);
                                DropDownList.scrollToItem($element, $element.lastActiveItem);
                            }
                        }
                    }
                }
            }
        });
    }

    protected static resize($element : DropDownList) : void {
        const id : string = $element.Id();
        if ($element.Width() >= DropDownList.minWidth) {
            if ($element.ShowButton()) {
                ElementManager.Hide(id + "_FieldIconEnvelop");
                ElementManager.Hide(id + "_ListIconEnvelop");
                ElementManager.Hide(id + "_DisableFieldIconEnvelop");
            } else {
                ElementManager.Show(id + "_FieldIconEnvelop");
                ElementManager.Show(id + "_ListIconEnvelop");
                ElementManager.Show(id + "_DisableFieldIconEnvelop");
            }

            if ($element.ShowField()) {
                const resizeField : any = ($id : string) : void => {
                    const fieldLeft : number = ElementManager.getOffsetWidth($id + "FieldLeft");
                    const fieldRight : number = ElementManager.getOffsetWidth($id + "FieldRight");
                    const fieldCenterHeight : number = ElementManager.getOffsetHeight($id + "FieldCenter");
                    let button : number = 0;
                    if ($element.ShowButton()) {
                        button = ElementManager.getOffsetWidth($id + "FieldButton") +
                            ElementManager.getCssIntegerValue($id + "FieldButton", "margin-left") +
                            ElementManager.getCssIntegerValue($id + "FieldButton", "margin-right");
                    }

                    const fieldCenterWidth : number = $element.Width() - fieldLeft - fieldRight - button;
                    if (fieldCenterWidth > 0) {
                        ElementManager.setCssProperty($id + "List", "width", $element.Width() - button);
                        ElementManager.setWidth($id + "FieldCenter", fieldCenterWidth);
                        $element.fieldWidth = fieldCenterWidth + fieldLeft + fieldRight;
                        const textOffset : number = ElementManager.getCssIntegerValue($id + "Field_Text", "left");
                        const iconWidth : number = ElementManager.getOffsetWidth($id + "FieldIconEnvelop");

                        ElementManager.setWidth($id + "Field_Item",
                            fieldCenterWidth + fieldLeft + fieldRight - textOffset - iconWidth);

                        ElementManager.setHeight($id + "Field_Item",
                            fieldCenterHeight -
                            ElementManager.getCssIntegerValue($id + "FieldCenter", "border-top-width") -
                            ElementManager.getCssIntegerValue($id + "FieldCenter", "border-bottom-width"));
                    }
                };
                if (!$element.Enabled()) {
                    resizeField(id + "_Disable");
                } else {
                    resizeField(id + "_");
                }
            } else {
                $element.fieldWidth = $element.Width();
            }
        }
    }

    private static scrollToItem($element : DropDownList, $itemIndex : number) : void {
        ElementManager.getElement($element.Id() + "_AvailableContent").scrollTop =
            ElementManager.getElement($element.Id() + "_Item_" + $itemIndex).offsetTop;

        const scrollEventArgs : ScrollEventArgs = new ScrollEventArgs();
        scrollEventArgs.Owner($element);
        scrollEventArgs.OrientationType(OrientationType.VERTICAL);
        scrollEventArgs.Position(DropDownList.scrollTop($element));
        $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, scrollEventArgs);
        $element.getEventsManager().FireEvent(DropDownList.ClassName(), EventType.ON_SCROLL, scrollEventArgs);
    }

    private static scrollTop($element : DropDownList, $value? : number) : number {
        let value : number = -1;
        const element : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_AvailableContent");
        if (ElementManager.Exists(element)) {
            if (ObjectValidator.IsSet($value)) {
                value = Property.PositiveInteger(value, $value);
                element.scrollTop = Math.ceil(
                    (element.scrollHeight - element.clientHeight) / 100 * value);
            } else {
                value = Math.ceil(
                    100 / (element.scrollHeight - element.clientHeight) * element.scrollTop);
            }
        }
        if (value !== -1) {
            if (value < 0) {
                value = 0;
            }
            if (value > 100) {
                value = 100;
            }
        }
        return value;
    }

    private static onBodyScrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                            $reflection : Reflection) : void {
        const element : DropDownList = <DropDownList>$manager.getHovered();
        if (!ObjectValidator.IsEmptyOrNull(element) &&
            $reflection.IsMemberOf(element, DropDownList)) {
            $eventArgs.PreventDefault();
            let position : number = DropDownList.scrollTop(element);
            if (position >= 0) {
                if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                    position += $eventArgs.Position() * 5;
                } else {
                    position -= $eventArgs.Position() * 5;
                }
                if (position < 0) {
                    position = 0;
                }
                if (position > 100) {
                    position = 100;
                }
                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(element);
                eventArgs.OrientationType(OrientationType.VERTICAL);
                eventArgs.Position(DropDownList.scrollTop(element, position));
                element.getEventsManager().FireEvent(element, EventType.ON_SCROLL, eventArgs);
                element.getEventsManager().FireEvent(DropDownList.ClassName(), EventType.ON_SCROLL, eventArgs);
            }
        }
    }

    private static onScrollBarMoveEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ScrollBar)) {
            let element : any = <ScrollBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ScrollBar) &&
                ElementManager.Exists(element.Parent().Id())) {
                element = <DropDownList>element.Parent();
                if ($reflection.IsMemberOf(element, DropDownList)) {
                    const envelop : HTMLElement = ElementManager.getElement(element.Id() + "_AvailableContent");
                    envelop.scrollTop = Math.ceil((envelop.scrollHeight - envelop.clientHeight) / 100 * $eventArgs.Position());
                }
            }
        }
    }

    private static onScrollBarCompleteEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                                   $reflection : Reflection) : void {
        let element : any = <ScrollBar>$eventArgs.Owner();
        if ($reflection.IsMemberOf(element, ScrollBar) &&
            ElementManager.Exists(element.Parent().Id())) {
            element = <DropDownList>element.Parent();
            if ($reflection.IsMemberOf(element, DropDownList)) {
                if ($manager.IsActive(element)) {
                    ElementManager.getElement(element.Id() + "_Input").focus();
                }
            }
        }
    }

    private static initSelection($element : DropDownList) : void {
        if ($element.getSelectedIndex() !== -1) {
            DropDownList.ItemTurnActive($element, $element.selectedIndex);
            (<any>$element).changed = false;
        }
    }

    private static getInfo($element : DropDownList) : object {
        const id : string = $element.Id();
        const listLeft : number = ElementManager.getElement(id + "_ListTopLeft").offsetWidth;
        const listRight : number = ElementManager.getElement(id + "_ListTopRight").offsetWidth;
        const listTop : number = ElementManager.getElement(id + "_ListTopCenter").offsetHeight;
        const listBottom : number = ElementManager.getElement(id + "_ListBottomCenter").offsetHeight;

        const contentOffsetLeft : number = listLeft + ElementManager.getElement(id + "_Content").offsetLeft;
        const contentOffsetTop : number = listTop + ElementManager.getElement(id + "_Content").offsetTop;

        const headerHeight : number = ElementManager.getElement(id + "_SelectedItem").offsetHeight;
        const headerOffset : number =
            ElementManager.getElement(id + "_ListIconEnvelop").offsetWidth +
            ElementManager.getElement(id + "_SelectedItem_Text").offsetLeft;

        const listWidth : number = $element.fieldWidth - listLeft - listRight;
        let listHeight : number = headerHeight;

        if ($element.items.Length() !== 0) {
            const itemCount : number = $element.MaxVisibleItemsCount() === -1 ?
                $element.items.Length() : $element.MaxVisibleItemsCount();

            let index : number;
            listHeight = 0;
            for (index = 0; index < itemCount; index++) {
                if (ElementManager.Exists(id + "_Item_" + index)) {
                    listHeight += ElementManager.getOffsetHeight(id + "_Item_" + index, true);
                }
            }
        }

        const contentWidth : number = listWidth + listLeft + listRight - 2 * contentOffsetLeft;
        const contentHeight : number = listHeight + headerHeight;

        return {
            contentHeight,
            contentOffsetLeft,
            contentOffsetTop,
            contentWidth,
            headerHeight,
            headerOffset,
            listBottom,
            listHeight,
            listLeft,
            listRight,
            listTop,
            listWidth
        };
    }

    private static finalizeListShow($element : DropDownList, $info : any) : void {
        const id : string = $element.Id();
        ElementManager.BringToFront(id);

        let listHeight : number = 0;
        let index : number;
        for (index = 0; index < $element.items.Length(); index++) {
            listHeight += ElementManager.getElement(id + "_Item_" + index).offsetHeight;
        }

        if (listHeight - $info.listHeight > 0) {
            ElementManager.setCssProperty(id + "_ScrollBarEnvelop", "height", $info.listHeight);
            ElementManager.Show(id + "_ScrollBarEnvelop");
            $element.verticalScrollBar.Visible(true);

            const resizeEventArgs : ResizeEventArgs = new ResizeEventArgs();
            resizeEventArgs.Owner($element);
            resizeEventArgs.Width($element.Width());
            resizeEventArgs.Height($element.Height());
            resizeEventArgs.AvailableWidth($info.listWidth);
            resizeEventArgs.AvailableHeight(listHeight);
            resizeEventArgs.ScrollBarWidth(ElementManager.getElement(id + "_ScrollBarEnvelop").offsetWidth);
            resizeEventArgs.ScrollBarHeight($info.listHeight);
            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, resizeEventArgs);
            $element.getEventsManager().FireEvent(DropDownList.ClassName(), EventType.ON_RESIZE, resizeEventArgs);

            const scrollBarWidth : number = ElementManager.getElement(id + "_ScrollBarEnvelop").offsetWidth;
            const scrollBarManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(id + "ScrollBarManipulator");
            scrollBarManipulatorArgs.Owner($element);
            scrollBarManipulatorArgs.DirectionType(DirectionType.UP);
            scrollBarManipulatorArgs.ProgressType(ProgressType.LINEAR);
            scrollBarManipulatorArgs.Step(1);
            scrollBarManipulatorArgs.RangeStart(0);
            scrollBarManipulatorArgs.RangeEnd(scrollBarWidth + 5);
            scrollBarManipulatorArgs.CurrentValue(0);
            scrollBarManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
            scrollBarManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE);

            $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_CHANGE,
                ($eventArgs : ValueProgressEventArgs) : void => {
                    ElementManager.setWidth(id + "_AvailableContent", $info.contentWidth - $eventArgs.CurrentValue());
                });
            $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_COMPLETE,
                ($eventArgs : ValueProgressEventArgs) : void => {
                    $eventArgs.DirectionType(DirectionType.DOWN);
                    $eventArgs.Step(8);
                    $eventArgs.RangeStart(scrollBarWidth);
                    $eventArgs.CompleteEventType(EventType.ON_COMPLETE + "2");

                    $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_COMPLETE + "2",
                        () : void => {
                            let index : number;
                            for (index = 0; index < $element.items.Length(); index++) {
                                const itemHeight : number =
                                    ElementManager.getElement(id + "_Item_" + index + "_Text").offsetHeight +
                                    2 * ElementManager.getElement(id + "_Item_" + index + "_Text").offsetTop;
                                ElementManager.setHeight(id + "_Item_" + index + "_Status", itemHeight);
                                ElementManager.setHeight(id + "_Item_" + index + "_Envelop", itemHeight);
                            }
                        });

                    ValueProgressManager.Execute($eventArgs);
                });
            ValueProgressManager.Execute(scrollBarManipulatorArgs);
        }

        if ($element.getSelectedIndex() !== -1 && $element.items.Length() > 0) {
            DropDownList.scrollToItem($element, $element.selectedIndex);
        } else {
            const scrollEventArgs : ScrollEventArgs = new ScrollEventArgs();
            scrollEventArgs.Owner($element);
            scrollEventArgs.OrientationType(OrientationType.VERTICAL);
            scrollEventArgs.Position(DropDownList.scrollTop($element));
            $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, scrollEventArgs);
            $element.getEventsManager().FireEvent(DropDownList.ClassName(), EventType.ON_SCROLL, scrollEventArgs);
        }
    }

    private static allowBlur($value : boolean) : void {
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        const elements : ArrayList<IGuiCommons> = manager.getType(DropDownList);
        elements.foreach(($element : DropDownList) : void => {
            if (manager.IsActive($element)) {
                $element.blurEnabled = $value;
            } else {
                $element.blurEnabled = true;
            }

            if ($element.blurEnabled) {
                ElementManager.setCssProperty($element.Id() + "_List", "z-index", "999");
            } else {
                ElementManager.BringToFront($element.Id() + "_List");
            }
        });
    }

    /**
     * @param {DropDownListType} [$dropDownListType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($dropDownListType? : DropDownListType, $id? : string) {
        super($dropDownListType, $id);

        this.height = -1;

        this.items = new ArrayList<DropDownListItem>();
        this.itemOptions = new ArrayList<ArrayList<number>>();
        this.selectedItem = new DropDownListItem(this.Id(), <any>this, -1);

        let scrollBarClass : any = this.getScrollBarClass();
        if (ObjectValidator.IsEmptyOrNull(scrollBarClass)) {
            scrollBarClass = ScrollBar;
        }
        this.verticalScrollBar = new scrollBarClass(OrientationType.VERTICAL, this.Id() + "_ScrollBar");
        this.verticalScrollBar.Visible(false);
        this.verticalScrollBar.Size(this.Height());
    }

    /**
     * @returns {IDropDownListEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : IDropDownListEvents {
        return <IDropDownListEvents>super.getEvents();
    }

    /**
     * @param {DropDownListType} [$dropDownListType] Specify type of element look and feel.
     * @returns {DropDownListType} Returns type of element's look and feel.
     */
    public GuiType($dropDownListType? : DropDownListType) : DropDownListType {
        return <DropDownListType>super.GuiType($dropDownListType);
    }

    /**
     * @param {string|number} [$value] Specify element's  value.
     * @returns {string} Returns element's value.
     */
    public Value($value? : string | number) : string {
        if (!ObjectValidator.IsEmptyOrNull($value) && !ObjectValidator.IsString($value)) {
            $value = $value.toString();
        }

        if (!this.IsCompleted() && this.IsPersistent()) {
            this.selectedValue = this.valuesPersistence.Variable(this.InstancePath());
        } else if (ObjectValidator.IsSet($value)) {
            if (ObjectValidator.IsString($value)) {
                this.selectedValue = Property.NullString(this.selectedValue, <string>$value);
            } else if (ObjectValidator.IsInteger($value) && $value >= 0) {
                this.selectedValue = $value.toString();
            } else if (ObjectValidator.IsEmptyOrNull($value)) {
                this.selectedValue = null;
            }
        }
        if (!ObjectValidator.IsSet(this.selectedValue)) {
            this.selectedValue = null;
        }
        return this.selectedValue;
    }

    /**
     * @param {number} [$value] Specify width value of element.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        const width : number = super.Width($value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            DropDownList.resize(this);
        }
        return width;
    }

    /**
     * @param {number} [$value] Specify maximal number of items, which can be shown in menu content.
     * @returns {number} Returns maximal number of visible items.
     */
    public MaxVisibleItemsCount($value? : number) : number {
        return this.visibleItemsCount = !ObjectValidator.IsSet($value) || $value === 0 ?
            -1 : Property.PositiveInteger(this.visibleItemsCount, $value, 1, 20);
    }

    /**
     * @param {number} $text Specify visible text value of the item.
     * @param {string|number} [$value] Specify not visible value of the item.
     * @param {number} [$styleClassName] Specify class name specific for the item.
     * @returns {void}
     */
    public Add($text : string, $value? : string | number, $styleClassName? : string) : void {
        const newItem : DropDownListItem = new DropDownListItem($text, this, this.items.Length());
        newItem.Value($value);
        newItem.StyleClassName($styleClassName);
        const hash : number = newItem.getHash();
        if (!this.items.KeyExists(hash)) {
            const itemIndex : number = newItem.Index();
            this.items.Add(newItem, hash);
            const option : string = StringUtils.ToUpperCase(StringUtils.getCharacterAt(newItem.Text(), 0));
            if (!ObjectValidator.IsEmptyOrNull(option)) {
                if (!this.itemOptions.KeyExists(option)) {
                    this.itemOptions.Add(new ArrayList<number>(), option);
                }
                this.itemOptions.getItem(option).Add(itemIndex);
            }

            if (ElementManager.IsVisible(this.Id())) {
                this.items.getLast().SeparatorVisible(true);
                newItem.SeparatorVisible(false);
                ElementManager.AppendHtml(this.Id() + "_AvailableContent",
                    newItem.getInnerHtml().Draw((<any>this).outputEndOfLine + "                                     "));
                newItem.getEvents().setEvent(EventType.ON_MOUSE_OVER, ($eventArgs : MouseEventArgs) : void => {
                    DropDownList.ItemTurnActive($eventArgs.Owner(), itemIndex);
                });
                newItem.getEvents().setEvent(EventType.ON_CLICK, ($eventArgs : MouseEventArgs) : void => {
                    DropDownList.SelectItem($eventArgs.Owner(), itemIndex);
                });
                newItem.getEvents().Subscribe();
                DropDownList.resize(this);
            }
        }
    }

    /**
     * @returns {void}
     */
    public Clear() : void {
        this.items.Clear();
        this.itemOptions.Clear();
        this.selectedIndex = -1;
        this.selectedValue = null;
        this.Hint(this.Hint());
        if (this.IsCompleted()) {
            DropDownList.forceSetValue(this);
            ElementManager.setInnerHtml(this.Id() + "_AvailableContent", "");
        }
    }

    /**
     * @returns {number} Returns current number of items, which are in hold by DropDownList.
     */
    public getItemsCount() : number {
        return this.items.Length();
    }

    /**
     * @param {string|number} $value Specify item, which should selected. Item can be selected by text, value or index.
     * Items are indexed from 0.
     * @returns {void}
     */
    public Select($value : string | number) : void {
        if (!this.IsCompleted() && this.IsPersistent()) {
            const persistVariable : any = this.valuesPersistence.Variable(this.InstancePath());
            if (!ObjectValidator.IsEmptyOrNull(persistVariable)) {
                const persistValue : string = persistVariable.toString();
                if ($value !== persistValue) {
                    $value = persistValue;
                }
            }

        }
        let itemIndex : number = 0;
        let selectedItem : DropDownListItem = null;
        this.items.foreach(($item : DropDownListItem) : boolean => {
            if ((ObjectValidator.IsInteger($value) && $value === itemIndex) ||
                $value === $item.Text() || $value === $item.Value()) {
                $item.IsSelected(true);
                selectedItem = $item;
                return false;
            }
            itemIndex++;
            return true;
        });
        if (!ObjectValidator.IsEmptyOrNull(selectedItem)) {
            if (ObjectValidator.IsEmptyOrNull(this.selectedValue)) {
                this.selectedValue = selectedItem.Value();
            }
            if (this.IsLoaded() && ElementManager.IsVisible(this.Id())) {
                DropDownList.SelectItem(this, itemIndex);
            } else {
                this.selectedIndex = itemIndex;
                this.selectedItem = selectedItem;
            }
        }
    }

    /**
     * @param {boolean} [$value] Specify, if DropDownList field should be visible.
     * @returns {boolean} Returns true, if field is visible, otherwise false.
     */
    public ShowField($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.showField)) {
            this.showField = true;
        }
        if (ObjectValidator.IsSet($value)) {
            this.showField = Property.Boolean(this.showField, $value);
            if (!this.showField) {
                this.showButton = true;
            }
            this.showHideButtonField();
        }

        return this.showField;
    }

    /**
     * @param {boolean} [$value] Specify, if DropDownList button should be visible.
     * @returns {boolean} Returns true, if button is visible, otherwise false.
     */
    public ShowButton($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.showButton)) {
            this.showButton = true;
        }
        if (ObjectValidator.IsSet($value)) {
            this.showButton = Property.Boolean(this.showButton, $value);
            if (!this.showButton) {
                this.showField = true;
            }
            this.showHideButtonField();
        }

        return this.showButton;
    }

    /**
     * @param {string} [$value] Specify alternate text or help for filed in case of not selected or empty data.
     * @returns {string} Returns element's alternate text or help.
     */
    public Hint($value? : string) : string {
        const hint : string = super.Hint($value);
        if (!ObjectValidator.IsEmptyOrNull($value) && ObjectValidator.IsString($value) && !this.IsCompleted()) {
            this.selectedItem.Text($value);
            this.selectedItem.StyleClassName("Hint");
        }
        return hint;
    }

    protected getScrollBarClass() : any {
        return ScrollBar;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, DropDownListType, DropDownListType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!DropDownListType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of drop down list type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().Subscriber(this.Id() + "_Field");

        if (this.MaxVisibleItemsCount() !== -1 && this.MaxVisibleItemsCount() > this.items.Length()) {
            this.MaxVisibleItemsCount(this.items.Length());
        }

        this.Select(this.Value());

        this.getListEvents().setOnMouseOver(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
            this.getEvents().FireAsynchronousMethod(() : void => {
                if ($manager.IsHovered($eventArgs.Owner())) {
                    DropDownList.allowBlur(false);
                }
            }, 50);
            $manager.setHovered($eventArgs.Owner(), true);
            $eventArgs.StopAllPropagation();
        });
        this.getListEvents().setOnMouseOut(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
            this.getEvents().FireAsynchronousMethod(() : void => {
                if (!$manager.IsHovered($eventArgs.Owner())) {
                    DropDownList.allowBlur(true);
                }
            }, 50);
            $manager.setHovered($eventArgs.Owner(), false);
        });

        this.getListHeaderEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
            ElementManager.TurnOn($eventArgs.Owner() + "_Enabled");
        });
        this.getListHeaderEvents().setOnMouseOut(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
            this.getEvents().FireAsynchronousMethod(() : void => {
                if (!$manager.IsHovered($eventArgs.Owner())) {
                    DropDownList.allowBlur(true);
                }
            }, 50);
            ElementManager.TurnOff($eventArgs.Owner() + "_Enabled");
        });
        this.getListHeaderEvents().setOnClick(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
            $manager.setActive(this, true);
            DropDownList.allowBlur(true);
            DropDownList.Blur();
        });

        this.verticalScrollBar.getEvents().setOnButton(DropDownList.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnChange(DropDownList.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnScroll(DropDownList.onScrollBarCompleteEventHandler);

        this.getEvents().setOnResize(ScrollBar.ResizeEventHandler);
        this.getEvents().setOnScroll(ScrollBar.ScrollEventHandler);
        WindowManager.getEvents().setOnScroll(DropDownList.onBodyScrollEventHandler);

        this.getEvents().setOnStart(($eventArgs : EventArgs) : void => {
            DropDownList.resize($eventArgs.Owner());
            DropDownList.initSelection($eventArgs.Owner());
        });

        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            const element : DropDownList = <DropDownList>$eventArgs.Owner();
            element.getEvents().Subscribe(element.Id() + "_FieldButton");
            element.getListEvents().Subscribe();
            element.getListHeaderEvents().Subscribe();
            element.getListHeaderEvents().Subscribe(element.Id() + "_ListButton");
            element.getListHeaderEvents().Subscribe(element.Id() + "_ListIcon");
            element.items.foreach(($element : DropDownListItem) : void => {
                const itemIndex : number = $element.Index();
                $element.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                    DropDownList.ItemTurnActive($eventArgs.Owner(), itemIndex);
                });
                $element.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                    DropDownList.SelectItem($eventArgs.Owner(), itemIndex);
                });
                $element.getEvents().Subscribe();
            });
        });

        this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
            DropDownList.resize($eventArgs.Owner());
            DropDownList.initSelection($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        let fieldStyle : string = "";
        if (this.getSelectedIndex() > -1) {
            fieldStyle = "Selected";
        }
        const width70 : number = Math.ceil(this.Width() * 0.7);

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            disabledOption
                .Id(this.Id() + "_Disabled").StyleClassName(GeneralCssNames.DISABLE)
                .Visible(!this.Enabled())
                .GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_DisableField")
                    .StyleClassName("Field")
                    .Visible(this.ShowField())
                    .Add(this.addElement(this.Id() + "_DisableFieldEnvelop")
                        .Add(this.addElement()
                            .StyleClassName(GeneralCssNames.BACKGROUND)
                            .Add(this.addElement(this.Id() + "_DisableFieldLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_DisableFieldCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Width(width70)
                                .Add(this.addElement(this.Id() + "_DisableField_StyleName")
                                    .Add(this.addElement(this.Id() + "_DisableField_Item")
                                        .StyleClassName("Item")
                                        .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                        .Add(this.addElement(this.Id() + "_DisableField_Text").StyleClassName(GeneralCssNames.TEXT))
                                    )
                                )
                            )
                            .Add(this.addElement(this.Id() + "_DisableFieldRight").StyleClassName(GeneralCssNames.RIGHT))
                            .Add(this.addElement(this.Id() + "_DisableFieldIconEnvelop")
                                .StyleClassName("DownArrow")
                                .Visible(false)
                                .Add(this.addElement(this.Id() + "_DisableFieldIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                        )
                    )
                )
                .Add(this.addElement(this.Id() + "_DisableFieldButton")
                    .StyleClassName(this.getButtonStyle())
                    .GuiTypeTag("FieldButton")
                    .Visible(this.ShowButton())
                    .Add(this.addElement(this.Id() + "_DisableFieldButtonEnvelop")
                        .Add(this.addElement(this.Id() + "_DisableFieldButtonIconEnvelop")
                            .StyleClassName("DownArrow")
                            .Add(this.addElement(this.Id() + "_DisableFieldButtonIcon").StyleClassName(GeneralCssNames.ICON))
                        )
                    )
                );
        }

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.GuiType())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .StyleClassName(GeneralCssNames.OFF)
                    .Visible(this.Enabled())
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.selectorElement())
                    .Add(this.addElement(this.Id() + "_Field")
                        .StyleClassName("Field")
                        .Visible(this.ShowField())
                        .Add(this.addElement(this.Id() + "_FieldEnvelop")
                            .StyleClassName(fieldStyle)
                            .Add(this.addElement()
                                .StyleClassName(GeneralCssNames.BACKGROUND)
                                .Add(this.addElement(this.Id() + "_FieldLeft").StyleClassName(GeneralCssNames.LEFT))
                                .Add(this.addElement(this.Id() + "_FieldCenter").StyleClassName(GeneralCssNames.CENTER)
                                    .Width(width70)
                                    .Add(this.addElement(this.Id() + "_Field_StyleName")
                                        .Add(this.addElement(this.Id() + "_Field_Item")
                                            .StyleClassName("Item")
                                            .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                            .Add(this.addElement(this.Id() + "_Field_Text").StyleClassName(GeneralCssNames.TEXT))
                                        )
                                    )
                                )
                                .Add(this.addElement(this.Id() + "_FieldRight").StyleClassName(GeneralCssNames.RIGHT))
                                .Add(this.addElement(this.Id() + "_FieldIconEnvelop")
                                    .StyleClassName("DownArrow")
                                    .Visible(false)
                                    .Add(this.addElement(this.Id() + "_FieldIcon").StyleClassName(GeneralCssNames.ICON))
                                )
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_FieldButton")
                        .StyleClassName(this.getButtonStyle())
                        .GuiTypeTag("FieldButton")
                        .Visible(this.ShowButton())
                        .Add(this.addElement(this.Id() + "_FieldButtonEnvelop")
                            .Add(this.addElement(this.Id() + "_FieldButtonIconEnvelop")
                                .StyleClassName("DownArrow")
                                .Add(this.addElement(this.Id() + "_FieldButtonIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_List")
                        .StyleClassName("List")
                        .Visible(false)
                        .Add(this.addElement(this.Id() + "_ListEnvelop")
                            .StyleClassName(GeneralCssNames.BACKGROUND)
                            .Add(this.addElement(this.Id() + "_ListTop").StyleClassName(GeneralCssNames.TOP)
                                .Add(this.addElement(this.Id() + "_ListTopLeft").StyleClassName(GeneralCssNames.LEFT))
                                .Add(this.addElement(this.Id() + "_ListTopCenter").StyleClassName(GeneralCssNames.CENTER)
                                    .Width(this.Width())
                                )
                                .Add(this.addElement(this.Id() + "_ListTopRight").StyleClassName(GeneralCssNames.RIGHT))
                            )
                            .Add(this.addElement(this.Id() + "_ListMiddle")
                                .StyleClassName(GeneralCssNames.MIDDLE)
                                .Add(this.addElement(this.Id() + "_ListMiddleLeft").StyleClassName(GeneralCssNames.LEFT).Height(1))
                                .Add(this.addElement(this.Id() + "_ListMiddleCenter")
                                    .StyleClassName(GeneralCssNames.CENTER)
                                    .Width(this.Width()).Height(1)
                                    .Add(this.addElement(this.Id() + "_Content")
                                        .StyleClassName("Content")
                                        .Width(this.Width()).Height(1)
                                        .Add(this.addElement(this.Id() + "_SelectedItem")
                                            .StyleClassName("Selected")
                                            .Add(this.addElement(this.Id() + "_SelectedItem_StyleName")
                                                .Add(this.addElement(this.Id() + "_SelectedItem_Content")
                                                    .StyleClassName("Item")
                                                    .Add(this.addElement(this.Id() + "_SelectedItem_ContentEnvelop")
                                                        .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                                        .Add(this.addElement(this.Id() + "_SelectedItem_Text")
                                                            .StyleClassName(GeneralCssNames.TEXT)
                                                        )
                                                    )
                                                )
                                                .Add(this.addElement(this.Id() + "_SelectedItem_Separator").StyleClassName("Separator"))
                                            )
                                        )
                                        .Add(this.addElement(this.Id() + "_ListIconEnvelop")
                                            .StyleClassName("UpArrow")
                                            .Add(this.addElement(this.Id() + "_ListIcon").StyleClassName(GeneralCssNames.ICON))
                                        )
                                        .Add(this.addElement(this.Id() + "_Available")
                                            .StyleClassName("Available")
                                            .Height(0)
                                            .Add(this.addElement(this.Id() + "_AvailableContent")
                                                .StyleClassName("Content")
                                                .Height(0)
                                                .Add(this.itemsHtml())
                                            )
                                            .Add(this.addElement(this.Id() + "_ScrollBarEnvelop")
                                                .StyleClassName("ScrollBarEnvelop")
                                                .Visible(false)
                                                .Add(this.verticalScrollBar)
                                            )
                                        )
                                    )
                                )
                                .Add(this.addElement(this.Id() + "_ListMiddleRight").StyleClassName(GeneralCssNames.RIGHT).Height(1))
                            )
                            .Add(this.addElement(this.Id() + "_ListBottom")
                                .StyleClassName(GeneralCssNames.BOTTOM)
                                .Add(this.addElement(this.Id() + "_ListBottomLeft")
                                    .Add(this.addElement().StyleClassName(GeneralCssNames.LEFT))
                                )
                                .Add(this.addElement(this.Id() + "_ListBottomCenter")
                                    .StyleClassName(GeneralCssNames.CENTER)
                                    .Width(this.Width())
                                )
                                .Add(this.addElement(this.Id() + "_ListBottomRight")
                                    .Add(this.addElement().StyleClassName(GeneralCssNames.RIGHT))
                                )
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_ListButton")
                        .StyleClassName(this.getButtonStyle())
                        .GuiTypeTag("ListButton")
                        .Visible(false)
                        .Add(this.addElement(this.Id() + "_ListButtonEnvelop")
                            .Add(this.addElement(this.Id() + "_ListButtonIconEnvelop")
                                .StyleClassName("UpArrow")
                                .Add(this.addElement(this.Id() + "_ListButtonIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                        )
                    )
                )
                .Add(disabledOption)
            );
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.height = -1;

        if (ObjectValidator.IsEmptyOrNull(this.items)) {
            this.items = new ArrayList<DropDownListItem>();
        }
        this.itemOptions = new ArrayList<ArrayList<number>>();
        this.selectedItem = new DropDownListItem(this.Id(), <any>this, -1);

        if (ObjectValidator.IsSet(this.verticalScrollBar)) {
            this.verticalScrollBar.Visible(false);
            this.verticalScrollBar.Size(this.Height());
        }
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "height", "initWidth", "fieldWidth",
            "showField", "showButton",
            "blurEnabled",
            "itemOptions",
            "visibleItemsCount",
            "selectedIndex", "selectedValue", "selectedItem",
            "lastActiveItem",
            "listEvents", "listHeaderEvents"
        );
        if (this.items.IsEmpty()) {
            exclude.push("items");
        }
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push(
            "initWidth", "fieldWidth", "blurEnabled",
            "listEvents", "listHeaderEvents",
            "lastActiveItem"
        );
        if (this.ShowField()) {
            exclude.push("showField");
        }
        if (this.ShowButton()) {
            exclude.push("showButton");
        }
        if (this.MaxVisibleItemsCount() === -1) {
            exclude.push("visibleItemsCount");
        }
        if (this.getSelectedIndex() === -1) {
            exclude.push("selectedIndex");
        }
        if (ObjectValidator.IsEmptyOrNull(this.Value())) {
            exclude.push("selectedValue");
        }

        return exclude;
    }

    protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : string) => void) : void {
        const cleanup : any = ($id : string) : void => {
            if (ElementManager.Exists($id)) {
                $preparationResultHandler($id, ElementManager.getInnerHtml($id));
                ElementManager.setInnerHtml($id, "");
            }
        };
        cleanup(this.Id() + "_Field_Text");
        cleanup(this.Id() + "_DisableField_Text");
    }

    protected afterCacheCreation($id : string, $value : string) : void {
        ElementManager.setInnerHtml($id, $value);
    }

    private getSelectedIndex() : number {
        if (!ObjectValidator.IsSet(this.selectedIndex)) {
            this.selectedIndex = -1;
        }
        return this.selectedIndex;
    }

    private getListEvents() : ElementEventsManager {
        if (!ObjectValidator.IsSet(this.listEvents)) {
            this.listEvents = new ElementEventsManager(this, this.Id() + "_Content");
        }
        return this.listEvents;
    }

    private getListHeaderEvents() : ElementEventsManager {
        if (!ObjectValidator.IsSet(this.listHeaderEvents)) {
            this.listHeaderEvents = new ElementEventsManager(this, this.Id() + "_SelectedItem");
        }
        return this.listHeaderEvents;
    }

    private itemsHtml() : IGuiElement {
        const items : IGuiElement = this.addElement();
        const itemsLength : number = this.items.Length() - 1;
        this.items.foreach(($item : DropDownListItem) : void => {
            $item.SeparatorVisible($item.Index() < itemsLength);
            items.Add($item.getInnerHtml());
        });
        return items;
    }

    private getButtonStyle() : string {
        let output : string = "Button";
        if (!this.ShowField()) {
            output = "WithOutField Button";
        }
        return output;
    }

    private showHideButtonField() : void {
        if (ElementManager.IsVisible(this.Id())) {
            if (this.ShowField()) {
                ElementManager.Show(this.Id() + "_Field");
                ElementManager.Show(this.Id() + "_DisableField");
            } else {
                ElementManager.Hide(this.Id() + "_Field");
                ElementManager.Hide(this.Id() + "_DisableField");
            }
            if (this.ShowButton()) {
                ElementManager.Show(this.Id() + "_FieldButton");
                ElementManager.Show(this.Id() + "_DisableFieldButton");
            } else {
                ElementManager.Hide(this.Id() + "_FieldButton");
                ElementManager.Hide(this.Id() + "_DisableFieldButton");
            }
            ElementManager.setClassName(this.Id() + "_FieldButton", this.getButtonStyle());
            ElementManager.setClassName(this.Id() + "_DisableFieldButton", this.getButtonStyle());
            ElementManager.setClassName(this.Id() + "_ListButton", this.getButtonStyle());
            DropDownList.resize(this);
        }
    }
}
