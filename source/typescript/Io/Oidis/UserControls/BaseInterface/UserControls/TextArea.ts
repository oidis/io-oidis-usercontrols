/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { ITextAreaEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/ITextAreaEvents.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ITextArea } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ITextArea.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { Borders } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Borders.js";
import { ElementOffset } from "@io-oidis-gui/Io/Oidis/Gui/Structures/ElementOffset.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { KeyEventHandler } from "@io-oidis-gui/Io/Oidis/Gui/Utils/KeyEventHandler.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { BaseField } from "../../Primitives/BaseField.js";
import { BasePanel as UserControlsBasePanel } from "../../Primitives/BasePanel.js";
import { ResizeBar } from "../Components/ResizeBar.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { TextAreaType } from "../Enums/UserControls/TextAreaType.js";
import { Label } from "./Label.js";

/**
 * TextArea class renders element, which is suitable for user text input with multiple line entry.
 */
export class TextArea extends BaseField implements ITextArea {
    protected static minWidth : number = 100;
    protected static minHeight : number = 40;

    public counterText : Label;
    private height : number;
    private readOnly : boolean;
    private lengthLimit : number;
    private maxWidth : number;
    private maxHeight : number;
    private readonly verticalScrollBar : ScrollBar;
    private readonly resizeBar : ResizeBar;
    private startWidth : number;
    private startHeight : number;

    /**
     * @param {TextArea} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Focus($element : TextArea) : void {
        BaseField.Focus($element);
        const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_Input");
        inputElement.focus();
        TextArea.contentHandler($element);
        TextArea.counterShow($element);
    }

    protected static onKeyPressEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const element : TextArea = <TextArea>$eventArgs.Owner();
        TextArea.contentHandler(element);
        TextArea.scroll(element);
        element.getEvents().FireAsynchronousMethod(() : void => {
            TextArea.counterShow(element);
        });
        if ($manager.IsActive(element)) {
            if (element.LengthLimit() > 0) {
                const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(element.Id() + "_Input");
                if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) &&
                    !KeyEventHandler.IsNavigate($eventArgs.NativeEventArgs()) &&
                    StringUtils.Length(inputElement.value) === element.LengthLimit()) {
                    $eventArgs.PreventDefault();
                }
            }
        }
    }

    protected static onKeyUpEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const element : TextArea = <TextArea>$eventArgs.Owner();
        TextArea.contentHandler(element);
        TextArea.scroll(element);
        element.getEvents().FireAsynchronousMethod(() : void => {
            TextArea.counterShow(element);
        });
        BaseField.onKeyUpEventHandler($eventArgs, $manager);
    }

    protected static forceSetValue($element : TextArea, $value : string) : void {
        BaseField.forceSetValue($element, $value);
        TextArea.counterShow($element);
        TextArea.contentHandler($element);
        TextArea.scroll($element);
    }

    protected static resize($element : TextArea) : void {
        if ($element.Width() >= TextArea.minWidth && $element.Height() >= TextArea.minHeight) {
            const resize : any = ($id : string) : void => {
                if (ElementManager.Exists($id + "Envelop")) {
                    const eventArgs : ResizeEventArgs = new ResizeEventArgs();
                    eventArgs.Owner($element);
                    eventArgs.Width($element.Width());
                    eventArgs.Height($element.Height());
                    eventArgs.AvailableWidth($element.Width());
                    eventArgs.AvailableHeight($element.Height());
                    eventArgs.ScrollBarWidth(0);
                    eventArgs.ScrollBarHeight(0);
                    $element.getEventsManager().FireEvent($element, EventType.BEFORE_RESIZE, eventArgs, false);
                    $element.getEventsManager().FireEvent(TextArea.ClassName(), EventType.BEFORE_RESIZE, eventArgs, false);

                    ElementManager.setSize($id + "Content", $element.Width(), $element.Height());
                    if ($element.IsLoaded()) {
                        TextArea.contentHandler($element);
                    }

                    let scrollBarSize : Size = new Size();
                    if ($element.Enabled()) {
                        scrollBarSize = new Size($element.verticalScrollBar.Id(), true);
                        scrollBarSize.Height($element.Height() - ElementManager.getElement($id + "Envelop").offsetTop);
                        if (ElementManager.IsVisible($element.resizeBar.Id())) {
                            if ($element.resizeBar.ResizeableType() !== ResizeableType.NONE) {
                                scrollBarSize.Height(scrollBarSize.Height() -
                                    ElementManager.getOffsetHeight($element.resizeBar.Id(), true));
                            }
                        }
                    }

                    const borders : Borders = new Borders();
                    borders.Left(ElementManager.getOffsetWidth($id + "TopLeft"));
                    borders.Right(ElementManager.getOffsetWidth($id + "TopRight"));
                    borders.Top(ElementManager.getOffsetHeight($id + "TopLeft"));
                    borders.Bottom(ElementManager.getOffsetHeight($id + "BottomLeft"));
                    const centerSize : Size = new Size();
                    centerSize.Width($element.Width() - borders.Left() - borders.Right());
                    centerSize.Height($element.Height() - borders.Top() - borders.Bottom());

                    ElementManager.setWidth($id + "TopCenter", centerSize.Width());
                    ElementManager.setHeight($id + "MiddleLeft", centerSize.Height());
                    ElementManager.setSize($id + "MiddleCenter", centerSize.Width(), centerSize.Height());
                    ElementManager.setHeight($id + "MiddleRight", centerSize.Height());
                    ElementManager.setWidth($id + "BottomCenter", centerSize.Width());

                    eventArgs.AvailableWidth($element.Width() - scrollBarSize.Width());
                    eventArgs.AvailableHeight($element.Height() - scrollBarSize.Height());
                    eventArgs.ScrollBarWidth(scrollBarSize.Width());
                    eventArgs.ScrollBarHeight(scrollBarSize.Height());
                    $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                    $element.getEventsManager().FireEvent(TextArea.ClassName(), EventType.ON_RESIZE, eventArgs, false);
                }
            };
            if ($element.Enabled()) {
                resize($element.Id() + "_");
            } else {
                resize($element.Id() + "_Disabled");
            }
        }
    }

    private static onResizeStartEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                             $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ResizeBar)) {
            let element : any = <ResizeBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ResizeBar)) {
                element = <TextArea>element.Parent();
                if ($reflection.IsMemberOf(element, TextArea)) {
                    $manager.setActive(element, true);

                    const maxSize : Size = new Size();
                    if (!ObjectValidator.IsEmptyOrNull(element.Parent()) &&
                        $reflection.IsMemberOf(element.Parent(), BasePanel)) {
                        let envelop : HTMLElement = <HTMLElement>ElementManager.getElement(element.Id() + "_" + GeneralCssNames.COLUMN);
                        if (ObjectValidator.IsEmptyOrNull(envelop)) {
                            envelop = <HTMLElement>ElementManager.getElement(element.Parent().Id() + "_PanelContentEnvelop");
                            const wrapper : HTMLElement = <HTMLElement>ElementManager.getElement(element.Id() + "_GuiWrapper");
                            maxSize.Width(envelop.offsetWidth - wrapper.offsetLeft - 10 + envelop.scrollLeft);
                            maxSize.Height(envelop.offsetHeight - wrapper.offsetTop - 10 + envelop.scrollTop);
                        } else {
                            maxSize.Width(envelop.offsetWidth);
                        }

                        element.MaxWidth(maxSize.Width());
                        element.MaxHeight(maxSize.Height());
                    } else {
                        const offset : ElementOffset = ElementManager.getAbsoluteOffset(element.Id());
                        const windowInfo : Size = WindowManager.getSize();
                        maxSize.Width(windowInfo.Width() - 50 - offset.Left());
                        maxSize.Height(windowInfo.Height() - 50 - offset.Top());
                    }

                    if (element.MaxWidth() === -1 || element.MaxWidth() > maxSize.Width()) {
                        element.MaxWidth(maxSize.Width());
                    }

                    if (element.MaxHeight() === -1 || element.MaxHeight() > maxSize.Height()) {
                        element.MaxHeight(maxSize.Height());
                    }

                    element.startWidth = element.Width();
                    element.startHeight = element.Height();
                    TextArea.TurnActive(element, $manager, $reflection);
                }
            }
        }
    }

    private static onResizeChangeEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                              $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ResizeBar)) {
            let element : any = <ResizeBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ResizeBar)) {
                element = <TextArea>element.Parent();
                if ($reflection.IsMemberOf(element, TextArea)) {
                    const newSize : Size = new Size();
                    newSize.Width(element.Width());
                    newSize.Height(element.Height());

                    const envelop : HTMLElement = <HTMLElement>ElementManager.getElement(element.Id() + "_" + GeneralCssNames.COLUMN);
                    if (!ObjectValidator.IsEmptyOrNull(envelop)) {
                        element.MaxWidth(envelop.offsetWidth);
                    }

                    if ($eventArgs.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                        $eventArgs.ResizeableType() === ResizeableType.HORIZONTAL) {
                        newSize.Width(element.startWidth + $eventArgs.DistanceX());
                        if (newSize.Width() < TextArea.minWidth) {
                            newSize.Width(TextArea.minWidth);
                        } else if (newSize.Width() > element.MaxWidth()) {
                            newSize.Width(element.MaxWidth());
                        }
                    }

                    if ($eventArgs.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                        $eventArgs.ResizeableType() === ResizeableType.VERTICAL) {
                        newSize.Height(element.startHeight + $eventArgs.DistanceY());
                        if (newSize.Height() < TextArea.minHeight) {
                            newSize.Height(TextArea.minHeight);
                        } else if (newSize.Height() > element.MaxHeight()) {
                            newSize.Height(element.MaxHeight());
                        }
                    }

                    element.Width(newSize.Width());
                    element.Height(newSize.Height());

                    TextArea.resize(element);
                    TextArea.scroll(element);
                }
            }
        }
    }

    private static onResizeCompleteEventHandler($eventArgs : ResizeBarEventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ResizeBar)) {
            let element : any = <ResizeBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ResizeBar)) {
                element = <TextArea>element.Parent();
                if ($reflection.IsMemberOf(element, TextArea)) {
                    if ($manager.IsActive(element)) {
                        TextArea.Focus(element);
                    } else {
                        element.Error(element.Error());
                    }
                }
            }
        }
    }

    private static scrollTop($element : TextArea, $value? : number) : number {
        let value : number = -1;
        const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_Input");
        if (ElementManager.Exists(inputElement)) {
            if (ObjectValidator.IsSet($value)) {
                value = Property.PositiveInteger(value, $value);
                inputElement.scrollTop = Math.ceil(
                    (inputElement.scrollHeight - inputElement.clientHeight) / 100 * value);
            } else {
                value = Math.ceil(
                    100 / (inputElement.scrollHeight - inputElement.clientHeight) * inputElement.scrollTop);
            }
        }
        return value;
    }

    private static scroll($element : TextArea) : void {
        const position : number = TextArea.scrollTop($element);
        if (position >= 0) {
            const eventArgs : ScrollEventArgs = new ScrollEventArgs();
            eventArgs.Owner($element);
            eventArgs.OrientationType(OrientationType.VERTICAL);
            eventArgs.Position(position);
            $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, eventArgs);
            $element.getEventsManager().FireEvent(TextArea.ClassName(), EventType.ON_SCROLL, eventArgs);
        }
    }

    private static onBodyScrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                            $reflection : Reflection) : void {
        const element : TextArea = <TextArea>$manager.getHovered();
        if (!ObjectValidator.IsEmptyOrNull(element) &&
            $reflection.IsMemberOf(element, TextArea)) {
            $eventArgs.PreventDefault();
            let position : number = TextArea.scrollTop(element);
            if (position >= 0) {
                $manager.setActive(element, true);

                if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                    position += $eventArgs.Position() * 5;
                } else {
                    position -= $eventArgs.Position() * 5;
                }
                if (position < 0) {
                    position = 0;
                }
                if (position > 100) {
                    position = 100;
                }

                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(element);
                eventArgs.OrientationType(OrientationType.VERTICAL);
                eventArgs.Position(TextArea.scrollTop(element, position));
                element.getEventsManager().FireEvent(element, EventType.ON_SCROLL, eventArgs);
                element.getEventsManager().FireEvent(TextArea.ClassName(), EventType.ON_SCROLL, eventArgs);
            }
        }
    }

    private static onScrollBarMoveEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ScrollBar)) {
            let element : any = <ScrollBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ScrollBar) &&
                ElementManager.Exists(element.Parent().Id())) {
                element = <TextArea>element.Parent();
                if ($reflection.IsMemberOf(element, TextArea)) {
                    const position : number = TextArea.scrollTop(element, $eventArgs.Position());
                    if (position >= 0) {
                        TextArea.TurnActive(element, $manager, $reflection);
                    }
                }
            }
        }
    }

    private static onScrollBarCompleteEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                                   $reflection : Reflection) : void {
        let element : any = <ScrollBar>$eventArgs.Owner();
        if ($reflection.IsMemberOf(element, ScrollBar) &&
            ElementManager.Exists(element.Parent().Id())) {
            element = <TextArea>element.Parent();
            if ($reflection.IsMemberOf(element, TextArea)) {
                if ($manager.IsActive(element)) {
                    TextArea.Focus(element);
                } else {
                    element.Error(element.Error());
                }
            }
        }
    }

    private static counterShow($element : TextArea) : void {
        if ($element.LengthLimit() !== -1) {
            if (!$element.counterText.Visible()) {
                $element.counterText.Visible(true);
            }
            const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_Input");
            if (ElementManager.Exists(inputElement)) {
                if (!($element.Enabled() &&
                    !ObjectValidator.IsEmptyOrNull($element.Hint()) && ObjectValidator.IsEmptyOrNull(inputElement.value))) {
                    const currentLength : number = StringUtils.Length(inputElement.value);
                    if (currentLength < $element.LengthLimit()) {
                        const innerHtml : number = $element.LengthLimit() - currentLength;
                        ElementManager.setInnerHtml($element.Id() + "_CounterValue", innerHtml.toString());
                    } else {
                        ElementManager.setInnerHtml($element.Id() + "_CounterValue", "0");
                    }
                } else {
                    ElementManager.setInnerHtml($element.Id() + "_CounterValue", $element.LengthLimit().toString());
                }
            }
        } else {
            $element.counterText.Visible(false);
        }
    }

    private static contentHandler($element : TextArea) : void {
        let statusId : string = $element.Id() + "_";
        if (!$element.Enabled()) {
            statusId = $element.Id() + "_Disabled";
        }
        const envelop : HTMLElement = <HTMLElement>ElementManager.getElement(statusId + "Envelop");
        if (ElementManager.Exists(envelop)) {
            const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_Input");

            const envelopSize : Size = new Size();
            const scrollBarWidth : number = ElementManager.getOffsetWidth($element.verticalScrollBar + "_Content_Offset",
                false, true);
            envelopSize.Width($element.Width() - ElementManager.getWidthOffset(envelop.id) - scrollBarWidth);
            envelopSize.Height($element.Height() - ElementManager.getHeightOffset(envelop.id));

            ElementManager.setSize(input, envelopSize.Width(), envelopSize.Height());
            const scrollBarId : string = $element.verticalScrollBar.Id();
            if ($element.Enabled()) {
                if (input.scrollHeight > input.clientHeight || input.scrollWidth > input.clientWidth) {
                    if (!ElementManager.IsVisible(scrollBarId)) {
                        $element.verticalScrollBar.Visible(true);
                        ElementManager.setOpacity(scrollBarId, 0);
                        ElementManager.ChangeOpacity($element.verticalScrollBar, DirectionType.UP, 10);
                        $element.getEvents().FireAsynchronousMethod(() : void => {
                            TextArea.resize($element);
                        }, false);
                    }
                } else {
                    $element.verticalScrollBar.Visible(false);
                }
                const hint : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_HintInput");
                if (!ObjectValidator.IsEmptyOrNull($element.Hint()) && ObjectValidator.IsEmptyOrNull(input.value)) {
                    ElementManager.setSize(hint, envelopSize.Width(), envelopSize.Height());
                }
            }
            ElementManager.setSize(input, envelopSize.Width(), envelopSize.Height());
            ElementManager.setSize(envelop, envelopSize.Width(), envelopSize.Height());
        }
    }

    /**
     * @param {TextAreaType} [$textAreaType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($textAreaType? : TextAreaType, $id? : string) {
        super($textAreaType, $id);

        this.height = 100;

        let scrollBarClass : any = this.getScrollBarClass();
        if (ObjectValidator.IsEmptyOrNull(scrollBarClass)) {
            scrollBarClass = ScrollBar;
        }
        this.verticalScrollBar = new scrollBarClass(OrientationType.VERTICAL, this.Id() + "_ScrollBar");
        this.verticalScrollBar.Visible(false);
        this.verticalScrollBar.Size(90);

        let resizeBarClass : any = this.getResizeBarClass();
        if (ObjectValidator.IsEmptyOrNull(resizeBarClass)) {
            resizeBarClass = ResizeBar;
        }
        this.resizeBar = new resizeBarClass(ResizeableType.NONE, this.Id() + "_ResizeBar");
        this.resizeBar.DisableAsynchronousDraw();

        let counterTextClass : any = this.getCounterTextClass();
        if (ObjectValidator.IsEmptyOrNull(counterTextClass)) {
            counterTextClass = Label;
        }
        this.counterText = new counterTextClass("{0}", this.Id() + "_CounterText");
        this.counterText.StyleClassName("Counter");
        this.counterText.Visible(false);
        this.counterText.DisableAsynchronousDraw();
    }

    /**
     * @returns {ITextAreaEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : ITextAreaEvents {
        return <ITextAreaEvents>super.getEvents();
    }

    /**
     * @param {TextAreaType} [$textAreaType] Specify type of element look and feel.
     * @returns {TextAreaType} Returns type of element's look and feel.
     */
    public GuiType($textAreaType? : TextAreaType) : TextAreaType {
        return <TextAreaType>super.GuiType($textAreaType);
    }

    /**
     * @param {number} [$value] Specify height value of element.
     * @returns {number} Returns element's height value.
     */
    public Height($value? : number) : number {
        if (ObjectValidator.IsSet($value) && this.height !== $value) {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            this.height = Property.PositiveInteger(this.height, $value, thisClass.minHeight);
            this.verticalScrollBar.Size(Math.ceil(this.height * 0.9));
            if (this.IsLoaded()) {
                thisClass.resize(this);
            }
        }
        return this.height;
    }

    /**
     * @param {number} [$value] Specify maximal width value of element.
     * @returns {number} Returns maximal element's width value.
     */
    public MaxWidth($value? : number) : number {
        if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.maxWidth)) {
            this.maxWidth = -1;
        } else {
            this.maxWidth = Property.PositiveInteger(this.maxWidth, $value, TextArea.minWidth);
        }

        return this.maxWidth;
    }

    /**
     * @param {number} [$value] Specify maximal height value of element.
     * @returns {number} Returns maximal element's height value.
     */
    public MaxHeight($value? : number) : number {
        if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.maxHeight)) {
            this.maxHeight = -1;
        } else {
            this.maxHeight = Property.PositiveInteger(this.maxHeight, $value, TextArea.minHeight);
        }
        return this.maxHeight;
    }

    /**
     * @param {ResizeableType} [$type] Specify type of element's resize direction.
     * @returns {ResizeableType} Returns type of element's resize direction.
     */
    public ResizeableType($type? : ResizeableType) : ResizeableType {
        return this.resizeBar.ResizeableType($type);
    }

    /**
     * @param {boolean} [$value] Specify, if text area value is editable by user or not.
     * @returns {boolean} Returns true, if text area is not editable, otherwise false.
     */
    public ReadOnly($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.readOnly)) {
            this.readOnly = false;
        }
        this.readOnly = Property.Boolean(this.readOnly, $value);
        return this.readOnly;
    }

    /**
     * @param {number} [$value] Specify maximal length of entered data.
     * @returns {number} Returns maximal length, which is allowed to be entered to the input.
     */
    public LengthLimit($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            if ($value === -1) {
                this.lengthLimit = -1;
            } else {
                this.lengthLimit = Property.PositiveInteger(this.lengthLimit, $value, 1);
            }
            if (this.lengthLimit > 0 && !ObjectValidator.IsEmptyOrNull(this.Value())) {
                this.Value(this.Value());
            }
        }
        if (!ObjectValidator.IsSet(this.lengthLimit)) {
            this.lengthLimit = -1;
        }
        return this.lengthLimit;
    }

    /**
     * @param {string} [$value]  Value or arguments object, which should be set to the TextArea#
     * @returns {string} Returns  current object's value.
     */
    public Value($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            if (!ObjectValidator.IsSet(this.lengthLimit)) {
                this.lengthLimit = -1;
            }
            if (this.lengthLimit > 0 && StringUtils.Length($value) > this.lengthLimit) {
                $value = StringUtils.Substring($value, 0, this.lengthLimit);
            }
        }
        const currentValue : string = <string>super.Value($value);
        if (ObjectValidator.IsSet($value) && this.IsLoaded() && !this.getGuiManager().IsActive(this)) {
            TextArea.contentHandler(this);
            this.getEvents().FireAsynchronousMethod(() : void => {
                TextArea.counterShow(this);
            });
        }

        return currentValue;
    }

    /**
     * @returns {IScrollBar} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IScrollBar
     */
    protected getScrollBarClass() : any {
        return ScrollBar;
    }

    /**
     * @returns {IResizeBar} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IResizeBar
     */
    protected getResizeBarClass() : any {
        return ResizeBar;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getCounterTextClass() : any {
        return Label;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, TextAreaType, TextAreaType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!TextAreaType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of textArea type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        this.resizeBar.getEvents().setOnResizeStart(TextArea.onResizeStartEventHandler);
        this.resizeBar.getEvents().setOnResizeChange(TextArea.onResizeChangeEventHandler);
        this.resizeBar.getEvents().setOnResizeComplete(TextArea.onResizeCompleteEventHandler);

        this.verticalScrollBar.getEvents().setOnButton(TextArea.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnChange(TextArea.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnScroll(TextArea.onScrollBarCompleteEventHandler);

        this.getEvents().setOnResize(ScrollBar.ResizeEventHandler);
        this.getEvents().setOnScroll(ScrollBar.ScrollEventHandler);
        this.getEvents().setOnResize(UserControlsBasePanel.ContentFocusHandler);
        WindowManager.getEvents().setOnScroll(TextArea.onBodyScrollEventHandler);
        this.getEvents().setOnFocus(UserControlsBasePanel.ContentFocusHandler);

        this.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void => {
            const element : TextArea = <TextArea>$eventArgs.Owner();
            if (ElementManager.IsVisible(element.verticalScrollBar.Id())) {
                $manager.setHovered(element, true);
            }
        });

        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            TextArea.resize($eventArgs.Owner());
            TextArea.counterShow($eventArgs.Owner());
            TextArea.contentHandler($eventArgs.Owner());
            TextArea.scroll($eventArgs.Owner());
        });

        this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
            TextArea.resize($eventArgs.Owner());
            TextArea.counterShow($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const width95 : number = Math.ceil(this.Width() * 0.95);
        const height85 : number = Math.ceil(this.Height() * 0.85);

        const input : HTMLTextAreaElement = document.createElement("textarea");
        input.id = this.Id() + "_Input";
        input.name = this.Id();
        input.style.width = width95 + "px";
        input.style.height = height85 + "px";

        if (!ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
            input.tabIndex = this.TabIndex();
        }
        if (this.ReadOnly()) {
            input.readOnly = true;
        }

        const hintInput : HTMLTextAreaElement = document.createElement("textarea");
        hintInput.id = this.Id() + "_HintInput";
        hintInput.className = "Hint";
        hintInput.style.width = width95 + "px";
        hintInput.style.height = height85 + "px";
        hintInput.style.display = "none";
        hintInput.readOnly = true;
        hintInput.disabled = true;

        const contextValue : HTMLSpanElement = document.createElement("span");
        contextValue.id = this.Id() + "_CounterValue";
        contextValue.className = "Value";
        contextValue.innerHTML = "" + Math.ceil(this.LengthLimit() - StringUtils.Length(this.Value()));
        this.counterText.Text(StringUtils.Format(this.counterText.Text(), this.addElement().Add(contextValue).Draw("")));

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            const disabledInput : HTMLTextAreaElement = document.createElement("textarea");
            disabledInput.id = this.Id() + "_DisabledInput";
            disabledInput.style.width = width95 + "px";
            disabledInput.style.height = height85 + "px";
            disabledInput.readOnly = true;
            disabledInput.disabled = true;

            disabledOption
                .Id(this.Id() + "_Disabled")
                .StyleClassName(GeneralCssNames.DISABLE)
                .GuiTypeTag(this.getGuiTypeTag())
                .Visible(!this.Enabled())
                .Add(this.addElement(this.Id() + "_DisabledBackground")
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_DisabledTop")
                        .StyleClassName(GeneralCssNames.TOP)
                        .Add(this.addElement(this.Id() + "_DisabledTopLeft").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_DisabledTopCenter").StyleClassName(GeneralCssNames.CENTER).Width(width95))
                        .Add(this.addElement(this.Id() + "_DisabledTopRight").StyleClassName(GeneralCssNames.RIGHT))
                    )
                    .Add(this.addElement(this.Id() + "_DisabledMiddle")
                        .StyleClassName(GeneralCssNames.MIDDLE)
                        .Add(this.addElement(this.Id() + "_DisabledMiddleLeft").StyleClassName(GeneralCssNames.LEFT).Height(height85))
                        .Add(this.addElement(this.Id() + "_DisabledMiddleCenter")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Add(this.addElement(this.Id() + "_DisabledContent")
                                .StyleClassName("Content")
                                .Add(this.addElement(this.Id() + "_DisabledEnvelop")
                                    .StyleClassName("Envelop")
                                    .Width(width95).Height(height85)
                                    .Add(disabledInput)
                                )
                                .Add(this.verticalScrollBar)
                            )
                        )
                        .Add(this.addElement(this.Id() + "_DisabledMiddleRight").StyleClassName(GeneralCssNames.RIGHT).Height(height85))
                    )
                    .Add(this.addElement(this.Id() + "_DisabledBottom")
                        .StyleClassName(GeneralCssNames.BOTTOM)
                        .Add(this.addElement(this.Id() + "_DisabledBottomLeft").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_DisabledBottomCenter")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(width95)
                        )
                        .Add(this.addElement(this.Id() + "_DisabledBottomRight").StyleClassName(GeneralCssNames.RIGHT))
                    )
                );
        }

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Visible(this.Enabled())
                    .Add(this.addElement(this.Id() + "_Background")
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_Top")
                            .StyleClassName(GeneralCssNames.TOP)
                            .Add(this.addElement(this.Id() + "_TopLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_TopCenter").StyleClassName(GeneralCssNames.CENTER).Width(width95))
                            .Add(this.addElement(this.Id() + "_TopRight").StyleClassName(GeneralCssNames.RIGHT))
                        )
                        .Add(this.addElement(this.Id() + "_Middle")
                            .StyleClassName(GeneralCssNames.MIDDLE)
                            .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT).Height(height85))
                            .Add(this.addElement(this.Id() + "_MiddleCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Add(this.addElement(this.Id() + "_Content")
                                    .StyleClassName("Content")
                                    .Add(this.addElement(this.Id() + "_Envelop")
                                        .StyleClassName("Envelop")
                                        .Width(width95).Height(height85)
                                        .Add(hintInput)
                                        .Add(input)
                                    )
                                    .Add(this.verticalScrollBar)
                                )
                            )
                            .Add(this.addElement(this.Id() + "_MiddleRight").StyleClassName(GeneralCssNames.RIGHT).Height(height85))
                        )
                        .Add(this.addElement(this.Id() + "_Bottom")
                            .StyleClassName(GeneralCssNames.BOTTOM)
                            .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_BottomCenter")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Width(width95)
                                .Add(this.resizeBar)
                            )
                            .Add(this.addElement(this.Id() + "_BottomRight").StyleClassName(GeneralCssNames.RIGHT))
                        )
                    )
                )
                .Add(disabledOption)
                .Add(this.addElement().StyleClassName("CounterEnvelop").Add(this.counterText))
            );
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.height = 100;

        if (ObjectValidator.IsSet(this.verticalScrollBar)) {
            this.verticalScrollBar.Visible(false);
            this.verticalScrollBar.Size(90);
        }

        if (ObjectValidator.IsSet(this.resizeBar)) {
            this.resizeBar.DisableAsynchronousDraw();
        }

        if (ObjectValidator.IsSet(this.counterText)) {
            this.counterText.StyleClassName("Counter");
            this.counterText.Visible(false);
            this.counterText.DisableAsynchronousDraw();
        }
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "readOnly", "lengthLimit",
            "height",
            "maxWidth", "maxHeight",
            "startWidth", "startHeight");
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push(
            "maxWidth", "maxHeight",
            "startWidth", "startHeight"
        );
        if (this.readOnly === false) {
            exclude.push("readOnly");
        }
        if (this.lengthLimit === -1) {
            exclude.push("lengthLimit");
        }
        return exclude;
    }
}
