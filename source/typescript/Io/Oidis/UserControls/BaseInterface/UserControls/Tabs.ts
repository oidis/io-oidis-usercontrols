/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { OpacityEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/OpacityEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/IEventsHandler.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ITab } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ITab.js";
import { ITabs } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ITabs.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { TextSelectionManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/TextSelectionManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { FormsObject } from "../../Primitives/FormsObject.js";
import { DragBar } from "../Components/DragBar.js";
import { ResizeBar } from "../Components/ResizeBar.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { ButtonType } from "../Enums/UserControls/ButtonType.js";
import { TabsType } from "../Enums/UserControls/TabsType.js";
import { Button } from "./Button.js";
import { ImageButton } from "./ImageButton.js";

/**
 * Tabs class renders horizontal tab menu.
 */
export class Tabs extends FormsObject implements ITabs {
    private static threadCallback : any;
    private static threadTick : number;
    private static tabPressed : boolean = false;

    private guiType : TabsType;
    private items : ArrayList<string>;
    private width : number;
    private itemsWidth : number;
    private selectedItemWidth : number;
    private beforeSelectedWidth : number;
    private position : number;
    private readonly leftArrow : ImageButton;
    private readonly rightArrow : ImageButton;

    /**
     * @param {Tabs} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOn($element : Tabs, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element) &&
            !$manager.IsActive(<IClassName>ScrollBar) &&
            !$manager.IsActive(<IClassName>ResizeBar) &&
            !$manager.IsActive(<IClassName>DragBar)) {
            ElementManager.TurnOn($element.Id() + "_Status");
        }
    }

    /**
     * @param {Tabs} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOff($element : Tabs, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element)) {
            ElementManager.TurnOff($element.Id() + "_Status");
        }
    }

    /**
     * @param {Tabs} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnActive($element : Tabs, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (Tabs.tabPressed) {
            ElementManager.TurnActive($element.Id() + "_Status");
            Tabs.tabPressed = false;
        }
    }

    protected static setErrorStyle($element : Tabs) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.OFF);
        }
    }

    protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void {
        if ($eventArgs.getKeyCode() === KeyMap.RIGHT_ARROW || $eventArgs.getKeyCode() === KeyMap.LEFT_ARROW) {
            if ($manager.IsActive(<IClassName>Tabs)) {
                const elements : ArrayList<Tabs> = <ArrayList<Tabs>>$manager.getActive(<IClassName>Tabs);
                elements.foreach(($element : Tabs) : void => {
                    if ($eventArgs.getKeyCode() === KeyMap.RIGHT_ARROW) {
                        Tabs.moveMinus($element);
                    } else if ($eventArgs.getKeyCode() === KeyMap.LEFT_ARROW) {
                        Tabs.movePlus($element);
                    }
                });
            }
        } else if ($eventArgs.getKeyCode() === KeyMap.TAB) {
            Tabs.tabPressed = true;
        }
    }

    private static resize($element : Tabs) : void {
        if ($element.Width() !== -1) {
            ElementManager.setWidth($element.Id() + "_Background", $element.Width());

            $element.itemsWidth = 0;
            $element.selectedItemWidth = 0;
            $element.beforeSelectedWidth = 0;
            let selectedFound : boolean = false;
            $element.items.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    const item : Button = <Button>$element[$propertyName];
                    const width : number = item.Width();

                    ElementManager.setCssProperty(item.Id() + "_Selector", "width",
                        width - ElementManager.getCssIntegerValue(item.Id() + "_Selector", "left") * 2);

                    $element.itemsWidth += width;
                    if (item.IsSelected()) {
                        $element.selectedItemWidth = width;
                        selectedFound = true;
                    }
                    if (!selectedFound) {
                        $element.beforeSelectedWidth += width;
                    }
                }
            });

            let envelopWidth : number = $element.itemsWidth * 2;
            if (envelopWidth < $element.Width()) {
                envelopWidth = $element.Width();
            }
            ElementManager.setWidth($element.Id() + "_Items", envelopWidth);

            let position : number = 0;
            const widthAfterSelected : number =
                $element.itemsWidth - $element.beforeSelectedWidth - $element.selectedItemWidth;
            const availableWidth : number = $element.Width() - $element.selectedItemWidth;
            if ($element.beforeSelectedWidth === 0) {
                position = 0;
            } else if (availableWidth < 0) {
                position = $element.beforeSelectedWidth;
            } else if (widthAfterSelected <= 0) {
                position = $element.itemsWidth - $element.selectedItemWidth - availableWidth;
            } else {
                position = Math.ceil($element.beforeSelectedWidth - availableWidth / 2);
            }

            Tabs.moveTo($element, position);
        }
    }

    private static selectorShowEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        if (!$manager.IsActive(<IClassName>ScrollBar) &&
            !$manager.IsActive(<IClassName>ResizeBar) &&
            !$manager.IsActive(<IClassName>DragBar)) {
            ElementManager.Show($eventArgs.Owner().Id() + "_Selector");
        }
    }

    private static selectorHideEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        ElementManager.Hide($eventArgs.Owner().Id() + "_Selector");
    }

    private static movePlus($element : Tabs) : void {
        let position : number = ElementManager.getElement($element.Id() + "_Items").offsetLeft + 15;
        if (position > 0) {
            position = 0;
        }
        Tabs.moveTo($element, position);
    }

    private static moveMinus($element : Tabs) : void {
        let position : number = ElementManager.getElement($element.Id() + "_Items").offsetLeft - 15;
        const maxPosition : number = (-1) * ($element.itemsWidth - $element.Width());
        if (position < maxPosition) {
            position = maxPosition;
        }
        Tabs.moveTo($element, position);
    }

    private static moveTo($element : Tabs, $position : number) : void {
        if ($element.Width() < $element.itemsWidth) {
            if ($position > 0) {
                $position = (-1) * $position;
            }

            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
            manipulatorArgs.Owner($element);
            if ($element.position < $position) {
                manipulatorArgs.DirectionType(DirectionType.UP);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.RangeEnd($position);
            } else {
                manipulatorArgs.DirectionType(DirectionType.DOWN);
                manipulatorArgs.RangeStart($position);
                manipulatorArgs.RangeEnd($element.position);
            }
            manipulatorArgs.ProgressType(ProgressType.LINEAR);
            manipulatorArgs.Step(5);

            manipulatorArgs.ChangeEventType(this.ClassName() + "_" + EventType.ON_CHANGE);
            manipulatorArgs.CompleteEventType(this.ClassName() + "_" + EventType.ON_COMPLETE);

            const eventHandler : IEventsHandler = ($eventArgs : ValueProgressEventArgs) : void => {
                const element : Tabs = <Tabs>$eventArgs.Owner();
                ElementManager.setCssProperty(element.Id() + "_Items", "left", $eventArgs.CurrentValue());
                element.position = $eventArgs.CurrentValue();
                Tabs.handleArrowsVisibility(element);
            };
            $element.getEventsManager().setEvent($element, this.ClassName() + "_" + EventType.ON_CHANGE, eventHandler);
            $element.getEventsManager().setEvent($element, this.ClassName() + "_" + EventType.ON_COMPLETE, eventHandler);
            ValueProgressManager.Execute(manipulatorArgs);
        } else {
            ElementManager.setCssProperty($element.Id() + "_Items", "left", 0);
            $element.position = 0;
        }
        Tabs.handleArrowsVisibility($element);
    }

    private static moveStop() : void {
        clearTimeout(Tabs.threadCallback);
        Tabs.threadTick = 0;
    }

    private static handleArrowsVisibility($element : Tabs, $force : boolean = false) : void {
        if ($element.Width() < $element.itemsWidth) {
            if ($element.position < 0) {
                if (!$element.leftArrow.Visible()) {
                    $element.leftArrow.Visible(true);
                    ElementManager.setOpacity($element.leftArrow.Id(), 0);
                    ElementManager.ChangeOpacity($element.leftArrow, DirectionType.UP, 5);
                }
            } else if ($force || !$element.getGuiManager().IsHovered($element.leftArrow)) {
                $element.getEventsManager().setEvent($element.leftArrow, OpacityEventType.COMPLETE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                            $eventArgs.Owner().Visible(false);
                        }
                    });
                ElementManager.ChangeOpacity($element.leftArrow, DirectionType.DOWN, 10);
            }

            if ($element.position > (-1) * ($element.itemsWidth - $element.Width())) {
                if (!$element.rightArrow.Visible()) {
                    $element.rightArrow.Visible(true);
                    $element.getEvents().FireAsynchronousMethod(() : void => {
                        ElementManager.setCssProperty($element.rightArrow.Id(), "left",
                            $element.Width() - ElementManager.getElement($element.rightArrow.Id()).offsetWidth);
                    });

                    ElementManager.setOpacity($element.rightArrow.Id(), 0);
                    ElementManager.ChangeOpacity($element.rightArrow, DirectionType.UP, 5);
                }
            } else if ($force || !$element.getGuiManager().IsHovered($element.rightArrow)) {
                $element.getEventsManager().setEvent($element.rightArrow, OpacityEventType.COMPLETE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                            $eventArgs.Owner().Visible(false);
                        }
                    });
                ElementManager.ChangeOpacity($element.rightArrow, DirectionType.DOWN, 10);
            }
        } else {
            $element.leftArrow.Visible(false);
            $element.rightArrow.Visible(false);
        }
    }

    private static leftArrowOnClickEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        Tabs.movePlus($eventArgs.Owner().Parent());
    }

    private static leftArrowOnMouseDownEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        const element : ImageButton = <ImageButton>$eventArgs.Owner();
        const parent : Tabs = <Tabs>element.Parent();
        TextSelectionManager.Clear();
        $manager.setActive(parent, true);
        Tabs.moveStop();
        Tabs.movePlus(parent);
        if (parent.position < 0) {
            Tabs.threadCallback = element.getEvents().FireAsynchronousMethod(() : void => {
                Tabs.leftArrowOnMouseDownEventHandler($eventArgs, $manager);
            }, Tabs.threadTick < 3 ? 100 : 30);
            Tabs.threadTick++;
        }
    }

    private static rightArrowOnClickEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        Tabs.moveMinus($eventArgs.Owner().Parent());
    }

    private static rightArrowOnMouseDownEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        const element : ImageButton = <ImageButton>$eventArgs.Owner();
        const parent : Tabs = <Tabs>element.Parent();
        TextSelectionManager.Clear();
        $manager.setActive(parent, true);
        Tabs.moveStop();
        Tabs.moveMinus(parent);
        if (parent.position > (-1) * (parent.itemsWidth - parent.Width())) {
            Tabs.threadCallback = element.getEvents().FireAsynchronousMethod(() : void => {
                Tabs.rightArrowOnMouseDownEventHandler($eventArgs, $manager);
            }, Tabs.threadTick < 3 ? 100 : 30);
            Tabs.threadTick++;
        }
    }

    private static arrowMouseUpEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager) : void {
        Tabs.moveStop();
        Tabs.handleArrowsVisibility($eventArgs.Owner().Parent(), true);
    }

    /**
     * @param {TabsType} [$tabsType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($tabsType? : TabsType, $id? : string) {
        super($id);

        this.width = -1;
        this.position = 0;
        this.items = new ArrayList<string>();
        this.getEvents().Subscriber(this.Id() + "_Type");

        this.guiType = this.guiTypeValueSetter($tabsType);

        let imageButtonClass : any = this.getImageButtonClass();
        if (ObjectValidator.IsEmptyOrNull(imageButtonClass)) {
            imageButtonClass = ImageButton;
        }
        this.leftArrow = new imageButtonClass();
        this.leftArrow.Visible(false);
        this.leftArrow.getGuiOptions().Add(GuiOptionType.DISABLE);
        this.leftArrow.DisableAsynchronousDraw();

        this.rightArrow = new imageButtonClass();
        this.rightArrow.Visible(false);
        this.rightArrow.getGuiOptions().Add(GuiOptionType.DISABLE);
        this.rightArrow.DisableAsynchronousDraw();

        this.position = 0;
    }

    /**
     * @param {string} $value Push this type of value to the tabs list.
     * @returns {boolean} Returns true, if value has been added to the tabs list, otherwise false.
     */
    public Add($value : string) : boolean {
        if (ObjectValidator.IsString($value)) {
            let buttonClass : any = this.getButtonClass();
            if (ObjectValidator.IsEmptyOrNull(buttonClass)) {
                buttonClass = Button;
            }
            const newItem : Button = new buttonClass(ButtonType.GENERAL, this.Id() + "_Tab_" + this.items.Length());
            newItem.Text($value);
            newItem.Parent(this);
            newItem.getGuiOptions().Add(GuiOptionType.SELECTED);
            newItem.getGuiOptions().Add(GuiOptionType.DISABLE);
            newItem.DisableAsynchronousDraw();

            this.items.Add(newItem.Id());
            this[newItem.Id()] = newItem;
            return true;
        }

        return false;
    }

    /**
     * @param {number} [$value] Specify element's width value.
     * If value is -1, element will has auto size, otherwise width has to be more that 100.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        this.width = Property.PositiveInteger(this.width, $value, 100);
        if (ObjectValidator.IsSet($value)) {
            if ($value === -1) {
                this.width = $value;
            }

            if (ElementManager.IsVisible(this.Id())) {
                Tabs.resize(this);
            }
        }
        return this.width;
    }

    /**
     * @param {number} $value Specify item index. Items are indexed from 0.
     * @returns {ITab} Returns tabs list item, if tab with desired index exists, otherwise null.
     */
    public getItem($value : number) : ITab {
        if (this.items.KeyExists($value)) {
            const propertyName : string = this.items.getItem($value);
            if (this.hasOwnProperty(propertyName)) {
                return <ITab>this[propertyName];
            }
        }
        return null;
    }

    /**
     * Clean up all registered items.
     * @returns {void}
     */
    public Clear() : void {
        this.items.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                this[$propertyName] = null;
            }
        });
        this.items.Clear();
    }

    /**
     * @param {string|number} $value Specify item, which should selected. Item can be selected by id or index.
     * Items are indexed from 0. If $value is equal to -1 all tabs will be deselected.
     * @returns {void}
     */
    public Select($value : string | number) : void {
        let selected : boolean = false;
        let item : Button;
        if (ObjectValidator.IsInteger($value) && $value === -1) {
            this.items.foreach(($propertyName : string, $key? : number) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    item = <Button>this[$propertyName];
                    item.IsSelected(false);
                    item.StyleClassName("NotSelected");
                }
            });
            selected = true;
        } else if (ObjectValidator.IsString($value) || ObjectValidator.IsInteger($value) && this.items.KeyExists($value)) {
            this.items.foreach(($propertyName : string, $key? : number) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    item = <Button>this[$propertyName];
                    if (ObjectValidator.IsInteger($value) && $key === $value ||
                        ObjectValidator.IsString($value) && item.Id() === $value) {
                        item.IsSelected(true);
                        item.StyleClassName("Selected");
                        selected = true;
                    } else {
                        item.IsSelected(false);
                        item.StyleClassName("NotSelected");
                    }
                }
            });
        }
        if (this.IsLoaded() && selected) {
            Tabs.resize(this);
        }
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        const enabled : boolean = super.Enabled($value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            this.items.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    const item : Button = <Button>this[$propertyName];
                    if (!enabled || (enabled && item.Enabled())) {
                        ElementManager.Enabled(item.Id(), enabled);
                        (<any>Button).resize(item);
                    }
                }
            });
            this.leftArrow.Enabled(enabled);
            this.rightArrow.Enabled(enabled);
        }
        return enabled;
    }

    /**
     * @param {TabsType} [$tabsType] Specify type of element look and feel.
     * @returns {TabsType} Returns type of element's look and feel.
     */
    public GuiType($tabsType? : TabsType) : TabsType {
        if (ObjectValidator.IsSet($tabsType)) {
            this.guiType = this.guiTypeValueSetter($tabsType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    protected innerCode() : IGuiElement {
        if (this.Width() === -1) {
            this.Width(300);
        }

        this.items.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                const item : Button = <Button>this[$propertyName];
                item.GuiType(ButtonType.GENERAL);
                item.getGuiOptions().Add(GuiOptionType.SELECTED);
                item.getGuiOptions().Add(GuiOptionType.DISABLE);

                item.getEvents().setOnFocus(Tabs.selectorShowEventHandler);
                item.getEvents().setOnBlur(Tabs.selectorHideEventHandler);

                item.getEvents().setOnMouseOver(Tabs.selectorShowEventHandler);
                item.getEvents().setOnMouseOut(Tabs.selectorHideEventHandler);
                item.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                    const element : Button = <Button>$eventArgs.Owner();
                    const parent : Tabs = <Tabs>element.Parent();
                    parent.Select(element.Id());
                });

                item.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                    const element : Button = <Button>$eventArgs.Owner();
                    element.Width(-1);
                    Tabs.resize(<Tabs>element.Parent());
                });
            }
        });

        this.leftArrow.StyleClassName("LeftArrow");
        this.leftArrow.getEvents().setOnClick(Tabs.leftArrowOnClickEventHandler);
        this.leftArrow.getEvents().setOnMouseDown(Tabs.leftArrowOnMouseDownEventHandler);
        this.leftArrow.getEvents().setOnMouseUp(Tabs.arrowMouseUpEventHandler);

        this.rightArrow.StyleClassName("RightArrow");
        this.rightArrow.getEvents().setOnClick(Tabs.rightArrowOnClickEventHandler);
        this.rightArrow.getEvents().setOnMouseDown(Tabs.rightArrowOnMouseDownEventHandler);
        this.rightArrow.getEvents().setOnMouseUp(Tabs.arrowMouseUpEventHandler);

        this.rightArrow.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            const element : ImageButton = <ImageButton>$eventArgs.Owner();
            const parent : Tabs = <Tabs>element.Parent();
            ElementManager.setCssProperty(element.Id(), "left",
                parent.Width() - ElementManager.getElement(element.Id()).offsetWidth);
        });
        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            const element : Tabs = <Tabs>$eventArgs.Owner();
            element.items.foreach(($propertyName : string) : void => {
                if (element.hasOwnProperty($propertyName)) {
                    const item : Button = <Button>element[$propertyName];
                    item.getEvents().Subscribe(item.Id() + "_Selector");
                }
            });
            Tabs.resize($eventArgs.Owner());
        });

        this.getEvents().setOnMouseDown(() : void => {
            Tabs.tabPressed = false;
        });

        WindowManager.getEvents().setOnMouseUp(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
            const tabs : ArrayList<Tabs> = <ArrayList<Tabs>>$manager.getActive(<IClassName>Tabs);
            tabs.foreach(($tab : Tabs) : void => {
                Tabs.moveStop();
                Tabs.handleArrowsVisibility($tab, true);
                $manager.setActive($tab, false);
            });
        });
        WindowManager.getEvents().setOnMouseDown(() : void => {
            Tabs.tabPressed = false;
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const itemsContainer : IGuiElement = this.addElement();
        this.items.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                const item : Button = <Button>this[$propertyName];
                itemsContainer.Add(this.addElement()
                    .StyleClassName("Item")
                    .Add(item)
                    .Add(this.addElement(item.Id() + "_Selector").StyleClassName("Selector"))
                );
            }
        });

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(GeneralCssNames.OFF)
                .Add(this.addElement(this.Id() + "_Background")
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Width(this.Width())
                    .Add(this.selectorElement())
                    .Add(this.leftArrow)
                    .Add(this.addElement(this.Id() + "_Items")
                        .StyleClassName("Items")
                        .Add(itemsContainer)
                    )
                    .Add(this.rightArrow)
                )
            );
    }

    protected guiContentId() : string {
        return this.Id() + "_Status";
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, TabsType, TabsType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!TabsType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of tabs type instead of StyleClassName method.");
        return false;
    }

    /**
     * @returns {IButton} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IButton
     */
    protected getButtonClass() : any {
        return Button;
    }

    /**
     * @returns {IImageButton} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IImageButton
     */
    protected getImageButtonClass() : any {
        return ImageButton;
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();

        this.width = -1;
        this.position = 0;
        if (ObjectValidator.IsEmptyOrNull(this.items)) {
            this.items = new ArrayList<string>();
        }
        if (ObjectValidator.IsSet(this.leftArrow)) {
            this.leftArrow.Visible(false);
        }
        if (ObjectValidator.IsSet(this.rightArrow)) {
            this.rightArrow.Visible(false);
        }
        this.getEvents().Subscriber(this.Id() + "_Type");
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        if (this.items.IsEmpty()) {
            exclude.push("items");
        }
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("itemsWidth", "selectedItemWidth", "beforeSelectedWidth");
        return exclude;
    }
}
