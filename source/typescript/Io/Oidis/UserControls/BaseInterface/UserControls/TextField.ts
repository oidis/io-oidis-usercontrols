/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { PersistenceType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PersistenceType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/IEventsHandler.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ITextField } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ITextField.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { KeyEventHandler } from "@io-oidis-gui/Io/Oidis/Gui/Utils/KeyEventHandler.js";
import { BaseField } from "../../Primitives/BaseField.js";
import { BasePanel } from "../../Primitives/BasePanel.js";
import { AutocompleteOption } from "../../Structures/AutocompleteOption.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { SelectBox } from "../Components/SelectBox.js";
import { SelectBoxType } from "../Enums/Components/SelectBoxType.js";
import { IconType } from "../Enums/UserControls/IconType.js";
import { TextFieldType } from "../Enums/UserControls/TextFieldType.js";

/**
 * TextField class renders element, which is suitable for user text input.
 */
export class TextField extends BaseField implements ITextField {
    protected autocompleteBox : SelectBox;
    private iconType : IconType;
    private passwordEnabled : boolean;
    private readOnly : boolean;
    private onlyNumberAllowed : boolean;
    private lengthLimit : number;
    private autocompleteEnabled : boolean;
    private autocompleteData : ArrayList<AutocompleteOption>;
    private autocompleteDataHash : number;
    private minAutocompleteSearchLength : number;
    private autocompleteAttributes : string;

    /**
     * @returns {void}
     */
    public static Blur() : void {
        if (!GuiObjectManager.getInstanceSingleton().IsActive(<IClassName>ScrollBar)) {
            BaseField.Blur();
        }
    }

    protected static setErrorStyle($element : TextField) : void {
        if (!$element.autocompleteBox.Visible()) {
            BaseField.setErrorStyle($element);
        } else {
            $element.autocompleteBox.getEvents().setOnHide(
                ($eventArgs : EventArgs, $manager? : GuiObjectManager) : void => {
                    const element : TextField = <TextField>$eventArgs.Owner().Parent();
                    if (!$manager.IsActive(element)) {
                        BaseField.setErrorStyle(element);
                    }
                });
        }
    }

    protected static onKeyUpEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const element : TextField = $eventArgs.Owner();
        if (!$manager.IsActive(element.autocompleteBox) || $eventArgs.getKeyCode() !== KeyMap.ENTER) {
            BaseField.onKeyUpEventHandler($eventArgs, $manager);
        }
    }

    protected static onKeyPressEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const element : TextField = <TextField>$eventArgs.Owner();
        if ($manager.IsActive(element)) {
            if (element.LengthLimit() > 0) {
                const inputElement : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(element.Id() + "_Input");
                if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) &&
                    StringUtils.Length(inputElement.value) === element.LengthLimit()) {
                    $eventArgs.PreventDefault();
                }
            }
        }
    }

    protected static resize($element : TextField) : void {
        if ($element.Width() >= TextField.minWidth) {
            const resize : any = ($id : string) : void => {
                if (ElementManager.Exists($id + "Input")) {
                    const leftSize : number = ElementManager.getOffsetWidth($id + "Left");
                    const rightSize : number = ElementManager.getOffsetWidth($id + "Right");
                    let envelopWidth : number = $element.Width() - leftSize - rightSize;
                    if (envelopWidth < 0) {
                        envelopWidth = 0;
                    }
                    ElementManager.setWidth($id + "Center", envelopWidth);

                    const inputOffset : number = ElementManager.getCssIntegerValue($id + "Envelop", "left");
                    let inputWidth : number = envelopWidth + 2 * inputOffset;
                    if (inputWidth < TextField.minWidth) {
                        inputWidth = TextField.minWidth;
                    }
                    ElementManager.setWidth($id + "Envelop", inputWidth);

                    const iconWidth : number = ElementManager.getOffsetWidth($id + "IconEnvelop");
                    ElementManager.setWidth($id + "Input", inputWidth - iconWidth);
                    if ($element.Enabled()) {
                        ElementManager.setCssProperty($id + "HintInput", "width", inputWidth - iconWidth);
                    }
                }
            };
            if ($element.Enabled() || !$element.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                resize($element.Id() + "_");
            } else {
                resize($element.Id() + "_Disabled");
            }
        }
    }

    private static fillAutocompleteBox($element : TextField) : void {
        $element.autocompleteIconVisible(true);
        const data : ArrayList<AutocompleteOption> = $element.getAutocompleteData();
        let dataHash : number = 0;
        if (!ObjectValidator.IsEmptyOrNull(data)) {
            const fieldValue : string = (<HTMLInputElement>ElementManager.getElement($element.Id() + "_Input")).value;
            const lookFor : string = StringUtils.ToLowerCase(fieldValue);
            if (ObjectValidator.IsEmptyOrNull(lookFor) ||
                StringUtils.Length(StringUtils.Remove(lookFor, " ")) >= $element.minAutocompleteSearchLength) {
                const occurrenceLevel : ArrayList<any[]> = new ArrayList<any[]>();

                $element.getAutocompleteData().foreach(($data : AutocompleteOption) : void => {
                    let available : boolean = false;
                    let text : string = $data.Text();
                    let partsInCount : number = 0;
                    if (ObjectValidator.IsEmptyOrNull(lookFor)) {
                        available = true;
                    } else if (text !== fieldValue) {
                        const lookForParts : string[] = StringUtils.Split(lookFor, " ");
                        let lengthLimitPass : boolean = false;
                        let index : number;
                        for (index = 0; index < lookForParts.length; index++) {
                            if (StringUtils.Length(lookForParts[index]) >= $element.minAutocompleteSearchLength) {
                                lengthLimitPass = true;
                                break;
                            }
                        }
                        if (lengthLimitPass) {
                            const tagsIndexes : number[][] = [];
                            const joinRanges : any = ($range1 : number[], $range2 : number[]) : boolean => {
                                let included : boolean = false;
                                if (!ObjectValidator.IsEmptyOrNull($range1) && !ObjectValidator.IsEmptyOrNull($range2)) {
                                    if ($range1[0] >= $range2[0] && $range1[1] <= $range2[1]) {
                                        included = true;
                                    } else if ($range1[0] < $range2[0] && $range1[1] >= $range2[0] && $range1[1] <= $range2[1]) {
                                        included = true;
                                        $range2[0] = $range1[0];
                                    } else if ($range1[0] > $range2[0] && $range1[0] <= $range2[1] && $range1[1] > $range2[1]) {
                                        included = true;
                                        $range2[1] = $range1[1];
                                    } else if ($range1[0] < $range2[0] && $range1[1] > $range2[1]) {
                                        included = true;
                                        $range2[0] = $range1[0];
                                        $range2[1] = $range1[1];
                                    }
                                }
                                return included;
                            };
                            for (index = 0; index < lookForParts.length; index++) {
                                if (StringUtils.ContainsIgnoreCase($data.LookupData(), lookForParts[index])) {
                                    available = true;
                                    partsInCount++;
                                    const partLength : number = StringUtils.Length(lookForParts[index]);
                                    const regExp : RegExp = new RegExp(lookForParts[index], "gi");
                                    let regResult : RegExpExecArray = regExp.exec(text);
                                    while (regResult) {
                                        const range : number[] = [
                                            regExp.lastIndex - partLength, regExp.lastIndex
                                        ];
                                        let included : boolean = false;
                                        let tagIndex : number;
                                        for (tagIndex = 0; tagIndex < tagsIndexes.length; tagIndex++) {
                                            included = joinRanges(range, tagsIndexes[tagIndex]);
                                        }
                                        if (!included) {
                                            if (tagsIndexes.indexOf(range) === -1) {
                                                tagsIndexes.push(range);
                                            }
                                        }
                                        regResult = regExp.exec(text);
                                    }
                                }
                            }

                            const tagsCount : number = tagsIndexes.length;
                            if (tagsCount > 0) {
                                const textLength : number = StringUtils.Length(text);
                                const splitIndexes : number[][] = [];
                                let minSplitValue : number = -1;
                                let variantIndex : number;
                                for (variantIndex = 0; variantIndex < tagsCount; variantIndex++) {
                                    let minValue : number = textLength;
                                    let minIndex : number = 0;
                                    let rangeIndex : number;
                                    for (rangeIndex = 0; rangeIndex < tagsCount; rangeIndex++) {
                                        if (minValue > tagsIndexes[rangeIndex][0] && minSplitValue < tagsIndexes[rangeIndex][0]) {
                                            minValue = tagsIndexes[rangeIndex][0];
                                            minIndex = rangeIndex;
                                        }
                                    }
                                    splitIndexes[variantIndex] = tagsIndexes[minIndex];
                                    minSplitValue = tagsIndexes[minIndex][0];
                                }
                                let splitIndex : number;
                                for (variantIndex = 0; variantIndex < (splitIndexes.length * 2 - 1); variantIndex++) {
                                    for (splitIndex = 0; splitIndex < splitIndexes.length - 1; splitIndex++) {
                                        if (joinRanges(splitIndexes[splitIndex], splitIndexes[splitIndex + 1])) {
                                            splitIndexes[splitIndex] = null;
                                        }
                                    }
                                }

                                const textSplit : string[] = [];
                                if (!ObjectValidator.IsEmptyOrNull(splitIndexes[0])) {
                                    if (splitIndexes[0][0] !== 0) {
                                        textSplit.push((<any>text).slice(0, splitIndexes[0][0]));
                                    }
                                }
                                const lastSplitIndex : number = splitIndexes.length - 1;
                                let nextSplitIndex : number = 0;
                                for (splitIndex = 0; splitIndex <= lastSplitIndex; splitIndex++) {
                                    if (!ObjectValidator.IsEmptyOrNull(splitIndexes[splitIndex])) {
                                        if (splitIndex > 0) {
                                            textSplit.push((<any>text).slice(nextSplitIndex, splitIndexes[splitIndex][0]));
                                        }
                                        textSplit.push(
                                            "<span class=\"LookUp\">",
                                            (<any>text).slice(splitIndexes[splitIndex][0], splitIndexes[splitIndex][1]),
                                            "</span>"
                                        );
                                        nextSplitIndex = splitIndexes[splitIndex][1];
                                    }
                                }
                                if (!ObjectValidator.IsEmptyOrNull(splitIndexes[lastSplitIndex])) {
                                    if (splitIndexes[lastSplitIndex][1] !== textLength) {
                                        textSplit.push((<any>text).slice(splitIndexes[lastSplitIndex][1], textLength));
                                    }
                                }
                                text = textSplit.join("");
                            }
                        }
                    }
                    if (available) {
                        if (!occurrenceLevel.KeyExists(partsInCount)) {
                            occurrenceLevel.Add([], partsInCount);
                        }
                        const option : object = {
                            style: $data.StyleClassName(),
                            text,
                            value: $data.Value()
                        };
                        dataHash += StringUtils.getCrc(JSON.stringify(option));
                        occurrenceLevel.getItem(partsInCount).push(option);
                    }
                });
                if (!occurrenceLevel.IsEmpty() && $element.autocompleteDataHash !== dataHash) {
                    const iconHandler : IEventsHandler = () : void => {
                        $element.autocompleteIconVisible(false);
                    };
                    let lastOption : any = (<any>$element.autocompleteBox).getItems().getLast();
                    if (!ObjectValidator.IsEmptyOrNull(lastOption)) {
                        $element.getEventsManager().RemoveHandler(lastOption.Id(), EventType.ON_COMPLETE, iconHandler);
                    }

                    $element.autocompleteBox.Clear();
                    occurrenceLevel.SortByKeyDown();
                    occurrenceLevel.foreach(($data : any[]) : void => {
                        let index : number;
                        for (index = 0; index < $data.length; index++) {
                            $element.autocompleteBox.Add(
                                $data[index].text,
                                $data[index].value,
                                $data[index].style);
                        }
                    });

                    lastOption = (<any>$element.autocompleteBox).getItems().getLast();
                    if (!ObjectValidator.IsEmptyOrNull(lastOption)) {
                        $element.getEventsManager().setEvent(lastOption.Id(), EventType.ON_COMPLETE, iconHandler);
                    }
                    $element.autocompleteDataHash = dataHash;
                } else {
                    $element.autocompleteIconVisible(false);
                }
                if ($element.autocompleteBox.OptionsCount() === 0) {
                    $element.autocompleteIconVisible(false);
                }
                $element.autocompleteBox.Visible(!occurrenceLevel.IsEmpty() && $element.autocompleteBox.OptionsCount() > 0);
            } else {
                $element.autocompleteIconVisible(false);
                $element.autocompleteBox.Visible(false);
            }
        } else {
            $element.autocompleteIconVisible(false);
        }
    }

    /**
     * @param {TextFieldType} [$textFieldType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($textFieldType? : TextFieldType, $id? : string) {
        super($textFieldType, $id);

        let selectBoxClass : any = this.getSelectBoxClass();
        if (ObjectValidator.IsEmptyOrNull(selectBoxClass)) {
            selectBoxClass = SelectBox;
        }
        this.autocompleteBox = new selectBoxClass(SelectBoxType.GENERAL, this.Id() + "_SelectBox");
        this.autocompleteAttributes = "";
    }

    /**
     * @param {TextFieldType} [$textFieldType] Specify type of element look and feel.
     * @returns {TextFieldType} Returns type of element's look and feel.
     */
    public GuiType($textFieldType? : TextFieldType) : TextFieldType {
        return <TextFieldType>super.GuiType($textFieldType);
    }

    /**
     * @param {IconType} [$value] Specify type of icon, which should be rendered with the element.
     * @returns {IconType} Returns type of icon, which belongs to the element.
     */
    public IconName($value? : IconType) : IconType {
        if (ObjectValidator.IsSet($value)) {
            this.iconType = $value;
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_IconEnvelop", this.iconType.toString());
                ElementManager.setClassName(this.Id() + "_DisabledIconEnvelop", this.iconType.toString());
            }
        }
        if (!ObjectValidator.IsSet(this.iconType)) {
            this.iconType = "";
        }

        return this.iconType;
    }

    /**
     * @param {string} [$value] Specify element group's value.
     * @returns {string} Returns element group's value.
     */
    public Value($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            if (!ObjectValidator.IsSet(this.lengthLimit)) {
                this.lengthLimit = -1;
            }
            if (this.lengthLimit > 0 && StringUtils.Length($value.toString()) > this.lengthLimit) {
                $value = StringUtils.Substring($value, 0, this.lengthLimit);
            }
            if (this.onlyNumberAllowed === true && !ObjectValidator.IsDigit($value)) {
                $value = "";
            }
        }
        return <string>super.Value($value);
    }

    /**
     * @param {number} [$value] Specify element's width value.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        let width : number = super.Width();
        if (ObjectValidator.IsSet($value) && width !== $value) {
            width = super.Width($value);
            this.autocompleteBox.Width(width);
        }
        return width;
    }

    /**
     * @param {boolean} [$value] Specify, if text field value is editable by user or not.
     * @returns {boolean} Returns true, if text filed is not editable, otherwise false.
     */
    public ReadOnly($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.readOnly)) {
            this.readOnly = false;
        }
        this.readOnly = Property.Boolean(this.readOnly, $value);
        if (this.IsCompleted()) {
            (<HTMLInputElement>ElementManager.getElement(this.Id() + "_Input")).readOnly = this.readOnly;
        }
        return this.readOnly;
    }

    /**
     * Hide user entered data.
     * @returns {void}
     */
    public setPasswordEnabled() : void {
        this.passwordEnabled = true;
    }

    /**
     * Disable show of previously submitted data.
     * @returns {void}
     */
    public setAutocompleteDisable() : void {
        this.autocompleteEnabled = false;
    }

    public AutocompleteAttributes($value? : string) : string {
        return this.autocompleteAttributes = Property.String(this.autocompleteAttributes, $value);
    }

    /**
     * @returns {void}
     */
    public setOnlyNumbersAllowed() : void {
        this.onlyNumberAllowed = true;
        this.Value(this.Value());
        this.getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
            if (!KeyEventHandler.IsEdit($eventArgs.NativeEventArgs()) &&
                !KeyEventHandler.IsInteger($eventArgs.NativeEventArgs(), $eventArgs.Owner().Id() + "_Input")) {
                $eventArgs.PreventDefault();
            }
        });
    }

    /**
     * @param {number} [$value] Specify maximal length of entered data.
     * @returns {number} Returns maximal length, which is allowed to be entered to the input.
     */
    public LengthLimit($value? : number) : number {
        if (ObjectValidator.IsSet($value) && $value === -1 || !ObjectValidator.IsSet(this.lengthLimit)) {
            this.lengthLimit = -1;
        } else {
            this.lengthLimit = Property.PositiveInteger(this.lengthLimit, $value, 1);
        }
        if (this.lengthLimit > 0 && !ObjectValidator.IsEmptyOrNull(this.Value().toString())) {
            this.Value(this.Value());
        }

        return this.lengthLimit;
    }

    /**
     * @param {ArrayList<AutocompleteOption>} [$value] Specify options, which should be used for value autocomplete.
     * @returns {void}
     */
    public setAutocompleteData($value : ArrayList<AutocompleteOption>) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.autocompleteData = $value;
        }
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, TextFieldType, TextFieldType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!TextFieldType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of textField type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        if (!ObjectValidator.IsSet(this.autocompleteEnabled)) {
            this.autocompleteEnabled = true;
        }

        this.getEvents().setOnFocus(BasePanel.ContentFocusHandler);

        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            TextField.resize($eventArgs.Owner());
        });

        this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
            TextField.resize($eventArgs.Owner());
        });

        if (this.autocompleteEnabled === true) {
            this.autocompleteBox.Visible(false);
            this.autocompleteBox.DisableCharacterNavigation();
            this.autocompleteBox.DisableAdvancedSelection();
            this.autocompleteBox.setInitSize(this.Width(), 0);
            this.autocompleteBox.Width(this.Width());
            this.minAutocompleteSearchLength = 3;
            this.autocompleteBox.MaxVisibleItemsCount(5);

            this.autocompleteBox.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                const element : SelectBox = <SelectBox>$eventArgs.Owner();
                const parent : TextField = <TextField>element.Parent();
                parent.Value(element.Value());
                parent.autocompleteEnabled = false;
                element.Visible(false);
            });

            this.getEvents().setOnDoubleClick(($eventArgs : EventArgs) : void => {
                TextField.fillAutocompleteBox(<TextField>$eventArgs.Owner());
            });

            this.getEvents().setOnBlur(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const element : TextField = <TextField>$eventArgs.Owner();
                    if (!$manager.IsActive(<IClassName>ScrollBar)) {
                        element.autocompleteBox.Visible(false);
                        element.autocompleteIconVisible(false);
                    }
                });

            this.getSelectorEvents().setEvent(EventType.ON_KEY_UP, ($eventArgs : KeyEventArgs) : void => {
                const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                const element : TextField = <TextField>$eventArgs.Owner();
                if (element.autocompleteEnabled) {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        const keyCode : number = eventArgs.getKeyCode();
                        if (keyCode !== KeyMap.ESC) {
                            if (!element.autocompleteBox.Visible() && keyCode === KeyMap.DOWN_ARROW ||
                                keyCode === KeyMap.BACKSPACE ||
                                keyCode === KeyMap.DELETE ||
                                !KeyEventHandler.IsNavigate(eventArgs.NativeEventArgs()) &&
                                keyCode !== KeyMap.ENTER) {
                                let fillEnabled : boolean = true;
                                if (!element.autocompleteBox.Visible() &&
                                    (keyCode === 17 || keyCode === 18 || keyCode === 19 || keyCode === 20) /* Ctrl */ ||
                                    (eventArgs.NativeEventArgs().ctrlKey && keyCode === 65)/* Ctrl+a */) {
                                    fillEnabled = false;
                                } else if (keyCode === KeyMap.BACKSPACE || keyCode === KeyMap.DELETE) {
                                    const value : string = StringUtils.Remove(
                                        (<HTMLInputElement>ElementManager.getElement(element.Id() + "_Input")).value, " ");
                                    if (StringUtils.Length(value) < element.minAutocompleteSearchLength) {
                                        fillEnabled = false;
                                    }
                                }
                                if (fillEnabled) {
                                    TextField.fillAutocompleteBox(element);
                                } else {
                                    element.autocompleteBox.Visible(false);
                                }
                            }
                        } else {
                            element.autocompleteBox.Visible(false);
                        }
                    }, 30);
                } else {
                    element.autocompleteEnabled = true;
                }
            });
        }

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const width95 : number = Math.ceil(this.Width() * 0.95);
        const input : HTMLInputElement = document.createElement("input");
        input.id = this.Id() + "_Input";
        input.name = this.Id();
        input.style.width = width95 + "px";

        if (!ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
            input.tabIndex = this.TabIndex();
        }
        if (this.autocompleteEnabled === false) {
            input.autocomplete = "off";
        } else if (!ObjectValidator.IsEmptyOrNull(this.autocompleteAttributes)) {
            input.autocomplete = this.autocompleteAttributes;
        }
        if (this.ReadOnly()) {
            input.readOnly = true;
        }

        let fieldType : string = "text";
        if (this.passwordEnabled === true) {
            fieldType = "password";
        }
        input.type = fieldType;
        input.value = "";

        const hintInput : HTMLInputElement = document.createElement("input");
        hintInput.id = this.Id() + "_HintInput";
        hintInput.className = "Hint";
        hintInput.type = "text";
        hintInput.style.width = width95 + "px";
        hintInput.style.display = "none";
        hintInput.readOnly = true;
        hintInput.disabled = true;
        hintInput.value = "";
        if (!ObjectValidator.IsEmptyOrNull(this.autocompleteAttributes)) {
            hintInput.autocomplete = this.autocompleteAttributes;
        }

        const inputWrap : HTMLFormElement = document.createElement("form");
        inputWrap.appendChild(hintInput);
        inputWrap.appendChild(input);

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            const disabledInput : HTMLInputElement = document.createElement("input");
            disabledInput.id = this.Id() + "_DisabledInput";
            disabledInput.type = fieldType;
            disabledInput.style.width = width95 + "px";
            disabledInput.readOnly = true;
            disabledInput.disabled = true;
            disabledInput.value = "";
            const disabledWrap : HTMLFormElement = document.createElement("form");
            disabledWrap.appendChild(disabledInput);

            disabledOption
                .Id(this.Id() + "_Disabled").StyleClassName(GeneralCssNames.DISABLE)
                .Visible(!this.Enabled())
                .GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_DisabledBackground")
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_DisabledLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_DisabledCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(width95)
                        .Add(this.addElement(this.Id() + "_DisabledEnvelop")
                            .StyleClassName("Envelop")
                            .Add(this.addElement(this.Id() + "_DisabledIconEnvelop")
                                .StyleClassName(this.IconName())
                                .Add(this.addElement(this.Id() + "_DisabledIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                            .Add(this.addElement()
                                .setAttribute("float", "left")
                                .setAttribute("position", "relative")
                                .Add(disabledWrap)
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_DisabledRight").StyleClassName(GeneralCssNames.RIGHT))
                );
        }

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .Visible(this.Enabled())
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.addElement(this.Id() + "_Background")
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_Center")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(width95)
                            .Add(this.addElement(this.Id() + "_Envelop")
                                .StyleClassName("Envelop")
                                .Add(this.addElement(this.Id() + "_IconEnvelop")
                                    .StyleClassName(this.IconName())
                                    .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
                                )
                                .Add(this.addElement()
                                    .setAttribute("float", "left")
                                    .setAttribute("position", "relative")
                                    .Add(inputWrap)
                                )
                            )
                        )
                        .Add(this.addElement(this.Id() + "_Right")
                            .StyleClassName(GeneralCssNames.RIGHT)
                            .Add(this.addElement(this.Id() + "_RightIconEnvelop")
                                .StyleClassName(this.autocompleteEnabled === true ? this.getAutocompleteIconName() : "")
                                .Add(this.addElement(this.Id() + "_RightIcon").StyleClassName(GeneralCssNames.ICON))
                            )
                        )
                    )
                    .Add(this.autocompleteEnabled === true ? this.autocompleteBox : "")
                )
                .Add(disabledOption)
            );
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("readOnly", "passwordEnabled", "autocompleteEnabled", "onlyNumberAllowed", "lengthLimit");
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("autocompleteData", "autocompleteDataHash", "minAutocompleteSearchLength");

        if (this.readOnly === false) {
            exclude.push("readOnly");
        }
        if (this.passwordEnabled === false) {
            exclude.push("passwordEnabled");
        }
        if (this.autocompleteEnabled === true) {
            exclude.push("autocompleteEnabled");
        }
        if (this.lengthLimit === -1) {
            exclude.push("lengthLimit");
        }
        if (this.onlyNumberAllowed === false) {
            exclude.push("onlyNumberAllowed");
        }
        if (ObjectValidator.IsEmptyOrNull(this.iconType)) {
            exclude.push("iconType");
        }
        return exclude;
    }

    /**
     * @returns {ISelectBox} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.ISelectBox
     */
    protected getSelectBoxClass() : any {
        return SelectBox;
    }

    protected getAutocompleteIconName() : IconType {
        return IconType.SPINNER_SMALL;
    }

    private getAutocompleteData() : ArrayList<AutocompleteOption> {
        if (!ObjectValidator.IsEmptyOrNull(this.autocompleteData)) {
            return this.autocompleteData;
        }
        const autofillPersistence : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.AUTOFILL);
        if (autofillPersistence.Exists(this.Id())) {
            this.autocompleteData = <ArrayList<AutocompleteOption>>autofillPersistence.Variable(this.Id());
        } else {
            this.autocompleteData = new ArrayList<AutocompleteOption>();
        }
        return this.autocompleteData;
    }

    private autocompleteIconVisible($value : boolean) : void {
        if (this.IsCompleted()) {
            if ($value && this.autocompleteEnabled) {
                ElementManager.setClassName(this.Id() + "_RightIconEnvelop", <string>this.getAutocompleteIconName());
                ElementManager.Show(this.Id() + "_RightIcon");
            } else {
                ElementManager.Hide(this.Id() + "_RightIcon");
            }
        }
    }
}
