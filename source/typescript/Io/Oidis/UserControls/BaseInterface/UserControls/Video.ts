/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { VideoEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/VideoEventType.js";
import { IVideoEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IVideoEvents.js";
import { IMediaSource } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IMediaSource.js";
import { IVideo } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IVideo.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { Audio } from "./Audio.js";

export class Video extends Audio implements IVideo {
    private fullscreen : boolean;

    constructor($id? : string) {
        super($id);
        this.fullscreen = false;
    }

    public getEvents() : IVideoEvents {
        return <IVideoEvents>super.getEvents();
    }

    public setSize($width : number, $height : number) : void {
        this.getControl(($element : HTMLVideoElement) : void => {
            $element.width = $width;
            $element.height = $height;
            ElementManager.Scale($element, $width, $height);
        });
    }

    public FullScreen($value? : boolean) : boolean {
        this.fullscreen = Property.Boolean(this.fullscreen, $value);
        this.getControl(($element : HTMLVideoElement) : void => {
            if (this.fullscreen) {
                $element.requestFullscreen().then(() : void => {
                    this.fireEvent(VideoEventType.ON_FULLSCREEN_START, "This video is in fullscreen now.");
                }, ($reason : any) : void => {
                    this.fireEvent(VideoEventType.ON_ERROR, "There was an error while getting into fullscreen: " + $reason);
                });
            } else {
                if (document.fullscreenEnabled) {
                    document.exitFullscreen().then(() : void => {
                        this.fireEvent(VideoEventType.ON_FULLSCREEN_END, "This video out of fullscreen now.");
                    }, ($reason : any) : void => {
                        this.fireEvent(VideoEventType.ON_ERROR, "There was an error while getting out of fullscreen: " + $reason);
                    });
                }
            }
        });
        return this.fullscreen;
    }

    protected getControl($callback : ($element : HTMLVideoElement) => void) : void {
        super.getControl($callback);
    }

    protected getControlTag() : string {
        return "video";
    }

    protected getMime($source : IMediaSource) : string {
        let codecs : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull($source.codecs)) {
            codecs = $source.codecs;
        } else {
            switch ($source.type) {
            case "video/mp4":
                codecs = ["avc1.42C01E", "mp4a.40.2"];
                break;
            case "video/webm":
                codecs = ["vorbis", "vp8"];
                break;
            default:
                LogIt.Warning("Unrecognized video type " + $source.type + " so codes has not been added.");
                break;
            }
        }
        let mime : string = $source.type + ";";
        if (!ObjectValidator.IsEmptyOrNull(codecs)) {
            mime += " codecs=\"" + codecs.join(",") + "\"";
        }
        return mime;
    }
}
