/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IPanelHolderStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Strategies/IPanelHolderStrategy.js";
import { IAccordion } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IAccordion.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BasePanel } from "../../Primitives/BasePanel.js";
import { BasePanelHolder } from "../../Primitives/BasePanelHolder.js";
import { BasePanelHolderViewer } from "../../Primitives/BasePanelHolderViewer.js";
import { BasePanelHolderViewerArgs } from "../../Primitives/BasePanelHolderViewerArgs.js";
import { HorizontalPanelHolderStrategy } from "../../Strategies/HorizontalPanelHolderStrategy.js";
import { VerticalPanelHolderStrategy } from "../../Strategies/VerticalPanelHolderStrategy.js";
import { AccordionResizeType } from "../Enums/UserControls/AccordionResizeType.js";
import { AccordionType } from "../Enums/UserControls/AccordionType.js";

/**
 * Accordion class renders accordion type of element.
 */
export class Accordion extends BasePanel implements IAccordion {
    private static defaultWidth : number = 785;
    private static defaultHeight : number = 530;

    protected orientation : OrientationType;
    private guiType : AccordionType;
    private holders : ArrayList<string>;
    private resizeType : AccordionResizeType;
    private activeHolders : ArrayList<BasePanelHolder>;
    private queue : () => void;

    public static recomputeContentSizes($element : Accordion) : void {
        if ($element.ResizeType() !== AccordionResizeType.RESPONSIVE) {
            let child : BasePanelHolder = null;
            $element.holders.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    child = <BasePanelHolder>$element[$propertyName];
                    if (ObjectValidator.IsEmptyOrNull(child.PrioritySize())) {
                        const contentSize : number = child.Strategy().getContentBasedSize(child);
                        child.OpenedSize(contentSize);
                        child.ScaledOpenedSize(contentSize);
                    }
                }
            });
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            Accordion.resize($element);
        }
    }

    protected static contentResizeHandler($panel : Accordion, $width : number, $height : number) : void {
        super.contentResizeHandler($panel, $width, $height);
        let firstScalableId : string = null;
        let child : BasePanelHolder = null;
        const sizeInfo : ISizeInfo = Accordion.getSizeInfo($panel);
        const normalizedSizes : any = {};

        let remainingSize : number = sizeInfo.parentSize;
        $panel.holders.foreach(($propertyName : string) : void => {
            if ($panel.hasOwnProperty($propertyName)) {
                child = <BasePanelHolder>$panel[$propertyName];
                if (ElementManager.IsVisible(child.Id())) {
                    if (child.IsScalable() && $panel.ResizeType() === AccordionResizeType.RESPONSIVE &&
                        ObjectValidator.IsEmptyOrNull($panel.activeHolders.getItem(child.Id()))) {
                        normalizedSizes[$propertyName] = Math.ceil(((child.IsOpened() ? child.OpenedSize() :
                            child.getTotalSize()) - child.getHeaderSize()) * sizeInfo.mult + child.getHeaderSize());
                    } else {
                        normalizedSizes[$propertyName] = child.getTotalSize();
                    }
                    firstScalableId = ObjectValidator.IsEmptyOrNull(firstScalableId) &&
                    normalizedSizes[child.Id()] !== child.getHeaderSize() && child.IsScalable() ? child.Id() : firstScalableId;
                    remainingSize -= normalizedSizes[$propertyName] + (ElementManager.IsVisible(child.Id() + "_Separator") ?
                        ($panel.orientation === OrientationType.HORIZONTAL ?
                            ElementManager.getEnvelopWidth(child.Id() + "_Separator") :
                            ElementManager.getEnvelopHeight(child.Id() + "_Separator")) : 0);
                }
            }
        });

        let offsetApplyId : string = null;
        if ($panel.ResizeType() === AccordionResizeType.RESPONSIVE) {
            const sizeOverflow : number = sizeInfo.totalSize - sizeInfo.parentSize;
            if (sizeInfo.mult < 1 && Math.abs(remainingSize) <= 1 && sizeOverflow > 0) {
                offsetApplyId = firstScalableId;
            } else {
                remainingSize = 0;
            }
        }

        if ($panel.orientation === OrientationType.HORIZONTAL) {
            if ($panel.ResizeType() !== AccordionResizeType.RESPONSIVE) {
                ElementManager.setCssProperty($panel.Id() + "_PanelContent", "width", sizeInfo.totalSize + "px");
                ElementManager.setCssProperty($panel.Id() + "_Type", "width", sizeInfo.totalSize + "px");
            }
            ElementManager.setCssProperty($panel.Id() + "_PanelContent", "height", "100%");
            ElementManager.setCssProperty($panel.Id() + "_Type", "height", "100%");
        } else {
            ElementManager.ClearCssProperty($panel.Id() + "_PanelContent", "height");
            ElementManager.ClearCssProperty($panel.Id() + "_PanelContent", "width");
            ElementManager.ClearCssProperty($panel.Id() + "_Type", "height");
            ElementManager.ClearCssProperty($panel.Id() + "_Type", "width");
        }

        $panel.holders.foreach(($propertyName : string) : void => {
            if ($panel.hasOwnProperty($propertyName)) {
                child = <BasePanelHolder>$panel[$propertyName];
                if (ElementManager.IsVisible(child.Id()) && !ObjectValidator.IsEmptyOrNull(normalizedSizes[$propertyName])) {
                    const targetSize : number = normalizedSizes[$propertyName] + ($propertyName === offsetApplyId ? remainingSize : 0);
                    if ($panel.ResizeType() === AccordionResizeType.RESPONSIVE ||
                        ObjectValidator.IsEmptyOrNull($panel.activeHolders) ||
                        !ObjectValidator.IsEmptyOrNull($panel.activeHolders.getItem(child.Id()))) {
                        if ((child.IsOpened() || $propertyName === offsetApplyId) &&
                            (ObjectValidator.IsEmptyOrNull($panel.activeHolders.getItem(child.Id())) ||
                                child.getTotalSize() === child.ScaledOpenedSize())) {
                            child.ScaledOpenedSize(targetSize);
                        }
                        child.Strategy().ResizeHolder(child, targetSize);
                    }
                }
            }
        });

        $panel.getEvents().FireAsynchronousMethod(() : void => {
            if ($panel.activeHolders.Length() === 1 && $panel.activeHolders.getFirst().StyleClassName() === "Last" &&
                $panel.ResizeType() === AccordionResizeType.DEFAULT) {
                if ($panel.orientation === OrientationType.HORIZONTAL) {
                    BasePanel.scrollLeft($panel, 100);
                } else {
                    BasePanel.scrollTop($panel, 100);
                }
            } else if ($panel.Scrollable()) {
                BasePanel.scrollToCurrentPosition($panel);
            }
        });
    }

    private static getSizeInfo($element : Accordion,
                               $openHolders : ArrayList<BasePanelHolder> = Accordion.getOpenHolders($element)) : ISizeInfo {
        let child : BasePanelHolder;
        let scalableSize : number = 0;
        let totalSize : number = 0;
        const parentSize : number = $element.orientation === OrientationType.HORIZONTAL ?
            ElementManager.getCssIntegerValue($element.Id(), "width") :
            ElementManager.getCssIntegerValue($element.Id(), "height");
        $element.holders.foreach(($propertyName : string) : void => {
            if ($element.hasOwnProperty($propertyName)) {
                child = <BasePanelHolder>$element[$propertyName];
                if (ElementManager.IsVisible(child.Id())) {
                    const separatorSize : number = ElementManager.IsVisible(child.Id() + "_Separator") ?
                        ($element.orientation === OrientationType.HORIZONTAL ?
                            ElementManager.getEnvelopWidth(child.Id() + "_Separator") :
                            ElementManager.getEnvelopHeight(child.Id() + "_Separator")) : 0;
                    if ($element.ResizeType() === AccordionResizeType.RESPONSIVE) {
                        if (ObjectValidator.IsEmptyOrNull(child.PrioritySize())) {
                            child.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PCT}));
                        }
                        child.OpenedSize(child.PrioritySize().Normalize(parentSize, UnitType.PX));
                    }
                    if ($openHolders.Contains(child)) {
                        if (child.IsScalable() && $element.ResizeType() === AccordionResizeType.RESPONSIVE) {
                            scalableSize += child.OpenedSize() - child.getHeaderSize();
                        }
                        totalSize += child.OpenedSize();
                    } else {
                        totalSize += child.getTotalSize();
                    }
                    totalSize += separatorSize;
                }
            }
        });
        const mult : number = scalableSize === 0 ? 1 : Math.min(1, 1 - (totalSize - parentSize) / scalableSize);
        return {totalSize, parentSize, scalableSize, mult};
    }

    private static normalizeHolderSizes($element : Accordion) : void {
        const sizeInfo : ISizeInfo = Accordion.getSizeInfo($element, Accordion.getOpenHolders($element, $element.activeHolders));
        let child : BasePanelHolder = null;
        $element.holders.foreach(($propertyName : string) : void => {
            if ($element.hasOwnProperty($propertyName)) {
                child = <BasePanelHolder>$element[$propertyName];
                const bodySize : number = child.OpenedSize() - child.getHeaderSize();
                child.ScaledOpenedSize(Math.ceil(bodySize * (child.IsScalable() &&
                    $element.ResizeType() === AccordionResizeType.RESPONSIVE ? sizeInfo.mult : 1) +
                    child.getHeaderSize()));
            }
        });
    }

    private static setOpened($element : Accordion, $value : boolean, $filter? : Array<number | IClassName>,
                             $isExclusive? : boolean) : void {
        let index : number = 0;
        if (!$element.getGuiManager().IsActive($element)) {
            const filterList : ArrayList<number | IClassName> = ArrayList.ToArrayList($filter);
            const openFilter : ArrayList<number | IClassName> = new ArrayList<number>();
            const closeFilter : ArrayList<number | IClassName> = new ArrayList<number>();
            $element.holders.foreach(($propertyName : string) : void => {
                if ($element.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = <BasePanelHolder>$element[$propertyName];
                    if ((ObjectValidator.IsEmptyOrNull($filter) ||
                        filterList.Contains(index) ||
                        filterList.Contains(Reflection.getInstance().getClass(holder.getBody().getClassName())))) {
                        if ($isExclusive) {
                            if ($value) {
                                if (!holder.IsOpened()) {
                                    openFilter.Add(index);
                                }
                            } else {
                                if (holder.IsOpened()) {
                                    closeFilter.Add(index);
                                }
                            }
                        } else {
                            holder.IsOpened($value);
                        }
                    } else if ($isExclusive) {
                        if (holder.IsOpened()) {
                            closeFilter.Add(index);
                        }
                    }
                    index++;
                }
            });
            if ($isExclusive) {
                if (closeFilter.IsEmpty()) {
                    if (!openFilter.IsEmpty()) {
                        this.setOpened($element, true, openFilter.ToArray(), false);
                    }
                } else {
                    this.setOpened($element, false, closeFilter.ToArray(), false);
                    if (!openFilter.IsEmpty()) {
                        $element.queue = () : void => {
                            this.setOpened($element, true, openFilter.ToArray(), false);
                            $element.queue = null;
                        };
                    }
                }
            }
        } else {
            $element.queue = () : void => {
                this.setOpened($element, $value, $filter, $isExclusive);
                $element.queue = null;
            };
        }
    }

    private static getOpenHolders($element : Accordion,
                                  $pseudoOpened? : ArrayList<BasePanelHolder>) : ArrayList<BasePanelHolder> {
        const holders : ArrayList<BasePanelHolder> = new ArrayList<BasePanelHolder>();
        let child : BasePanelHolder = null;
        $element.holders.foreach(($propertyName : string) : void => {
            if ($element.hasOwnProperty($propertyName)) {
                child = <BasePanelHolder>$element[$propertyName];
                if (child.IsOpened()) {
                    holders.Add(child, child.Id());
                }
            }
        });
        if (!ObjectValidator.IsEmptyOrNull($pseudoOpened)) {
            $pseudoOpened.foreach(($element : BasePanelHolder) : void => {
                holders.Add($element, $element.Id());
            });
        }
        return holders;
    }

    /**
     * @param {ArrayList<BasePanelHolderViewerArgs>} $args Specify arguments for panel holders,
     * which should be registered by the constructor.
     * @param {AccordionType} [$accordionType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($args : ArrayList<BasePanelHolderViewerArgs>, $accordionType? : AccordionType, $id? : string) {
        super($id);
        this.Width(Accordion.defaultWidth);
        this.Height(Accordion.defaultHeight);
        this.guiType = this.guiTypeValueSetter($accordionType);
        this.holders = new ArrayList<string>();
        this.activeHolders = new ArrayList<BasePanelHolder>();
        const holderViewerClass : BasePanelHolderViewer = this.getPanelHolderViewerClass();

        if (!ObjectValidator.IsEmptyOrNull($args)) {
            $args.foreach(($holderArgs : BasePanelHolderViewerArgs, $key? : number) : void => {
                if (!ObjectValidator.IsEmptyOrNull($holderArgs) && $holderArgs.IsMemberOf(BasePanelHolderViewerArgs) &&
                    !ObjectValidator.IsEmptyOrNull($holderArgs.HolderViewerClass())) {
                    this.holders.Add(null, $key.toString());
                    this.addChildPanel(holderViewerClass, $holderArgs, ($parent : Accordion, $child : BasePanelHolder,
                                                                        $childIndex? : number) : void => {
                        const propertyName : string = $child.Id();
                        $parent[propertyName] = $child;
                        $child.IsResizeDelegated(true);
                        $child.Strategy(this.getHolderStrategy(this.guiType));
                        $parent.holders.Add(propertyName, $childIndex.toString());
                    });
                }
            });
        }

        this.horizontalScrollBar.StyleClassName(this.getClassNameWithoutNamespace() + "Bar");
        this.verticalScrollBar.StyleClassName(this.getClassNameWithoutNamespace() + "Bar");
        this.resizeType = AccordionResizeType.DEFAULT;
        this.Scrollable(true);
    }

    public Width($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            super.Width($value);
            if (this.IsLoaded()) {
                Accordion.normalizeHolderSizes(this);
            }
        }
        return super.Width();
    }

    public Height($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            super.Height($value);
            if (this.IsLoaded()) {
                Accordion.normalizeHolderSizes(this);
            }
        }
        return super.Height();
    }

    /**
     * @param {any} [$accordionType] Specify type of element look and feel.
     * @returns {any} Returns type of element's look and feel.
     */
    public GuiType($accordionType? : AccordionType) : AccordionType {
        if (ObjectValidator.IsSet($accordionType)) {
            this.guiType = this.guiTypeValueSetter($accordionType);
            const strategy : IPanelHolderStrategy = this.getHolderStrategy(this.guiType);
            this.holders.foreach(($propertyName : string) : void => {
                if (this.hasOwnProperty($propertyName)) {
                    const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                    holder.Strategy(strategy);
                }
            });
            if (this.IsLoaded()) {
                Accordion.scrollTop(this, 0);
                Accordion.scrollLeft(this, 0);
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                Accordion.normalizeHolderSizes(this);
                Accordion.resize(this);
            }
        }
        return this.guiType;
    }

    /**
     * @param {ResizeType} [$value] Specify how resizing should be handled.
     * @returns {ResizeType} Returns type of resize that should be applied.
     */
    public ResizeType($value? : AccordionResizeType) : AccordionResizeType {
        if (ObjectValidator.IsSet($value)) {
            this.resizeType = $value;
        }
        return this.resizeType;
    }

    /**
     * @param {ArrayList<BasePanelHolderViewerArgs>} $args Specify panel holder's arguments,
     * which should be passed to the registered panel holders.
     * @returns {void}
     */
    public setPanelHoldersArgs($args : ArrayList<BasePanelHolderViewerArgs>) : void {
        if (!ObjectValidator.IsEmptyOrNull($args)) {
            let childIndex : number = 0;
            this.holders.foreach(($propertyName : string) : boolean => {
                if (this.hasOwnProperty($propertyName)) {
                    if ($args.KeyExists(childIndex)) {
                        this.setChildPanelArgs(
                            (<BasePanelHolder>this[$propertyName]).Id(), $args.getItem(childIndex));
                        childIndex++;
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        }
    }

    /**
     * Clean up all registered panel holders
     * @returns {void}
     */
    public Clear() : void {
        this.holders.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                this[$propertyName] = null;
            }
        });
        this.holders.Clear();
    }

    /**
     * Collapses all registered or index/ClassName defined panel holders
     * @param {Array<number | IClassName>} [$filter] Specify list of panel which should be collapsed
     * @returns {void}
     */
    public Collapse(...$filter : Array<number | IClassName>) : void {
        Accordion.setOpened(this, false, $filter);
    }

    /**
     * Opens all registered or index/ClassName defined, panel holders
     * @param {Array<number | IClassName>} [$filter] Specify list of panels which should be expanded
     * @returns {void}
     */
    public Expand(...$filter : Array<number | IClassName>) : void {
        Accordion.setOpened(this, true, $filter);
    }

    /**
     * Opens all registered or index/ClassName defined, panel holders exclusively - closing others.
     * @param {Array<number | IClassName>} [$filter] Specify list of panels which should be expanded
     * @returns {void}
     */
    public ExpandExclusive(...$filter : Array<number | IClassName>) : void {
        Accordion.setOpened(this, true, $filter, true);
    }

    /**
     * @param {number|IClassName} $filter Specify filter which should be applied on panel search algorythm
     * @returns {any} Returns panel tied to index or ClassName
     */
    public getPanel($filter : number | IClassName) : any {
        let index : number = 0;
        let panel : any = null;
        this.holders.foreach(($propertyName : string) : boolean => {
            if (this.hasOwnProperty($propertyName)) {
                const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                if ($filter === index || $filter === Reflection.getInstance().getClass(holder.getBody().getClassName())) {
                    panel = holder.getBody();
                    return false;
                }
                index++;
            }
        });
        return panel;
    }

    protected getPanelHolderViewerClass() : any {
        return BasePanelHolderViewer;
    }

    protected guiTypeValueSetter($value : any) : any {
        this.guiType = Property.EnumType(this.guiType, $value, AccordionType, AccordionType.HORIZONTAL);

        if (ObjectValidator.IsSet($value)) {
            switch (this.guiType) {
            case AccordionType.HORIZONTAL:
                this.orientation = OrientationType.HORIZONTAL;
                break;
            case AccordionType.VERTICAL:
                this.orientation = OrientationType.VERTICAL;
                break;
            default:
                this.orientation = OrientationType.HORIZONTAL;
            }
        }

        return this.guiType;
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!AccordionType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of accordion type instead of StyleClassName method.");
        return false;
    }

    protected cssInterfaceName() : string {
        return Reflection.getInstance().getClass(this.getClassName()).NamespaceName();
    }

    protected innerCode() : IGuiElement {
        let holderIndex : number = 0;
        let completedPanelNum : number = 0;
        const openedHolders : number[] = [];
        const lastHolderIndex : number = this.holders.Length() - 1;

        this.DisableAsynchronousDraw();

        this.getEvents().setOnResize(() : void => {
            if (ElementManager.IsVisible(this.Id() + "_Loader")) {
                ElementManager.setHeight(this.Id() + "_Loader",
                    ElementManager.getCssIntegerValue(this.Id() + "_PanelContentEnvelop", "height"));
                ElementManager.setWidth(this.Id() + "_Loader",
                    ElementManager.getCssIntegerValue(this.Id() + "_PanelContentEnvelop", "width"));
            }
        });

        this.holders.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                if (holder.IsOpened() && holder.getBodySize() === 0) {
                    openedHolders.push(holderIndex);
                    holder.IsOpened(false);
                }

                if (!holder.IsLoaded()) {
                    if (holderIndex === 0) {
                        if (ObjectValidator.IsEmptyOrNull(holder.StyleClassName())) {
                            holder.StyleClassName("First");
                        } else {
                            holder.StyleClassName(holder.StyleClassName() + " First");
                        }
                    } else if (holderIndex === lastHolderIndex) {
                        if (ObjectValidator.IsEmptyOrNull(holder.StyleClassName())) {
                            holder.StyleClassName("Last");
                        } else {
                            holder.StyleClassName(holder.StyleClassName() + " Last");
                        }
                    }
                }

                if (this.orientation === OrientationType.VERTICAL) {
                    holder.Width(this.Width());
                } else {
                    holder.Height(this.Height());
                }

                holder.getEvents().setOnComplete(() : void => {
                    completedPanelNum++;
                    if (completedPanelNum === this.holders.Length()) {
                        ElementManager.Hide(this.Id() + "_LoaderIcon");
                        const hideLoader : any = () : void => {
                            ElementManager.ChangeOpacity(this.Id() + "_Loader", DirectionType.DOWN, 10,
                                () : void => {
                                    ElementManager.Hide(this.Id() + "_Loader");
                                });
                        };
                        if (openedHolders.length !== 0) {
                            this.getEventsManager().FireAsynchronousMethod(() : void => {
                                hideLoader();
                                this.Expand.apply(this, openedHolders); // eslint-disable-line prefer-spread
                            }, 100);
                        } else {
                            hideLoader();
                        }
                    }
                });

                holder.getEvents().setBeforeOpen(() : void => {
                    this.activeHolders.Add(holder, holder.Id());
                    Accordion.normalizeHolderSizes(this);
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        this.getGuiManager().setActive(this, true);
                    });
                });

                holder.getEvents().setBeforeClose(() : void => {
                    this.activeHolders.Add(holder, holder.Id());
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        this.getGuiManager().setActive(this, true);
                    });
                });

                const onChangeEnd : any = ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const parent : Accordion = $eventArgs.Owner().Parent();
                    parent.activeHolders.RemoveAt(parent.activeHolders.IndexOf($eventArgs.Owner()));

                    const getFirstQueuedHolder : any = () : BasePanelHolder => {
                        let minTime : number = Number.POSITIVE_INFINITY;
                        let queuedHolder : BasePanelHolder = null;
                        parent.holders.foreach(($propertyName : string) : void => {
                            if (this.hasOwnProperty($propertyName)) {
                                const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                                const queue : any = holder.getQueue();
                                if (!ObjectValidator.IsEmptyOrNull(holder.getQueue())) {
                                    if (queue.time < minTime) {
                                        minTime = queue.time;
                                        queuedHolder = holder;
                                    }
                                }
                            }
                        });
                        return queuedHolder;
                    };

                    if (parent.activeHolders.IsEmpty()) {
                        const queuedHolder : BasePanelHolder = getFirstQueuedHolder();
                        parent.getGuiManager().setActive(parent, false);
                        parent.activeHolders.Clear();

                        if (ObjectValidator.IsEmptyOrNull(queuedHolder) && ObjectValidator.IsEmptyOrNull(parent.queue)) {
                            if (parent.Scrollable()) {
                                Accordion.resize(parent);
                            }
                        }

                        if (!ObjectValidator.IsEmptyOrNull(queuedHolder)) {
                            queuedHolder.getQueue().handler();
                        } else if (ObjectValidator.IsFunction(parent.queue)) {
                            parent.queue();
                        }
                    }

                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                };

                holder.getEvents().setOnOpen(onChangeEnd);
                holder.getEvents().setOnClose(onChangeEnd);
                holder.getEvents().setOnChange(() : void => {
                    if (this.activeHolders.IsEmpty() || this.activeHolders.getFirst() === holder) {
                        Accordion.resize(this);
                    }
                });
                holderIndex++;
            }
        });
        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const output : IGuiElement = this.addElement(this.Id() + "_Type").StyleClassName(this.guiType).GuiTypeTag(this.getGuiTypeTag());
        let index : number = 0;
        this.holders.foreach(($propertyName : string) : void => {
            if (this.hasOwnProperty($propertyName)) {
                const holder : BasePanelHolder = <BasePanelHolder>this[$propertyName];
                output
                    .Add(this.addElement(holder.Id() + "_Separator")
                        .StyleClassName("Separator")
                        .Visible(index > 0)
                    )
                    .Add(holder);
                index++;
            }
        });
        output.Add(this.addElement(this.Id() + "_Loader").StyleClassName("Loader")
            .Add(this.addElement(this.Id() + "_LoaderBackground").StyleClassName(GeneralCssNames.BACKGROUND))
            .Add(this.addElement(this.Id() + "_LoaderIcon").StyleClassName(GeneralCssNames.ICON)));
        return output;
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.Width(Accordion.defaultWidth);
        this.Height(Accordion.defaultHeight);
        if (!ObjectValidator.IsSet(this.holders)) {
            this.holders = new ArrayList<string>();
        }
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        if (ObjectValidator.IsEmptyOrNull(this.holders)) {
            exclude.push("holders");
        }
        return exclude;
    }

    /**
     * @returns {IIcon} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
     */
    protected getLoaderIconClass() : any {
        return null;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getLoaderTextClass() : any {
        return null;
    }

    /**
     * @param {AccordionType} [$accordionType] Specify type of element look and feel.
     * @returns {IPanelHolderStrategy} Returns strategy of said type.
     */
    protected getHolderStrategy($accordionType : AccordionType) : IPanelHolderStrategy {
        switch ($accordionType) {
        case AccordionType.HORIZONTAL:
            return new HorizontalPanelHolderStrategy();
        case AccordionType.VERTICAL:
            return new VerticalPanelHolderStrategy();
        default:
            return new VerticalPanelHolderStrategy();
        }
    }
}

export interface ISizeInfo {
    totalSize : number;
    parentSize : number;
    scalableSize : number;
    mult : number;
}

// generated-code-start
export const ISizeInfo = globalThis.RegisterInterface(["totalSize", "parentSize", "scalableSize", "mult"]);
// generated-code-end
