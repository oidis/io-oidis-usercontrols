/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IIcon } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IIcon.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { FormsObject } from "../../Primitives/FormsObject.js";
import { IconType } from "../Enums/UserControls/IconType.js";

/**
 * Icon class renders icon element as independent user control.
 */
export class Icon extends FormsObject implements IIcon {
    private iconType : IconType;

    /**
     * @param {IconType} $iconType Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($iconType : IconType, $id? : string) {
        super($id);
        this.iconType = this.iconTypeValueSetter($iconType);
    }

    /**
     * @param {IconType} [$type] Specify type of element look and feel.
     * @returns {IconType} Returns type of element's look and feel.
     */
    public IconType($type? : IconType) : IconType {
        if (ObjectValidator.IsSet($type)) {
            this.iconType = this.iconTypeValueSetter($type);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.iconType.toString());
            }
        }
        return this.iconType;
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!IconType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use IconType method for set of icon type instead of StyleClassName method.");
        return false;
    }

    protected iconTypeValueSetter($value : any) : any {
        return Property.EnumType(this.iconType, $value, IconType, IconType.GENERAL);
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Status")
            .StyleClassName(this.statusCss())
            .Add(this.addElement(this.Id() + "_Type")
                .StyleClassName(this.iconType).GuiTypeTag("Icon")
                .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
            );
    }

    protected cssContainerName() : string {
        return "Icons";
    }
}
