/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IInputLabel } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IInputLabel.js";
import { Borders } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Borders.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { InputLabelType } from "../Enums/UserControls/InputLabelType.js";
import { Label } from "./Label.js";

/**
 * InputLabel class renders simple text with HTML background structure.
 */
export class InputLabel extends Label implements IInputLabel {
    private static minWidth : number = 20;
    private guiType : InputLabelType;
    private width : number;
    private error : boolean;
    private borders : Borders;

    private static resize($element : InputLabel) : void {
        if ($element.Width() >= InputLabel.minWidth) {
            const resize : any = ($id : string) : void => {
                const leftWidth : number = ElementManager.getOffsetWidth($id + "Left");
                const rightWidth : number = ElementManager.getOffsetWidth($id + "Right");
                let centerWidth : number = $element.Width() - leftWidth - rightWidth;
                if (centerWidth < 0) {
                    centerWidth = 0;
                }
                ElementManager.setWidth($id + "Center", centerWidth);
                ElementManager.setWidth($id + "Envelop",
                    $element.Width() -
                    ElementManager.getCssIntegerValue($id + "Left", "border-left-width") -
                    ElementManager.getCssIntegerValue($id + "Right", "border-right-width"));
            };
            if ($element.Enabled() || !$element.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                resize($element.Id() + "_");
            } else {
                resize($element.Id() + "_Disabled");
            }
        }
    }

    /**
     * @param {InputLabelType} [$inputLabelType] Specify type of element look and feel.
     * @param {string} [$text] Specify label content.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($inputLabelType? : InputLabelType, $text? : string, $id? : string) {
        super($text, $id);
        this.guiType = this.guiTypeValueSetter($inputLabelType);
    }

    /**
     * @param {InputLabelType} [$inputLabelType] Specify type of element look and feel.
     * @returns {InputLabelType} Returns type of element's look and feel.
     */
    public GuiType($inputLabelType? : InputLabelType) : InputLabelType {
        if (ObjectValidator.IsSet($inputLabelType)) {
            this.guiType = this.guiTypeValueSetter($inputLabelType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }

        return this.guiType;
    }

    /**
     * @param {string} [$value] Set text value, which should be displayed as element context.
     * @returns {string} Returns element's text value.
     */
    public Text($value? : string) : string {
        let value : string = super.Text();
        if (ObjectValidator.IsSet($value) && value !== $value) {
            value = super.Text($value);
            if (this.IsCompleted()) {
                InputLabel.resize(this);
            }
        }
        return value;
    }

    /**
     * @param {number} [$value] Specify width value of element.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        if (ObjectValidator.IsSet($value) && this.width !== $value) {
            this.width = Property.PositiveInteger(this.width, $value, InputLabel.minWidth);
            if (this.IsCompleted()) {
                InputLabel.resize(this);
            }
        }
        return this.width;
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        let enabled : boolean = super.Enabled();
        if (ObjectValidator.IsSet($value)) {
            if (enabled !== $value) {
                enabled = super.Enabled($value);
                if (this.IsCompleted() && ElementManager.IsVisible(this.Id())) {
                    InputLabel.resize(this);
                }
            }
            if (enabled) {
                this.Error(!ObjectValidator.IsSet(this.error) ? false : this.error);
            }
        }

        return enabled;
    }

    /**
     * @param {boolean} [$value] Specify, if element is in error status or not.
     * @returns {boolean} Returns true, if element is in error state, otherwise false.
     */
    public Error($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.error)) {
            this.error = false;
        }
        this.error = Property.Boolean(this.error, $value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            if (this.error) {
                ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.ERROR);
            } else {
                ElementManager.setClassName(this.Id() + "_Status", "");
            }
        }

        return this.error;
    }

    protected statusCss() : string {
        let envelopType : string;
        if (this.Enabled()) {
            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            this.Error() ? envelopType = GeneralCssNames.ERROR : envelopType = "";
        } else {
            envelopType = GeneralCssNames.DISABLE;
        }

        return envelopType;
    }

    protected errorCssName() : string {
        return this.Error() ? GeneralCssNames.ERROR : "";
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, InputLabelType, InputLabelType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!InputLabelType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiTypeTag method for set of inputLabel type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
            InputLabel.resize($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const width90 : number = Math.ceil(this.Width() * 0.90);

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            disabledOption
                .Id(this.Id() + "_Disabled")
                .Visible(!this.Enabled())
                .GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_DisabledBackground")
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_DisabledLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_DisabledCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(width90)
                        .Add(this.addElement(this.Id() + "_DisabledEnvelop")
                            .StyleClassName("Envelop")
                            .Add(this.addElement(this.Id() + "_DisabledText")
                                .StyleClassName(GeneralCssNames.TEXT)
                                .Add(this.Text())
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_DisabledRight").StyleClassName(GeneralCssNames.RIGHT))
                );
        }

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .Visible(this.Enabled())
                    .GuiTypeTag(this.getGuiTypeTag())
                    .Add(this.addElement(this.Id() + "_Background")
                        .StyleClassName(GeneralCssNames.BACKGROUND)
                        .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                        .Add(this.addElement(this.Id() + "_Center")
                            .StyleClassName(GeneralCssNames.CENTER)
                            .Width(width90)
                            .Add(this.addElement(this.Id() + "_Envelop")
                                .StyleClassName("Envelop")
                                .Add(this.addElement(this.Id() + "_Text")
                                    .StyleClassName(GeneralCssNames.TEXT)
                                    .Add(this.Text())
                                )
                            )
                        )
                        .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                    )
                )
                .Add(disabledOption)
            );
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("borders");
        if (this.error === false) {
            exclude.push("error");
        }
        if (!this.IsLoaded()) {
            exclude.push("width");
        }
        return exclude;
    }
}
