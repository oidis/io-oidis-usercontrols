/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AudioEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/AudioEventType.js";
import { MediaEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MediaEventArgs.js";
import { IAudioEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IAudioEvents.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IAudio } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IAudio.js";
import { IMediaSource } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IMediaSource.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { FormsObject } from "../../Primitives/FormsObject.js";

export class Audio extends FormsObject implements IAudio {
    private source : IMediaSource | MediaStream;
    private hasStreamSource : boolean;
    private sourceBuffer : SourceBuffer;
    private muted : boolean;
    private paused : boolean;
    private ended : boolean;
    private controls : boolean;
    private autoplay : boolean;
    private loop : boolean;
    private duration : number;
    private volume : number;
    private time : number;

    constructor($id? : string) {
        super($id);
        this.source = null;
        this.hasStreamSource = false;
        this.sourceBuffer = null;
        this.muted = false;
        this.paused = false;
        this.ended = false;
        this.controls = false;
        this.autoplay = false;
        this.loop = false;
        this.duration = -1;
        this.volume = 1;
        this.time = -1;
    }

    public getEvents() : IAudioEvents {
        return <IAudioEvents>super.getEvents();
    }

    public Source($source? : IMediaSource | MediaStream) : IMediaSource | MediaStream {
        if (!ObjectValidator.IsEmptyOrNull($source)) {
            this.source = $source;
            this.getControl(($element : HTMLAudioElement) : void => {
                if (this.source instanceof MediaStream) {
                    this.Stop();
                    this.source = $source;
                    if (ObjectValidator.IsSet($element.srcObject)) {
                        $element.srcObject = <MediaStream>this.source;
                    } else {
                        $element.src = <any>this.source;
                    }
                    this.hasStreamSource = true;
                } else {
                    const sourceConfig : IMediaSource = <IMediaSource>$source;
                    this.hasStreamSource = false;
                    const sourceId : string = this.Id() + "_Source";
                    let source : HTMLSourceElement;
                    if (ElementManager.Exists(sourceId, true) &&
                        (ObjectValidator.IsEmptyOrNull(sourceConfig.chunkId) || sourceConfig.chunkId === 0)) {
                        this.Stop();
                        this.sourceBuffer = null;
                    }
                    this.source = $source;
                    if (!ElementManager.Exists(sourceId, true)) {
                        source = document.createElement("source");
                        source.id = sourceId;
                        $element.appendChild(source);
                    } else {
                        source = <HTMLSourceElement>ElementManager.getElement(sourceId);
                    }
                    source.type = Property.String(source.type, sourceConfig.type);
                    if (StringUtils.StartsWith(sourceConfig.src, "http://") ||
                        StringUtils.StartsWith(sourceConfig.src, "https://") ||
                        StringUtils.StartsWith(sourceConfig.src, "file://")) {
                        source.src = sourceConfig.src;
                    } else if (!ObjectValidator.IsSet(sourceConfig.chunkId)) {
                        source.src = "data:" + source.type + ";base64," + sourceConfig.src;
                    } else {
                        const mime : string = this.getMime(sourceConfig);
                        if (("MediaSource" in window) && MediaSource.isTypeSupported(mime)) {
                            if (ObjectValidator.IsEmptyOrNull(this.sourceBuffer)) {
                                const mediaSource : MediaSource = new MediaSource();
                                source.src = URL.createObjectURL(mediaSource);
                                mediaSource.addEventListener("sourceopen", () : void => {
                                    this.sourceBuffer = mediaSource.addSourceBuffer(mime);
                                    this.sourceBuffer.addEventListener("error", ($event : ErrorEvent) : void => {
                                        if (ObjectValidator.IsEmptyOrNull($event.message)) {
                                            LogIt.Error($event.message);
                                        } else {
                                            LogIt.Debug("Unrecognized SourceBuffer error");
                                        }
                                    });
                                    this.sourceBuffer.addEventListener("seeking", () : void => {
                                        if (mediaSource.readyState === "open") {
                                            this.sourceBuffer.abort();
                                        }
                                    });
                                    this.sourceBuffer.appendBuffer(Convert.Base64ToArrayBuffer(sourceConfig.src));
                                }, false);
                            } else {
                                try {
                                    this.sourceBuffer.appendBuffer(Convert.Base64ToArrayBuffer(sourceConfig.src));
                                } catch (ex) {
                                    let owner : string = this.getClassName();
                                    if (!ObjectValidator.IsEmptyOrNull($element.error.message)) {
                                        owner = $element.error.message;
                                    }
                                    LogIt.Error(owner, ex);
                                }
                            }
                        } else {
                            LogIt.Error("Unsupported mime type codec: " + mime);
                        }
                    }
                }
            });
        }
        return this.source;
    }

    public Play($inLoop? : boolean) : void {
        this.Looped($inLoop);
        if (this.Paused()) {
            this.getControl(($element : HTMLAudioElement) : void => {
                $element
                    .play()
                    .then(() : void => {
                        this.fireEvent(AudioEventType.ON_PLAY, "Media stream has been resumed.");
                    }, ($reason : any) : void => {
                        this.fireEvent(AudioEventType.ON_ERROR, "There was an error while resuming this media stream: " + $reason);
                    });
            });
        }
    }

    public Pause() : void {
        if (!this.Paused()) {
            this.getControl(($element : HTMLAudioElement) : void => {
                $element.pause();
                this.fireEvent(AudioEventType.ON_PAUSE, "Media stream has been paused.");
            });
        }
    }

    public Stop() : void {
        if (this.hasStreamSource) {
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                (<MediaStream>this.source).getTracks().forEach(($track : MediaStreamTrack) : void => {
                    $track.stop();
                });
            }
            this.source = null;
            this.hasStreamSource = false;
            this.fireEvent(AudioEventType.ON_STOP, "Media stream has been stopped.");
        }
        this.getControl(($element : HTMLAudioElement) : void => {
            const sourceId : string = this.Id() + "_Source";
            if (ElementManager.Exists(sourceId, true)) {
                const source : HTMLSourceElement = <HTMLSourceElement>ElementManager.getElement(sourceId);
                source.parentNode.removeChild(source);
                $element.load();
                this.sourceBuffer = null;
            }
        });
    }

    public Mute($value? : boolean) : boolean {
        this.muted = Property.Boolean(this.muted, $value);
        this.getControl(($element : HTMLAudioElement) : void => {
            const eventType : string = $value ? AudioEventType.ON_MUTE : AudioEventType.ON_UNMUTE;
            const message : string = "Media stream has been " + ($value ? "muted" : "unmuted") + ".";
            if (ObjectValidator.IsSet($value)) {
                $element.muted = this.muted;
                this.fireEvent(eventType, message);
            }
        });
        return this.muted;
    }

    public Duration() : number {
        this.getControl(($element : HTMLAudioElement) : void => {
            this.duration = $element.duration;
        });
        return !this.hasStreamSource ? this.duration : -1;
    }

    public CurrentTime($value? : number) : number {
        this.getControl(($element : HTMLAudioElement) : void => {
            if (ObjectValidator.IsSet($value)) {
                $element.currentTime = $value;
            }
            this.time = $element.currentTime;
        });
        return this.time;
    }

    public Paused() : boolean {
        this.getControl(($element : HTMLAudioElement) : void => {
            this.paused = $element.paused;
        });
        return this.paused;
    }

    public Ended() : boolean {
        this.getControl(($element : HTMLAudioElement) : void => {
            this.ended = $element.ended;
        });
        return this.ended;
    }

    public WithControls($value? : boolean) : boolean {
        this.controls = Property.Boolean(this.controls, $value);
        if (ObjectValidator.IsSet($value)) {
            this.getControl(($element : HTMLAudioElement) : void => {
                $element.controls = this.controls;
            });
        }
        return this.controls;
    }

    public Autoplay($value? : boolean) : boolean {
        this.autoplay = Property.Boolean(this.autoplay, $value);
        if (ObjectValidator.IsSet($value)) {
            this.getControl(($element : HTMLAudioElement) : void => {
                $element.autoplay = this.autoplay;
            });
        }
        return this.autoplay;
    }

    public Looped($value? : boolean) : boolean {
        this.loop = Property.Boolean(this.loop, $value);
        if (ObjectValidator.IsSet($value)) {
            this.getControl(($element : HTMLAudioElement) : void => {
                $element.loop = this.loop;
            });
        }
        return this.loop;
    }

    public Volume($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.volume = $value;
            this.getControl(($element : HTMLAudioElement) : void => {
                $element.volume = this.volume;
            });
        }
        return this.volume;
    }

    protected innerHtml() : IGuiElement {
        const audio : HTMLElement = document.createElement(this.getControlTag());
        audio.id = this.Id() + "_Output";
        this.getControl(($element : HTMLAudioElement) : void => {
            $element.oncanplay = () : void => {
                this.fireEvent(AudioEventType.ON_OPEN);
            };
            $element.ontimeupdate = () : void => {
                this.fireEvent(AudioEventType.ON_CHANGE);
            };
        });
        return this.addElement().Add(audio);
    }

    protected getControlTag() : string {
        return "audio";
    }

    protected getControl($callback : ($element : HTMLAudioElement) => void) : void {
        if (this.IsCompleted()) {
            $callback(<HTMLAudioElement>ElementManager.getElement(this.Id() + "_Output"));
        } else {
            this.getEvents().setOnComplete(() : void => {
                $callback(<HTMLAudioElement>ElementManager.getElement(this.Id() + "_Output"));
            });
        }
    }

    protected getMime($source : IMediaSource) : string {
        let codecs : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull($source.codecs)) {
            codecs = $source.codecs;
        }
        let mime : string = $source.type;
        if (!ObjectValidator.IsEmptyOrNull(codecs)) {
            mime += "; codecs=\"" + codecs.join(",") + "\"";
        }
        return mime;
    }

    protected fireEvent($eventType : string, $message? : string) : void {
        const eventArgs : MediaEventArgs = new MediaEventArgs();
        eventArgs.Owner(this);
        eventArgs.EventMessage($message);
        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
        this.getEventsManager().FireEvent(thisClass.ClassName(), $eventType, eventArgs);
        this.getEventsManager().FireEvent(this, $eventType, eventArgs);
    }
}
