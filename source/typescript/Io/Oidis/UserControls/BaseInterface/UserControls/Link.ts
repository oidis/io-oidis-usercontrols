/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ILink } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ILink.js";
import { LinkSelector } from "../../Utils/LinkSelector.js";
import { Label } from "./Label.js";

/**
 * Link class renders clickable simple text.
 */
export class Link extends Label implements ILink {
    private selector : LinkSelector;

    /**
     * @param {Link} $element Specify element, which should be handled.
     * @param {boolean} [$force=false] If true, than do not care about TAB navigation state.
     * @returns {void}
     */
    public static Focus($element : Link, $force : boolean = false) : void {
        LinkSelector.Focus($element, $force);
    }

    /**
     * @returns {void}
     */
    public static Blur() : void {
        LinkSelector.Blur(<IClassName>Link);
    }

    /**
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($id? : string) {
        super("", $id);
        this.getEvents().Subscriber(this.Id());
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        return this.getSelector().Enabled(super.Enabled($value));
    }

    /**
     * @param {string} [$value] Set link value, which will be used for redirect in case of element click.
     * @returns {string} Returns element's link value.
     */
    public ReloadTo($value? : string) : string {
        return this.getSelector().ReloadTo($value);
    }

    /**
     * @param {boolean} [$value] Specify, if reloaded content should be opened in new window.
     * @returns {boolean} Returns true, if reloaded content will be opened in new window, otherwise false.
     */
    public OpenInNewWindow($value? : boolean) : boolean {
        return this.getSelector().OpenInNewWindow($value);
    }

    /**
     * @param {number} [$value] Set index for TAB key navigation in the page.
     * @returns {number} Returns index of element in the page based on TAB key elements register.
     */
    public TabIndex($value? : number) : number {
        return this.getSelector().TabIndex($value);
    }

    public IsPreventingScroll() : boolean {
        return true;
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.getEvents().Subscriber(this.Id());
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("selector");
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("selector");
        return exclude;
    }

    protected innerCode() : IGuiElement {
        if (ObjectValidator.IsEmptyOrNull(this.getSelector().getValue())) {
            this.getSelector().ReloadTo(this.getHttpManager().CreateLink(""));
        }
        return super.innerCode().Add(this.getSelector().getInnerCode());
    }

    protected innerHtml() : IGuiElement {
        const innerHtml : IGuiElement = super.innerHtml();
        (<IGuiElement>innerHtml.getChildElements().getFirst()).StyleClassName(GeneralCssNames.OFF);
        return innerHtml;
    }

    private getSelector() : LinkSelector {
        if (!ObjectValidator.IsSet(this.selector)) {
            this.selector = new LinkSelector(this);
        }
        return this.selector;
    }
}
