/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { ILabelEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/ILabelEvents.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ILabel } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ILabel.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BaseGuiObject } from "../../Primitives/BaseGuiObject.js";

/**
 * Label class renders simple text.
 */
export class Label extends BaseGuiObject implements ILabel {
    private text : string;

    /**
     * @param {string} [$text] Specify label content.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($text? : string, $id? : string) {
        super($id);
        this.getEvents().Subscriber(this.Id() + "_Enabled");
        if (ObjectValidator.IsSet($text) && $text !== null) {
            this.Text($text);
        } else {
            this.Text(this.Id());
        }
    }

    /**
     * @returns {ILabelEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : ILabelEvents {
        return <ILabelEvents>super.getEvents();
    }

    /**
     * @param {string} [$value] Set text value, which should be displayed as element context.
     * @returns {string} Returns element's text value.
     */
    public Text($value? : string) : string {
        if (ObjectValidator.IsSet($value) && this.text !== $value) {
            this.text = Property.NullString(this.text, $value);
            if (this.IsLoaded()) {
                ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                ElementManager.setInnerHtml(this.Id() + "_DisabledText", $value);

                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                this.getEventsManager().FireEvent(thisClass.ClassName(), EventType.ON_CHANGE, eventArgs);
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
            }
        }
        return this.text;
    }

    /**
     * @param {string} [$value] Set text value, which should be displayed as element context.
     * @returns {string} Returns element's text value, if value is set, otherwise null.
     */
    public Value($value? : string) : any {
        if (ObjectValidator.IsSet($value)) {
            this.Text($value);
        }
        return null;
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        const enabled : boolean = super.Enabled($value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            if (enabled || this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                ElementManager.Enabled(this.Id(), enabled);
            }
        }
        return enabled;
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const args : IGuiCommonsArg[] = super.getArgs();
        let index : number;
        for (index = 0; index < args.length; index++) {
            if (args[index].name === "Value") {
                args[index].value = this.Text();
                break;
            }
        }
        return args;
    }

    protected innerHtml() : IGuiElement {
        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            disabledOption
                .Id(this.Id() + "_Disabled")
                .StyleClassName(GeneralCssNames.DISABLE)
                .Visible(!this.Enabled())
                .Add(this.addElement(this.Id() + "_DisabledText")
                    .StyleClassName(GeneralCssNames.TEXT)
                    .Add(this.Text())
                );
        }

        return this.addElement()
            .Add(this.addElement(this.Id() + "_Enabled")
                .Visible(this.Enabled())
                .Add(this.addElement(this.Id() + "_Text")
                    .StyleClassName(GeneralCssNames.TEXT)
                    .Add(this.Text())
                ))
            .Add(disabledOption);
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.getEvents().Subscriber(this.Id() + "_Enabled");
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("text");
        return exclude;
    }
}
