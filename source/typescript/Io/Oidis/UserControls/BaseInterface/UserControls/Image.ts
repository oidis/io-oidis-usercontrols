/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IllegalArgumentException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/IllegalArgumentException.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ImageDataSourceType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ImageDataSourceType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { HttpManager } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/HttpManager.js";
import { ImageFilters } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageFilters.js";
import { ImageOutputArgs } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageOutputArgs.js";
import { ImageTransform } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageTransform.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IImage } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IImage.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BaseGuiObject } from "../../Primitives/BaseGuiObject.js";
import { LinkSelector } from "../../Utils/LinkSelector.js";
import { DragBar } from "../Components/DragBar.js";
import { ResizeBar } from "../Components/ResizeBar.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { IconType } from "../Enums/UserControls/IconType.js";
import { ImageType } from "../Enums/UserControls/ImageType.js";
import { Icon } from "./Icon.js";
import { Label } from "./Label.js";

/**
 * Image class renders images specified by filesystem path or by css.
 */
export class Image extends BaseGuiObject implements IImage {
    public alternateText : Label;
    public loadingSpinner : Icon;
    public loadingText : Label;

    private guiType : ImageType;
    private isSelected : boolean;
    private source : string;
    private sourceLink : string;
    private sourceAlternateText : string;
    private sourceType : ImageDataSourceType;
    private selector : LinkSelector;
    private loadingSpinnerEnabled : boolean;
    private opacityShowEnabled : boolean;
    private width : number;
    private height : number;
    private sourceLoaded : boolean;
    private outputSource : HTMLImageElement;
    private outputArgs : ImageOutputArgs;
    private outputArgsHash : number;
    private version : number;
    private canvasData : string;

    /**
     * @param {Image} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnOn($element : Image) : void {
        if ($element.Enabled() && $element.sourceLoaded) {
            const manager : GuiObjectManager = $element.getGuiManager();
            if (!manager.IsActive($element) && !$element.IsSelected() &&
                !manager.IsActive(<IClassName>ScrollBar) &&
                !manager.IsActive(<IClassName>ResizeBar) &&
                !manager.IsActive(<IClassName>DragBar)) {
                manager.getActive(Image).foreach(($element : Image) : void => {
                    manager.setActive($element, false);
                    Image.TurnOff($element);
                });
                ElementManager.TurnOn($element.Id() + "_Status");
                if (!ObjectValidator.IsEmptyOrNull($element.getSelector().getValue())) {
                    ElementManager.setCssProperty($element.Id(), "cursor", "pointer");
                }
            }
        }
    }

    /**
     * @param {Image} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnOff($element : Image) : void {
        if (!$element.getGuiManager().IsActive($element)) {
            if ($element.Enabled()) {
                if ($element.IsSelected()) {
                    ElementManager.TurnActive($element.Id() + "_Status");
                } else {
                    ElementManager.TurnOff($element.Id() + "_Status");
                    if (ObjectValidator.IsEmptyOrNull($element.getSelector().getValue())) {
                        LinkSelector.Blur(<IClassName>Image);
                    }
                }
                ElementManager.setCssProperty($element.Id(), "cursor", "default");
            }
        }
    }

    /**
     * @param {Image} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnActive($element : Image) : void {
        ElementManager.TurnActive($element.Id() + "_Status");
        ElementManager.setCssProperty($element.Id(), "cursor", "pointer");
    }

    /**
     * @param {Image} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Show($element : Image) : void {
        if (!$element.sourceLoaded && $element.OpacityShowEnabled()) {
            ElementManager.setOpacity($element.Id() + "_Image", 0);
        }
        BaseGuiObject.Show($element);
    }

    /**
     * @param {Image} $element Specify element, which should be handled.
     * @param {boolean} [$force=false] If true, than do not care about TAB navigation state.
     * @returns {void}
     */
    public static Focus($element : Image, $force : boolean = false) : void {
        if ($element.sourceLoaded && !ObjectValidator.IsEmptyOrNull($element.getSelector().getValue())) {
            LinkSelector.Focus($element, $force);
        }
    }

    /**
     * @returns {void}
     */
    public static Blur() : void {
        LinkSelector.Blur(<IClassName>Image);
    }

    private static load($element : Image) : void {
        const id : string = $element.Id();
        if ($element.Visible()) {
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent(Image.ClassName(), EventType.ON_START, eventArgs);

            if (!$element.IsCached()) {
                const backgroundSize : Size = new Size($element.Id() + "_Background", true);
                if (backgroundSize.Width() === 0) {
                    ElementManager.setWidth($element.Id() + "_Background", $element.width);
                }
                if (backgroundSize.Height() === 0) {
                    ElementManager.setHeight($element.Id() + "_Background", $element.height);
                }

                if (ElementManager.IsVisible(id + "_Loading")) {
                    ElementManager.Show(id + "_LoadingContent");
                    Image.resizeLoader($element);
                }
            }

            if ($element.OpacityShowEnabled()) {
                ElementManager.setOpacity(id + "_Image", 0);
            }

            if ($element.sourceType === ImageDataSourceType.STYLE_CLASS_NAME && ObjectValidator.IsEmptyOrNull($element.canvasData)) {
                Image.showImage($element);
            } else {
                const image : HTMLImageElement = <HTMLImageElement>ElementManager.getElement(id + "_Image");
                const eventsManager : ElementEventsManager = new ElementEventsManager($element, id + "_Image");
                eventsManager.setEvent(EventType.ON_LOAD, ($eventArgs : EventArgs) : void => {
                    const element : Image = <Image>$eventArgs.Owner();
                    if (!element.sourceLoaded) {
                        if (ElementManager.IsVisible(element.Id() + "_Loading")) {
                            ElementManager.Hide(element.Id() + "_Loading");
                        }
                        ElementManager.Show(element.Id() + "_Loaded");
                        image[EventType.ON_LOAD] = () : void => {
                            // register default on load handler
                        };
                        Image.showImage(element);
                    }
                });
                eventsManager.Subscribe();
                if (!ObjectValidator.IsEmptyOrNull(image)) {
                    if (ObjectValidator.IsEmptyOrNull(image.getAttribute("src"))) {
                        $element.getEvents().FireAsynchronousMethod(() : void => {
                            image.src = null;
                            image.crossOrigin = "Anonymous";
                            if (ObjectValidator.IsEmptyOrNull($element.canvasData)) {
                                image.src = $element.getImageSourceLink();
                            } else {
                                image.src = $element.canvasData;
                                $element.canvasData = "";
                            }
                        }, false);
                    } else {
                        Image.showImage($element);
                    }
                    if (!ObjectValidator.IsEmptyOrNull($element.width)) {
                        image.width = $element.width;
                        image.height = $element.height;
                    }
                }
            }
        }
    }

    private static resizeLoader($element : Image) : void {
        const id : string = $element.Id();
        if (ElementManager.IsVisible(id + "_Loading") && $element.Visible()) {
            let imageSize : Size;
            if (ObjectValidator.IsEmptyOrNull($element.OutputArgs())) {
                imageSize = new Size(id);
            } else {
                imageSize = new Size();
                imageSize.Width($element.OutputArgs().getWidth());
                imageSize.Height($element.OutputArgs().getHeight());
            }
            ElementManager.Show(id + "_LoadingContent");
            const spinnerId : string = $element.loadingSpinner.Id();
            const loadingTextId : string = $element.loadingText.Id() + "_Text";
            ElementManager.setCssProperty(loadingTextId, "position", "fixed");
            const textSize : Size = new Size(loadingTextId, true);
            ElementManager.setCssProperty(loadingTextId, "position", "relative");
            ElementManager.setCssProperty(spinnerId, "position", "fixed");
            const spinnerSize : Size = new Size(spinnerId, true);
            ElementManager.setCssProperty(spinnerId, "position", "relative");

            const loaderSize : Size = new Size();
            loaderSize.Width(spinnerSize.Width() + textSize.Width());
            loaderSize.Height(spinnerSize.Height() + textSize.Height());
            ElementManager.setSize(id + "_Loading", imageSize.Width(), imageSize.Height());
            if (loaderSize.Width() > imageSize.Width()) {
                loaderSize.Width(spinnerSize.Width());
                ElementManager.Hide(loadingTextId);
                ElementManager.Hide(id + "_LoadingContent");
            } else {
                ElementManager.setSize(id + "_LoadingContent", loaderSize.Width(), loaderSize.Height());
            }
        }
    }

    private static showImage($element : Image) : void {
        if (!$element.sourceLoaded) {
            const showImageWithoutArgs : any = () : void => {
                $element.sourceLoaded = true;
                if ($element.OpacityShowEnabled()) {
                    ElementManager.ChangeOpacity($element.Id() + "_Image", DirectionType.UP, 10);
                }
                Image.TurnOff($element);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEventsManager().FireEvent(Image.ClassName(), EventType.ON_LOAD, eventArgs);
            };
            const showDynamicImage : any = () : void => {
                $element.outputSource = <HTMLImageElement>ElementManager.getElement($element.Id() + "_Image");
                const rotation : number = $element.outputArgs.Rotation();
                if (rotation > 0) {
                    const thetaPositive : number = Math.cos(Convert.DegToRad(rotation));
                    const thetaNegative : number = (-1) * thetaPositive;
                    ElementManager.setCssProperty($element.outputSource, "transform", "rotate(" + rotation + "deg)");
                    ElementManager.setCssProperty($element.outputSource, "-webkit-transform", "rotate(" + rotation + "deg)");
                    ElementManager.setCssProperty($element.outputSource, "-moz-transform", "rotate(" + rotation + "deg)");
                    ElementManager.setCssProperty($element.outputSource, "-o-transform", "rotate(" + rotation + "deg)");
                    ElementManager.setCssProperty($element.outputSource, "-ms-transform", "rotate(" + rotation + "deg)");
                    const filterMatrix : string =
                        "M11=" + thetaPositive + ", M12=" + thetaNegative + ", M21=" + thetaPositive + ", M22=" + thetaPositive;
                    ElementManager.setCssProperty($element.outputSource, "filter",
                        "progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', " + filterMatrix + ")");
                    ElementManager.setCssProperty($element.outputSource, "-ms-filter",
                        "\"progid:DXImageTransform.Microsoft.Matrix(SizingMethod='auto expand', " + filterMatrix + ")\"");
                }
                showImageWithoutArgs();
            };
            if ((ObjectValidator.IsEmptyOrNull($element.outputArgs))) {
                showImageWithoutArgs();
            } else if ($element.useCanvas()) {
                if ($element.sourceType === ImageDataSourceType.STYLE_CLASS_NAME) {
                    let src : string =
                        StringUtils.Remove(ElementManager.getCssValue($element.Id() + "_Image", "background-image"), "url(\"", "\")");
                    if (StringUtils.Contains(src, "://")) {
                        const input : HTMLImageElement = document.createElement("img");
                        input.crossOrigin = "Anonymous";
                        input.onload = () : void => {
                            try {
                                $element.applyOutputArgs(input);
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        };

                        src += (StringUtils.Contains(src, "?") ? "&" : "?") + "version=" + $element.version;
                        if (!$element.outputArgs.FrontEndCacheEnabled()) {
                            src += "&dummy=" + new Date().getTime();
                        }
                        input.src = src;
                    } else {
                        showImageWithoutArgs();
                    }
                } else if (!StringUtils.Contains($element.getImageSourceLink(), "/DynamicImage/" + $element.sourceType + "/")) {
                    $element.applyOutputArgs(<HTMLImageElement>ElementManager.getElement($element.Id() + "_Image"));
                } else {
                    showDynamicImage();
                }
            } else {
                showDynamicImage();
            }
        }
    }

    /**
     * @param {string} $source Specify source of the image, which should be rendered.
     * @param {ImageType} [$imageType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($source : string, $imageType? : ImageType, $id? : string) {
        super($id);
        this.guiType = this.guiTypeValueSetter($imageType);

        this.alternateText = new Label(null, this.Id() + "_AlternateText");
        this.alternateText.DisableAsynchronousDraw();

        let loadingSpinnerClass : any = this.getLoadingSpinnerClass();
        if (ObjectValidator.IsEmptyOrNull(loadingSpinnerClass)) {
            loadingSpinnerClass = Icon;
        }
        this.loadingSpinner = new loadingSpinnerClass(IconType.SPINNER_SMALL, this.Id() + "_LoadingSpinner");
        this.loadingSpinner.DisableAsynchronousDraw();

        let loadingTextClass : any = this.getLoadingTextClass();
        if (ObjectValidator.IsEmptyOrNull(loadingTextClass)) {
            loadingTextClass = Label;
        }
        this.loadingText = new loadingTextClass("Loading ...", this.Id() + "_LoadingText");
        this.loadingText.DisableAsynchronousDraw();

        this.source = "UndefinedCssName";
        this.Source($source);
        this.version = 0;
    }

    /**
     * @param {ImageType} [$imageType] Specify type of element look and feel.
     * @returns {ImageType} Returns type of element's look and feel.
     */
    public GuiType($imageType? : ImageType) : ImageType {
        if (ObjectValidator.IsSet($imageType)) {
            this.guiType = this.guiTypeValueSetter($imageType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    /**
     * @param {boolean} [$value] Specify element's look and feel status.
     * @returns {boolean} Returns element's look and feel status.
     */
    public IsSelected($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.isSelected)) {
            this.isSelected = false;
        }
        this.isSelected = Property.Boolean(this.isSelected, $value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            if (this.isSelected) {
                ElementManager.TurnActive(this.Id() + "_Status");
            } else {
                ElementManager.TurnOff(this.Id() + "_Status");
            }
        }
        return this.isSelected;
    }

    /**
     * @param {string} [$value] Specify image source [css class name, file path or image metadata].
     * @returns {string} Returns image source.
     */
    public Source($value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value) && this.source !== $value) {
            const currentSourceType : ImageDataSourceType = this.sourceType;
            if (StringUtils.Contains($value, "/", "\\", ".")) {
                this.source = $value;
                this.sourceAlternateText = $value;
                this.sourceLink = $value;
                this.sourceType = ImageDataSourceType.FILE_SYSTEM;
            } else {
                this.source = $value;
                this.sourceAlternateText = $value;
                this.sourceLink = "";
                this.sourceType = ImageDataSourceType.STYLE_CLASS_NAME;
                this.alternateText.Visible(false);
                this.LoadingSpinnerEnabled(false);
            }
            if (this.IsLoaded()) {
                if (this.sourceType === currentSourceType) {
                    this.sourceLoaded = false;

                    if (!this.useCanvas() || ObjectValidator.IsEmptyOrNull(this.outputArgs)) {
                        if (this.sourceType === ImageDataSourceType.FILE_SYSTEM) {
                            const image : HTMLImageElement = <HTMLImageElement>ElementManager.getElement(this.Id() + "_Image");
                            image.removeAttribute("src");
                            if (this.alternateText.Visible()) {
                                image.alt = this.sourceAlternateText;
                            }
                            if (this.LoadingSpinnerEnabled()) {
                                ElementManager.Hide(this.Id() + "_Loaded");
                                ElementManager.Show(this.Id() + "_Loading");
                                Image.resizeLoader(this);
                            }
                        } else if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME) {
                            let hidden : string = "";
                            if (this.OpacityShowEnabled()) {
                                hidden = GeneralCssNames.HIDDEN;
                                if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                                    hidden += " ";
                                }
                            }
                            ElementManager.setClassName(this.Id() + "_Image", hidden + this.source);
                            ElementManager.setInnerHtml(this.Id() + "_Image", this.sourceAlternateText);
                        }
                        this.alternateText.Text("");
                        this.Link("");
                        Image.load(this);
                    } else {
                        if (this.sourceType === ImageDataSourceType.FILE_SYSTEM) {
                            if (this.LoadingSpinnerEnabled()) {
                                ElementManager.Hide(this.Id() + "_Loaded");
                                ElementManager.Show(this.Id() + "_Loading");
                                Image.resizeLoader(this);
                            }
                        } else if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME) {
                            ElementManager.setClassName(this.Id() + "_Image", this.OpacityShowEnabled() ? GeneralCssNames.HIDDEN : "");
                            ElementManager.setInnerHtml(this.Id() + "_Image", this.sourceAlternateText);
                        }
                        this.alternateText.Text("");
                        this.Link("");

                        const input : HTMLImageElement = document.createElement("img");
                        input.crossOrigin = "Anonymous";
                        input.onload = () : void => {
                            try {
                                this.applyOutputArgs(input);
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        };
                        const loadImage : any = ($source : string) : void => {
                            if (!StringUtils.StartsWith(this.sourceLink, "data:image/")) {
                                $source += (StringUtils.Contains($source, "?") ? "&" : "?") + "version=" + this.version;
                            }
                            if (!this.outputArgs.FrontEndCacheEnabled()) {
                                $source += "&dummy=" + new Date().getTime();
                            }
                            input.src = $source;
                        };
                        if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME) {
                            const sourceLoader : HTMLElement = document.createElement("div");
                            sourceLoader.id = this.getClassName() + "StyleClassNameLoader" + new Date().getTime();
                            ElementManager.getElement(this.Id() + "_Image").parentNode.appendChild(sourceLoader);
                            this.getEvents().FireAsynchronousMethod(() : void => {
                                ElementManager.setClassName(sourceLoader, this.source);
                                const src : string = StringUtils.Remove(
                                    ElementManager.getCssValue(sourceLoader.id, "background-image"), "url(\"", "\")");
                                sourceLoader.parentElement.removeChild(sourceLoader);
                                loadImage(src);
                            });
                        } else {
                            loadImage(this.source);
                        }
                    }
                } else {
                    ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException(
                        "New data source can not be set, because change of source type " +
                        "is not allowed at runtime for image \"" + this.Id() + "\"."));
                }
            }
        }
        return this.source;
    }

    /**
     * @param {string} [$value] Set link value, which will be used for redirect in case of element click.
     * @param {boolean} [$openNewWindow=true] Specify, if reloaded content should be opened in new window.
     * @returns {string} Returns element's link value.
     */
    public Link($value? : string, $openNewWindow : boolean = true) : string {
        if (ObjectValidator.IsSet($value)) {
            this.getSelector().ReloadTo($value);
            this.getSelector().OpenInNewWindow($openNewWindow);
        }
        return this.getSelector().ReloadTo();
    }

    /**
     * @param {boolean} [$value] Specify, if loading of the image should be covered by spinner.
     * @returns {boolean} Returns true, if image load is covered by spinner, otherwise false.
     */
    public LoadingSpinnerEnabled($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.loadingSpinnerEnabled)) {
            this.loadingSpinnerEnabled = true;
        }
        this.loadingSpinnerEnabled = Property.Boolean(this.loadingSpinnerEnabled, $value);
        return this.loadingSpinnerEnabled;
    }

    /**
     * @param {boolean} [$value] Specify, if show of the image should be gradually.
     * @returns {boolean} Returns true, if show of the image is gradual, otherwise false.
     */
    public OpacityShowEnabled($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.opacityShowEnabled)) {
            this.opacityShowEnabled = true;
        }
        this.opacityShowEnabled = Property.Boolean(this.opacityShowEnabled, $value);
        return this.opacityShowEnabled;
    }

    /**
     * @param {number} $width Specify image width.
     * @param {number} $height Specify image height.
     * @returns {void}
     */
    public setSize($width : number, $height : number) : void {
        this.width = Property.PositiveInteger(this.width, $width);
        this.height = Property.PositiveInteger(this.height, $height);
        if (!ObjectValidator.IsEmptyOrNull(this.outputArgs) && this.outputArgs.FillEnabled()) {
            this.outputArgs.setSize(this.getWidth(), this.getHeight());
        }
    }

    public Width($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.setSize($value, this.height);
            return this.width;
        }
        return this.getWidth();
    }

    public Height($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            this.setSize(this.width, $value);
            return this.height;
        }
        return this.getHeight();
    }

    /**
     * @returns {number} Returns image width.
     */
    public getWidth() : number {
        if (ObjectValidator.IsEmptyOrNull(this.width) && this.IsCompleted()) {
            return ElementManager.getCssIntegerValue(this.Id() + "_Image", "width");
        }
        return this.width;
    }

    /**
     * @returns {number} Returns image height.
     */
    public getHeight() : number {
        if (ObjectValidator.IsEmptyOrNull(this.height) && this.IsCompleted()) {
            return ElementManager.getCssIntegerValue(this.Id() + "_Image", "height");
        }
        return this.height;
    }

    /**
     * @returns {string} Returns image raw data, if image has been loaded and reading of image data is supported otherwise null.
     */
    public getStream() : string {
        if (this.IsLoaded() && this.useCanvas()) {
            return ImageTransform.getStream(<HTMLCanvasElement>ElementManager.getElement(this.Id() + "_Image"));
        }
        return null;
    }

    /**
     * @param {number} [$value] Set index for TAB key navigation in the page.
     * @returns {number} Returns index of element in the page based on TAB key elements register.
     */
    public TabIndex($value? : number) : number {
        return this.getSelector().TabIndex($value);
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        return this.getSelector().Enabled(super.Enabled($value));
    }

    /**
     * @param {ImageOutputArgs} [$value] Specify arguments for manipulation with image source.
     * @returns {ImageOutputArgs} Returns output arguments, if arguments have been specified, otherwise null.
     */
    public OutputArgs($value? : ImageOutputArgs) : ImageOutputArgs {
        if (ObjectValidator.IsSet($value)) {
            const argsHash : number = $value.getHash();
            if (ObjectValidator.IsEmptyOrNull(this.outputArgs) || this.outputArgsHash !== argsHash) {
                this.outputArgs = $value;
                this.outputArgsHash = argsHash;
                if (this.outputArgs.getWidth() > 0 && this.outputArgs.getHeight() > 0) {
                    this.setSize(this.outputArgs.getWidth(), this.outputArgs.getHeight());
                } else {
                    this.outputArgs.setSize(this.getWidth(), this.getHeight());
                }
                if (this.IsCompleted()) {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        this.applyOutputArgs(this.outputSource);
                    }, false);
                }
            }
        }
        return this.outputArgs;
    }

    /**
     * @param {number} [$value] Specify data source version.
     * @returns {number} Returns data source version.
     */
    public Version($value? : number) : number {
        return this.version = Property.Integer(this.version, $value);
    }

    /**
     * @returns {IIcon} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
     */
    protected getLoadingSpinnerClass() : any {
        return Icon;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getLoadingTextClass() : any {
        return Label;
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();

        if (ObjectValidator.IsSet(this.alternateText)) {
            this.alternateText.DisableAsynchronousDraw();
        }
        if (ObjectValidator.IsSet(this.loadingSpinner)) {
            this.loadingSpinner.DisableAsynchronousDraw();
        }
        if (ObjectValidator.IsSet(this.loadingText)) {
            this.loadingText.DisableAsynchronousDraw();
        }
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "isSelected", "selector",
            "sourceLoaded",
            "loadingSpinnerEnabled", "opacityShowEnabled",
            "width", "height",
            "outputSource", "outputArgs", "outputArgsHash",
            "canvasData"
        );
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push("selector", "sourceLoaded", "outputSource", "outputArgs", "outputArgsHash");
        if (this.IsSelected() === false) {
            exclude.push("isSelected");
        }
        if (this.LoadingSpinnerEnabled() === true) {
            exclude.push("loadingSpinnerEnabled");
        }
        if (this.OpacityShowEnabled() === true) {
            exclude.push("opacityShowEnabled");
        }
        if (ObjectValidator.IsEmptyOrNull(this.canvasData)) {
            exclude.push("canvasData");
        }
        return exclude;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, ImageType, ImageType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!ImageType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of Image type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        switch (this.guiType) {
        case ImageType.GENERAL:
            break;
        case ImageType.GALLERY_PHOTO:
            this.setSize(800, 800);
            break;
        default:
            break;
        }

        this.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
            Image.TurnOn($eventArgs.Owner());
        });
        this.getEvents().setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
            Image.TurnOff($eventArgs.Owner());
        });
        this.getEvents().setOnMouseUp(() : void => {
            Image.Blur();
        });

        this.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
            const element : Image = <Image>$eventArgs.Owner();
            element.sourceLoaded = false;
            Image.load(element);
            if (element.alternateText.Visible()) {
                this.getEventsManager().FireEvent(element.alternateText, EventType.ON_COMPLETE);
            }
        });

        if (this.IsLoaded()) {
            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                const element : Image = <Image>$eventArgs.Owner();
                element.sourceLoaded = false;
                Image.load(element);
            });
        }

        return super.innerCode().Add(this.getSelector().getInnerCode());
    }

    protected innerHtml() : IGuiElement {
        let alternateText : string = "";
        if (this.alternateText.Visible()) {
            if (this.alternateText.Text() !== this.alternateText.Id()) {
                alternateText = this.alternateText.Text();
            } else {
                alternateText = this.sourceAlternateText;
            }
        }

        let hidden : string = "";
        if (this.OpacityShowEnabled()) {
            hidden = GeneralCssNames.HIDDEN + " ";
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                hidden += " ";
            }
        }

        let output : IGuiElement = this.addElement()
            .StyleClassName(GeneralCssNames.BACKGROUND)
            .Add(this.addElement(this.Id() + "_Image")
                .StyleClassName(hidden + this.source)
                .Add(alternateText)
            )
            .Add(this.addElement().StyleClassName(GeneralCssNames.FOREGROUND));
        if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME && !this.LoadingSpinnerEnabled()) {
            this.loadingSpinner.Visible(false);
            this.loadingText.Visible(false);
        } else if (this.sourceType === ImageDataSourceType.FILE_SYSTEM) {
            const input : HTMLImageElement = document.createElement("img");
            input.id = this.Id() + "_Image";
            input.className = hidden;
            input.alt = alternateText;
            input.crossOrigin = "Anonymous";
            if (!this.LoadingSpinnerEnabled() && !this.OpacityShowEnabled()) {
                input.src = this.getImageSourceLink();
            }

            output = this.addElement(this.Id() + "_Background")
                .StyleClassName(GeneralCssNames.BACKGROUND)
                .Add(this.addElement(this.Id() + "_Loading")
                    .StyleClassName("Loading")
                    .Visible(this.LoadingSpinnerEnabled())
                    .Width(this.width).Height(this.height)
                    .Add(this.addElement(this.Id() + "_LoadingContent")
                        .StyleClassName("Content")
                        .Add(this.loadingSpinner)
                        .Add(this.loadingText)
                    )
                )
                .Add(this.addElement(this.Id() + "_Loaded")
                    .StyleClassName("Loaded")
                    .Visible(!this.LoadingSpinnerEnabled())
                    .Add(input)
                )
                .Add(this.addElement(this.Id() + "_Foreground").StyleClassName(GeneralCssNames.FOREGROUND));
        }

        return this.addElement().StyleClassName(this.guiType).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(GeneralCssNames.OFF)
                .Add(output)
            );
    }

    protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : string) => void) : void {
        let image : HTMLCanvasElement = <HTMLCanvasElement>ElementManager.getElement(this.Id() + "_Image");
        if (!ObjectValidator.IsEmptyOrNull(this.outputArgs) && this.useCanvas()) {
            this.canvasData = image.toDataURL("image/png");
            const tmpImage : HTMLImageElement = document.createElement("img");
            tmpImage.id = this.Id() + "_Image";
            image.parentNode.appendChild(tmpImage);
            image.parentNode.removeChild(image);
            image = <HTMLCanvasElement>ElementManager.getElement(this.Id() + "_Image", true);
        }

        if (this.LoadingSpinnerEnabled() || this.OpacityShowEnabled()) {
            ElementManager.Hide(this.Id() + "_Loaded");
            ElementManager.Show(this.Id() + "_Loading");

            if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME && this.OpacityShowEnabled()) {
                ElementManager.setOpacity(this.Id() + "_Image", 0);
                let cssClassName : string = GeneralCssNames.HIDDEN;
                if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                    cssClassName += " " + this.source;
                }
                ElementManager.setClassName(this.Id() + "_Image", cssClassName);
            } else if (this.sourceType === ImageDataSourceType.FILE_SYSTEM) {
                image.removeAttribute("src");
            }
        }

        $preparationResultHandler(this.Id(), "");
    }

    protected afterCacheCreation($id : string, $value : string) : void {
        ElementManager.Hide(this.Id() + "_Loading");
        ElementManager.Show(this.Id() + "_Loaded");

        const image : HTMLImageElement = <HTMLImageElement>ElementManager.getElement(this.Id() + "_Image");
        if (!ObjectValidator.IsEmptyOrNull(this.canvasData)) {
            image.src = this.canvasData;
        } else {
            if (this.sourceType === ImageDataSourceType.STYLE_CLASS_NAME && this.OpacityShowEnabled()) {
                ElementManager.setOpacity(this.Id() + "_Image", 100);
                ElementManager.setClassName(this.Id() + "_Image", this.source);
            } else if (this.sourceType === ImageDataSourceType.FILE_SYSTEM) {
                if (this.LoadingSpinnerEnabled() || this.OpacityShowEnabled()) {
                    image.crossOrigin = "Anonymous";
                    image.src = this.getImageSourceLink();
                }
            }
        }
    }

    private getSelector() : LinkSelector {
        if (!ObjectValidator.IsSet(this.selector)) {
            this.selector = new LinkSelector(this);
            this.selector.setLinkFormatter(($value : string) : string => {
                const manager : HttpManager = this.getHttpManager();
                if (ObjectValidator.IsEmptyOrNull(this.getSelector().getValue())) {
                    if (this.alternateText.Text() !== this.alternateText.Id() &&
                        !ObjectValidator.IsEmptyOrNull(this.alternateText.Text())) {
                        $value = manager.getRequest().getHostUrl() +
                            "#" + manager.CreateLink("/" + this.alternateText.Text());
                    } else {
                        $value = StringUtils.Substring(
                                manager.getRequest().getHostUrl(), 0, StringUtils.IndexOf(manager.getRequest().getHostUrl(), "/", false)) +
                            "/" + this.sourceAlternateText;
                    }
                } else {
                    $value = manager.CreateLink($value);
                    if (StringUtils.Contains($value, manager.getRequest().getRelativeRoot())) {
                        if (this.getSelector().OpenInNewWindow()) {
                            $value = manager.getRequest().getHostUrl() + "#" + $value;
                        } else {
                            $value = "#" + $value;
                        }
                    }
                }
                return $value;
            });
        }
        return this.selector;
    }

    private applyOutputArgs($input : HTMLImageElement) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.outputArgs) && this.IsLoaded()) {
            this.outputSource = $input;
            if (this.outputArgs.getWidth() === 0 || this.outputArgs.getHeight() === 0) {
                if (this.getWidth() > 0 || this.getHeight() > 0) {
                    this.outputArgs.setSize(this.getWidth(), this.getHeight());
                } else {
                    this.outputArgs.setSize(
                        ElementManager.getCssIntegerValue(this.Id() + "_Image", "width"),
                        ElementManager.getCssIntegerValue(this.Id() + "_Image", "height"));
                }
                if (this.outputArgs.getWidth() === 0 || this.outputArgs.getHeight() === 0) {
                    this.outputArgs.setSize($input.naturalWidth, $input.naturalHeight);
                }
            }

            Image.resizeLoader(this);

            const showImage : any = () : void => {
                if (ElementManager.IsVisible(this.Id() + "_Loading")) {
                    ElementManager.Hide(this.Id() + "_Loading");
                }
                ElementManager.Show(this.Id() + "_Loaded");
                if (!this.sourceLoaded) {
                    this.sourceLoaded = true;
                    if (this.OpacityShowEnabled()) {
                        ElementManager.setClassName(ElementManager.getElement(this.Id() + "_Image"), GeneralCssNames.HIDDEN);
                        ElementManager.ChangeOpacity(this.Id() + "_Image", DirectionType.UP, 10);
                    }
                }
                Image.TurnOff(this);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                this.getEventsManager().FireEvent(Image.ClassName(), EventType.ON_LOAD, eventArgs);
            };

            if (!ObjectValidator.IsEmptyOrNull($input)) {
                if (this.useCanvas()) {
                    const output : HTMLCanvasElement = document.createElement("canvas");
                    output.width = this.outputArgs.getWidth();
                    output.height = this.outputArgs.getHeight();
                    let source : HTMLCanvasElement = ImageTransform.ToCanvas($input);
                    if (this.outputArgs.Quality() < 100) {
                        source = ImageTransform.Quality(source, this.outputArgs.Quality() / 100);
                    }
                    source = ImageTransform.Resize(source, output.width, output.height,
                        this.outputArgs.FillEnabled(), this.outputArgs.Rotation());

                    const sourceContext : CanvasRenderingContext2D = source.getContext("2d");
                    let pixels : ImageData = sourceContext.getImageData(0, 0, source.width, source.height);

                    if (this.outputArgs.BlurEnabled()) {
                        pixels = ImageFilters.GaussianBlur(pixels, 50);
                    }
                    if (this.outputArgs.InvertEnabled()) {
                        pixels = ImageFilters.Invert(pixels);
                    }
                    if (this.outputArgs.GrayscaleEnabled()) {
                        pixels = ImageFilters.Grayscale(pixels);
                    }
                    if (this.outputArgs.Brightness() !== 0) {
                        pixels = ImageFilters.Brightness(pixels, this.outputArgs.Brightness());
                    }
                    if (this.outputArgs.Contrast() !== 0) {
                        pixels = ImageFilters.Contrast(pixels, this.outputArgs.Contrast());
                    }
                    sourceContext.clearRect(0, 0, source.width, source.height);
                    sourceContext.putImageData(pixels, 0, 0);

                    source = ImageTransform.Zoom(source, this.outputArgs.Zoom());
                    source = ImageTransform.Crop(source, this.outputArgs.CropDimension());

                    const finalizeCanvas : ($input : HTMLCanvasElement, $output : HTMLCanvasElement) => void =
                        ($input : HTMLCanvasElement, $output : HTMLCanvasElement) : void => {
                            const rotation : number = this.outputArgs.Rotation();
                            if (!this.outputArgs.CornerOnEnvelop()) {
                                $input = ImageTransform.ModifyCorners($input, this.outputArgs.Corners(), rotation);
                            }
                            if (rotation === 0 || rotation === 180 || rotation === 360) {
                                $input = ImageTransform.ToCenter($input, $output.width, $output.height);
                            } else if ((rotation === 90 || rotation === 270) && (
                                $output.width > $output.height && $input.height > $input.width ||
                                $output.height > $output.width && $input.width > $input.height)) {
                                $input = ImageTransform.ToCenter($input, $output.height, $output.width);
                            } else {
                                $input = ImageTransform.ToCenter($input, $output.width * 2, $output.height * 2);
                            }
                            $input = ImageTransform.Rotate($input, rotation, $output.width, $output.height);

                            if (this.outputArgs.CornerOnEnvelop()) {
                                $input = ImageTransform.ModifyCorners($input, this.outputArgs.Corners());
                            }
                            $output.getContext("2d").drawImage($input, 0, 0, $output.width, $output.height);
                            $output.id = this.Id() + "_Image";
                            const target : HTMLImageElement = <HTMLImageElement>ElementManager.getElement(this.Id() + "_Image", true);
                            target.parentNode.appendChild($output);
                            target.parentNode.removeChild(target);
                            ElementManager.CleanElementCache(this.Id() + "_Image");
                            showImage();
                        };

                    if (this.outputArgs.WaterMarkEnabled() && !ObjectValidator.IsEmptyOrNull(this.outputArgs.WaterMarkSource())) {
                        const waterMarkInput : HTMLImageElement = document.createElement("img");
                        waterMarkInput.crossOrigin = "Anonymous";
                        waterMarkInput.onload = () : void => {
                            try {
                                source = ImageTransform.AddWatermark(source, ImageTransform.ToCanvas(waterMarkInput),
                                    this.outputArgs.FillEnabled(), this.outputArgs.Rotation());
                                finalizeCanvas(source, output);
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        };
                        waterMarkInput.onerror = () : void => {
                            try {
                                finalizeCanvas(source, output);
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        };
                        let src : string = this.outputArgs.WaterMarkSource();
                        if (!this.outputArgs.FrontEndCacheEnabled()) {
                            src += "?dummy=" + new Date().getTime();
                        }
                        waterMarkInput.src = src;
                    } else {
                        finalizeCanvas(source, output);
                    }
                } else if (this.sourceType !== ImageDataSourceType.STYLE_CLASS_NAME) {
                    $input.src = this.getImageSourceLink();
                } else {
                    showImage();
                }
            }
        }
    }

    private getImageSourceLink() : string {
        if (ObjectValidator.IsEmptyOrNull(this.sourceLink)) {
            return "";
        }
        if (StringUtils.StartsWith(this.sourceLink, "data:image/")) {
            return this.sourceLink;
        }
        const manager : HttpManager = this.getHttpManager();
        if (this.useCanvas()) {
            return this.sourceLink +
                (StringUtils.Contains(this.sourceLink, "?") ? "&" : "?") + "version=" + this.version +
                (!ObjectValidator.IsEmptyOrNull(this.outputArgs) && !this.outputArgs.FrontEndCacheEnabled() ?
                    "&dummy=" + new Date().getTime() : "");
        } else if (manager.getRequest().IsOnServer() && !manager.getRequest().IsIdeaHost()) {
            if (!ObjectValidator.IsEmptyOrNull(this.outputArgs)) {
                return manager.CreateLink("/DynamicImage/" +
                    this.sourceType + "/" + ObjectEncoder.Base64(this.sourceLink) +
                    "/" + this.outputArgs.ToUrlData() +
                    "/" + this.version);
            } else if (this.sourceType !== ImageDataSourceType.STYLE_CLASS_NAME) {
                return manager.CreateLink("/DynamicImage/" +
                    this.sourceType + "/" + ObjectEncoder.Base64(this.sourceLink) +
                    "/" + this.version);
            }
        }
        return this.sourceLink + (StringUtils.Contains(this.sourceLink, "?") ? "&" : "?") + "version=" + this.version;
    }

    private useCanvas() : boolean {
        return ImageTransform.IsSupported() &&
            (ObjectValidator.IsEmptyOrNull(this.outputArgs) ||
                !ObjectValidator.IsEmptyOrNull(this.outputArgs) && this.outputArgs.CanvasEnabled());
    }
}
