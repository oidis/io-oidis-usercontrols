/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IButtonEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IButtonEvents.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiCommonsListArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsListArg.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IButton } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IButton.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { DragBar } from "../Components/DragBar.js";
import { ResizeBar } from "../Components/ResizeBar.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { ButtonType } from "../Enums/UserControls/ButtonType.js";
import { ImageButton } from "./ImageButton.js";

/**
 * Button class renders button type of element.
 */
export class Button extends ImageButton implements IButton {
    private text : string;
    private width : number;

    /**
     * @param {Button} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOn($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element) &&
            !$manager.IsActive(<IClassName>ScrollBar) &&
            !$manager.IsActive(<IClassName>ResizeBar) &&
            !$manager.IsActive(<IClassName>DragBar)) {
            ElementManager.TurnOn($element.Id() + "_Enabled");
        }
    }

    /**
     * @param {Button} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOff($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element)) {
            ElementManager.TurnOff($element.Id() + "_Enabled");
        }
    }

    /**
     * @param {Button} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnActive($element : Button, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        ElementManager.TurnActive($element.Id() + "_Enabled");
    }

    /**
     * @param {Button} $element Specify element, which should be handled.
     * @param {boolean} [$value] Specify state of button selection.
     * @returns {void}
     */
    public static TurnSelected($element : Button, $value : boolean) : void {
        ImageButton.TurnSelected($element, $value);
        Button.resize($element);
    }

    private static resize($element : Button) : void {
        if ($element.Width() !== -1) {
            const resize : any = ($id : string) : void => {
                const width : number = $element.Width();
                const centerWidth : number = width -
                    ElementManager.getOffsetWidth($id + "Left") -
                    ElementManager.getOffsetWidth($id + "Right");

                const contentEnvelopWidth : number = width -
                    ElementManager.getCssIntegerValue($id + "Left", "border-left-width") -
                    ElementManager.getCssIntegerValue($id + "Right", "border-right-width");

                const maxTextWidth : number = contentEnvelopWidth -
                    ElementManager.getWidthOffset($id + "Text") -
                    ElementManager.getEnvelopWidth($id + "IconEnvelop") -
                    ElementManager.getEnvelopWidth($id + "Splitter");

                ElementManager.setWidth($id + "ContentEnvelop", contentEnvelopWidth);
                ElementManager.setWidth($id + "Center", centerWidth);
                ElementManager.setCssProperty($id + "Text", "width", maxTextWidth);
            };

            const id : string = $element.Id();
            if (ElementManager.IsVisible($element.Id() + "_Enabled")) {
                if ($element.IsSelected() && $element.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED) &&
                    ElementManager.Exists(id + "_StaticContent")) {
                    resize(id + "_Static");
                } else {
                    resize(id + "_");
                }
            } else if ($element.getGuiOptions().Contains(GuiOptionType.DISABLE) &&
                ElementManager.Exists(id + "_DisabledContent")) {
                resize(id + "_Disabled");
            }

            $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE);
            $element.getEventsManager().FireEvent(Button.ClassName(), EventType.ON_RESIZE);
        }
    }

    /**
     * @param {ButtonType} [$buttonType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($buttonType? : ButtonType, $id? : string) {
        super("", $id);
        this.text = this.Id();
        this.width = -1;
        this.GuiType($buttonType);
    }

    /**
     * @returns {IButtonEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : IButtonEvents {
        return <IButtonEvents>super.getEvents();
    }

    /**
     * @param {ButtonType} [$buttonType] Specify type of element look and feel.
     * @returns {ButtonType} Returns type of element's look and feel.
     */
    public GuiType($buttonType? : ButtonType) : ButtonType {
        return <ButtonType>super.GuiType($buttonType);
    }

    /**
     * @param {string} [$value] Set text value, which should be displayed as element context.
     * @returns {string} Returns element's text value.
     */
    public Text($value? : string) : string {
        if (ObjectValidator.IsSet($value) && this.text !== $value) {
            this.text = Property.String(this.text, $value);
            if (this.IsLoaded()) {
                ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                ElementManager.setInnerHtml(this.Id() + "_StaticText", $value);
                ElementManager.setInnerHtml(this.Id() + "_DisabledText", $value);
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
                this.getEventsManager().FireEvent(Button.ClassName(), EventType.ON_CHANGE);
                this.getEvents().FireAsynchronousMethod(() : void => {
                    Button.resize(this);
                }, false);
            }
        }
        return this.text;
    }

    /**
     * @param {number} [$value] Specify element's width value.
     * If value is -1, element will has auto size, otherwise width has to be more that 30.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            if (this.width !== $value) {
                this.width = Property.PositiveInteger(this.width, $value, 30);
                if ($value === -1) {
                    if (!this.IsLoaded()) {
                        this.width = $value;
                    } else {
                        this.width = -1;
                        this.width = this.Width();
                    }
                }

                if (ElementManager.IsVisible(this.Id())) {
                    Button.resize(this);
                }
            }
        } else {
            if (this.width === -1 && this.IsLoaded()) {
                this.width = ElementManager.getElement(this.Id()).offsetWidth;
                const getWidth : any = ($id : string) : number => {
                    ElementManager.ClearCssProperty($id + "Text", "width");
                    ElementManager.setCssProperty($id + "Content", "position", "fixed");
                    let contentWidth : number = ElementManager.getElement($id + "Content").offsetWidth;
                    if (contentWidth === 0) {
                        this.unhide(($elementId : string, $element : HTMLElement) : boolean => {
                            if ($elementId === $id + "Content") {
                                contentWidth = 1 + $element.offsetWidth;
                                return false;
                            }
                            return true;
                        });
                    }
                    const width : number = contentWidth +
                        ElementManager.getCssIntegerValue($id + "Left", "border-left-width") +
                        ElementManager.getCssIntegerValue($id + "Right", "border-right-width");
                    ElementManager.setCssProperty($id + "Content", "position", "relative");
                    return width;
                };
                if (this.IsSelected() && this.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED)) {
                    this.width = getWidth(this.Id() + "_Static");
                } else if (!this.Enabled() && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    this.width = getWidth(this.Id() + "_Disabled");
                } else if (this.Enabled()) {
                    this.width = getWidth(this.Id() + "_");
                }
            }
        }
        return this.width;
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        let enabled : boolean = super.Enabled();
        if (ObjectValidator.IsSet($value)) {
            if (enabled !== $value) {
                enabled = super.Enabled($value);
                if (ElementManager.IsVisible(this.Id())) {
                    Button.resize(this);
                }
            }
        }
        return enabled;
    }

    /**
     * @returns {Size} Returns current element's width and height based on available information.
     */
    public getSize() : Size {
        const size : Size = super.getSize();
        size.Width(this.Width());
        if (this.IsLoaded()) {
            size.Height(ElementManager.getElement(this.guiContentId()).offsetHeight);
        }
        return size;
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const args : IGuiCommonsArg[] = super.getArgs();
        let index : number;
        for (index = 0; index < args.length; index++) {
            if (args[index].name === "Value") {
                args[index].value = this.Text();
                break;
            }
        }
        args.push({
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: this.Text()
        });
        args.push({
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: this.Width()
        });
        args.push(<IGuiCommonsListArg>{
            items: ButtonType.getProperties(),
            name : "GuiType",
            type : GuiCommonsArgType.LIST,
            value: ButtonType.getKey(<string>this.GuiType())
        });
        return args;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
     * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
     * @returns {void}
     */
    public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
        switch ($value.name) {
        case "Value":
        case "Text":
            this.Text(<string>$value.value);
            break;
        case "Width":
            this.Width(<number>$value.value);
            break;
        case "Height":
            ElementManager.setCssProperty(this.Id(), "height", <number>$value.value);
            break;
        case "GuiType":
            this.GuiType(ButtonType[<string>$value.value]);
            break;
        default:
            super.setArg($value, $force);
            break;
        }
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, ButtonType, ButtonType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!ButtonType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of button type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            Button.resize($eventArgs.Owner());
        });

        this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
            Button.resize($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const selectedOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
            selectedOption
                .Id(this.Id() + "_Static")
                .StyleClassName(GeneralCssNames.ON).GuiTypeTag(this.getGuiTypeTag()).Visible(this.IsSelected())
                .Add(this.addElement()
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_StaticLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_StaticCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(this.Width())
                        .Add(this.addElement(this.Id() + "_StaticContentEnvelop")
                            .StyleClassName("Envelop")
                            .Add(this.addElement(this.Id() + "_StaticContent")
                                .StyleClassName("Content")
                                .Add(this.addElement(this.Id() + "_StaticIconEnvelop")
                                    .StyleClassName(this.IconName())
                                    .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                )
                                .Add(this.addElement(this.Id() + "_StaticSplitter").StyleClassName(GeneralCssNames.SPLITTER))
                                .Add(this.addElement(this.Id() + "_StaticText")
                                    .StyleClassName(GeneralCssNames.TEXT)
                                    .Add(this.text)
                                )
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_StaticRight").StyleClassName(GeneralCssNames.RIGHT))
                );
        }

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            disabledOption
                .Id(this.Id() + "_Disabled")
                .StyleClassName(GeneralCssNames.DISABLE).GuiTypeTag(this.getGuiTypeTag()).Visible(!this.Enabled())
                .Add(this.addElement()
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_DisabledLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_DisabledCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(this.Width())
                        .Add(this.addElement(this.Id() + "_DisabledContentEnvelop")
                            .StyleClassName("Envelop")
                            .Add(this.addElement(this.Id() + "_DisabledContent")
                                .StyleClassName("Content")
                                .Add(this.addElement(this.Id() + "_DisabledIconEnvelop")
                                    .StyleClassName(this.IconName())
                                    .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                )
                                .Add(this.addElement(this.Id() + "_DisabledSplitter").StyleClassName(GeneralCssNames.SPLITTER))
                                .Add(this.addElement(this.Id() + "_DisabledText")
                                    .StyleClassName(GeneralCssNames.TEXT)
                                    .Add(this.text)
                                )
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_DisabledRight").StyleClassName(GeneralCssNames.RIGHT))
                );
        }

        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.errorCssName())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .StyleClassName(GeneralCssNames.OFF)
                    .Visible(this.Enabled())
                    .Add(this.addElement(this.Id() + "_Active")
                        .Visible(!this.IsSelected()).GuiTypeTag(this.getGuiTypeTag())
                        .Add(this.selectorElement())
                        .Add(this.addElement()
                            .StyleClassName(GeneralCssNames.BACKGROUND)
                            .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                            .Add(this.addElement(this.Id() + "_Center")
                                .StyleClassName(GeneralCssNames.CENTER)
                                .Width(this.Width())
                                .Add(this.addElement(this.Id() + "_ContentEnvelop")
                                    .StyleClassName("Envelop")
                                    .Add(this.addElement(this.Id() + "_Content")
                                        .StyleClassName("Content")
                                        .Add(this.addElement(this.Id() + "_IconEnvelop")
                                            .StyleClassName(this.IconName())
                                            .Add(this.addElement().StyleClassName(GeneralCssNames.ICON))
                                        )
                                        .Add(this.addElement(this.Id() + "_Splitter").StyleClassName(GeneralCssNames.SPLITTER))
                                        .Add(this.addElement(this.Id() + "_Text")
                                            .StyleClassName(GeneralCssNames.TEXT)
                                            .Add(this.text)
                                        )
                                    )
                                )
                            )
                            .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                        )
                    )
                    .Add(selectedOption)
                )
                .Add(disabledOption)
            );
    }

    protected guiContentId() : string {
        if (this.IsSelected() && this.getGuiOptions().Contains(GuiOptionType.SELECTED, GuiOptionType.ACTIVED)) {
            return this.Id() + "_StaticContentEnvelop";
        } else if (!this.Enabled() && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            return this.Id() + "_DisabledContentEnvelop";
        } else if (this.Enabled()) {
            return this.Id() + "_ContentEnvelop";
        }
        return this.Id();
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.text = this.Id();
        this.width = -1;
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("text", "width");
        return exclude;
    }
}
