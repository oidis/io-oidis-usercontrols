/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { ProgressBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ProgressBarEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { IProgressBarEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IProgressBarEvents.js";
import { IEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/IEventsHandler.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IProgressBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IProgressBar.js";
import { Borders } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Borders.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { BaseGuiObject } from "../../Primitives/BaseGuiObject.js";
import { ProgressBarType } from "../Enums/UserControls/ProgressBarType.js";

/**
 * ProgressBar class renders element for capturing of process progression
 */
export class ProgressBar extends BaseGuiObject implements IProgressBar {
    private value : number;
    private rangeStart : number;
    private rangeEnd : number;
    private width : number;
    private guiType : ProgressBarType;

    private static resize($element : ProgressBar) : void {
        const id : string = $element.Id();
        const borders : Borders = new Borders();
        borders.Left(ElementManager.getOffsetWidth(id + "_Left"));
        borders.Right(ElementManager.getOffsetWidth(id + "_Right"));
        const centerWidth : number = $element.Width() - borders.Left() - borders.Right();
        ElementManager.setWidth(id + "_Center", centerWidth);

        const offset : number = 2 * ElementManager.getCssIntegerValue(id + "_BarEnvelop", "left") +
            ElementManager.getCssIntegerValue(id + "_Left", "border-left-width") +
            ElementManager.getCssIntegerValue(id + "_Right", "border-right-width");
        borders.Left(ElementManager.getOffsetWidth(id + "_BarLeft"));
        borders.Right(ElementManager.getOffsetWidth(id + "_BarRight"));

        ElementManager.setWidth(id + "_BarEnvelop", $element.Width() - offset);
        ElementManager.setWidth(id + "_BarCenter", $element.Width() - borders.Left() - borders.Right() - offset);

        if ($element.IsCompleted()) {
            const percentage : number = 1 /
                ($element.RangeEnd() - $element.RangeStart()) * ($element.Value() - $element.RangeStart());

            ElementManager.setWidth(id + "_Bar", Math.ceil($element.Width() * percentage));
        }
        ProgressBar.process($element);
    }

    private static process($element : ProgressBar) : void {
        const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
        manipulatorArgs.Owner($element);
        if (manipulatorArgs.CurrentValue() < $element.Value()) {
            manipulatorArgs.DirectionType(DirectionType.UP);
            manipulatorArgs.RangeStart($element.RangeStart());
            manipulatorArgs.RangeEnd($element.Value());
        } else {
            manipulatorArgs.DirectionType(DirectionType.DOWN);
            manipulatorArgs.RangeStart($element.Value());
            manipulatorArgs.RangeEnd($element.RangeEnd());
        }
        manipulatorArgs.ProgressType(ProgressType.LINEAR);
        manipulatorArgs.Step(3);

        manipulatorArgs.ChangeEventType(this.ClassName() + "_" + EventType.ON_CHANGE);
        manipulatorArgs.CompleteEventType(this.ClassName() + "_" + EventType.ON_COMPLETE);

        const eventHandler : IEventsHandler = ($eventArgs : ValueProgressEventArgs) : void => {
            const element : ProgressBar = <ProgressBar>$eventArgs.Owner();
            const percentage : number = 1 /
                (element.RangeEnd() - element.RangeStart()) * ($eventArgs.CurrentValue() - element.RangeStart());

            ElementManager.setWidth(element.Id() + "_Bar", Math.ceil(element.Width() * percentage));

            const eventArgs : ProgressBarEventArgs = new ProgressBarEventArgs();
            eventArgs.Owner(element);
            eventArgs.CurrentValue($eventArgs.CurrentValue());
            eventArgs.RangeStart(element.RangeStart());
            eventArgs.RangeEnd(element.RangeEnd());
            eventArgs.Percentage(percentage * 100);

            element.getEventsManager().FireEvent(element, EventType.ON_CHANGE, eventArgs);
            element.getEventsManager().FireEvent(ProgressBar.ClassName(), EventType.ON_CHANGE, eventArgs);
        };

        $element.getEventsManager().setEvent($element, this.ClassName() + "_" + EventType.ON_CHANGE, eventHandler);
        $element.getEventsManager().setEvent($element, this.ClassName() + "_" + EventType.ON_COMPLETE, eventHandler);
        ValueProgressManager.Execute(manipulatorArgs);
    }

    /**
     * @param {ProgressBarType} [$progressBarType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($progressBarType? : ProgressBarType, $id? : string) {
        super($id);
        this.value = 0;
        this.rangeStart = 0;
        this.rangeEnd = 100;
        this.width = 250;
        this.guiType = this.guiTypeValueSetter($progressBarType);
    }

    /**
     * @returns {IProgressBarEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : IProgressBarEvents {
        return <IProgressBarEvents>super.getEvents();
    }

    /**
     * @param {ProgressBarType} [$progressBarType] Specify type of element look and feel.
     * @returns {ProgressBarType} Returns type of element's look and feel.
     */
    public GuiType($progressBarType? : ProgressBarType) : ProgressBarType {
        if (ObjectValidator.IsSet($progressBarType)) {
            this.guiType = this.guiTypeValueSetter($progressBarType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    /**
     * @param {number} [$value]  Value or arguments object, which should be set to the ProgressBar#
     * @returns {number} Returns  object value.
     */
    public Value($value? : number) : number {
        if (this.Enabled()) {
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (this.value !== $value) {
                    this.setChanged();
                }
            }

            if (ObjectValidator.IsInteger($value)) {
                if ($value < this.rangeStart) {
                    $value = this.rangeStart;
                }
                if ($value > this.rangeEnd) {
                    $value = this.rangeEnd;
                }
                this.value = $value;
            }

            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    ProgressBar.process(this);
                }, false);
            }
        }
        return this.value;
    }

    /**
     * @param {number} [$value] Specify start value of the progress.
     * @returns {number} Returns start value of the progress.
     */
    public RangeStart($value? : number) : number {
        this.rangeStart = Property.Integer(this.rangeStart, $value);
        if (this.value < this.rangeStart) {
            this.value = this.rangeStart;
        }
        if (this.rangeEnd < this.rangeStart) {
            this.rangeEnd = this.rangeStart;
        }
        return this.rangeStart;
    }

    /**
     * @param {number} [$value] Specify end value of the progress.
     * @returns {number} Returns end value of the progress.
     */
    public RangeEnd($value? : number) : number {
        this.rangeEnd = Property.Integer(this.rangeEnd, $value, this.rangeStart);
        if (this.value > this.rangeEnd) {
            this.value = this.rangeEnd;
        }
        return this.rangeEnd;
    }

    /**
     * @param {number} [$value] Specify element's width value.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        this.width = Property.PositiveInteger(this.width, $value, 50);
        if (ObjectValidator.IsSet($value) && this.IsLoaded()) {
            ProgressBar.resize(this);
        }
        return this.width;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, ProgressBarType, ProgressBarType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!ProgressBarType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of ProgressBar type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnStart(($eventArgs : EventArgs) : void => {
            ProgressBar.resize($eventArgs.Owner());
        });

        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            const element : ProgressBar = <ProgressBar>$eventArgs.Owner();
            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(element.Id());
            manipulatorArgs.Owner(element);
            if (element.IsCached()) {
                manipulatorArgs.CurrentValue(0);
            }
        });

        this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
            ProgressBar.resize($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        let statusCss : string = "";
        if (!this.Enabled()) {
            statusCss = GeneralCssNames.DISABLE;
        }
        const width90 : number = Math.ceil(this.Width() * 0.9);

        return this.addElement(this.Id() + "_Type").StyleClassName(this.guiType)
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(statusCss)
                .GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement()
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_Center")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(width90)
                        .Add(this.addElement(this.Id() + "_Bar")
                            .StyleClassName("Bar")
                            .Add(this.addElement(this.Id() + "_BarEnvelop")
                                .StyleClassName("Envelop")
                                .Add(this.addElement(this.Id() + "_BarLeft").StyleClassName(GeneralCssNames.LEFT))
                                .Add(this.addElement(this.Id() + "_BarCenter").StyleClassName(GeneralCssNames.CENTER))
                                .Add(this.addElement(this.Id() + "_BarRight").StyleClassName(GeneralCssNames.RIGHT))
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                )
            );
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.value = 0;
        this.rangeStart = 0;
        this.rangeEnd = 100;
        this.width = 250;
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "value", "rangeStart", "rangeEnd",
            "width"
        );

        return exclude;
    }

    protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : number) => void) : void {
        ElementManager.setWidth(this.Id() + "_Bar", 0);
        const percentage : number = 1 /
            (this.RangeEnd() - this.RangeStart()) * (this.Value() - this.RangeStart());
        $preparationResultHandler(this.Id() + "_Bar", Math.ceil(this.Width() * percentage));
    }

    protected afterCacheCreation($id : string, $value : number) : void {
        ElementManager.setWidth($id, $value);
    }
}
