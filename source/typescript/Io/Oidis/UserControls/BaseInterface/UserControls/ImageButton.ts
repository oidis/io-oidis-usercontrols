/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IImageButton } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IImageButton.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { FormsObject } from "../../Primitives/FormsObject.js";
import { DragBar } from "../Components/DragBar.js";
import { ResizeBar } from "../Components/ResizeBar.js";
import { ScrollBar } from "../Components/ScrollBar.js";
import { IconType } from "../Enums/UserControls/IconType.js";
import { ImageButtonType } from "../Enums/UserControls/ImageButtonType.js";

/**
 * ImageButton class renders element with button behaviour, but without possibility to set the element's size.
 */
export class ImageButton extends FormsObject implements IImageButton {
    private guiType : ImageButtonType;
    private iconType : IconType;
    private isSelected : boolean;

    /**
     * @param {ImageButton} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOn($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element) &&
            !$manager.IsActive(<IClassName>ScrollBar) &&
            !$manager.IsActive(<IClassName>ResizeBar) &&
            !$manager.IsActive(<IClassName>DragBar)) {
            ElementManager.TurnOn($element.Id() + "_Active");
        }
    }

    /**
     * @param {ImageButton} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOff($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive($element)) {
            ElementManager.TurnOff($element.Id() + "_Active");
        }
    }

    /**
     * @param {ImageButton} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnActive($element : ImageButton, $manager? : GuiObjectManager,
                             $reflection? : Reflection) : void {
        ElementManager.TurnActive($element.Id() + "_Active");
    }

    /**
     * @param {ImageButton} $element Specify element, which should be handled.
     * @param {boolean} [$value] Specify state of button selection.
     * @returns {void}
     */
    public static TurnSelected($element : ImageButton, $value : boolean) : void {
        if ($value) {
            ElementManager.Hide($element.Id() + "_Active");
            ElementManager.Show($element.Id() + "_Static");
        } else {
            ElementManager.Hide($element.Id() + "_Static");
            ElementManager.Show($element.Id() + "_Active");
        }
    }

    protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
        $eventArgs.StopAllPropagation();
        $manager.setHovered($eventArgs.Owner(), true);
    }

    /**
     * @param {ImageButtonType} [$imageButtonType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($imageButtonType? : ImageButtonType, $id? : string) {
        super($id);
        this.guiType = this.guiTypeValueSetter($imageButtonType);
    }

    /**
     * @param {ImageButtonType} [$imageButtonType] Specify type of element look and feel.
     * @returns {ImageButtonType} Returns type of element's look and feel.
     */
    public GuiType($imageButtonType? : ImageButtonType) : ImageButtonType {
        if (ObjectValidator.IsSet($imageButtonType)) {
            this.guiType = this.guiTypeValueSetter($imageButtonType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    /**
     * @param {IconType} [$value] Specify type of icon, which should be rendered with the element.
     * @returns {IconType} Returns type of icon, which belongs to the element.
     */
    public IconName($value? : IconType) : IconType {
        if (ObjectValidator.IsSet($value)) {
            this.iconType = $value;
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_IconEnvelop", this.iconType.toString());
                if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
                    ElementManager.setClassName(this.Id() + "_StaticIconEnvelop", this.iconType.toString());
                }
                if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    ElementManager.setClassName(this.Id() + "_DisabledIconEnvelop", this.iconType.toString());
                }
            }
        }
        if (!ObjectValidator.IsSet(this.iconType)) {
            this.iconType = "";
        }
        return this.iconType;
    }

    /**
     * @param {boolean} [$value] Specify element's look and feel status.
     * @returns {boolean} Returns element's look and feel status.
     */
    public IsSelected($value? : boolean) : boolean {
        this.isSelected = Property.Boolean(this.isSelected, $value);
        if (this.isSelected && !this.IsLoaded()) {
            this.getGuiOptions().Add(GuiOptionType.ACTIVED);
        }
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id()) &&
            this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
            Reflection.getInstance().getClass(this.getClassName()).TurnSelected(this, this.isSelected);
        }
        if (ObjectValidator.IsEmptyOrNull(this.isSelected)) {
            this.isSelected = false;
        }
        return this.isSelected;
    }

    public IsPreventingScroll() : boolean {
        return true;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, ImageButtonType, ImageButtonType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!ImageButtonType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of imageButton type instead of StyleClassName method.");
        return false;
    }

    protected availableGuiOptions() : ArrayList<GuiOptionType> {
        const options : ArrayList<GuiOptionType> = super.availableGuiOptions();
        options.Add(GuiOptionType.ACTIVED);
        options.Add(GuiOptionType.SELECTED);
        return options;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().Subscriber(this.Id() + "_Active");

        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
        this.getEvents().setOnMouseOver(thisClass.onHoverEventHandler);

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const selectedOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.ACTIVED, GuiOptionType.SELECTED)) {
            selectedOption
                .Id(this.Id() + "_Static").StyleClassName(GeneralCssNames.ON).Visible(this.IsSelected())
                .Add(this.addElement(this.Id() + "_StaticIconEnvelop")
                    .StyleClassName(this.IconName())
                    .Add(this.addElement(this.Id() + "_StaticIcon").StyleClassName(GeneralCssNames.ICON))
                );
        }

        const disabledOption : IGuiElement = this.addElement();
        if (this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
            disabledOption
                .Id(this.Id() + "_Disabled").Visible(!this.Enabled())
                .Add(this.addElement(this.Id() + "_DisabledIconEnvelop")
                    .StyleClassName(this.IconName())
                    .Add(this.addElement(this.Id() + "_DisabledIcon").StyleClassName(GeneralCssNames.ICON))
                );
        }

        return this.addElement(this.Id() + "_Type")
            .StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_Status")
                .StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Enabled")
                    .Visible(this.Enabled())
                    .Add(this.addElement(this.Id() + "_Active")
                        .StyleClassName(GeneralCssNames.OFF).Visible(!this.IsSelected())
                        .Add(this.selectorElement())
                        .Add(this.addElement(this.Id() + "_IconEnvelop")
                            .StyleClassName(this.IconName())
                            .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
                        )
                    )
                    .Add(selectedOption)
                )
                .Add(disabledOption)
            );
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("isSelected", "iconType");
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        if (this.IsSelected() === false) {
            exclude.push("isSelected");
        }
        if (ObjectValidator.IsEmptyOrNull(this.iconType)) {
            exclude.push("iconType");
        }
        return exclude;
    }
}
