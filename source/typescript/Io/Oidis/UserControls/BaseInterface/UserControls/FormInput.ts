/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IFormInput } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IFormInput.js";
import { BaseGuiGroupObject } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseGuiGroupObject.js";
import { BaseGuiGroupObjectArgs } from "@io-oidis-gui/Io/Oidis/Gui/Structures/BaseGuiGroupObjectArgs.js";
import { FormsObject } from "../../Primitives/FormsObject.js";
import { BaseFormInputArgs } from "../../Structures/BaseFormInputArgs.js";
import { CheckBoxFormArgs } from "../../Structures/CheckBoxFormArgs.js";
import { DropDownListFormArgs } from "../../Structures/DropDownListFormArgs.js";
import { TextFieldFormArgs } from "../../Structures/TextFieldFormArgs.js";
import { ToolTip } from "../Components/ToolTip.js";
import { NotificationType } from "../Enums/Components/NotificationType.js";
import { DropDownListType } from "../Enums/UserControls/DropDownListType.js";
import { TextFieldType } from "../Enums/UserControls/TextFieldType.js";
import { CheckBox } from "./CheckBox.js";
import { DropDownList } from "./DropDownList.js";
import { Label } from "./Label.js";
import { TextField } from "./TextField.js";

/**
 * FormInput class renders group of GUI elements based on passed arguments.
 */
export class FormInput extends BaseGuiGroupObject implements IFormInput {

    /**
     * @param {BaseFormInputArgs} [$value] Specify configuration arguments for the GUI element.
     * @returns {BaseFormInputArgs} Returns the element's configuration arguments.
     */
    public Configuration($value? : BaseFormInputArgs) : BaseFormInputArgs {
        return <BaseFormInputArgs>super.Configuration($value);
    }

    /**
     * @returns {number} Returns calculated width in case of, that element has been loaded otherwise, returns configured width.
     */
    public getWidth() : number {
        if (this.IsCompleted()) {
            const element : FormsObject = this.getInputElement();
            if (!ObjectValidator.IsEmptyOrNull(element) && !element.IsMemberOf(CheckBox)) {
                return (<TextField>element).Width();
            }
            return super.getWidth();
        }
        return this.Configuration().Width();
    }

    public Width($value? : number) : number {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.Configuration().Width($value);
            this.Configuration(this.Configuration());
        }
        return this.getWidth();
    }

    /**
     * @param {string|number|boolean} [$value]  Value or arguments object, which should be set to the FormInput.
     * @returns {string|number|boolean} Returns value or arguments object, if value has been specified, otherwise null.
     */
    public Value($value? : string | number | boolean) : string | number | boolean {
        const element : FormsObject = this.getInputElement();
        if (!ObjectValidator.IsEmptyOrNull(element) && element.IsMemberOf(DropDownList)) {
            if (!this.IsLoaded()) {
                return this.Configuration().Value($value);
            } else {
                (<DropDownList>element).Select(<string | number>$value);
                return element.Value($value);
            }
        }
        return super.Value($value);
    }

    /**
     * Clean up value and items connected with Input element
     * @returns {void}
     */
    public Clear() : void {
        const element : FormsObject = this.getInputElement();
        if (!ObjectValidator.IsEmptyOrNull(element)) {
            if (element.IsMemberOf(DropDownList)) {
                const args : DropDownListFormArgs = <DropDownListFormArgs>this.Configuration();
                args.Clear();
                args.ForceSetValue(true);
                this.Configuration(args);
            } else if (element.IsMemberOf(CheckBox)) {
                (<CheckBox>element).Checked(false);
            } else {
                element.Value("");
            }
        }
    }

    /**
     * @returns {IToolTip} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IToolTip
     */
    protected getTitleClass() : any {
        return ToolTip;
    }

    protected registerGroup($args? : BaseFormInputArgs) : void {
        if ($args.IsMemberOf(CheckBoxFormArgs)) {
            this.registerElement(new CheckBox(), "input");
            this.registerElement(new Label("*", this.Id() + "_Required"), "requiredLabel");
        } else {
            this.registerElement(new Label("", this.Id() + "_Name"), "label");
            this.registerElement(new Label("*", this.Id() + "_Required"), "requiredLabel");
            if ($args.IsMemberOf(TextFieldFormArgs)) {
                this.registerElement(new TextField(TextFieldType.GENERAL), "input");
                this.getInputElement().Notification().GuiType(NotificationType.GENERAL);
            } else if ($args.IsMemberOf(DropDownListFormArgs)) {
                this.registerElement(new DropDownList(DropDownListType.GENERAL), "input");
            }
        }
    }

    protected getInputElement() : FormsObject {
        return this.getElement<FormsObject>("input");
    }

    protected innerCode() : IGuiElement {
        this.getElement<Label>("requiredLabel").StyleClassName("Required");

        return super.innerCode();
    }

    protected updateHandler($args? : BaseFormInputArgs) : void {
        super.updateHandler(<BaseGuiGroupObjectArgs>$args);

        this.getElement<Label>("requiredLabel").Text($args.RequiredContent());
        this.getElement<Label>("requiredLabel").Visible($args.IsRequired());

        if ($args.IsMemberOf(CheckBoxFormArgs)) {
            (<CheckBox>this.getInputElement()).Text($args.Name());
        } else {
            this.getElement<Label>("label").Text($args.Name());
            if ($args.IsMemberOf(TextFieldFormArgs)) {
                const input : TextField = <TextField>this.getInputElement();
                const args : TextFieldFormArgs = <TextFieldFormArgs>$args;
                input.Hint(args.Hint());
                input.LengthLimit(args.Size());
                if (!this.IsLoaded()) {
                    input.ReadOnly(args.ReadOnly());
                }
                if (args.IntegerOnly()) {
                    input.setOnlyNumbersAllowed();
                }
                if (args.PasswordEnabled()) {
                    input.setPasswordEnabled();
                }
            } else if ($args.IsMemberOf(DropDownListFormArgs)) {
                const input : DropDownList = <DropDownList>this.getInputElement();
                const args : DropDownListFormArgs = <DropDownListFormArgs>$args;
                input.Clear();
                const names : ArrayList<string> = args.getNames();
                const values : ArrayList<any> = args.getValues();
                let index : number;
                names.Reindex();
                values.Reindex();
                for (index = 0; index < names.Length(); index++) {
                    if (!ObjectValidator.IsEmptyOrNull(names.getItem(index))) {
                        input.Add(names.getItem(index), values.getItem(index));
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull($args.Hint())) {
                    input.Hint($args.Hint());
                }
                if (!ObjectValidator.IsEmptyOrNull($args.Value())) {
                    if ($args.ForceSetValue() && input.IsLoaded()) {
                        Reflection.getInstance().getClass(input.getClassName()).forceSetValue(input, $args.Value());
                    } else {
                        input.Select($args.Value());
                        this.getGuiManager().setActive(input, false);
                    }
                }
            }
        }
    }

    protected resizeHandler($args? : BaseFormInputArgs) : void {
        const input : FormsObject = this.getInputElement();
        if (ObjectValidator.IsEmptyOrNull(input) ||
            !ObjectValidator.IsEmptyOrNull(input) && !input.IsMemberOf(CheckBox)) {
            if (ObjectValidator.IsEmptyOrNull($args.Name())) {
                this.getElement<Label>("label").Visible(false);
                this.getElement<Label>("requiredLabel").Visible(false);
            }
            if (!ObjectValidator.IsEmptyOrNull(input)) {
                (<TextField>input).Width($args.Width());
            }
        }
        super.resizeHandler(<BaseGuiGroupObjectArgs>$args);
    }
}
