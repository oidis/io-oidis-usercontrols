/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IDragBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IDragBar.js";
import { IDragBarEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IDragBarEvents.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { TextSelectionManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/TextSelectionManager.js";

/**
 * DragBar class provides element's component enabling drag of parent element.
 */
export class DragBar extends GuiCommons implements IDragBar {

    private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                            $reflection : Reflection) : void {
        const element : DragBar = <DragBar>$eventArgs.Owner();
        if ($reflection.IsMemberOf(element, DragBar)) {
            $manager.setActive(element, true);

            let pointerEvents : string;
            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(<IClassName>DragBar)) {
                            pointerEvents = document.body.style.pointerEvents;
                            TextSelectionManager.Disable(true);
                            $args.StopAllPropagation();
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>DragBar);
                            elements.foreach(($element : DragBar) : void => {
                                $args.Owner($element);
                                $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_START, $args, false);
                                $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_START, $args, false);
                                document.body.style.cursor = "move";
                            });
                        }
                    });
                });

            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(<IClassName>DragBar)) {
                            document.body.style.pointerEvents = "none";
                            $args.StopAllPropagation();
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>DragBar);
                            elements.foreach(($element : DragBar) : void => {
                                $args.Owner($element);
                                $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_CHANGE, $args);
                                $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_CHANGE, $args);
                            });
                        }
                    });
                });

            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(<IClassName>DragBar)) {
                            document.body.style.pointerEvents = pointerEvents;
                            TextSelectionManager.Disable(false);
                            TextSelectionManager.Clear();
                            const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>DragBar);
                            elements.foreach(($element : DragBar) : void => {
                                $args.Owner($element);
                                $element.getEventsManager().FireEvent($element, EventType.ON_DRAG_COMPLETE, $args, false);
                                $element.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_COMPLETE, $args, false);
                                $manager.setActive($element, false);
                            });
                        }
                    });
                });
        }
    }

    /**
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($id? : string) {
        super($id);
    }

    /**
     * @returns {IDragBarEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : IDragBarEvents {
        return <IDragBarEvents>super.getEvents();
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnMouseDown(DragBar.moveInit);

        return super.innerCode();
    }
}
