/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileHandlerEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/FileHandlerEventType.js";
import { WebServiceClientType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/WebServiceClientType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { MessageEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/MessageEventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { IWebServiceClient, IWebServiceRequestFormatterData } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { FileHandler } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/FileHandler.js";
import { JsonpFileReader } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebServiceClientFactory } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceClientFactory.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { FileUploadEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/FileUploadEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { FileUploadEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/FileUploadEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { IFileTransferProtocol, IFileUpload } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { IErrorEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IErrorEventsHandler.js";
import { IFileUploadEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IFileUploadEvents.js";
import { IFileUploadEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IFileUploadEventsHandler.js";
import { IEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/IEventsHandler.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";

/**
 * FileUpload class provides wrapper for accessing of external files.
 */
export class FileUpload extends GuiCommons implements IFileUpload {
    private files : FileList;
    private value : string;
    private dropZoneEvents : ElementEventsManager;
    private clientConfiguration : WebServiceConfiguration;
    private multipleEnabled : boolean;
    private filter : string[];
    private maxFileSize : number;
    private maxChunkSize : number;
    private uploadEnabled : boolean;
    private handlers : ArrayList<FileHandler>;
    private client : IWebServiceClient;
    private chunk : IFileTransferProtocol;

    /**
     * @param {WebServiceConfiguration|string} [$configuration] Specify configuration, which should be used for connection to server.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($configuration? : WebServiceConfiguration | string, $id? : string) {
        super($id);
        this.value = "";
        this.multipleEnabled = true;
        this.filter = [];
        this.maxFileSize = 0;
        this.maxChunkSize = 1024 * 500;
        this.handlers = new ArrayList<FileHandler>();
        this.uploadEnabled = true;
        this.dropZoneEvents = new ElementEventsManager(this);
        if (!ObjectValidator.IsEmptyOrNull($configuration)) {
            if (ObjectValidator.IsString($configuration)) {
                this.clientConfiguration = new WebServiceConfiguration(<string>$configuration);
            } else {
                this.clientConfiguration = <WebServiceConfiguration>$configuration;
            }
        } else {
            this.clientConfiguration = new WebServiceConfiguration();
            this.clientConfiguration.ServerAddress(this.getHttpManager().CreateLink(""));
        }
    }

    /**
     * @returns {IFileUploadEvents} Returns events connected with the element.
     */
    public getEvents() : IFileUploadEvents {
        const events : IFileUploadEvents = <IFileUploadEvents>super.getEvents();
        const globalEvents : EventsManager = this.getEventsManager();
        events.setOnError = ($handler : IErrorEventsHandler) : void => {
            globalEvents.setEvent(this, EventType.ON_ERROR, $handler);
        };
        events.setOnAboard = ($handler : IFileUploadEventsHandler) : void => {
            globalEvents.setEvent(this, FileUploadEventType.ON_ABOARD, $handler);
        };
        events.setOnUploadStart = ($handler : IFileUploadEventsHandler) : void => {
            globalEvents.setEvent(this, FileUploadEventType.ON_UPLOAD_START, $handler);
        };
        events.setOnUploadChange = ($handler : IFileUploadEventsHandler) : void => {
            globalEvents.setEvent(this, FileUploadEventType.ON_UPLOAD_CHANGE, $handler);
        };
        events.setOnUploadComplete = ($handler : IFileUploadEventsHandler) : void => {
            globalEvents.setEvent(this, FileUploadEventType.ON_UPLOAD_COMPLETE, $handler);
        };
        this.getEvents = () : IFileUploadEvents => {
            return events;
        };
        return events;
    }

    /**
     * @param {string|HTMLElement|IGuiCommons} [$id] Specify element, which should be associated with open of file browser dialog.
     * @returns {void}
     */
    public setOpenElement($id : string | HTMLElement | IGuiCommons) : void {
        if (!ObjectValidator.IsEmptyOrNull($id)) {
            if (this.IsCompleted()) {
                const element : HTMLElement = ElementManager.getElement($id);
                if (!ObjectValidator.IsEmptyOrNull(element)) {
                    if (FileHandler.IsSupported()) {
                        let events : ElementEventsManager;
                        if (ObjectValidator.IsObject($id) && ObjectValidator.IsSet((<IGuiCommons>$id).getEvents)) {
                            events = <ElementEventsManager>(<IGuiCommons>$id).getEvents();
                        } else if (ObjectValidator.IsString($id)) {
                            const element : IGuiCommons = this.getGuiManager().getAll().getItem(<string>$id);
                            if (!ObjectValidator.IsEmptyOrNull(element)) {
                                events = <ElementEventsManager>element.getEvents();
                            } else {
                                events = new ElementEventsManager(this, <string>$id);
                            }
                        } else {
                            if (ObjectValidator.IsSet((<HTMLElement>$id).id)) {
                                events = new ElementEventsManager(this, (<HTMLElement>$id).id);
                            } else {
                                events = <any>{
                                    Subscribe() : void {
                                        // declare events interface
                                    },
                                    setOnClick($callback : IEventsHandler) : void {
                                        (<HTMLElement>$id).onclick = () : void => {
                                            try {
                                                $callback();
                                            } catch (ex) {
                                                ExceptionsManager.HandleException(ex);
                                            }
                                        };
                                    }
                                };
                            }
                        }
                        events.setOnClick(() : void => {
                            if (this.Enabled()) {
                                ElementManager.Show(this.Id() + "_Input");
                                ElementManager.getElement(this.Id() + "_Input").click();
                                ElementManager.Hide(this.Id() + "_Input");
                            }
                        });
                        events.Subscribe();
                    } else {
                        this.clientConfiguration.Load(() : void => {
                            const target : HTMLIFrameElement = document.createElement("iframe");
                            target.id = this.Id() + "_Target";
                            target.name = this.Id() + "_Target";

                            const form : HTMLFormElement = document.createElement("form");
                            form.id = this.Id() + "_Form";
                            form.target = target.name;
                            form.method = "post";
                            form.enctype = "multipart/form-data";
                            form.encoding = "multipart/form-data";
                            form.action = this.clientConfiguration.getServerUrl() + "Upload";

                            const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_Input");
                            const clone : HTMLInputElement = <HTMLInputElement>input.cloneNode(true);
                            clone.onchange = () : void => {
                                try {
                                    this.files = clone.files;
                                    this.getValue();
                                } catch (ex) {
                                    ExceptionsManager.HandleException(ex);
                                }
                            };
                            form.appendChild(clone);
                            input.parentNode.removeChild(input);
                            ElementManager.CleanElementCache(this.Id() + "_Input");

                            const files : HTMLInputElement = document.createElement("input");
                            files.id = this.Id() + "_FileIds";
                            files.name = "FileIds";
                            files.type = "hidden";
                            form.appendChild(files);

                            if (!WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                                const clientId : HTMLInputElement = document.createElement("input");
                                clientId.id = this.Id() + "_ClientId";
                                clientId.name = "ClientId";
                                clientId.type = "hidden";
                                clientId.value = StringUtils.getCrc(
                                    new Date().getTime().toString() + Math.floor(Math.random() * 1000).toString()) + "";
                                form.appendChild(clientId);
                            }

                            if (this.maxFileSize > 0) {
                                const maxSize : HTMLInputElement = document.createElement("input");
                                maxSize.id = this.Id() + "MaxFileSize";
                                maxSize.name = "MaxFileSize";
                                maxSize.type = "hidden";
                                maxSize.value = this.maxFileSize + "";
                                form.appendChild(maxSize);
                            }

                            const fileDialog : HTMLLabelElement = document.createElement("label");
                            fileDialog.id = this.Id() + "_InputDialog";
                            fileDialog.htmlFor = this.Id() + "_Input";
                            form.appendChild(fileDialog);

                            element.parentNode.insertBefore(target, element);
                            element.parentNode.insertBefore(form, element);
                            ElementManager.setCssProperty(fileDialog, "position", "absolute");
                            ElementManager.setCssProperty(fileDialog, "z-index", "1000");
                            ElementManager.setCssProperty(fileDialog, "background-color", "white");
                            ElementManager.setOpacity(fileDialog, 0);
                            ElementManager.setCssProperty(fileDialog, "cursor", ElementManager.getCssValue(element.id, "cursor"));

                            const parent : IGuiCommons = this.getGuiManager().getAll().getItem(element.id);
                            const setSize : any = ($size : Size) : void => {
                                ElementManager.setSize(fileDialog, $size.Width(), $size.Height());
                            };
                            if (!ObjectValidator.IsEmptyOrNull(parent)) {
                                if (!parent.IsCompleted()) {
                                    parent.getEvents().setOnComplete(() : void => {
                                        setSize(parent.getSize());
                                    });
                                } else {
                                    setSize(parent.getSize());
                                }
                                parent.getEvents().setOnClick(() : void => {
                                    ElementManager.Show(this.Id() + "_Input");
                                });
                                parent.getEvents().setOnMouseOver(() : void => {
                                    setSize(parent.getSize());
                                });
                                parent.getEvents().Subscribe(fileDialog.id);
                            } else {
                                setSize(new Size(element.id));
                                fileDialog.onclick = () : void => {
                                    ElementManager.Show(this.Id() + "_Input");
                                };
                            }
                        }, () : void => {
                            const eventArgs : ErrorEventArgs = new ErrorEventArgs("Unable to get configuration for upload server.");
                            this.getEventsManager().FireEvent(this, EventType.ON_ERROR, eventArgs);
                            this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
                        });
                    }
                }
            } else {
                this.getEvents().setOnComplete(() : void => {
                    this.setOpenElement($id);
                });
            }
        }
    }

    /**
     * @param {string|HTMLElement|IGuiCommons} [$id] Specify element, which should allows files drop to the browser.
     * @returns {void}
     */
    public setDropZone($id : string | HTMLElement | IGuiCommons) : void {
        if (WindowManager.IsDropSupported() && !ObjectValidator.IsEmptyOrNull($id)) {
            if (this.IsCompleted()) {
                const dropZone : HTMLElement = ElementManager.getElement($id);
                if (!ObjectValidator.IsEmptyOrNull(dropZone)) {
                    this.dropZoneEvents.Subscribe(dropZone.id);
                }
            } else {
                this.getEvents().setOnComplete(() : void => {
                    this.setDropZone($id);
                });
            }
        }
    }

    /**
     * @param {Callback} $onLoad Specify asynchronous callback, which should be executed
     * when the files content is ready for consumption.
     * @returns {void}
     */
    public getStream($onLoad : ($data : ArrayList<string>) => void) : void {
        const data : ArrayList<string> = new ArrayList<string>();
        if (FileHandler.IsSupported() && !ObjectValidator.IsEmptyOrNull(this.files)) {
            this.handlers.Clear();
            let index : number;
            const length : number = this.files.length;
            for (index = 0; index < length; index++) {
                const reader : FileHandler = new FileHandler(this.files[index]);
                this.handlers.Add(reader, this.Id() + "_Stream" + index);
            }
            this.getEventsManager().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_COMPLETE,
                ($eventArgs : EventArgs) : void => {
                    const handler : FileHandler = <FileHandler>$eventArgs.Owner();
                    if (this.handlers.Contains(handler) &&
                        StringUtils.Contains(<string>this.handlers.getKey(handler), this.Id() + "_Stream")) {
                        data.Add(handler.Data(true), handler.getName());
                        this.handlers.RemoveAt(this.handlers.IndexOf(handler));
                        if (this.handlers.IsEmpty()) {
                            $onLoad(data);
                        }
                    }
                });
            this.handlers.foreach(($handler : FileHandler) : void => {
                $handler.Load();
            });
        } else {
            $onLoad(data);
        }
    }

    /**
     * @returns {string} Returns value, which represents currently selected files.
     */
    public Value() : string {
        return this.value;
    }

    /**
     * Provide upload of selected files to the specified server.
     * @param {Callback} [$chunksHandler] Specify custom chunks handler, which should be used instead of default service layer.
     * @returns {void}
     */
    public Upload($chunksHandler? : ($data : IFileTransferProtocol, $done : ($data : IFileTransferProtocol) => void,
                                     $isAboard : boolean) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.value) && this.Enabled()) {
            if (FileHandler.IsSupported()) {
                this.handlers.Clear();
                let fileIndex : number = 0;
                this.chunk = <IFileTransferProtocol>{
                    end  : 0,
                    id   : "",
                    index: 0,
                    size : 0,
                    start: 0
                };
                let reader : FileHandler;
                const readFileChunk : ($fileIndex : number) => void = ($fileIndex : number) : void => {
                    if (this.uploadEnabled) {
                        if ($fileIndex < this.files.length) {
                            if (this.chunk.index === 0) {
                                fileIndex = $fileIndex;
                                reader = new FileHandler(this.files[$fileIndex]);
                                this.chunk.id = StringUtils.getSha1(new Date().getTime() + reader.getName());
                                this.chunk.size = reader.getSize();
                                this.chunk.start = 0;
                                this.chunk.end = 0;
                                this.handlers.Add(reader, this.chunk.id);
                            }
                            let patternPassed : boolean = false;
                            if (!ObjectValidator.IsEmptyOrNull(this.filter)) {
                                let index : number;
                                for (index = 0; index < this.filter.length; index++) {
                                    if (StringUtils.PatternMatched(
                                            StringUtils.Replace("*" + this.filter[index], "**", "*"), reader.getName()) ||
                                        StringUtils.PatternMatched(this.filter[index], reader.getType())) {
                                        patternPassed = true;
                                    }
                                }
                            } else {
                                patternPassed = true;
                            }
                            if (!patternPassed) {
                                const eventArgs : ErrorEventArgs =
                                    new ErrorEventArgs("The uploaded file does not match required file type.");
                                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
                                readFileChunk(++fileIndex);
                            } else if (this.maxFileSize > 0 && this.chunk.size > this.maxFileSize) {
                                const eventArgs : ErrorEventArgs =
                                    new ErrorEventArgs("The uploaded file exceeds the upload max file size.");
                                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
                                readFileChunk(++fileIndex);
                            } else {
                                if (this.chunk.start === 0) {
                                    const eventArgs : FileUploadEventArgs = new FileUploadEventArgs();
                                    eventArgs.Owner(this);
                                    eventArgs.File(reader);
                                    eventArgs.Id(this.chunk.id);
                                    eventArgs.Index(this.chunk.index);
                                    eventArgs.Name(reader.getName());
                                    this.getEventsManager().FireEvent(this, FileUploadEventType.ON_UPLOAD_START, eventArgs);
                                    this.getEventsManager()
                                        .FireEvent(this.getClassName(), FileUploadEventType.ON_UPLOAD_START, eventArgs);
                                }
                                if (this.chunk.size > 0) {
                                    this.chunk.end = this.chunk.start + this.maxChunkSize;
                                    if (this.chunk.end > this.chunk.size) {
                                        this.chunk.end = this.chunk.size;
                                    }
                                    reader.Load(this.chunk.start, this.chunk.end);
                                }
                            }
                        } else {
                            fileIndex = 0;
                        }
                    }
                };

                const processChunks : any = ($uploadHandler : ($data : IFileTransferProtocol,
                                                               $done : ($data : IFileTransferProtocol) => void) => void,
                                             $aboardHandler : ($data : IFileTransferProtocol,
                                                               $done : ($data : IFileTransferProtocol) => void) => void) : void => {
                    this.getEventsManager().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_COMPLETE,
                        ($eventArgs : EventArgs) : void => {
                            const handler : FileHandler = <FileHandler>$eventArgs.Owner();
                            if (this.handlers.KeyExists(this.chunk.id)) {
                                const aboardUpload : any = () : void => {
                                    const eventArgs : FileUploadEventArgs = new FileUploadEventArgs();
                                    eventArgs.Owner(this);
                                    eventArgs.File(handler);
                                    eventArgs.Id(this.chunk.id);
                                    eventArgs.Name(handler.getName());
                                    this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ABOARD, eventArgs);
                                    this.getEventsManager()
                                        .FireEvent(this.getClassName(), FileUploadEventType.ON_ABOARD, eventArgs);
                                    $aboardHandler({
                                        end  : this.chunk.end,
                                        id   : this.chunk.id,
                                        index: this.chunk.index,
                                        path : ObjectEncoder.Base64(handler.getName(), true),
                                        size : this.chunk.size,
                                        start: this.chunk.start
                                    }, ($data : IFileTransferProtocol) : void => {
                                        this.handlers.RemoveAt(
                                            this.handlers.IndexOf(this.handlers.getItem($data.id)));
                                        if (this.handlers.IsEmpty()) {
                                            this.uploadEnabled = true;
                                        }
                                    });
                                };
                                if (this.uploadEnabled) {
                                    const args : FileUploadEventArgs = new FileUploadEventArgs();
                                    args.Id(this.chunk.id);
                                    args.Index(this.chunk.index);
                                    args.File(handler);
                                    args.Name(handler.getName());
                                    args.CurrentValue(this.chunk.end);
                                    args.RangeEnd(this.chunk.size);
                                    this.getEventsManager().FireEvent(this, FileUploadEventType.ON_UPLOAD_CHANGE, args);
                                    this.getEventsManager()
                                        .FireEvent(this.getClassName(), FileUploadEventType.ON_UPLOAD_CHANGE, args);
                                    $uploadHandler({
                                        data : handler.Data(true),
                                        end  : this.chunk.end,
                                        id   : this.chunk.id,
                                        index: this.chunk.index,
                                        path : ObjectEncoder.Base64(handler.getName(), true),
                                        size : this.chunk.size,
                                        start: this.chunk.start
                                    }, ($data : IFileTransferProtocol) : void => {
                                        if (this.uploadEnabled) {
                                            $data.path = ObjectDecoder.Base64($data.path);
                                            const fireComplete : any = () : void => {
                                                const eventArgs : FileUploadEventArgs = new FileUploadEventArgs();
                                                eventArgs.Owner(this);
                                                eventArgs.File(handler);
                                                eventArgs.Id($data.id);
                                                eventArgs.Index($data.index);
                                                eventArgs.Name($data.path);
                                                this.getEventsManager().FireEvent(this,
                                                    FileUploadEventType.ON_UPLOAD_COMPLETE, eventArgs);
                                                this.getEventsManager().FireEvent(this.getClassName(),
                                                    FileUploadEventType.ON_UPLOAD_COMPLETE, eventArgs);
                                            };
                                            if ($data.end < $data.size) {
                                                this.chunk.start = $data.end;
                                                this.chunk.index = $data.index + 1;
                                                readFileChunk(fileIndex);
                                            } else if (fileIndex < this.files.length) {
                                                fireComplete();
                                                this.chunk.index = 0;
                                                readFileChunk(++fileIndex);
                                            } else {
                                                fireComplete();
                                            }
                                        } else {
                                            aboardUpload();
                                        }
                                    });
                                } else {
                                    aboardUpload();
                                }
                            }
                        });
                    readFileChunk(0);
                };
                if (!ObjectValidator.IsEmptyOrNull($chunksHandler)) {
                    processChunks(($data : IFileTransferProtocol, $done : ($data : IFileTransferProtocol) => void) : void => {
                        $chunksHandler($data, $done, false);
                    }, ($data : IFileTransferProtocol, $done : ($data : IFileTransferProtocol) => void) : void => {
                        $chunksHandler($data, $done, true);
                    });
                } else if (ObjectValidator.IsEmptyOrNull(this.client)) {
                    this.clientConfiguration.Load(
                        () : void => {
                            if (!ObjectValidator.IsEmptyOrNull(this.clientConfiguration.getSource())) {
                                const configuration : WebServiceConfiguration = new WebServiceConfiguration();
                                configuration.ServerLocation(this.clientConfiguration.ServerLocation());
                                configuration.ServerAddress(this.clientConfiguration.ServerAddress());
                                configuration.ServerBase(this.clientConfiguration.ServerBase() + "/xorigin");
                                configuration.ServerPort(this.clientConfiguration.ServerPort());
                                configuration.ServerProtocol(this.clientConfiguration.ServerProtocol());
                                configuration.ResponseUrl(this.clientConfiguration.ResponseUrl());
                                configuration.TimeoutLimit(this.clientConfiguration.TimeoutLimit());
                                this.clientConfiguration = configuration;
                            } else if (!StringUtils.Contains(this.clientConfiguration.ServerBase(), "/xorigin")) {
                                this.clientConfiguration.ServerBase(this.clientConfiguration.ServerBase() + "/xorigin");
                            }

                            this.client = WebServiceClientFactory.getClient(WebServiceClientType.HTTP, this.clientConfiguration);
                            this.client.getEvents().OnError(($eventArgs? : ErrorEventArgs) : void => {
                                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, $eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, $eventArgs);
                            });
                            this.client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                                if (ObjectValidator.IsObject($data.value)) {
                                    $data.value.id = StringUtils.getCrc(this.client.getId() + $data.value.data.link);
                                    $data.value.origin = this.getHttpManager().getRequest().getBaseUrl();
                                    $data.value.type = "ReloadTo";
                                    $data.key = $data.value.id;
                                }
                            });
                            this.client.setResponseFormatter(($data : any, $owner : IWebServiceClient,
                                                              $onSuccess : ($value : any, $key? : number) => void,
                                                              $onError : ($message : string | Error | Exception) => void) : void => {
                                if (ObjectValidator.IsString($data)) {
                                    $onSuccess($data);
                                } else {
                                    if (!ObjectValidator.IsSet($data.origin) || (
                                        !StringUtils.Contains($owner.getServerUrl(), $data.origin) &&
                                        !StringUtils.Contains($data.origin, "127.0.0.1", "http://localhost"))) {
                                        $onError("Bad server origin: \"" + $data.origin + "\"");
                                    } else {
                                        $onSuccess($data, $data.id);
                                    }
                                }
                            });
                            if (!ObjectValidator.IsEmptyOrNull(this.client)) {
                                processChunks(($data : any, $done : any) : void => {
                                    this.client.Send({
                                        data: {
                                            link: "Upload",
                                            post: {
                                                File: $data
                                            }
                                        }
                                    }, ($data : IFileChunkUploadResponse) : void => {
                                        $done($data.data);
                                    });
                                }, ($data : any, $done : any) : void => {
                                    this.client.Send({
                                        data: {
                                            link: "Upload/Remove",
                                            post: {
                                                File: $data
                                            }
                                        }
                                    }, ($data : IFileChunkUploadResponse) : void => {
                                        $done($data.data);
                                    });
                                });
                            } else {
                                const eventArgs : ErrorEventArgs = new ErrorEventArgs("Unable to create client for upload server.");
                                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
                            }
                        },
                        () : void => {
                            const eventArgs : ErrorEventArgs = new ErrorEventArgs("Unable to get configuration for upload server.");
                            this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                            this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
                        });
                } else {
                    readFileChunk(0);
                }
            } else if (!ObjectValidator.IsEmptyOrNull($chunksHandler)) {
                const eventArgs : ErrorEventArgs = new ErrorEventArgs("Chunks handler is not supported on this platform.");
                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
            } else {
                const form : HTMLFormElement = <HTMLFormElement>ElementManager.getElement(this.Id() + "_Form");
                if (!ObjectValidator.IsEmptyOrNull(form)) {
                    ElementManager.Show(this.Id() + "_Input");
                    const onSuccess : ($data : IFileTransferProtocol) => void = ($data : IFileTransferProtocol) : void => {
                        const eventArgs : FileUploadEventArgs = new FileUploadEventArgs();
                        eventArgs.Owner(this);
                        eventArgs.Id($data.id);
                        eventArgs.Name($data.path);
                        this.getEventsManager().FireEvent(this, FileUploadEventType.ON_UPLOAD_COMPLETE, eventArgs);
                        this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_UPLOAD_COMPLETE, eventArgs);
                    };
                    const onError : ($data : IFileTransferProtocol | string) => void =
                        ($data : IFileTransferProtocol | string) : void => {
                            const eventArgs : ErrorEventArgs = new ErrorEventArgs("Internal server error.");
                            if (ObjectValidator.IsSet((<IFileTransferProtocol>$data).error)) {
                                switch ((<IFileTransferProtocol>$data).error) {
                                case 1:
                                case 2:
                                    eventArgs.Message("The uploaded file exceeds the upload max file size.");
                                    break;
                                case 3:
                                case 4:
                                case 6:
                                case 7:
                                    eventArgs.Message("Unable to store uploaded file to the server.");
                                    break;
                                case 5:
                                    eventArgs.Message("Possible file upload attack.");
                                    break;
                                default:
                                    break;
                                }
                                this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, eventArgs);
                            } else {
                                eventArgs.Message(<string>$data);
                                this.getEventsManager().FireEvent(this, EventType.ON_ERROR, eventArgs);
                                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
                            }
                        };
                    const onResponse : ($data : IFileUploadSuccess | IFileUploadError) => void =
                        ($data : IFileUploadSuccess | IFileUploadError) : void => {
                            if (ObjectValidator.IsSet((<IFileUploadSuccess>$data).files)) {
                                let index : number;
                                const files : IFileTransferProtocol[] = (<IFileUploadSuccess>$data).files;
                                const filesLength : number = files.length;
                                for (index = 0; index < filesLength; index++) {
                                    files[index].path = ObjectDecoder.Base64(files[index].path);
                                    if (files[index].error === 0) {
                                        onSuccess(files[index]);
                                    } else {
                                        onError(files[index]);
                                    }
                                }
                            } else {
                                onError((<IFileUploadError>$data).error);
                            }
                        };

                    const timeoutId : any = setTimeout(() : void => {
                        try {
                            ElementManager.Hide(this.Id() + "_Input");
                            onError("Server timeout reached for file upload.");
                        } catch (ex) {
                            ExceptionsManager.HandleException(ex);
                        }
                    }, this.clientConfiguration.TimeoutLimit() + 500);

                    if (WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                        WindowManager.getEvents().setOnMessage(($eventArgs : MessageEventArgs) : void => {
                            clearTimeout(timeoutId);
                            ElementManager.Hide(this.Id() + "_Input");
                            onResponse(JSON.parse($eventArgs.NativeEventArgs().data));
                        });
                    } else {
                        ElementManager.getElement(this.Id() + "_Target").onload = () : void => {
                            try {
                                clearTimeout(timeoutId);
                                ElementManager.Hide(this.Id() + "_Input");
                                JsonpFileReader.Load(
                                    this.clientConfiguration.getServerUrl() +
                                    "Response/" + (<HTMLInputElement>ElementManager.getElement(this.Id() + "_ClientId")).value,
                                    onResponse,
                                    ($eventArgs : ErrorEvent) : void => {
                                        onError($eventArgs.message);
                                    });
                            } catch (ex) {
                                ExceptionsManager.HandleException(ex);
                            }
                        };
                    }
                    const fileIds : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_FileIds");
                    fileIds.value = JSON.stringify(this.generateFileIdsFor(FileUploadEventType.ON_UPLOAD_START));
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        form.submit();
                    }, true, 50);
                }
            }
        }
    }

    /**
     * @param {number} [$index] Specify file index, which upload should be aboard.
     * If index is not specified upload of all files will be aboard.
     * @returns {void}
     */
    public Aboard($index? : number) : void {
        if (FileHandler.IsSupported()) {
            this.uploadEnabled = false;
            if (ObjectValidator.IsSet($index)) {
                if (this.handlers.KeyExists($index)) {
                    this.handlers.getItem($index).Stop();
                }
            } else {
                this.handlers.foreach(($handler : FileHandler) : void => {
                    $handler.Stop();
                });
            }
        } else {
            (<HTMLIFrameElement>ElementManager.getElement(this.Id() + "_Target")).src = "";
            this.generateFileIdsFor(FileUploadEventType.ON_ABOARD);
        }
    }

    /**
     * @param {boolean} [$value] Specify, if selection of multiple files should be allowed.
     * @returns {boolean} Returns true, if selection of multiple files is allowed, otherwise false.
     */
    public MultipleSelectEnabled($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.multipleEnabled = Property.Boolean(this.multipleEnabled, $value);
            if (this.IsCompleted()) {
                const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_Input");
                if (!ObjectValidator.IsEmptyOrNull(input)) {
                    input.multiple = this.multipleEnabled;
                }
            }
        }
        return this.multipleEnabled;
    }

    /**
     * @param {...string[]} $value Specify filters, which should be applied to selected files.
     * @returns {string} Returns filter in string format, which should be applied to selected files.
     */
    public Filter(...$value : string[]) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            const value : string = $value.join(",");
            if (ObjectValidator.IsEmptyOrNull(value) || value === "*" || value === "*.*") {
                this.filter = [];
            } else {
                this.filter = StringUtils.Split(value, ",");
            }
            if (this.IsCompleted()) {
                const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_Input");
                if (!ObjectValidator.IsEmptyOrNull(input)) {
                    input.accept = this.Filter();
                }
            }
        }
        return StringUtils.Replace(this.filter.join(","), "*.", ".");
    }

    /**
     * @param {number|string} [$value] Specify maximal size of the uploaded file.
     * Supported is also value in string format with expected size unit.
     * @returns {number} Returns value for maximal size of the uploaded file in bytes.
     */
    public MaxFileSize($value? : number | string) : number {
        return this.maxFileSize = Property.Size(this.maxFileSize, $value);
    }

    /**
     * @param {number|string} [$value] Specify maximal size of the file upload chunk.
     * Supported is also value in string format with expected size unit.
     * @returns {number} Returns value for maximal size of the file upload chunk in bytes.
     */
    public MaxChunkSize($value? : number | string) : number {
        if (ObjectValidator.IsSet($value)) {
            this.maxChunkSize = Property.Size(this.maxChunkSize, $value);
            if (this.maxChunkSize < 1024) {
                this.maxChunkSize = 1024;
            }
        }
        return this.maxChunkSize;
    }

    protected innerCode() : IGuiElement {
        if (WindowManager.IsDropSupported()) {
            this.dropZoneEvents.setEvent(EventType.ON_DRAG_OVER, ($eventArgs : EventArgs) : void => {
                if ((<FileUpload>$eventArgs.Owner()).Enabled() && this.Enabled()) {
                    const eventArgs : DragEvent = <DragEvent>$eventArgs.NativeEventArgs();
                    if (ObjectValidator.IsSet(eventArgs.stopPropagation)) {
                        eventArgs.stopPropagation();
                    }
                    $eventArgs.PreventDefault();
                    eventArgs.dataTransfer.dropEffect = "copy";
                }
            });

            this.dropZoneEvents.setEvent(EventType.ON_DROP, ($eventArgs : EventArgs) : void => {
                if ((<FileUpload>$eventArgs.Owner()).Enabled() && this.Enabled()) {
                    const eventArgs : DragEvent = <DragEvent>$eventArgs.NativeEventArgs();
                    if (ObjectValidator.IsSet(eventArgs.stopPropagation)) {
                        eventArgs.stopPropagation();
                    }
                    $eventArgs.PreventDefault();
                    this.files = null;
                    if (ObjectValidator.IsSet(eventArgs.dataTransfer.files)) {
                        this.files = eventArgs.dataTransfer.files;
                    }
                    this.getValue();
                }
            });
        }
        if (FileHandler.IsSupported()) {
            this.getEventsManager().setEvent(FileHandler.ClassName(), FileHandlerEventType.ON_ERROR,
                ($eventArgs : ErrorEventArgs) : void => {
                    if (this.handlers.Contains($eventArgs.Owner())) {
                        this.getEventsManager().FireEvent(this, FileUploadEventType.ON_ERROR, $eventArgs);
                        this.getEventsManager().FireEvent(this.getClassName(), FileUploadEventType.ON_ERROR, $eventArgs);
                    }
                });

            this.getEvents().setOnComplete(() : void => {
                const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_Input");
                input.onchange = () : void => {
                    try {
                        this.files = input.files;
                        this.getValue();
                    } catch (ex) {
                        ExceptionsManager.HandleException(ex);
                    }
                };
            });
        }
        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        const input : HTMLInputElement = document.createElement("input");
        input.id = this.Id() + "_Input";
        input.name = "Files[]";
        input.type = "file";
        input.className = GeneralCssNames.GUI_SELECTOR;
        input.multiple = this.multipleEnabled;
        input.accept = this.Filter();
        input.style.display = "none";
        return this.addElement().Add(input);
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.value = "";
        this.multipleEnabled = true;
        this.filter = [];
        this.maxFileSize = 0;
        this.maxChunkSize = 1024 * 500;
        this.handlers = new ArrayList<FileHandler>();
        this.uploadEnabled = true;
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "files", "value",
            "uploadEnabled",
            "handlers", "client", "chunk"
        );
        if (this.multipleEnabled) {
            exclude.push("multipleEnabled");
        }
        if (ObjectValidator.IsEmptyOrNull(this.filter)) {
            exclude.push("filter");
        }
        if (this.maxFileSize === 0) {
            exclude.push("maxFileSize");
        }
        if (this.maxChunkSize !== 1024 * 500) {
            exclude.push("maxChunkSize");
        }

        return exclude;
    }

    private getValue() : void {
        let path : string = "";
        if (!ObjectValidator.IsEmptyOrNull(this.files)) {
            let index : number;
            const length : number = this.multipleEnabled ? this.files.length : 1;
            for (index = 0; index < length; index++) {
                const file : File = this.files[index];
                if (!ObjectValidator.IsEmptyOrNull(file)) {
                    if (index > 0) {
                        path += "; ";
                    }
                    path += file.name;
                }
            }
        } else {
            const input : HTMLInputElement = <HTMLInputElement>ElementManager.getElement(this.Id() + "_Input");
            path = StringUtils.Replace(input.value, "\\", "/");
            if (StringUtils.Contains(path, "/")) {
                path = StringUtils.Substring(path, StringUtils.IndexOf(path, "/", false) + 1);
            }
        }
        this.value = path;
        this.getEventsManager().FireEvent(this, EventType.ON_CHANGE);
        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE);
    }

    private generateFileIdsFor($eventType : FileUploadEventType) : string[] {
        const fileNames : string[] = StringUtils.Split(this.Value(), ",");
        let index : number;
        const length : number = fileNames.length;
        for (index = 0; index < length; index++) {
            const eventArgs : FileUploadEventArgs = new FileUploadEventArgs();
            eventArgs.Owner(this);
            eventArgs.Name(fileNames[index]);
            fileNames[index] = StringUtils.getSha1(new Date().getTime() + fileNames[index]);
            eventArgs.Id(fileNames[index]);
            this.getEventsManager().FireEvent(this, <string>$eventType, eventArgs);
            this.getEventsManager().FireEvent(this.getClassName(), <string>$eventType, eventArgs);
        }
        return fileNames;
    }
}

interface IFileUploadSuccess {
    files : IFileTransferProtocol[];
}

interface IFileUploadError {
    error : string;
}

interface IFileChunkUploadResponse {
    id : number;
    type : string;
    status : number;
    origin : string;
    data : IFileTransferProtocol;
}
