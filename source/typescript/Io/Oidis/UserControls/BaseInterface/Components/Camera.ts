/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IMediaTrackConstraintSet } from "../../Interfaces/UserControls/Video/IConstraints.js";

/**
 * Camera class provides wrapper for accessing of web camera.
 */
export class Camera extends BaseObject {
    private videoConstraints : IMediaTrackConstraintSet;

    /**
     * @param {IMediaTrackConstraintSet} [$constraints] Specify configuration, which should be used for camera stream.
     */
    constructor($constraints? : IMediaTrackConstraintSet) {
        super();
        this.videoConstraints = !ObjectValidator.IsSet($constraints) ? <IMediaTrackConstraintSet>{
            height: {
                exact: 500
            },
            width : {
                exact: 500
            }
        } : this.VideoConstraints($constraints);
    }

    public VideoConstraints($constraints? : IMediaTrackConstraintSet) : IMediaTrackConstraintSet {
        if (!ObjectValidator.IsEmptyOrNull($constraints)) {
            this.videoConstraints = $constraints;
        }
        return this.videoConstraints;
    }

    public getStream($callback : ($stream : MediaStream) => void, $id? : string) : void {
        const onError : any = ($reason : Error) : void => {
            LogIt.Error(this.getClassName(), $reason);
            $callback(null);
        };
        const constraints : any = {
            video: JsonUtils.Clone(this.videoConstraints)
        };
        if (!ObjectValidator.IsEmptyOrNull($id)) {
            constraints.video.deviceId.exact = $id;
        }
        if (navigator?.mediaDevices.getUserMedia) {
            navigator.mediaDevices
                .getUserMedia(constraints)
                .then(($stream : MediaStream) : void => {
                    $callback($stream);
                })
                .catch(onError);
        } else if ((<any>navigator).webkitGetUserMedia) {
            (<any>navigator).webkitGetUserMedia(this.videoConstraints, ($stream : MediaStream) : void => {
                $callback((<any>window).webkitURL.createObjectURL($stream));
            }, onError);
        } else if ((<any>navigator).mozGetUserMedia) {
            (<any>navigator).mozGetUserMedia(this.videoConstraints, ($stream : MediaStream) : void => {
                $callback($stream);
            }, onError);
        } else {
            onError(new Error("Camera stream is not supported on this device."));
        }
    }

    public getDevicesList($callback : ($ids : string[]) => void) : void {
        const onError : any = ($reason : Error) : void => {
            LogIt.Error(this.getClassName(), $reason);
            $callback([]);
        };
        if (navigator?.mediaDevices.enumerateDevices) {
            navigator.mediaDevices.enumerateDevices()
                .then(($deviceInfos : MediaDeviceInfo[]) : void => {
                    const list : string[] = [];
                    for (const deviceInfo of $deviceInfos) {
                        if (deviceInfo.kind === "videoinput") {
                            list.push(deviceInfo.deviceId);
                        }
                    }
                    $callback(list);
                })
                .catch(onError);
        } else {
            onError(new Error("Enumeration of devices is not supported in this browser."));
        }
    }
}
