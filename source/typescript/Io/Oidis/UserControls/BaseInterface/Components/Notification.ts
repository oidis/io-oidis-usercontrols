/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { INotification } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/INotification.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiCommonsListArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsListArg.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { NotificationType } from "../Enums/Components/NotificationType.js";

/**
 * Notification class provides element's component showing notification message associated with the element.
 */
export class Notification extends GuiCommons implements INotification {
    private text : string;
    private guiType : NotificationType;
    private size : number;

    /**
     * @param {Notification} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Show($element : Notification) : void {
        const id : string = $element.Id();
        if (!$element.IsLoaded()) {
            $element.Visible(true);
        }
        if (ElementManager.Exists(id + "_Text")) {
            ElementManager.Show(id);

            const left : number = ElementManager.getElement(id + "_Left").offsetWidth;
            const right : number = ElementManager.getElement(id + "_Right").offsetWidth;
            ElementManager.setWidth(id + "_Border", $element.Width());
            ElementManager.setWidth(id + "_Center", $element.Width() - left - right);

            const textHeight : number = ElementManager.getElement(id + "_Text").offsetHeight;
            const height : number = ElementManager.getElement(id + "_Center").offsetHeight;
            ElementManager.setCssProperty(id + "_Text", "top", (height - textHeight) / 2);
        }
    }

    /**
     * @param {string} [$text] Specify tooltip text value.
     * @param {NotificationType} [$notificationType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($text? : string, $notificationType? : NotificationType, $id? : string) {
        super($id);
        this.size = 100;
        this.Visible(false);
        this.guiType = this.guiTypeValueSetter($notificationType);
        this.Text($text);
    }

    /**
     * @param {NotificationType} [$notificationType] Specify type of element look and feel.
     * @returns {NotificationType} Returns type of element's look and feel.
     */
    public GuiType($notificationType? : NotificationType) : NotificationType {
        if (ObjectValidator.IsSet($notificationType)) {
            this.guiType = this.guiTypeValueSetter($notificationType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    /**
     * @param {number} [$value] Specify element's width value.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        this.size = Property.PositiveInteger(this.size, $value, 30);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            thisClass.Show(this);
        }
        return this.size;
    }

    /**
     * @param {string} [$value] Specify element's text value.
     * @returns {string} Returns element's text value.
     */
    public Text($value? : string) : string {
        this.text = Property.String(this.text, $value);
        if (ObjectValidator.IsSet($value) && this.IsLoaded()) {
            ElementManager.setInnerHtml(this.Id() + "_Text", $value);
            if (ElementManager.IsVisible(this.Id())) {
                const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                thisClass.Show(this);
            }
        }
        return this.text;
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const args : IGuiCommonsArg[] = super.getArgs();
        let index : number;
        for (index = 0; index < args.length; index++) {
            if (args[index].name === "Value") {
                args[index].value = this.Text();
                break;
            }
        }
        args.push({
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: this.Text()
        });
        args.push({
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: this.Width()
        });
        args.push(<IGuiCommonsListArg>{
            items: NotificationType.getProperties(),
            name : "GuiType",
            type : GuiCommonsArgType.LIST,
            value: NotificationType.getKey(<string>this.GuiType())
        });
        return args;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
     * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
     * @returns {void}
     */
    public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
        switch ($value.name) {
        case "Value":
        case "Text":
            this.Text(<string>$value.value);
            break;
        case "Width":
            this.Width(<number>$value.value);
            break;
        case "GuiType":
            this.GuiType(NotificationType[<string>$value.value]);
            break;
        default:
            super.setArg($value, $force);
            break;
        }
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, NotificationType, NotificationType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!NotificationType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiTypeTag method for set of notification type instead of StyleClassName method.");
        return false;
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement()
                .StyleClassName("Envelop")
                .Add(this.addElement(this.Id() + "_Border")
                    .StyleClassName(GeneralCssNames.BACKGROUND)
                    .Add(this.addElement(this.Id() + "_Left").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_Center")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Add(this.addElement(this.Id() + "_Text")
                            .StyleClassName(GeneralCssNames.TEXT)
                            .Add(this.text)
                        )
                    )
                    .Add(this.addElement(this.Id() + "_Right").StyleClassName(GeneralCssNames.RIGHT))
                )
            );
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.size = 100;
        this.Visible(false);
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("size");
        return exclude;
    }
}
