/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { ISelectBox } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/ISelectBox.js";
import { ISelectBoxEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/ISelectBoxEvents.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BaseGuiObject } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseGuiObject.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { SelectBoxOption } from "../../Primitives/SelectBoxOption.js";
import { SelectBoxType } from "../Enums/Components/SelectBoxType.js";
import { ScrollBar } from "./ScrollBar.js";

/**
 * SelectBox class renders selectable list of options.
 */
export class SelectBox extends GuiCommons implements ISelectBox {
    private static minWidth : number = 20;
    private static minHeight : number = 20;
    private static rollUpEnabled : boolean = true;

    private guiType : any;
    private initWidth : number;
    private initHeight : number;
    private width : number;
    private height : number;
    private items : ArrayList<SelectBoxOption>;
    private itemOptions : ArrayList<ArrayList<number>>;
    private visibleItemsCount : number;
    private selectedIndex : number;
    private selectedItem : SelectBoxOption;
    private lastActiveItem : SelectBoxOption;
    private readonly verticalScrollBar : ScrollBar;
    private openDirectionVertical : DirectionType;
    private openDirectionHorizontal : DirectionType;
    private characterNavigationEnabled : boolean;
    private advancedSelectionEnabled : boolean;

    /**
     * @param {SelectBox} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Show($element : SelectBox) : void {
        const allItems : SelectBoxOption[] = $element.items.getAll();
        const itemsCount : number = $element.getMaxVisibleItemsCount();
        let index : number;
        for (index = 0; index < itemsCount; index++) {
            SelectBoxOption.Resize(allItems[index]);
        }
        $element.items.foreach(($option : SelectBoxOption) : void => {
            $element.getEvents().FireAsynchronousMethod(() : void => {
                SelectBoxOption.TurnOff($option);
            }, false);
        });
        let info : any;
        let heightManipulatorArgs : ValueProgressEventArgs;

        if (!$element.getGuiManager().IsActive($element)) {
            const id : string = $element.Id();
            ElementManager.Show(id);

            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_SHOW, eventArgs);
            $element.getEventsManager().FireEvent($element, EventType.ON_SHOW, eventArgs);

            ElementManager.Show(id);
            ElementManager.BringToFront(id);

            if (!ObjectValidator.IsSet($element.initWidth)) {
                $element.initWidth = ElementManager.getElement(id + "_Type").offsetWidth;
            }
            if (!ObjectValidator.IsSet($element.initHeight)) {
                $element.initHeight = ElementManager.getElement(id + "_Type").offsetHeight;
            }
            if ($element.Height() === -1 && $element.MaxVisibleItemsCount() === -1) {
                $element.Height($element.initHeight);
            }
            info = SelectBox.getSizeInfo($element);

            if ($element.Width() >= SelectBox.minWidth &&
                ($element.Height() > SelectBox.minHeight || itemsCount > 0)) {
                ElementManager.setWidth(id + "_Envelop", $element.initWidth);

                const centerWidth : number = $element.initWidth - info.leftWidth - info.rightWidth;
                ElementManager.setWidth(id + "_TopCenter", centerWidth);
                ElementManager.setWidth(id + "_MiddleCenter", centerWidth);
                ElementManager.setWidth(id + "_BottomCenter", centerWidth);

                const contentWidth : number = $element.initWidth - 2 * info.contentOffsetLeft;
                ElementManager.setWidth(id + "_Content", contentWidth);
                ElementManager.setWidth(id + "_Options", info.contentWidth);

                info = SelectBox.getSizeInfo($element);

                const centerHeight : number = $element.initHeight - info.topHeight - info.bottomHeight + info.contentOffsetTop;
                ElementManager.setHeight(id + "_MiddleLeft", centerHeight);
                ElementManager.setHeight(id + "_MiddleCenter", centerHeight);
                ElementManager.setHeight(id + "_MiddleRight", centerHeight);

                if ($element.getSelectedIndex() !== -1) {
                    SelectBox.optionTurnActive($element, $element.selectedItem);
                }

                $element.getGuiManager().setActive($element, true);

                const widthManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(id + "Width");
                widthManipulatorArgs.Owner($element);
                widthManipulatorArgs.DirectionType(DirectionType.UP);
                widthManipulatorArgs.ProgressType(ProgressType.FAST_END);
                widthManipulatorArgs.RangeStart($element.initWidth);
                widthManipulatorArgs.RangeEnd(info.contentWidth);
                widthManipulatorArgs.CurrentValue($element.initWidth);
                if (widthManipulatorArgs.RangeStart() > widthManipulatorArgs.RangeEnd()) {
                    widthManipulatorArgs.RangeStart(widthManipulatorArgs.RangeEnd());
                    widthManipulatorArgs.CurrentValue(widthManipulatorArgs.RangeEnd());
                }
                widthManipulatorArgs.Step(12);
                widthManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
                widthManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Show");

                $element.getEventsManager().setEvent(id + "Width", EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const contentWidth : number = $eventArgs.CurrentValue();
                        ElementManager.setWidth(id + "_Envelop", contentWidth + 2 * info.contentOffsetLeft);

                        const width : number = contentWidth - info.leftWidth - info.rightWidth + 2 * info.contentOffsetLeft;
                        if ((<SelectBox>$eventArgs.Owner()).openDirectionHorizontal === DirectionType.LEFT) {
                            ElementManager.setCssProperty(id + "_Envelop", "left", (-1) * width);
                        }

                        ElementManager.setWidth(id + "_TopCenter", width);
                        ElementManager.setWidth(id + "_MiddleCenter", width);
                        ElementManager.setWidth(id + "_BottomCenter", width);

                        ElementManager.setWidth(id + "_Content", contentWidth);
                        ElementManager.setWidth(id + "_Options", contentWidth);
                    });

                heightManipulatorArgs = ValueProgressManager.get(id + "Height");
                heightManipulatorArgs.Owner($element);
                heightManipulatorArgs.DirectionType(DirectionType.UP);
                heightManipulatorArgs.ProgressType(ProgressType.FAST_END);
                heightManipulatorArgs.RangeStart($element.initHeight);
                heightManipulatorArgs.RangeEnd(info.contentHeight);
                heightManipulatorArgs.CurrentValue($element.initHeight);
                heightManipulatorArgs.Step(12);
                heightManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
                heightManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Show");

                $element.getEventsManager().setEvent(id + "Height", EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        const height : number =
                            $eventArgs.CurrentValue() - info.topHeight - info.bottomHeight + 2 * info.contentOffsetTop;
                        if ((<SelectBox>$eventArgs.Owner()).openDirectionVertical === DirectionType.UP) {
                            ElementManager.setCssProperty(id + "_Envelop", "top", (-1) * height);
                        }
                        ElementManager.setHeight(id + "_MiddleLeft", height);
                        ElementManager.setHeight(id + "_MiddleCenter", height);
                        ElementManager.setHeight(id + "_MiddleRight", height);

                        const currentAvailableHeight : number = $eventArgs.CurrentValue();
                        ElementManager.setHeight(id + "_Content", currentAvailableHeight);
                        ElementManager.setHeight(id + "_Options", currentAvailableHeight);
                    });

                const widthRange : number = widthManipulatorArgs.RangeEnd() - widthManipulatorArgs.RangeStart();
                const heightRange : number = heightManipulatorArgs.RangeEnd() - heightManipulatorArgs.RangeStart();
                $element.getEventsManager().setEvent(id + "Width", EventType.ON_COMPLETE + "Show",
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        if (widthRange > heightRange && (<SelectBox>$eventArgs.Owner()).OptionsCount() > 0) {
                            SelectBox.finalizeShow($eventArgs.Owner(), info);
                        }
                    });
                $element.getEventsManager().setEvent(id + "Height", EventType.ON_COMPLETE + "Show",
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        if (widthRange <= heightRange && (<SelectBox>$eventArgs.Owner()).OptionsCount() > 0) {
                            SelectBox.finalizeShow($eventArgs.Owner(), info);
                        }
                    });
                ValueProgressManager.Execute(widthManipulatorArgs);
                ValueProgressManager.Execute(heightManipulatorArgs);
            }
        } else {
            info = SelectBox.getSizeInfo($element);
            heightManipulatorArgs = ValueProgressManager.get($element.Id() + "Height");
            if (info.contentHeight < heightManipulatorArgs.CurrentValue()) {
                heightManipulatorArgs.RangeStart(info.contentHeight);
                heightManipulatorArgs.DirectionType(DirectionType.DOWN);
                ValueProgressManager.Remove(heightManipulatorArgs.OwnerId());
                ValueProgressManager.Execute(heightManipulatorArgs);
            } else if (info.contentHeight > heightManipulatorArgs.CurrentValue()) {
                heightManipulatorArgs.RangeStart(heightManipulatorArgs.CurrentValue());
                heightManipulatorArgs.RangeEnd(info.contentHeight);
                heightManipulatorArgs.DirectionType(DirectionType.UP);
                ValueProgressManager.Remove(heightManipulatorArgs.OwnerId());
                ValueProgressManager.Execute(heightManipulatorArgs);
            } else {
                SelectBox.finalizeShow($element, info);
            }
        }
    }

    /**
     * Disable visibility of the element on the screen.
     * @param {SelectBox} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Hide($element : SelectBox) : void {
        const eventArgs : EventArgs = new EventArgs();
        eventArgs.Owner($element);

        if (ValueProgressManager.Exists($element.Id() + "Width")) {
            const widthManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id() + "Width");
            widthManipulatorArgs.Owner($element);
            widthManipulatorArgs.DirectionType(DirectionType.DOWN);
            widthManipulatorArgs.Step(8);
            widthManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Hide");
            ValueProgressManager.Execute(widthManipulatorArgs);
        }

        if (ValueProgressManager.Exists($element.Id() + "Height")) {
            const heightManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id() + "Height");
            heightManipulatorArgs.Owner($element);
            heightManipulatorArgs.DirectionType(DirectionType.DOWN);
            heightManipulatorArgs.Step(8);
            heightManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE + "Hide");

            ElementManager.Hide($element.Id() + "_ScrollBarEnvelop");
            $element.verticalScrollBar.Visible(false);

            $element.getEventsManager().setEvent($element.Id() + "Height", EventType.ON_COMPLETE + "Hide",
                () : void => {
                    $element.getGuiManager().setActive($element, false);

                    ElementManager.SendToBack($element.Id());
                    ElementManager.Hide($element.Id());

                    $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_HIDE, eventArgs);
                    $element.getEventsManager().FireEvent($element, EventType.ON_HIDE, eventArgs);
                });
            ValueProgressManager.Execute(heightManipulatorArgs);
        }
    }

    private static onKeyDownEventHandler($eventArgs : KeyEventArgs, $manager? : GuiObjectManager) : void {
        const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>SelectBox);
        elements.foreach(($element : SelectBox) : void => {
            if ($eventArgs.getKeyCode() !== KeyMap.ESC) {
                if (!ObjectValidator.IsSet($element.lastActiveItem)) {
                    $element.lastActiveItem = null;
                }
                if ($eventArgs.getKeyCode() === KeyMap.ENTER ||
                    ($element.advancedSelectionEnabled && (
                        $eventArgs.getKeyCode() === KeyMap.SPACE ||
                        $eventArgs.getKeyCode() === KeyMap.LEFT_ARROW ||
                        $eventArgs.getKeyCode() === KeyMap.RIGHT_ARROW
                    ))) {
                    if (!ObjectValidator.IsEmptyOrNull($element.lastActiveItem)) {
                        SelectBox.selectOption($element, $element.lastActiveItem);
                    }
                } else if ($element.IsCompleted()) {
                    let selected : number = -1;
                    if (!ObjectValidator.IsEmptyOrNull($element.lastActiveItem)) {
                        selected = $element.lastActiveItem.Index();
                    }
                    if ($eventArgs.getKeyCode() === KeyMap.DOWN_ARROW) {
                        selected++;
                        if ($element.OptionsCount() <= selected && SelectBox.rollUpEnabled) {
                            selected = 0;
                        }
                        if (selected < $element.OptionsCount()) {
                            SelectBox.optionTurnActive($element, <SelectBoxOption>$element.items.getAll()[selected]);
                            SelectBox.scrollToOption($element, $element.lastActiveItem);
                        }
                        $eventArgs.PreventDefault();
                    } else if ($eventArgs.getKeyCode() === KeyMap.UP_ARROW) {
                        selected--;
                        if (selected < 0 && SelectBox.rollUpEnabled) {
                            selected = $element.OptionsCount() - 1;
                        }
                        if (selected >= 0) {
                            SelectBox.optionTurnActive($element, <SelectBoxOption>$element.items.getAll()[selected]);
                            SelectBox.scrollToOption($element, $element.lastActiveItem);
                        }
                        $eventArgs.PreventDefault();
                    } else if ($element.characterNavigationEnabled) {
                        const key : string = $eventArgs.getKeyChar();
                        if (key !== " ") {
                            const searchIndex : string = StringUtils.ToUpperCase(key);
                            if ($element.itemOptions.KeyExists(searchIndex)) {
                                const options : ArrayList<number> = $element.itemOptions.getItem(searchIndex);
                                const firstOption : number = options.getFirst();
                                if (options.Length() === 1) {
                                    selected = firstOption;
                                } else {
                                    const lastOption : number = options.getLast();
                                    if (selected === lastOption) {
                                        if (SelectBox.rollUpEnabled) {
                                            selected = firstOption;
                                        }
                                    } else {
                                        let index : number;
                                        for (index = 0; index < options.Length(); index++) {
                                            const current : number = options.getItem(index);
                                            if (selected < current) {
                                                selected = current;
                                                break;
                                            }
                                        }
                                    }
                                }
                                SelectBox.optionTurnActive($element, <SelectBoxOption>$element.items.getAll()[selected]);
                                SelectBox.scrollToOption($element, $element.lastActiveItem);
                            }
                        }
                    }
                }
            }
        });
    }

    private static optionTurnActive($element : SelectBox, $option : SelectBoxOption) : void {
        if (!$element.getGuiManager().IsActive(<IClassName>ScrollBar)) {
            if (!ObjectValidator.IsEmptyOrNull($element.lastActiveItem) && $option.Index() !== $element.lastActiveItem.Index()) {
                SelectBoxOption.TurnOff($element.lastActiveItem);
                SelectBox.rollUpEnabled = false;
            }
            SelectBoxOption.TurnActive($option);
            $element.lastActiveItem = $option;
        }
    }

    private static selectOption($element : SelectBox, $option : SelectBoxOption) : void {
        const eventArgs : EventArgs = new EventArgs();
        eventArgs.Owner($element);
        $element.getEventsManager().FireEvent($element, EventType.ON_SELECT, eventArgs);
        $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_SELECT, eventArgs);

        if ($element.getSelectedIndex() !== $option.Index()) {
            $element.selectedIndex = $option.Index();
            $element.selectedItem = $option;
            SelectBoxOption.TurnActive($option);

            $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, eventArgs);
            $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_CHANGE, eventArgs);
        }
    }

    private static initSelection($element : SelectBox) : void {
        if ($element.getSelectedIndex() !== -1) {
            $element.selectedIndex = -1;
            SelectBox.selectOption($element, $element.selectedItem);
            SelectBox.optionTurnActive($element, $element.selectedItem);
        }
    }

    private static scrollToOption($element : SelectBox, $option : SelectBoxOption) : void {
        if ($element.verticalScrollBar.Visible()) {
            ElementManager.getElement($element.Id() + "_Options").scrollTop =
                ElementManager.getElement($option.Id(), true).offsetTop;

            const scrollEventArgs : ScrollEventArgs = new ScrollEventArgs();
            scrollEventArgs.Owner($element);
            scrollEventArgs.OrientationType(OrientationType.VERTICAL);
            scrollEventArgs.Position(SelectBox.scrollTop($element));
            $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, scrollEventArgs);
            $element.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_SCROLL, scrollEventArgs);
        }
    }

    private static scrollTop($element : SelectBox, $value? : number) : number {
        let value : number = -1;
        const element : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($element.Id() + "_Options");
        if (ElementManager.Exists(element)) {
            if (ObjectValidator.IsSet($value)) {
                value = Property.PositiveInteger(value, $value);
                element.scrollTop = Math.ceil(
                    (element.scrollHeight - element.clientHeight) / 100 * value);
            } else {
                value = Math.ceil(
                    100 / (element.scrollHeight - element.clientHeight) * element.scrollTop);
            }
        }
        if (value !== -1) {
            if (value < 0) {
                value = 0;
            }
            if (value > 100) {
                value = 100;
            }
        }
        return value;
    }

    private static onBodyScrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                            $reflection : Reflection) : void {
        const element : SelectBox = <SelectBox>$manager.getHovered();
        if (!ObjectValidator.IsEmptyOrNull(element) &&
            $reflection.IsMemberOf(element, SelectBox)) {
            $eventArgs.PreventDefault();
            let position : number = SelectBox.scrollTop(element);
            if (position >= 0) {
                if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                    position += $eventArgs.Position() * 5;
                } else {
                    position -= $eventArgs.Position() * 5;
                }
                if (position < 0) {
                    position = 0;
                }
                if (position > 100) {
                    position = 100;
                }
                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(element);
                eventArgs.OrientationType(OrientationType.VERTICAL);
                eventArgs.Position(SelectBox.scrollTop(element, position));
                element.getEventsManager().FireEvent(element, EventType.ON_SCROLL, eventArgs);
                element.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_SCROLL, eventArgs);
            }
        }
    }

    private static onScrollBarMoveEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ScrollBar)) {
            let element : any = <ScrollBar>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, ScrollBar) &&
                ElementManager.Exists(element.Parent().Id())) {
                element = <SelectBox>element.Parent();
                if ($reflection.IsMemberOf(element, SelectBox)) {
                    const envelop : HTMLElement = ElementManager.getElement(element.Id() + "_Options");
                    envelop.scrollTop = Math.ceil((envelop.scrollHeight - envelop.clientHeight) / 100 * $eventArgs.Position());
                }
            }
        }
    }

    private static getSizeInfo($element : SelectBox) : object {
        const id : string = $element.Id();
        const leftWidth : number = ElementManager.getElement(id + "_TopLeft").offsetWidth;
        const rightWidth : number = ElementManager.getElement(id + "_TopRight").offsetWidth;
        const topHeight : number = ElementManager.getElement(id + "_TopCenter").offsetHeight;
        const bottomHeight : number = ElementManager.getElement(id + "_BottomCenter").offsetHeight;

        const contentOffsetLeft : number = leftWidth + ElementManager.getElement(id + "_Content").offsetLeft;
        const contentOffsetTop : number = topHeight + ElementManager.getElement(id + "_Content").offsetTop;

        const width : number = $element.Width() - leftWidth - rightWidth;
        let height : number = $element.Height();

        if ($element.OptionsCount() === 0) {
            height = $element.initHeight;
        } else if ($element.MaxVisibleItemsCount() !== -1) {
            height = 0;
            const allItems : SelectBoxOption[] = $element.items.getAll();
            let index : number;
            for (index = 0; index < $element.getMaxVisibleItemsCount(); index++) {
                const optionId : string = allItems[index].Id();
                if (ElementManager.Exists(optionId, true)) {
                    height += ElementManager.getElement(optionId).offsetHeight;
                }
            }
        }

        const contentWidth : number = width + leftWidth + rightWidth - 2 * contentOffsetLeft;

        return {
            bottomHeight,
            contentHeight: height,
            contentOffsetLeft,
            contentOffsetTop,
            contentWidth,
            height,
            leftWidth,
            rightWidth,
            topHeight,
            width
        };
    }

    private static finalizeShow($element : SelectBox, $info : any) : void {
        const id : string = $element.Id();
        ElementManager.BringToFront(id);

        let height : number = 0;
        let showScrollBar : boolean = false;
        $element.items.foreach(($option : SelectBoxOption) : boolean => {
            if (!ElementManager.Exists($option.Id())) {
                return false;
            }
            height += ElementManager.getElement($option.Id()).offsetHeight;
            showScrollBar = height - $info.height > 0;
            return !showScrollBar;
        });

        let resizeCounter : number = 0;
        const fireResizeEvent : () => void = () : void => {
            const info : any = SelectBox.getSizeInfo($element);
            if ($info.height === info.height) {
                resizeCounter = 0;
                const resizeEventArgs : ResizeEventArgs = new ResizeEventArgs();
                resizeEventArgs.Owner($element);
                resizeEventArgs.Width($element.Width());
                resizeEventArgs.Height($element.Height());
                resizeEventArgs.AvailableWidth(info.width);
                resizeEventArgs.AvailableHeight(height);
                resizeEventArgs.ScrollBarWidth(ElementManager.getElement(id + "_ScrollBarEnvelop").offsetWidth);
                resizeEventArgs.ScrollBarHeight(info.height);
                $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, resizeEventArgs);
                $element.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_RESIZE, resizeEventArgs);
            } else if (resizeCounter > 3) {
                SelectBox.finalizeShow($element, info);
            } else if (info.height !== 0) {
                resizeCounter++;
                $element.getEvents().FireAsynchronousMethod(fireResizeEvent, 50);
            }
        };
        $element.getEvents().FireAsynchronousMethod(fireResizeEvent);

        if (showScrollBar) {
            if (!ElementManager.IsVisible(id + "_ScrollBarEnvelop")) {
                ElementManager.setCssProperty(id + "_ScrollBarEnvelop", "height", $info.height);
                ElementManager.Show(id + "_ScrollBarEnvelop");
                $element.verticalScrollBar.Visible(true);

                const scrollBarWidth : number = ElementManager.getElement(id + "_ScrollBarEnvelop").offsetWidth;
                const scrollBarManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(id + "ScrollBarManipulator");
                scrollBarManipulatorArgs.Owner($element);
                scrollBarManipulatorArgs.DirectionType(DirectionType.UP);
                scrollBarManipulatorArgs.ProgressType(ProgressType.LINEAR);
                scrollBarManipulatorArgs.Step(1);
                scrollBarManipulatorArgs.RangeStart(0);
                scrollBarManipulatorArgs.RangeEnd(scrollBarWidth + 5);
                scrollBarManipulatorArgs.CurrentValue(0);
                scrollBarManipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
                scrollBarManipulatorArgs.CompleteEventType(EventType.ON_COMPLETE);

                $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        ElementManager.setWidth(id + "_Options", $info.contentWidth - $eventArgs.CurrentValue());
                    });
                $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_COMPLETE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        $eventArgs.DirectionType(DirectionType.DOWN);
                        $eventArgs.Step(8);
                        $eventArgs.RangeStart(scrollBarWidth);
                        $eventArgs.CompleteEventType(EventType.ON_COMPLETE + "2");

                        $element.getEventsManager().setEvent(id + "ScrollBarManipulator", EventType.ON_COMPLETE + "2",
                            () : void => {
                                const asyncManager : TimeoutManager = new TimeoutManager();
                                const allItems : SelectBoxOption[] = $element.items.getAll();
                                let blockIndex : number;
                                for (blockIndex = 0; blockIndex < Math.ceil(allItems.length / 10); blockIndex++) {
                                    asyncManager.Add(($blockIndex : number) : void => {
                                        let index : number;
                                        for (index = 0; index < 10; index++) {
                                            const itemIndex : number = ($blockIndex * 10) + index;
                                            SelectBoxOption.Resize(allItems[itemIndex]);
                                        }
                                    });
                                }
                                asyncManager.Execute();
                            });

                        ValueProgressManager.Execute($eventArgs);
                    });
                ValueProgressManager.Remove(scrollBarManipulatorArgs.OwnerId());
                ValueProgressManager.Execute(scrollBarManipulatorArgs);
            }

            if ($element.getSelectedIndex() !== -1 && $element.OptionsCount() > 0) {
                SelectBox.scrollToOption($element, $element.selectedItem);
            } else {
                const scrollEventArgs : ScrollEventArgs = new ScrollEventArgs();
                scrollEventArgs.Owner($element);
                scrollEventArgs.OrientationType(OrientationType.VERTICAL);
                scrollEventArgs.Position(SelectBox.scrollTop($element));
                $element.getEventsManager().FireEvent($element, EventType.ON_SCROLL, scrollEventArgs);
                $element.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_SCROLL, scrollEventArgs);
            }
        } else {
            ElementManager.Hide($element.Id() + "_ScrollBarEnvelop");
            $element.verticalScrollBar.Visible(false);
            ElementManager.setWidth($element.Id() + "_Options", $info.contentWidth);
        }
    }

    /**
     * @param {SelectBoxType} [$selectBoxType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($selectBoxType? : SelectBoxType, $id? : string) {
        super($id);

        this.guiType = this.guiTypeValueSetter($selectBoxType);
        let scrollBarClass : any = this.getScrollBarClass();
        if (ObjectValidator.IsEmptyOrNull(scrollBarClass)) {
            scrollBarClass = ScrollBar;
        }
        this.verticalScrollBar = new scrollBarClass(OrientationType.VERTICAL, this.Id() + "_ScrollBar");
        this.verticalScrollBar.Visible(false);
        this.verticalScrollBar.Size(this.Height());

        this.openDirectionVertical = DirectionType.DOWN;
        this.openDirectionHorizontal = DirectionType.RIGHT;

        this.characterNavigationEnabled = true;
        this.advancedSelectionEnabled = true;
    }

    /**
     * @returns {ISelectBoxEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : ISelectBoxEvents {
        return <ISelectBoxEvents>super.getEvents();
    }

    /**
     * @param {SelectBoxType} [$selectBoxType] Specify type of element look and feel.
     * @returns {SelectBoxType} Returns type of element's look and feel.
     */
    public GuiType($selectBoxType? : SelectBoxType) : SelectBoxType {
        if (ObjectValidator.IsSet($selectBoxType)) {
            this.guiType = this.guiTypeValueSetter($selectBoxType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }

        return this.guiType;
    }

    /**
     * @param {string|number} [$value] Specify value, which should be selected.
     * @returns {string} Returns slected value.
     */
    public Value($value? : string | number) : string {
        if (ObjectValidator.IsSet($value)) {
            this.Select($value);
        }
        if (!ObjectValidator.IsSet(this.selectedItem)) {
            this.selectedItem = new SelectBoxOption(this.Id(), this, -1);
        }
        return this.selectedItem.Value();
    }

    /**
     * @param {number} $width Specify initial width value.
     * @param {number} $height Specify initial height value.
     * @returns {void}
     */
    public setInitSize($width : number, $height : number) : void {
        if (!this.IsCompleted()) {
            this.initWidth = Property.PositiveInteger(this.initWidth, $width);
            this.initHeight = Property.PositiveInteger(this.initHeight, $height);
            if (ObjectValidator.IsEmptyOrNull(this.initWidth)) {
                this.initWidth = SelectBox.minWidth;
            }
            if (ObjectValidator.IsEmptyOrNull(this.initHeight)) {
                this.initHeight = SelectBox.minHeight;
            }
        }
    }

    /**
     * @param {DirectionType} $verticalDirection Specify vertical direction,
     * which should be used for open/close of the element
     * @param {DirectionType} $horizontalDirection Specify horizontal direction,
     * which should be used for open/close of the element
     * @returns {void}
     */
    public setOpenDirection($verticalDirection : DirectionType, $horizontalDirection : DirectionType) : void {
        if ($verticalDirection === DirectionType.UP || $verticalDirection === DirectionType.DOWN) {
            this.openDirectionVertical = $verticalDirection;
        }
        if ($horizontalDirection === DirectionType.LEFT || $horizontalDirection === DirectionType.RIGHT) {
            this.openDirectionHorizontal = $horizontalDirection;
        }
    }

    /**
     * Disable ability to select options by typing of start characters.
     * @returns {void}
     */
    public DisableCharacterNavigation() : void {
        this.characterNavigationEnabled = false;
    }

    /**
     * Disable selection of the option by navigation arrows or by SPACE key
     * @returns {void}
     */
    public DisableAdvancedSelection() : void {
        this.advancedSelectionEnabled = false;
    }

    /**
     * @param {number} [$value] Specify width value of element.
     * @returns {number} Returns element's width value.
     */
    public Width($value? : number) : number {
        return this.width = Property.PositiveInteger(this.width, $value, SelectBox.minWidth);
    }

    /**
     * @param {number} [$value] Specify height value of element.
     * @returns {number} Returns element's height value.
     */
    public Height($value? : number) : number {
        if (!ObjectValidator.IsSet(this.height)) {
            this.height = -1;
        }
        if (ObjectValidator.IsSet($value)) {
            if ($value === -1) {
                if (this.MaxVisibleItemsCount() === -1) {
                    this.MaxVisibleItemsCount(this.OptionsCount());
                }
                this.height = -1;
            } else {
                this.height = Property.PositiveInteger(this.height, $value, SelectBox.minHeight);
            }
        }
        return this.height;
    }

    /**
     * @param {number} [$value] Specify maximal number of items, which can be shown in menu content.
     * @returns {number} Returns maximal number of visible items.
     */
    public MaxVisibleItemsCount($value? : number) : number {
        if (!ObjectValidator.IsSet(this.visibleItemsCount)) {
            this.visibleItemsCount = -1;
        }
        return this.visibleItemsCount = Property.PositiveInteger(this.visibleItemsCount, $value, 1, 20);
    }

    /**
     * @param {string} $text Specify visible text value of the item.
     * @param {string|number} [$value] Specify not visible value of the item.
     * @param {string} [$styleClassName] Specify class name specific for the item.
     * @returns {void}
     */
    public Add($text : string, $value? : string | number, $styleClassName? : string) : void {
        const newItem : SelectBoxOption = new SelectBoxOption($text, this, this.OptionsCount());
        newItem.Value($value);
        newItem.StyleClassName($styleClassName);

        const newItemKey : number = newItem.getHash();
        if (!this.items.KeyExists(newItemKey)) {
            this.items.Add(newItem, newItemKey);
            if (this.IsCompleted()) {
                ElementManager.AppendHtml(this.Id() + "_Options", newItem.getInnerHtml().Draw(this.outputEndOfLine));
                if (this.OptionsCount() === 1) {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        SelectBox.scrollTop(this, 0);
                    });
                }
                if (this.getGuiManager().IsActive(this)) {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if (!newItem.IsCompleted()) {
                            this.initOption(newItem);
                        }
                    }, false);
                }
            }
        }
    }

    /**
     * @returns {void}
     */
    public Clear() : void {
        this.getItems().Clear();
        this.getItemOptions().Clear();
        this.selectedIndex = -1;
        this.selectedItem = new SelectBoxOption(this.Id(), this, -1);
        this.lastActiveItem = null;

        if (this.IsCompleted()) {
            ElementManager.setInnerHtml(this.Id() + "_Options", "");
            this.getEvents().FireAsynchronousMethod(() : void => {
                if (this.OptionsCount() === 0) {
                    SelectBox.finalizeShow(this, SelectBox.getSizeInfo(this));
                }
            }, 100);
        }
    }

    /**
     * @returns {number} Returns number of options shown in the select box.
     */
    public OptionsCount() : number {
        return this.getItems().Length();
    }

    /**
     * @param {string|number} $value Specify item, which should selected. Item can be selected by text, value or index.
     * Items are indexed from 0.
     * @returns {void}
     */
    public Select($value : string | number) : void {
        let itemIndex : number = 0;
        let selectedItem : SelectBoxOption = null;
        this.getItems().foreach(($item : SelectBoxOption) : boolean => {
            if ((ObjectValidator.IsInteger($value) && $value === itemIndex) ||
                $value === $item.Text() || $value === $item.Value()) {
                $item.IsSelected(true);
                selectedItem = $item;
                return false;
            }
            itemIndex++;
            return true;
        });
        if (!ObjectValidator.IsEmptyOrNull(selectedItem)) {
            if (this.IsLoaded() && ElementManager.IsVisible(this.Id())) {
                SelectBox.selectOption(this, selectedItem);
            } else {
                this.selectedIndex = itemIndex;
                this.selectedItem = selectedItem;
            }
        }
    }

    protected getScrollBarClass() : any {
        return ScrollBar;
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.GuiType(), $value, SelectBoxType, SelectBoxType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!SelectBoxType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of select box type instead of StyleClassName method.");
        return false;
    }

    protected innerCode() : IGuiElement {
        if (this.height === -1 && this.MaxVisibleItemsCount() === -1) {
            this.MaxVisibleItemsCount(this.getItems().Length());
        }
        if (!ObjectValidator.IsSet(this.openDirectionVertical)) {
            this.openDirectionVertical = DirectionType.DOWN;
        }
        if (!ObjectValidator.IsSet(this.openDirectionHorizontal)) {
            this.openDirectionHorizontal = DirectionType.RIGHT;
        }
        if (!ObjectValidator.IsSet(this.characterNavigationEnabled)) {
            this.characterNavigationEnabled = true;
        }
        if (!ObjectValidator.IsSet(this.advancedSelectionEnabled)) {
            this.advancedSelectionEnabled = true;
        }
        if (ObjectValidator.IsSet(this.verticalScrollBar)) {
            this.verticalScrollBar.Visible(false);
            this.verticalScrollBar.Size(this.Height());
        }

        this.Select(this.Value());

        this.verticalScrollBar.getEvents().setOnButton(SelectBox.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnChange(SelectBox.onScrollBarMoveEventHandler);
        this.getEvents().setOnResize(ScrollBar.ResizeEventHandler);
        this.getEvents().setOnScroll(ScrollBar.ScrollEventHandler);

        WindowManager.getEvents().setOnScroll(SelectBox.onBodyScrollEventHandler);
        WindowManager.getEvents().setOnKeyUp(() : void => {
            SelectBox.rollUpEnabled = true;
        });
        WindowManager.getEvents().setOnKeyDown(SelectBox.onKeyDownEventHandler);

        this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
            const element : SelectBox = <SelectBox>$eventArgs.Owner();
            $manager.setHovered(element, true);
            if (!ObjectValidator.IsEmptyOrNull(element.Parent())) {
                (<BaseGuiObject>element.Parent()).Title().Enabled(false);
            }
        });

        this.getEvents().setOnMouseOut(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
            const element : SelectBox = <SelectBox>$eventArgs.Owner();
            $manager.setHovered(element, false);
            if (!ObjectValidator.IsEmptyOrNull(element.Parent())) {
                (<BaseGuiObject>element.Parent()).Title().Enabled(true);
            }
        });

        this.getEvents().setOnResize(($eventArgs : EventArgs) : void => {
            const element : SelectBox = <SelectBox>$eventArgs.Owner();
            element.items.foreach(($option : SelectBoxOption) : void => {
                if (!$option.IsCompleted()) {
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        element.initOption($option);
                    }, false);
                }
            });
        });

        this.getEvents().setOnComplete(() : void => {
            if (ObjectValidator.IsEmptyOrNull(this.outputEndOfLine)) {
                this.outputEndOfLine = StringUtils.NewLine(false);
            }
            let optionsHtml : string = "";
            this.items.foreach(($option : SelectBoxOption) : void => {
                optionsHtml += $option.getInnerHtml().Draw(this.outputEndOfLine + "                           ");
            });

            ElementManager.AppendHtml(this.Id() + "_Options", optionsHtml);
            SelectBox.initSelection(this);
            this.getGuiManager().setActive(this, false);
            SelectBox.Show(this);
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_Envelop")
                .StyleClassName(GeneralCssNames.BACKGROUND)
                .Add(this.addElement(this.Id() + "_Top")
                    .StyleClassName(GeneralCssNames.TOP)
                    .Add(this.addElement(this.Id() + "_TopLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_TopCenter").StyleClassName(GeneralCssNames.CENTER).Width(this.Width()))
                    .Add(this.addElement(this.Id() + "_TopRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_Middle")
                    .StyleClassName(GeneralCssNames.MIDDLE)
                    .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT).Height(1))
                    .Add(this.addElement(this.Id() + "_MiddleCenter")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Width(this.Width()).Height(1)
                        .Add(this.addElement(this.Id() + "_Content")
                            .StyleClassName("Content")
                            .Width(this.Width()).Height(1)
                            .Add(this.addElement(this.Id() + "_Options").StyleClassName("Options").Height(0))
                            .Add(this.addElement(this.Id() + "_ScrollBarEnvelop")
                                .StyleClassName("ScrollBarEnvelop")
                                .Visible(false)
                                .Add(this.verticalScrollBar)
                            )
                        )
                    )
                    .Add(this.addElement(this.Id() + "_MiddleRight").StyleClassName(GeneralCssNames.RIGHT).Height(1))
                )
                .Add(this.addElement(this.Id() + "_Bottom")
                    .StyleClassName(GeneralCssNames.BOTTOM)
                    .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_BottomCenter").StyleClassName(GeneralCssNames.CENTER).Width(this.Width()))
                    .Add(this.addElement(this.Id() + "_BottomRight").StyleClassName(GeneralCssNames.RIGHT))
                )
            );
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push(
            "initWidth", "initHeight",
            "items", "itemOptions",
            "selectedItem", "lastActiveItem");

        if (this.Height() === -1) {
            exclude.push("height");
        }
        if (this.MaxVisibleItemsCount() === -1) {
            exclude.push("visibleItemsCount");
        }
        if (this.getSelectedIndex() === -1) {
            exclude.push("selectedIndex");
        }
        if (this.openDirectionVertical === DirectionType.DOWN) {
            exclude.push("openDirectionVertical");
        }
        if (this.openDirectionHorizontal === DirectionType.RIGHT) {
            exclude.push("openDirectionHorizontal");
        }
        if (this.characterNavigationEnabled === true) {
            exclude.push("characterNavigationEnabled");
        }
        if (this.advancedSelectionEnabled === true) {
            exclude.push("advancedSelectionEnabled");
        }
        return exclude;
    }

    protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : string) => void) : void {
        const id : string = this.Id() + "_Options";
        $preparationResultHandler(id, ElementManager.getInnerHtml(id));
        ElementManager.setInnerHtml(id, "");
    }

    protected afterCacheCreation($id : string, $value : string) : void {
        ElementManager.setInnerHtml($id, $value);
    }

    private getItems() : ArrayList<SelectBoxOption> {
        if (!ObjectValidator.IsSet(this.items)) {
            this.items = new ArrayList<SelectBoxOption>();
        }
        return this.items;
    }

    private getItemOptions() : ArrayList<ArrayList<number>> {
        if (!ObjectValidator.IsSet(this.itemOptions)) {
            this.itemOptions = new ArrayList<ArrayList<number>>();
        }
        return this.itemOptions;
    }

    private getSelectedIndex() : number {
        if (!ObjectValidator.IsSet(this.selectedIndex)) {
            this.selectedIndex = -1;
        }
        return this.selectedIndex;
    }

    private getMaxVisibleItemsCount() : number {
        const itemsCount : number = this.OptionsCount();
        if (this.MaxVisibleItemsCount() !== -1) {
            if (this.MaxVisibleItemsCount() > itemsCount) {
                return itemsCount;
            }
            return this.MaxVisibleItemsCount();
        }
        return itemsCount;
    }

    private initOption($element : SelectBoxOption) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            if (this.characterNavigationEnabled) {
                const option : string = StringUtils.ToUpperCase(StringUtils.getCharacterAt($element.Text(), 0));
                if (!this.getItemOptions().KeyExists(option)) {
                    this.itemOptions.Add(new ArrayList<number>(), option);
                }
                this.itemOptions.getItem(option).Add($element.Index());
            }
            this.items.getLast().SeparatorVisible(true);
            $element.SeparatorVisible(false);
            $element.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
                SelectBox.optionTurnActive($eventArgs.Owner(), $element);
            });
            $element.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                SelectBox.selectOption($eventArgs.Owner(), $element);
            });
            $element.getEvents().Subscribe(true);
            if (!$element.IsCompleted()) {
                this.getEventsManager().FireEvent($element.Id(), EventType.ON_COMPLETE);
                $element.IsCompleted(true);
            }
        }
    }
}
