/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { CursorType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/CursorType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IResizeBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IResizeBar.js";
import { IResizeBarEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IResizeBarEvents.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiCommonsListArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsListArg.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { Loader } from "../../Loader.js";

/**
 * DragBar class provides element's component enabling resize of parent element.
 */
export class ResizeBar extends GuiCommons implements IResizeBar {
    private resizeType : ResizeableType;
    private cursorType : CursorType;

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnOn($element : ResizeBar) : void {
        if (!$element.getGuiManager().IsActive(<IClassName>ResizeBar)) {
            ElementManager.TurnOn($element.Id() + "_Status");
        }
    }

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnOff($element : ResizeBar) : void {
        if (!$element.getGuiManager().IsActive(<IClassName>ResizeBar)) {
            ElementManager.setClassName($element.Id() + "_Status", "");
            document.body.style.cursor = "default";
        }
    }

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static TurnActive($element : ResizeBar) : void {
        ElementManager.TurnActive($element.Id() + "_Status");
    }

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @param {number} $value Specify new width value for handled element.
     * @returns {void}
     */
    public static setWidth($element : ResizeBar, $value : number) : void {
        ElementManager.setWidth($element.guiContentId(), $value);
    }

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @param {number} $value Specify new height value for handled element.
     * @returns {void}
     */
    public static setHeight($element : ResizeBar, $value : number) : void {
        ElementManager.setHeight($element.guiContentId(), $value);
    }

    /**
     * @param {ResizeBar} $element Specify element, which should be handled.
     * @param {number} $distanceX Specify distance from current width, which should be applied.
     * @param {number} $distanceY Specify distance from current height, which should be applied.
     * @returns {void}
     */
    public static ResizeTo($element : ResizeBar, $distanceX : number, $distanceY : number) : void {
        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
        eventArgs.Owner($element);
        eventArgs.DistanceX($distanceX);
        eventArgs.DistanceY($distanceY);
        $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs);
        $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE, eventArgs);
    }

    /**
     * Handle resize event
     * @param {ResizeEventArgs} $eventArgs Specify event args of resize event.
     * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
     * @param {Reflection} [$reflection] Provide reflection instance.
     * @returns {void}
     */
    public static ResizeEventHandler($eventArgs : ResizeEventArgs, $manager : GuiObjectManager,
                                     $reflection : Reflection) : void {
        if ($reflection.IsMemberOf($eventArgs, ResizeEventArgs)) {
            const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
            element.getChildElements().foreach(($child : ResizeBar) : void => {
                if ($reflection.IsMemberOf($child, ResizeBar) &&
                    ElementManager.IsVisible($child.Id())) {
                    if ($child.ResizeableType() === ResizeableType.HORIZONTAL) {
                        ResizeBar.setHeight($child, $eventArgs.AvailableHeight());
                    }
                    if ($child.ResizeableType() === ResizeableType.VERTICAL) {
                        ResizeBar.setWidth($child, $eventArgs.AvailableWidth());
                    }
                }
            });
        }
    }

    private static moveInit($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                            $reflection : Reflection) : void {
        const element : ResizeBar = <ResizeBar>$eventArgs.Owner();
        if ($reflection.IsMemberOf(element, ResizeBar)) {
            $manager.setActive(element, true);
            ResizeBar.TurnActive(element);

            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START, ResizeBar.onMoveStartEventHandler);
            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, ResizeBar.onMoveChangeEventHandler);
            element.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, ResizeBar.onMoveCompleteEventHandler);
        }
    }

    private static setMouseCursor($resizeableType : ResizeableType, $cursorType? : CursorType) : void {
        if (ObjectValidator.IsEmptyOrNull($cursorType)) {
            switch ($resizeableType) {
            case ResizeableType.HORIZONTAL_AND_VERTICAL:
                document.body.style.cursor = "nw-resize";
                break;
            case ResizeableType.HORIZONTAL:
                document.body.style.cursor = "w-resize";
                break;
            case ResizeableType.VERTICAL:
                document.body.style.cursor = "n-resize";
                break;
            default :
                document.body.style.cursor = "default";
                break;
            }
        } else {
            document.body.style.cursor = $cursorType.toString();
        }
    }

    private static onMoveStartEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
        Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
            if ($manager.IsActive(<IClassName>ResizeBar)) {
                $args.StopAllPropagation();
                const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>ResizeBar);
                elements.foreach(($element : ResizeBar) : void => {
                    if ($element.Enabled()) {
                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner($element);
                        eventArgs.ResizeableType($element.ResizeableType());
                        $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_START, eventArgs, false);
                        $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_START, eventArgs, false);

                        ResizeBar.setMouseCursor($element.ResizeableType(), $element.CursorType());
                    }
                });
            }
        });
    }

    private static onMoveChangeEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
        Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
            if ($manager.IsActive(<IClassName>ResizeBar)) {
                $args.StopAllPropagation();
                const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>ResizeBar);
                elements.foreach(($element : ResizeBar) : void => {
                    if ($element.Enabled()) {
                        ResizeBar.setMouseCursor($element.ResizeableType(), $element.CursorType());

                        let distanceX : number = 0;
                        let distanceY : number = 0;

                        if ($element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            $element.ResizeableType() === ResizeableType.HORIZONTAL) {
                            distanceX = $args.getDistanceX();
                        }
                        if ($element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            $element.ResizeableType() === ResizeableType.VERTICAL) {
                            distanceY = $args.getDistanceY();
                        }

                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner($element);
                        eventArgs.ResizeableType($element.ResizeableType());
                        eventArgs.DistanceX(distanceX);
                        eventArgs.DistanceY(distanceY);
                        $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_CHANGE, eventArgs, false);
                        $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_CHANGE, eventArgs, false);
                    }
                });
            }
        });
    }

    private static onMoveCompleteEventHandler($args : MoveEventArgs, $manager : GuiObjectManager) : void {
        Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
            if ($manager.IsActive(<IClassName>ResizeBar)) {
                $args.StopAllPropagation();
                const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>ResizeBar);
                elements.foreach(($element : ResizeBar) : void => {
                    if ($element.Enabled()) {
                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner($element);
                        eventArgs.ResizeableType($element.ResizeableType());
                        $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                        $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                        $element.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                        $element.getEventsManager().FireEvent(ResizeBar.ClassName(), EventType.ON_RESIZE, eventArgs, false);
                        $manager.setActive($element, false);
                        ResizeBar.TurnOff($element);
                    }
                });
                Loader.getInstance().getHttpResolver().getEvents().RemoveHandler(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                    ResizeBar.onMoveStartEventHandler);
                Loader.getInstance().getHttpResolver().getEvents().RemoveHandler(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                    ResizeBar.onMoveChangeEventHandler);
            }
        });
    }

    /**
     * @param {ResizeableType} [$resizeableType] Specify element's look and feel.
     * @param {CursorType} [$cursorType] Specify element's cursor handling type during resize.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($resizeableType? : ResizeableType, $cursorType? : CursorType, $id? : string) {
        super($id);
        this.ResizeableType($resizeableType);
        this.CursorType($cursorType);
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            if ($value) {
                ElementManager.ClearCssProperty(this.Id(), "pointer-events");
            } else {
                ElementManager.setCssProperty(this.Id(), "pointer-events", "none");
            }
        }
        return super.Enabled($value);
    }

    /**
     * @returns {IResizeBarEvents} Returns events manager subscribed to the item.
     */
    public getEvents() : IResizeBarEvents {
        return <IResizeBarEvents>super.getEvents();
    }

    /**
     * @param {ResizeableType} [$type] Specify element's look and feel.
     * @returns {ResizeableType} Returns type of element's behavior.
     */
    public ResizeableType($type? : ResizeableType) : ResizeableType {
        if (ObjectValidator.IsSet($type) && ResizeableType.Contains($type)) {
            this.resizeType = $type;
            if (ElementManager.Exists(this.Id())) {
                if ($type === ResizeableType.NONE) {
                    ResizeBar.Hide(this);
                } else {
                    ResizeBar.Show(this);
                }
            }
        }
        return this.resizeType;
    }

    /**
     * @param {CursorType} [$type] Specify element's cursor look and feel.
     * @returns {CursorType} Returns type of element's cursor.
     */
    public CursorType($type? : CursorType) : CursorType {
        if (ObjectValidator.IsSet($type) && CursorType.Contains($type)) {
            this.cursorType = $type;
        }
        return this.cursorType;
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const args : IGuiCommonsArg[] = super.getArgs();
        args.push(<IGuiCommonsListArg>{
            items: ResizeableType.getProperties(),
            name : "ResizeableType",
            type : GuiCommonsArgType.LIST,
            value: ResizeableType.getKey(<string>this.ResizeableType())
        });
        return args;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
     * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
     * @returns {void}
     */
    public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
        switch ($value.name) {
        case "Width":
            Reflection.getInstance().getClass(this.getClassName()).setWidth(this, <number>$value.value);
            break;
        case "Height":
            Reflection.getInstance().getClass(this.getClassName()).setHeight(this, <number>$value.value);
            break;
        case "ResizeableType":
            this.ResizeableType(ResizeableType[<string>$value.value]);
            break;
        default:
            super.setArg($value, $force);
            break;
        }
    }

    protected innerCode() : IGuiElement {
        this.getEvents().Subscriber(this.Id() + "_Position");

        if (this.resizeType === ResizeableType.NONE) {
            this.Visible(false);
        }

        this.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) : void => {
            ResizeBar.TurnOn($eventArgs.Owner());
        });
        this.getEvents().setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
            ResizeBar.TurnOff($eventArgs.Owner());
        });
        this.getEvents().setOnMouseDown(ResizeBar.moveInit);
        this.getEvents().setOnMouseUp(($eventArgs : MouseEventArgs) : void => {
            ResizeBar.TurnOff($eventArgs.Owner());
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Status").Add(this.addElement(this.Id() + "_Position").StyleClassName(this.resizeType));
    }

    protected guiContentId() : string {
        return this.Id() + "_Position";
    }
}
