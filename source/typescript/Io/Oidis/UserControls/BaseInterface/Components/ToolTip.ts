/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IToolTip } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IToolTip.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiCommonsListArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsListArg.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { Borders } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Borders.js";
import { ElementOffset } from "@io-oidis-gui/Io/Oidis/Gui/Structures/ElementOffset.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { ToolTipType } from "../Enums/Components/ToolTipType.js";

/**
 * ToolTip class provides element's component showing help or hint associated with the element.
 */
export class ToolTip extends GuiCommons implements IToolTip {
    private text : string;
    private guiType : ToolTipType;
    private elementPosition : ElementOffset;
    private elementBorders : Borders;

    /**
     * @param {ToolTip} $element Specify element, which should be handled.
     * @param {MouseEventArgs} [$eventArgs] Provide event args connected with mouse event.
     * @returns {void}
     */
    public static Show($element : ToolTip, $eventArgs? : MouseEventArgs) : void {
        const id : string = $element.Id();

        if (!ObjectValidator.IsEmptyOrNull($element.Text()) && $element.Enabled()) {
            ElementManager.Show(id);
            ElementManager.setCssProperty(id, "top", 0);
            ElementManager.setCssProperty(id, "left", 0);

            $element.position(ElementManager.getAbsoluteOffset(id));

            ToolTip.resize($element);
            ToolTip.Move($element, $eventArgs);
            ElementManager.setCssProperty(id, "position", "fixed");
            ElementManager.setCssProperty(id, "z-index", "10000");
            ElementManager.ChangeOpacity($element, DirectionType.UP, 11);
        }
    }

    /**
     * @param {ToolTip} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Hide($element : ToolTip) : void {
        const id : string = $element.Id();
        if (ElementManager.IsVisible(id)) {
            ElementManager.StopOpacityChange($element);
            ElementManager.ChangeOpacity($element, DirectionType.DOWN, 10, () : void => {
                ElementManager.setCssProperty($element, "z-index", "-1000");
            });
        }
    }

    /**
     * @param {ToolTip} $element Specify element, which should be handled.
     * @param {MouseEventArgs} [$eventArgs] Provide event args connected with mouse move event.
     * @returns {void}
     */
    public static Move($element : ToolTip, $eventArgs? : MouseEventArgs) : void {
        if (!ObjectValidator.IsEmptyOrNull($element.Text())) {
            const id : string = $element.Id();
            if (!ElementManager.IsVisible(id)) {
                ToolTip.Show($element, $eventArgs);
                return;
            }

            let x : number = 0;
            let y : number = 0;

            if (ObjectValidator.IsSet($eventArgs)) {
                const nativeEvent : MouseEvent = <MouseEvent>$eventArgs.NativeEventArgs();
                x = nativeEvent.clientX;
                y = nativeEvent.clientY;
            } else if ($element.getGuiManager().Exists($element.Parent())) {
                const parent : IGuiCommons = $element.Parent();
                if (parent.IsLoaded()) {
                    const parentOffset : ElementOffset =
                        ElementManager.getAbsoluteOffset(parent.Id());
                    x = parentOffset.Left();
                    y = parentOffset.Top();
                }
            }

            if (x < 0) {
                x = 0;
            }
            if (y < 0) {
                y = 0;
            }

            const toolTipEnvelopSize : Size = new Size(id + "_Border", true);
            if (!ObjectValidator.IsEmptyOrNull($element.position())) {
                let positionY : number = y - $element.position().Top() - toolTipEnvelopSize.Height() - 10;
                let positionX : number = x - $element.position().Left() + 8;

                if (positionY < 0) {
                    positionY = y - $element.position().Top() + 15;
                    positionX = x - $element.position().Left() + 20;
                }

                if (positionX + toolTipEnvelopSize.Width() > WindowManager.getSize().Width() - 15) {
                    positionX = positionX - toolTipEnvelopSize.Width() - 25;
                }

                ElementManager.setCssProperty(id, "top", positionY);
                ElementManager.setCssProperty(id, "left", positionX);
                if (!ObjectValidator.IsEmptyOrNull($element.borders())) {
                    positionY += $element.borders().Top();
                    positionX += $element.borders().Left();
                }
                ElementManager.setCssProperty(id + "_Text", "top", positionY);
                ElementManager.setCssProperty(id + "_Text", "left", positionX);
            }
        } else {
            ToolTip.Hide($element);
        }
    }

    private static resize($element : ToolTip) : void {
        let parentEnabled : boolean = true;
        const parent : IGuiCommons = $element.Parent();
        if (!ObjectValidator.IsEmptyOrNull(parent)) {
            parentEnabled = parent.Enabled();
        }

        if (parentEnabled) {
            const id : string = $element.Id();
            const text : HTMLElement = ElementManager.getElement(id + "_Text");
            const leftWidth : number = ElementManager.getElement(id + "_TopLeft").offsetWidth;
            const rightWidth : number = ElementManager.getElement(id + "_TopRight").offsetWidth;
            const borderWidth : number = leftWidth + rightWidth + text.offsetWidth;

            ElementManager.setWidth(id + "_Border", borderWidth);
            ElementManager.setWidth(id + "_TopMiddle", text.offsetWidth);
            ElementManager.setWidth(id + "_MiddleMiddle", text.offsetWidth);
            ElementManager.setWidth(id + "_BottomMiddle", text.offsetWidth);
            ElementManager.setHeight(id + "_MiddleLeft", text.offsetHeight);
            ElementManager.setHeight(id + "_MiddleMiddle", text.offsetHeight);
            ElementManager.setHeight(id + "_MiddleRight", text.offsetHeight);

            const borders : Borders = new Borders();
            borders.Top(ElementManager.getElement(id + "_TopMiddle").offsetHeight);
            borders.Left(ElementManager.getElement(id + "_MiddleLeft").offsetWidth);
            $element.borders(borders);
        }
    }

    /**
     * @param {string} [$text] Specify tooltip text value.
     * @param {ToolTipType} [$toolTipType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    constructor($text? : string, $toolTipType? : ToolTipType, $id? : string) {
        super($id);
        this.Visible(false);
        this.DisableAsynchronousDraw();
        this.guiType = this.guiTypeValueSetter($toolTipType);
        this.Text($text);
    }

    /**
     * @param {ToolTipType} [$toolTipType] Specify type of element's look and feel.
     * @returns {ToolTipType} Returns type of element's look and feel.
     */
    public GuiType($toolTipType? : ToolTipType) : ToolTipType {
        if (ObjectValidator.IsSet($toolTipType)) {
            this.guiType = this.guiTypeValueSetter($toolTipType);
            if (ElementManager.IsVisible(this.Id())) {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            }
        }
        return this.guiType;
    }

    /**
     * @param {string} [$value] Specify element's text value.
     * @returns {string} Returns element's text value.
     */
    public Text($value? : string) : string {
        this.text = Property.String(this.text, $value);
        if (ObjectValidator.IsSet($value)) {
            ElementManager.setInnerHtml(this.Id() + "_Text", $value);
            if (ElementManager.IsVisible(this.Id())) {
                if (ObjectValidator.IsEmptyOrNull($value)) {
                    ToolTip.Hide(this);
                } else {
                    ToolTip.resize(this);
                }
            }
        }
        return this.text;
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const args : IGuiCommonsArg[] = super.getArgs();
        args.push({
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: this.Text()
        });
        args.push(<IGuiCommonsListArg>{
            items: ToolTipType.getProperties(),
            name : "GuiType",
            type : GuiCommonsArgType.LIST,
            value: ToolTipType.getKey(<string>this.GuiType())
        });
        return args;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
     * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
     * @returns {void}
     */
    public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
        switch ($value.name) {
        case "Text":
            this.Text(<string>$value.value);
            break;
        case "GuiType":
            this.GuiType(ToolTipType[<string>$value.value]);
            break;
        default:
            super.setArg($value);
            break;
        }
    }

    protected guiTypeValueSetter($value : any) : any {
        return Property.EnumType(this.guiType, $value, ToolTipType, ToolTipType.GENERAL);
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        if (!ToolTipType.Contains($value)) {
            return super.styleClassNameSetterValidator($value);
        }
        Echo.Println("Use GuiType method for set of tooltip type instead of StyleClassName method.");
        return false;
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_Border")
                .StyleClassName(GeneralCssNames.BACKGROUND)
                .Add(this.addElement(this.Id() + "_Top")
                    .StyleClassName(GeneralCssNames.TOP)
                    .Add(this.addElement(this.Id() + "_TopLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_TopMiddle").StyleClassName(GeneralCssNames.CENTER))
                    .Add(this.addElement(this.Id() + "_TopRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_Middle")
                    .StyleClassName(GeneralCssNames.MIDDLE)
                    .Add(this.addElement(this.Id() + "_MiddleLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_MiddleMiddle")
                        .StyleClassName(GeneralCssNames.CENTER)
                        .Add(this.addElement(this.Id() + "_Text")
                            .StyleClassName(GeneralCssNames.TEXT)
                            .Add(this.text)
                        )
                    )
                    .Add(this.addElement(this.Id() + "_MiddleRight").StyleClassName(GeneralCssNames.RIGHT))
                )
                .Add(this.addElement(this.Id() + "_Bottom")
                    .StyleClassName(GeneralCssNames.BOTTOM)
                    .Add(this.addElement(this.Id() + "_BottomLeft").StyleClassName(GeneralCssNames.LEFT))
                    .Add(this.addElement(this.Id() + "_BottomMiddle").StyleClassName(GeneralCssNames.CENTER))
                    .Add(this.addElement(this.Id() + "_BottomRight").StyleClassName(GeneralCssNames.RIGHT))
                )
            );
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        super.setInstanceAttributes();
        this.Visible(false);
        this.DisableAsynchronousDraw();
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push("elementPosition", "elementBorders");
        return exclude;
    }

    private position($value? : ElementOffset) : ElementOffset {
        if (ObjectValidator.IsSet($value)) {
            this.elementPosition = $value;
        }
        return this.elementPosition;
    }

    private borders($value? : Borders) : Borders {
        if (ObjectValidator.IsSet($value)) {
            this.elementBorders = $value;
        }
        return this.elementBorders;
    }
}
