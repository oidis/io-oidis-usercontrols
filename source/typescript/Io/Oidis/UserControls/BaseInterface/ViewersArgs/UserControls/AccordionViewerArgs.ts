/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BasePanelHolderViewer } from "../../../Primitives/BasePanelHolderViewer.js";
import { BasePanelHolderViewerArgs } from "../../../Primitives/BasePanelHolderViewerArgs.js";
import { AccordionResizeType } from "../../Enums/UserControls/AccordionResizeType.js";
import { AccordionType } from "../../Enums/UserControls/AccordionType.js";
import { HorizontalPanelHolderViewer } from "../../Viewers/UserControls/HorizontalPanelHolderViewer.js";
import { VerticalPanelHolderViewer } from "../../Viewers/UserControls/VerticalPanelHolderViewer.js";

/**
 * AccordionViewerArgs is structure handling BasePanelHolders instances.
 */
export class AccordionViewerArgs extends BasePanelViewerArgs {
    private readonly holdersArgsList : ArrayList<BasePanelHolderViewerArgs>;
    private readonly guiType : AccordionType;
    private resizeType : AccordionResizeType;

    /**
     * @param {AccordionType} [$accordionType] Specify Accordion look and feel.
     */
    constructor($accordionType? : AccordionType) {
        super();

        this.holdersArgsList = new ArrayList<BasePanelHolderViewerArgs>();
        this.guiType = Property.EnumType(this.guiType, $accordionType, AccordionType, AccordionType.HORIZONTAL);
    }

    /**
     * @param {BasePanelHolderViewerArgs} [$value] Register panel holder arguments,
     * which should be generated as content of user control body.
     * @returns {void}
     */
    public AddPanelHoldersArgs($value : BasePanelHolderViewerArgs) : void {
        this.holdersArgsList.Add($value);
    }

    /**
     * @returns {ArrayList<BasePanelHolderViewerArgs>} Returns list of registered arguments for BasePanelHolder user controls.
     */
    public getPanelHoldersArgsList() : ArrayList<BasePanelHolderViewerArgs> {
        return this.holdersArgsList;
    }

    /**
     * @returns {AccordionType} Returns type of element's look and feel.
     */
    public getGuiType() : AccordionType {
        return this.guiType;
    }

    /**
     * @param {ResizeType} [$value] Specify how resizing should be handled.
     * @returns {ResizeType} Returns type of resize that should be applied.
     */
    public ResizeType($value? : AccordionResizeType) : AccordionResizeType {
        if (ObjectValidator.IsSet($value)) {
            this.resizeType = $value;
        }
        return this.resizeType;
    }

    /**
     * @returns {BasePanelHolderViewer} Returns class with type of BasePanelHolderViewer based on desired guiType.
     */
    public getPanelHolderViewerClass() : any {
        switch (this.guiType) {
        case AccordionType.VERTICAL:
            return VerticalPanelHolderViewer;
        case AccordionType.HORIZONTAL:
            return HorizontalPanelHolderViewer;
        default :
            return BasePanelHolderViewer;
        }
    }
}
