/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";

/**
 * DialogViewerArgs is abstract structure handling data connected with Dialog user control.
 */
export class DialogViewerArgs extends BasePanelViewerArgs {
    private panelArgs : BasePanelViewerArgs;
    private headerText : string;

    constructor() {
        super();
        super.Visible(false);
        this.headerText = "";
    }

    /**
     * @param {BasePanelViewerArgs} [$value] Specify arguments, which will be passed to the dialog's panel.
     * @returns {BasePanelViewerArgs} Returns dialog's panel arguments.
     */
    public PanelArgs($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
        if (!ObjectValidator.IsEmptyOrNull($value) &&
            Reflection.getInstance().IsMemberOf($value, BasePanelViewerArgs)) {
            this.panelArgs = $value;
        }
        return this.panelArgs;
    }

    /**
     * @param {string} [$value] Set value for text of dialog header.
     * @returns {string} Returns value of dialog's header text.
     */
    public HeaderText($value? : string) : string {
        return this.headerText = Property.String(this.headerText, $value);
    }
}
