/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { IconTest } from "../../../RuntimeTests/UserControls/IconTest.js";
import { IconType } from "../../Enums/UserControls/IconType.js";
import { Icon } from "../../UserControls/Icon.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class IconViewer extends BaseViewer {

    constructor($args? : BaseViewerArgs) {
        super($args);
        this.setInstance(new Icon(IconType.BLUE_SQUARE));
        /* dev:start */
        this.setTestSubscriber(IconTest);
        /* dev:end */
    }
}
