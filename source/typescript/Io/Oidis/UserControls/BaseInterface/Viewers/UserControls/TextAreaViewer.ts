/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { TextAreaTest } from "../../../RuntimeTests/UserControls/TextAreaTest.js";
import { TextAreaType } from "../../Enums/UserControls/TextAreaType.js";
import { TextArea } from "../../UserControls/TextArea.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class TextAreaViewer extends BaseViewer {

    constructor($args? : BaseViewerArgs) {
        super($args);
        this.setInstance(new TextArea(TextAreaType.GENERAL));
        /* dev:start */
        this.setTestSubscriber(TextAreaTest);
        /* dev:end */
    }
}
