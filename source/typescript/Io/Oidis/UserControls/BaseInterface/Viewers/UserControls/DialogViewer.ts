/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { DialogTest } from "../../../RuntimeTests/UserControls/DialogTest.js";
import { DialogType } from "../../Enums/UserControls/DialogType.js";
import { Dialog } from "../../UserControls/Dialog.js";
import { DialogViewerArgs } from "../../ViewersArgs/UserControls/DialogViewerArgs.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class DialogViewer extends BaseViewer {

    /* dev:start */
    protected static getTestViewerArgs() : DialogViewerArgs {
        const args : DialogViewerArgs = new DialogViewerArgs();
        args.Visible(true);
        return args;
    }

    /* dev:end */

    constructor($args? : BaseViewerArgs) {
        super($args);
        this.setInstance(new Dialog(DialogType.GENERAL));
        /* dev:start */
        this.setTestSubscriber(DialogTest);
        /* dev:end */
    }

    public getInstance() : Dialog {
        return <Dialog>super.getInstance();
    }

    public ViewerArgs($args? : DialogViewerArgs) : DialogViewerArgs {
        return <DialogViewerArgs>super.ViewerArgs($args);
    }

    protected argsHandler($instance : Dialog, $args : DialogViewerArgs) : void {
        if (!ObjectValidator.IsEmptyOrNull($args.PanelArgs())) {
            $instance.Value($args.PanelArgs());
        }
    }
}
