/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { AudioTest } from "../../../RuntimeTests/UserControls/AudioTest.js";
import { Audio } from "../../UserControls/Audio.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class AudioViewer extends BaseViewer {

    constructor($args? : BaseViewerArgs) {
        super($args);
        const instance : Audio = new Audio();
        this.setInstance(instance);
        /* dev:start */
        this.setTestSubscriber(AudioTest);
        /* dev:end */
    }
}
