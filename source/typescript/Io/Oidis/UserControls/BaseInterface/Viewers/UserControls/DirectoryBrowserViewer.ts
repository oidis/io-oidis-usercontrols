/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { DirectoryBrowserTest } from "../../../RuntimeTests/UserControls/DirectoryBrowserTest.js";
import { DirectoryBrowser } from "../../UserControls/DirectoryBrowser.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class DirectoryBrowserViewer extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new DirectoryBrowser());
        /* dev:start */
        this.setTestSubscriber(DirectoryBrowserTest);
        /* dev:end */
    }
}
