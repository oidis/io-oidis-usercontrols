/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { BasePanelHolder } from "../../../Primitives/BasePanelHolder.js";
import { BasePanelHolderViewer } from "../../../Primitives/BasePanelHolderViewer.js";
import { BasePanelHolderViewerArgs } from "../../../Primitives/BasePanelHolderViewerArgs.js";
import { HorizontalPanelHolderTest } from "../../../RuntimeTests/UserControls/HorizontalPanelHolderTest.js";
import { HorizontalPanelHolderStrategy } from "../../../Strategies/HorizontalPanelHolderStrategy.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class HorizontalPanelHolderViewer extends BasePanelHolderViewer {
    /* dev:start */
    protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
        const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        args.HeaderText("Header Text");
        args.DescriptionText("Desc. Text");
        args.IsOpened(true);
        args.PrioritySize(new PropagableNumber({number : 400, unitType: UnitType.PX}));
        args.Strategy(new HorizontalPanelHolderStrategy());

        return args;
    }

    /* dev:end */

    constructor($args? : BasePanelHolderViewerArgs) {
        super($args);
        /* dev:start */
        this.setTestSubscriber(HorizontalPanelHolderTest);
        /* dev:end */
    }

    public getInstance() : BasePanelHolder {
        return <BasePanelHolder>super.getInstance();
    }

    protected getInstanceClass() : any {
        return BasePanelHolder;
    }
}
