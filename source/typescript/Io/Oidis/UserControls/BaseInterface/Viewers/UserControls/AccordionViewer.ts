/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { BasePanelHolderViewerArgs } from "../../../Primitives/BasePanelHolderViewerArgs.js";
import { AccordionTest } from "../../../RuntimeTests/UserControls/AccordionTest.js";
import { UserControlsTestPanelViewer } from "../../../RuntimeTests/UserControlsTestPanelViewer.js";
import { AccordionType } from "../../Enums/UserControls/AccordionType.js";
import { Accordion } from "../../UserControls/Accordion.js";
import { AccordionViewerArgs } from "../../ViewersArgs/UserControls/AccordionViewerArgs.js";

/* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
export class AccordionViewer extends BasePanelViewer {

    /* dev:start */
    protected static getTestViewerArgs() : AccordionViewerArgs {
        const args : AccordionViewerArgs = new AccordionViewerArgs(AccordionType.VERTICAL);
        // args.ResizeType(AccordionResizeType.RESPONSIVE);

        let holderArgs : BasePanelHolderViewerArgs;
        holderArgs = new BasePanelHolderViewerArgs();

        holderArgs.HeaderText("Holder 1");
        holderArgs.DescriptionText("Desc.");
        holderArgs.PrioritySize(new PropagableNumber({number: 500, unitType: UnitType.PX}));
        holderArgs.IsOpened(false);
        args.AddPanelHoldersArgs(holderArgs);

        holderArgs = new BasePanelHolderViewerArgs();
        holderArgs.HeaderText("Holder 2");
        holderArgs.DescriptionText("Description txt");
        holderArgs.IsOpened(true);
        holderArgs.BodyViewerClass(UserControlsTestPanelViewer);
        args.AddPanelHoldersArgs(holderArgs);

        holderArgs = new BasePanelHolderViewerArgs();
        holderArgs.HeaderText("Holder 3");
        holderArgs.PrioritySize(new PropagableNumber({number: 100, unitType: UnitType.PX}));
        holderArgs.IsOpened(true);

        args.AddPanelHoldersArgs(holderArgs);

        return args;
    }

    /* dev:end */

    constructor($args? : AccordionViewerArgs) {
        super($args);
        if (!ObjectValidator.IsEmptyOrNull($args)) {
            this.setInstance(new Accordion($args.getPanelHoldersArgsList(), $args.getGuiType()));
        } else {
            this.setInstance(new Accordion(new AccordionViewerArgs(AccordionType.VERTICAL).getPanelHoldersArgsList()));
        }
        /* dev:start */
        this.setTestSubscriber(AccordionTest);
        /* dev:end */
    }

    public getInstance() : Accordion {
        return <Accordion>super.getInstance();
    }

    /**
     * @param {AccordionViewerArgs} [$args] Set user control viewer arguments.
     * @returns {AccordionViewerArgs} Returns user control viewer arguments.
     */
    public ViewerArgs($args? : AccordionViewerArgs) : AccordionViewerArgs {
        return <AccordionViewerArgs>super.ViewerArgs(<AccordionViewerArgs>$args);
    }

    protected normalImplementation() : void {
        const instance : Accordion = this.getInstance();

        if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())) {
            const args : AccordionViewerArgs = this.ViewerArgs();
            args.AsyncEnabled(true);
            instance.setPanelHoldersArgs(args.getPanelHoldersArgsList());
            instance.ResizeType(args.ResizeType());
        }
    }
}
