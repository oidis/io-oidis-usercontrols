/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class IconType extends BaseEnum {
    public static readonly GENERAL : string = "";
    public static readonly BLACK_SQUARE : string = "BlackSquare";
    public static readonly RED_SQUARE : string = "RedSquare";
    public static readonly BLUE_SQUARE : string = "BlueSquare";
    public static readonly SPINNER_BIG : string = "SpinnerBig";
    public static readonly SPINNER_SMALL : string = "SpinnerSmall";
}
