/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { IPanelHolderStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Strategies/IPanelHolderStrategy.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BasePanelHolder } from "../Primitives/BasePanelHolder.js";

/**
 * HorizontalPanelHolderStrategy class contains resize logic of horizontal panel holder.
 */
export class HorizontalPanelHolderStrategy extends BaseObject implements IPanelHolderStrategy {

    public ResizeHolder($element : BasePanelHolder, size? : number, $force? : boolean) : void {
        let newWidth : number;
        let newHeight : number;

        newHeight = ObjectValidator.IsEmptyOrNull($element.Parent()) ? $element.Height() :
            $element.Parent().Height() - ElementManager.getHeightOffset($element.Parent().Id()) -
            (ElementManager.IsVisible($element.Parent().Id() + "_Horizontal_ScrollBar") ?
                ElementManager.getCssIntegerValue($element.Parent().Id() + "_Horizontal_ScrollBar_ContentOffset", "height") : 0);
        if (ObjectValidator.IsSet(size)) {
            newWidth = size;
        } else if (!ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
            const parentWidth : number = ObjectValidator.IsEmptyOrNull($element.Parent()) ? 0 : $element.Parent().Width();
            newWidth = $element.PrioritySize().Normalize(parentWidth, UnitType.PX);
            if (ObjectValidator.IsEmptyOrNull($element.ScaledOpenedSize())) {
                $element.OpenedSize(newWidth);
                $element.ScaledOpenedSize(newWidth);
            }
        } else {
            return;
        }

        const headerSize : number = this.getHeaderSize($element);

        let widthChanged : boolean = false;
        let heightChanged : boolean = false;
        const innerPanel : BasePanel = $element.getBody();

        const innerWrapperWidth : number = Math.max(0, newWidth);
        const innerWidth : number = Math.max(Math.max(0, $element.ScaledOpenedSize()), innerWrapperWidth);

        if (innerPanel.Width() !== innerWidth) {
            innerPanel.Width(innerWidth - ElementManager.getWidthOffset($element.Id()));
            widthChanged = true;
        } else if ($element.Width() !== Math.max(headerSize, newWidth)) {
            widthChanged = true;
        } else if ($force) {
            widthChanged = true;
        }

        if ($element.Height() !== newHeight || innerPanel.Height() !== newHeight - ElementManager.getHeightOffset($element.Id()) -
            headerSize) {
            innerPanel.Height(newHeight - headerSize - ElementManager.getHeightOffset($element.Id()));
            heightChanged = true;
        } else if ($force) {
            heightChanged = true;
        }

        if (widthChanged || heightChanged) {
            const mult : number = (newWidth - headerSize) / ($element.ScaledOpenedSize() - headerSize);
            let width : number;
            let height : number;
            if (mult < 0.5) {
                width = headerSize;
                height = newHeight * (1 - (Math.max(0, mult) * 2));
            } else {
                width = newWidth * (Math.max(0, mult - 0.5) * 2);
                height = headerSize;
            }

            const headerLeftWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderLeft");
            const headerRightWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderRight");

            ElementManager.setWidth($element.Id() + "_HeaderCenter",
                Math.max(width, headerSize) - headerLeftWidth - headerRightWidth);

            height = Math.max(height, headerSize) - ElementManager.getHeightOffset($element.Id() + "_HeaderCenter");

            ElementManager.setHeight($element.Id() + "_HeaderLeft", height);
            ElementManager.setHeight($element.Id() + "_HeaderCenter", height);
            ElementManager.setHeight($element.Id() + "_HeaderRight", height);

            ElementManager.setCssProperty(innerPanel.Id(), "left", newWidth - $element.ScaledOpenedSize());
            ElementManager.setCssProperty(innerPanel.Id(), "top", headerSize);

            newWidth = Math.max(headerSize, newWidth);
            newHeight = Math.max(headerSize, newHeight);

            if (!ObjectValidator.IsEmptyOrNull($element.descriptionLabel) &&
                !ObjectValidator.IsEmptyOrNull($element.descriptionLabel.Text())) {
                $element.descriptionLabel.Visible(true);
                if (width > height) {
                    ElementManager.setClassName($element.Id() + "_Type", (<any>$element).getCssStatus(true));
                    const headerLabelWidth : number = ElementManager.getOffsetWidth($element.headerLabel.Id());
                    const maxWidth : number = width - ElementManager.getCssIntegerValue($element.headerLabel.Id(), "left") -
                        headerLeftWidth;
                    if (maxWidth < headerLabelWidth ||
                        maxWidth - headerLabelWidth < ElementManager.getOffsetWidth($element.descriptionLabel.Id()) +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-left") +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "right")) {
                        $element.descriptionLabel.Visible(false);
                    }
                } else {
                    ElementManager.setClassName($element.Id() + "_Type", (<any>$element).getCssStatus(false));
                    const headerLabelHeight : number = ElementManager.getOffsetHeight($element.headerLabel.Id());
                    const maxHeight : number = height - ElementManager.getCssIntegerValue($element.headerLabel.Id(), "top");
                    if (maxHeight < headerLabelHeight ||
                        maxHeight - headerLabelHeight < ElementManager.getOffsetHeight($element.descriptionLabel.Id()) +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-top") +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "bottom")) {
                        $element.descriptionLabel.Visible(false);
                    }
                }
            }

            if (widthChanged) {
                $element.Width(newWidth);
            }

            if (heightChanged) {
                $element.Height(newHeight);
            }
        }
    }

    public getParentSize($element : BasePanelHolder) : number {
        return ObjectValidator.IsEmptyOrNull($element.Parent()) ?
            0 : $element.Parent().Width();
    }

    public getContentBasedSize($element : BasePanelHolder) : number {
        const body : BasePanel = $element.getBody();
        let size : number = body.Width();
        if (ElementManager.Exists(body.Id() + "_PanelContent")) {
            ElementManager.ClearCssProperty(body.Id() + "_PanelContentEnvelop", "width");
            ElementManager.ClearCssProperty(body.Id() + "_PanelContent", "min-width");
            size = ElementManager.getEnvelopWidth(body.Id() + "_PanelContent") +
                ElementManager.getWidthOffset(body.Id()) +
                (ElementManager.IsVisible(body.Id() + "_Vertical_ScrollBar_ContentOffset") ?
                    ElementManager.getCssIntegerValue(body.Id() + "_Vertical_ScrollBar_ContentOffset", "width") : 0);
        }
        return size;
    }

    public getHeaderSize($element : BasePanelHolder) : number {
        return 38;
    }

    public getName() : string {
        return "Horizontal";
    }
}
