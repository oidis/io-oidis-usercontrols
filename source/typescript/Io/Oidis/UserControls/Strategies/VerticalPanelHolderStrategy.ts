/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { IPanelHolderStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Strategies/IPanelHolderStrategy.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BasePanelHolder } from "../Primitives/BasePanelHolder.js";

/**
 * VerticalPanelHolderStrategy class contains resize logic of horizontal panel holder.
 */
export class VerticalPanelHolderStrategy implements IPanelHolderStrategy {

    public ResizeHolder($element : BasePanelHolder, $size? : number, $force? : boolean) : void {
        let newWidth : number;
        let newHeight : number;

        newWidth = ObjectValidator.IsEmptyOrNull($element.Parent()) ? $element.Width() :
            ElementManager.getCssIntegerValue($element.Parent().Id() + "_PanelContentEnvelop", "width");
        if (ObjectValidator.IsSet($size)) {
            newHeight = $size;
        } else if (!ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
            const parentHeight : number = ObjectValidator.IsEmptyOrNull($element.Parent()) ? 0 : $element.Parent().Height();
            newHeight = $element.PrioritySize().Normalize(parentHeight, UnitType.PX);
            if (ObjectValidator.IsEmptyOrNull($element.ScaledOpenedSize())) {
                $element.OpenedSize(newHeight);
                $element.ScaledOpenedSize(newHeight);
            }
        } else {
            return;
        }

        const headerSize : number = this.getHeaderSize($element);

        let widthChanged : boolean = false;
        let heightChanged : boolean = false;
        const innerPanel : BasePanel = $element.getBody();

        const innerWrapperHeight : number = Math.max(0, newHeight - headerSize);
        const innerHeight : number = Math.max(Math.max(0, $element.ScaledOpenedSize() - headerSize), innerWrapperHeight);

        if (innerPanel.Height() !== innerHeight) {
            innerPanel.Height(innerHeight - ElementManager.getHeightOffset($element.Id()));
            heightChanged = true;
        } else if ($element.Height() !== newHeight) {
            heightChanged = true;
        } else if ($force) {
            heightChanged = true;
        }

        if (innerPanel.Width() !== newWidth - ElementManager.getWidthOffset($element.Id())) {
            innerPanel.Width(newWidth - ElementManager.getWidthOffset($element.Id()));
            widthChanged = true;
        } else if ($force) {
            widthChanged = true;
        }

        if (widthChanged || heightChanged) {
            newWidth = Math.max(headerSize, newWidth);
            newHeight = Math.max(headerSize, newHeight);

            if (widthChanged) {
                $element.Width(newWidth);
                const headerLeftWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderLeft");
                const headerRightWidth : number = ElementManager.getOffsetWidth($element.Id() + "_HeaderRight");

                const centerWidth : number = newWidth - headerLeftWidth - headerRightWidth;
                ElementManager.setWidth($element.Id() + "_HeaderCenter", centerWidth);
                ElementManager.setCssProperty($element.Id() + "_Header", "min-width", newWidth + "px");

                if (!ObjectValidator.IsEmptyOrNull($element.descriptionLabel) &&
                    !ObjectValidator.IsEmptyOrNull($element.descriptionLabel.Text())) {
                    $element.descriptionLabel.Visible(true);
                    const headerLabelWidth : number = ElementManager.getOffsetWidth($element.headerLabel.Id());
                    const maxWidth : number = newWidth - ElementManager.getElement($element.headerLabel.Id()).offsetLeft -
                        headerLeftWidth;
                    if (maxWidth < headerLabelWidth ||
                        maxWidth - headerLabelWidth < ElementManager.getOffsetWidth($element.descriptionLabel.Id()) +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "padding-left") +
                        ElementManager.getCssIntegerValue($element.descriptionLabel.Id(), "right")) {
                        $element.descriptionLabel.Visible(false);
                    }
                }
            }

            if (heightChanged) {
                $element.Height(newHeight);
                ElementManager.setCssProperty(innerPanel.Id(), "top", newHeight - $element.ScaledOpenedSize());
            }
        }
    }

    public getContentBasedSize($element : BasePanelHolder) : number {
        const body : BasePanel = $element.getBody();
        let size : number = body.Height();
        if (ElementManager.Exists(body.Id() + "_PanelContent")) {
            ElementManager.ClearCssProperty(body.Id() + "_PanelContentEnvelop", "height");
            size = ElementManager.getEnvelopHeight(body.Id() + "_PanelContent", true) +
                ElementManager.getHeightOffset(body.Id()) +
                (ElementManager.IsVisible(body.Id() + "_Horizontal_ScrollBar_ContentOffset") ?
                    ElementManager.getCssIntegerValue(body.Id() + "_Horizontal_ScrollBar_ContentOffset", "height") : 0);
        }
        size += $element.getHeaderSize();
        return size;
    }

    public getParentSize($element : BasePanelHolder) : number {
        return ObjectValidator.IsEmptyOrNull($element.Parent()) ?
            0 : $element.Parent().Height();
    }

    public getHeaderSize($element : BasePanelHolder) : number {
        return 38;
    }

    public getName() : string {
        return "Vertical";
    }
}
