/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IFormsObject } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IFormsObject.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { FormsObject as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/FormsObject.js";
import { DragBar } from "../BaseInterface/Components/DragBar.js";
import { ResizeBar } from "../BaseInterface/Components/ResizeBar.js";
import { ScrollBar } from "../BaseInterface/Components/ScrollBar.js";
import { ToolTip } from "../BaseInterface/Components/ToolTip.js";

/**
 * FormsObject should be used as abstract class for extending to the GUI objects type of input
 * and it is providing base methods connected with form elements.
 */
export abstract class FormsObject extends Parent implements IFormsObject {

    /**
     * @param {FormsObject} $element Specify element, which should be handled.
     * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
     * @param {Reflection} [$reflection] Specify instance of Reflection.
     * @returns {void}
     */
    public static TurnOn($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
        if (!$manager.IsActive(<IClassName>ScrollBar) &&
            !$manager.IsActive(<IClassName>ResizeBar) &&
            !$manager.IsActive(<IClassName>DragBar)) {
            Parent.TurnOn($element, $manager, $reflection);
        }
    }

    /**
     * @returns {void}
     */
    public static Blur() : void {
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        const elements : ArrayList<IGuiCommons> = manager.getActive();
        const reflection : Reflection = Reflection.getInstance();
        elements.foreach(($element : FormsObject) : void => {
            if (ObjectValidator.IsEmptyOrNull($element.Parent()) ||
                (!ObjectValidator.IsEmptyOrNull($element.Parent()) &&
                    !reflection.IsMemberOf($element, ScrollBar) &&
                    !reflection.IsMemberOf($element, ResizeBar) &&
                    !reflection.IsMemberOf($element, DragBar))) {
                manager.setActive($element, false);

                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                $element.getEventsManager().FireEvent($element.getClassName(), EventType.ON_BLUR, eventArgs);
                $element.getEventsManager().FireEvent($element, EventType.ON_BLUR, eventArgs);
                const elementClass : any = reflection.getClass($element.getClassName());
                if (ObjectValidator.IsSet(elementClass.TurnOff)) {
                    elementClass.TurnOff($element, manager, reflection);
                }

                if (reflection.IsMemberOf($element, FormsObject)) {
                    $element.Error($element.Error());
                }
            }
        });
    }

    /**
     * @returns {IToolTip} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IToolTip
     */
    protected getTitleClass() : any {
        return ToolTip;
    }

    /**
     * @returns {INotification} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.INotification
     */
    protected getNotificationClass() : any {
        return Notification;
    }
}
