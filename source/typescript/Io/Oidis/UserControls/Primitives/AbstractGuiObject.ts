/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IAbstractGuiObject } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IAbstractGuiObject.js";
import { AbstractGuiObject as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/AbstractGuiObject.js";
import { Notification } from "../BaseInterface/Components/Notification.js";
import { ToolTip } from "../BaseInterface/Components/ToolTip.js";

/**
 * AbstractGuiObject class provides GUI object, which can be used as temporary element needed
 */
export class AbstractGuiObject extends Parent implements IAbstractGuiObject {

    /**
     * @returns {ToolTip} Returns tooltip component associated with current GUI element.
     */
    public Title() : ToolTip {
        return <ToolTip>super.Title();
    }

    /**
     * @returns {Notification} Returns notification component associated with current GUI element.
     */
    public Notification() : Notification {
        return <Notification>super.Notification();
    }

    /**
     * @returns {IToolTip} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IToolTip
     */
    protected getTitleClass() : any {
        return ToolTip;
    }

    /**
     * @returns {INotification} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.INotification
     */
    protected getNotificationClass() : any {
        return Notification;
    }
}
