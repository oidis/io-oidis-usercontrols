/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { BasePanelHolderEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/BasePanelHolderEventType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { UnitType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/UnitType.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IBasePanelHolderEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IBasePanelHolderEvents.js";
import { IBasePanelHolder } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IBasePanelHolder.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IPanelHolderStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Strategies/IPanelHolderStrategy.js";
import { IImageButton } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IImageButton.js";
import { ILabel } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ILabel.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { ImageButton } from "../BaseInterface/UserControls/ImageButton.js";
import { Label } from "../BaseInterface/UserControls/Label.js";
import { BasePanel } from "./BasePanel.js";

/**
 * BasePanelHolder should be used as abstract class for extending to the GUI panels holder
 * suitable for external handling eg. by Accordion user control.
 */
export abstract class BasePanelHolder extends BasePanel implements IBasePanelHolder {
    public headerLabel : ILabel;
    public descriptionLabel : ILabel;
    public arrowButton : IImageButton;

    private guiType : any;
    private isOpened : boolean;
    private body : BasePanel;
    private headerEvents : ElementEventsManager;
    private prioritySize : PropagableNumber;
    private openedSize : number;
    private scaledOpenedSize : number;
    private sizeMult : number;
    private isResizeDelegated : boolean;
    private queue : any;
    private strategy : IPanelHolderStrategy;

    protected static open($element : BasePanelHolder) : void {
        if (!$element.getGuiManager().IsActive($element)) {
            $element.getBody().Visible(true);
            const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
            if (ObjectValidator.IsEmptyOrNull($element.PrioritySize())) {
                $element.OpenedSize($element.strategy.getContentBasedSize($element));
            } else {
                $element.OpenedSize($element.PrioritySize().Normalize($element.strategy.getParentSize($element), UnitType.PX));
            }
            $element.ScaledOpenedSize($element.OpenedSize());

            $element.getGuiManager().setActive($element, true);
            $element.getEventsManager().FireEvent($element, BasePanelHolderEventType.BEFORE_OPEN, false);
            $element.getEvents().FireAsynchronousMethod(() : void => {
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
                manipulatorArgs.Owner($element);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.RangeEnd(100);
                manipulatorArgs.CurrentValue(manipulatorArgs.RangeStart());
                manipulatorArgs.DirectionType(DirectionType.UP);
                manipulatorArgs.ChangeEventType($element.Id() + "_ShowBody_" + EventType.ON_CHANGE);
                manipulatorArgs.CompleteEventType($element.Id() + "_ShowBody_" + EventType.ON_COMPLETE);

                ElementManager.StopOpacityChange($element.Id());
                ElementManager.setOpacity($element.Id(), 75);
                ElementManager.setCssProperty($element.getBodyId(), "position", "relative");

                thisClass.setUpOpenAnimation(manipulatorArgs);

                $element.getEventsManager().setEvent($element, $element.Id() + "_ShowBody_" + EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        $element.sizeMult = $eventArgs.CurrentValue();
                        thisClass.onChangeBodyHandler($eventArgs.Owner(), $eventArgs);
                    });
                $element.getEventsManager().setEvent($element, $element.Id() + "_ShowBody_" + EventType.ON_COMPLETE,
                    ($eventArgs : ValueProgressEventArgs, $manager : GuiObjectManager) : void => {
                        const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
                        thisClass.onShowCompleteBodyHandler(element);
                        (<any>element).isOpened = true;

                        $manager.setActive(element, false);
                        $element.getEventsManager().FireEvent(element, BasePanelHolderEventType.ON_OPEN);

                        if (!$element.IsResizeDelegated() && !ObjectValidator.IsEmptyOrNull($element.queue)) {
                            $element.queue.handler();
                        }
                    });
                ValueProgressManager.Execute(manipulatorArgs);
            }, false, 10);
        }
    }

    protected static hide($element : BasePanelHolder) : void {
        if (!$element.getGuiManager().IsActive($element)) {
            $element.getGuiManager().setActive($element, true);
            (<any>$element).isOpened = false;
            $element.getEventsManager().FireEvent($element, BasePanelHolderEventType.BEFORE_CLOSE, false);
            $element.getEvents().FireAsynchronousMethod(() : void => {
                const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get($element.Id());
                manipulatorArgs.Owner($element);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.RangeEnd(100);
                manipulatorArgs.CurrentValue(manipulatorArgs.RangeEnd());
                manipulatorArgs.DirectionType(DirectionType.DOWN);
                manipulatorArgs.ChangeEventType($element.Id() + "_HideBody_" + EventType.ON_CHANGE);
                manipulatorArgs.CompleteEventType($element.Id() + "_HideBody_" + EventType.ON_COMPLETE);

                ElementManager.StopOpacityChange($element.Id());
                ElementManager.setOpacity($element.Id(), 75);
                ElementManager.setClassName($element.Id() + "_Type", $element.getCssStatus(false));
                ElementManager.setCssProperty($element.getBodyId(), "position", "relative");

                thisClass.setUpCloseAnimation(manipulatorArgs);

                $element.getEventsManager().setEvent($element, $element.Id() + "_HideBody_" + EventType.ON_CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        $element.sizeMult = $eventArgs.CurrentValue();
                        thisClass.onChangeBodyHandler($eventArgs.Owner(), $eventArgs);
                    });

                $element.getEventsManager().setEvent($element, $element.Id() + "_HideBody_" + EventType.ON_COMPLETE,
                    ($eventArgs : ValueProgressEventArgs, $manager : GuiObjectManager) : void => {

                        const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
                        thisClass.onHideCompleteBodyHandler(element);
                        element.getBody().Visible(false);
                        $manager.setActive($element, false);

                        $element.getEventsManager().FireEvent(element, BasePanelHolderEventType.ON_CLOSE, false);

                        if (!$element.IsResizeDelegated() && !ObjectValidator.IsEmptyOrNull($element.queue)) {
                            $element.queue.handler();
                        }
                    }
                );
                ValueProgressManager.Execute(manipulatorArgs);
            }, false, 10);
        }
    }

    protected static setUpOpenAnimation($args : ValueProgressEventArgs) : void {
        $args.ProgressType(ProgressType.SLOW_TAILS);
        $args.Step(5);
    }

    protected static setUpCloseAnimation($args : ValueProgressEventArgs) : void {
        $args.ProgressType(ProgressType.SLOW_TAILS);
        $args.Step(5);
    }

    protected static onChangeBodyHandler($element : BasePanelHolder) : void {
        if (!$element.IsResizeDelegated()) {
            $element.strategy.ResizeHolder($element, $element.getTotalSize());
        } else {
            $element.getEventsManager().FireEvent($element, EventType.ON_CHANGE, false);
        }
    }

    protected static onShowCompleteBodyHandler($element : BasePanelHolder) : void {
        ElementManager.setClassName($element.Id() + "_Type", $element.getCssStatus(true));
        ElementManager.ChangeOpacity($element, DirectionType.UP, 10);
    }

    protected static onHideCompleteBodyHandler($element : BasePanelHolder) : void {
        ElementManager.ChangeOpacity($element, DirectionType.UP, 10);
    }

    protected static toggleOpenHide($element : BasePanelHolder) : void {
        $element.IsOpened(!$element.IsOpened());
    }

    protected static onMouseOverEventHandler($element : BasePanelHolder, $manager : GuiObjectManager,
                                             $reflection : Reflection) : void {
        if (!ObjectValidator.IsEmptyOrNull($element.getImageButtonClass())) {
            const arrowClass : any = $reflection.getClass($element.arrowButton.getClassName());
            arrowClass.TurnOn($element.arrowButton, $manager, $reflection);
        }
        if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass())) {
            $element.headerLabel.Title().Enabled(false);
        }
    }

    protected static onMouseOutEventHandler($element : BasePanelHolder, $manager : GuiObjectManager,
                                            $reflection : Reflection) : void {
        if (!ObjectValidator.IsEmptyOrNull($element.getImageButtonClass())) {
            const arrowClass : any = $reflection.getClass($element.arrowButton.getClassName());
            arrowClass.TurnOff($element.arrowButton, $manager, $reflection);
        }
        if (!ObjectValidator.IsEmptyOrNull($element.getLabelClass()) && $element.IsOpened()) {
            $element.headerLabel.Title().Enabled(true);
        }
    }

    /**
     * @param {BasePanelViewer} $bodyClass Specify class name of panel viewer, which should be handled.
     * @param {BasePanelViewerArgs} [$args] Specify arguments for handled panel object viewer.
     * @param {any} [$holderType] Specify type of element look and feel.
     * @param {string} [$id] Force set element id instead of generated one.
     */
    protected constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
        super($id);
        this.guiType = this.guiTypeValueSetter($holderType);
        this.isOpened = true;
        this.prioritySize = null;
        this.sizeMult = 0;
        this.isResizeDelegated = false;
        this.queue = null;

        const labelClass : any = this.getLabelClass();
        if (!ObjectValidator.IsEmptyOrNull(labelClass)) {
            this.headerLabel = new labelClass();
            this.descriptionLabel = new labelClass();
        }

        const imageButtonClass : any = this.getImageButtonClass();
        if (!ObjectValidator.IsEmptyOrNull(imageButtonClass)) {
            this.arrowButton = new imageButtonClass();
        }

        if (!ObjectValidator.IsEmptyOrNull($args)) {
            $args.AsyncEnabled(false);
        }

        if (ObjectValidator.IsEmptyOrNull($bodyClass)) {
            this.body = new BasePanel();
            this.body.StyleClassName("Empty");
        } else {
            this.addChildPanel($bodyClass, $args, ($parent : BasePanelHolder, $child : BasePanel) : void => {
                $parent.body = $child;
            });
        }
    }

    /**
     * @returns {IBasePanelHolderEvents} Returns events connected with the element.
     */
    public getEvents() : IBasePanelHolderEvents {
        return <IBasePanelHolderEvents>super.getEvents();
    }

    public Strategy($value? : IPanelHolderStrategy) : IPanelHolderStrategy {
        if (ObjectValidator.IsSet($value)) {
            this.strategy = $value;
            if (this.IsLoaded()) {
                ElementManager.ClearCssProperty(this.Id() + "_HeaderLeft", "width");
                ElementManager.ClearCssProperty(this.Id() + "_HeaderLeft", "height");
                ElementManager.ClearCssProperty(this.Id() + "_HeaderCenter", "width");
                ElementManager.ClearCssProperty(this.Id() + "_HeaderCenter", "height");
                ElementManager.ClearCssProperty(this.Id() + "_HeaderRight", "width");
                ElementManager.ClearCssProperty(this.Id() + "_HeaderRight", "height");
                const bodyId : string = this.getBody().Id();
                ElementManager.ClearCssProperty(bodyId, "left");
                ElementManager.ClearCssProperty(bodyId, "right");
                ElementManager.getElement(this.Id() + "_Type").setAttribute("guiType", this.strategy.getName());
            }
        }
        return this.strategy;
    }

    /**
     * @param {boolean} [$value] Specify if holder should be in opened state.
     * @returns {boolean} Returns true, if holder is in opened mode, otherwise false.
     */
    public IsOpened($value? : boolean) : boolean {
        if (this.IsLoaded() && $value !== this.isOpened) {
            if (ObjectValidator.IsBoolean($value) && this.IsCompleted()) {
                const manager : GuiObjectManager = this.getGuiManager();
                if (!manager.IsActive(this) && !manager.IsActive(this.Parent())) {
                    const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                    if ($value) {
                        thisClass.open(this);
                    } else {
                        thisClass.hide(this);
                    }
                } else {
                    this.queue = {
                        handler: () : void => {
                            this.queue = null;
                            this.IsOpened($value);
                        },
                        time   : new Date().getTime()
                    };
                }
            }
        } else {
            this.isOpened = Property.Boolean(this.isOpened, $value);
        }
        return this.isOpened;
    }

    /**
     * @returns {number} Returns current header size.
     */
    public getHeaderSize() : number {
        return this.Strategy().getHeaderSize(this);
    }

    /**
     * @returns {number} Returns current holder primary axis body size.
     */
    public getBodySize() : number {
        return Math.floor((this.ScaledOpenedSize() - this.getHeaderSize()) / 100 * this.sizeMult);
    }

    /**
     * @returns {number} Returns current holder primary axis size.
     */
    public getTotalSize() : number {
        return this.getBodySize() + this.getHeaderSize();
    }

    /**
     * @param {number} [$value] Specify default, non-scaled open holder primary axis size.
     * @returns {number} Returns default, non-scaled open holder primary axis size.
     */
    public OpenedSize($value? : number) : number {
        return this.openedSize = Property.PositiveInteger(this.openedSize, $value);
    }

    /**
     * @param {number} [$value] Specify scaled open holder primary axis size.
     * @returns {number} Returns scaled open holder primary axis size.
     */
    public ScaledOpenedSize($value? : number) : number {
        return this.scaledOpenedSize = Property.PositiveInteger(this.scaledOpenedSize, $value);
    }

    /**
     * @param {PropagableNumber} [$value] Specify prioritized size.
     * @returns {PropagableNumber} Returns PropagableNumber instance specifying prioritized size.
     */
    public PrioritySize($value? : PropagableNumber) : PropagableNumber {
        if (ObjectValidator.IsSet($value)) {
            this.prioritySize = $value;
            if (this.IsLoaded() && this.IsOpened()) {
                this.strategy.ResizeHolder(this, this.getTotalSize());
            }
        }
        return this.prioritySize;
    }

    public IsScalable() : boolean {
        return ObjectValidator.IsEmptyOrNull(this.PrioritySize()) ? true :
            this.PrioritySize().Normalize(9999, UnitType.PX) !==
            this.PrioritySize().Normalize(0, UnitType.PX);
    }

    /**
     * @returns {any} Returns queue of holder next animation steps
     */
    public getQueue() : any {
        return this.queue;
    }

    /**
     * @returns {BasePanel} Returns BasePanel instance held by the holder element, if instance has been set, otherwise null.
     */
    public getBody() : BasePanel {
        return this.body;
    }

    /**
     * @returns {ElementEventsManager} Returns events connected with holder header.
     */
    public getHeaderEvents() : ElementEventsManager {
        if (!ObjectValidator.IsSet(this.headerEvents)) {
            this.headerEvents = new ElementEventsManager(this, this.Id() + "_Header");
        }
        return this.headerEvents;
    }

    /**
     * @param {boolean} [$value] Specify whether or not resize during animation is delegated to subscriber of onChange event
     * - used for animation grouping.
     * @returns {boolean} Returns whether or not resize during animation is delegated to subscriber of onChange event
     * - used for animation grouping..
     */
    public IsResizeDelegated($value? : boolean) : boolean {
        return this.isResizeDelegated = Property.Boolean(this.isResizeDelegated, $value);
    }

    protected getBodyId() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.getBody())) {
            return this.getBody().Id();
        } else {
            return this.Id() + "_EmptyContent";
        }
    }

    protected innerCode() : IGuiElement {
        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
        this.body.Visible(true);

        if (!ObjectValidator.IsEmptyOrNull(this.getLabelClass())) {
            this.headerLabel.Visible(true);
            this.descriptionLabel.Visible(true);
        }

        if (!ObjectValidator.IsEmptyOrNull(this.getImageButtonClass())) {
            this.arrowButton.Visible(true);
            this.arrowButton.getEvents().setOnClick(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    $eventArgs.StopAllPropagation();
                    thisClass.toggleOpenHide(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager);
                });

            this.arrowButton.getEvents().setOnMouseOver(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    thisClass.onMouseOverEventHandler(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager, $reflection);
                });

            this.arrowButton.getEvents().setOnMouseOut(
                ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    thisClass.onMouseOutEventHandler(<BasePanelHolder>$eventArgs.Owner().Parent(), $manager, $reflection);
                });
        }

        this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
            const element : BasePanelHolder = <BasePanelHolder>$eventArgs.Owner();
            element.getHeaderEvents().Subscribe();
            if (!ObjectValidator.IsEmptyOrNull(this.getLabelClass())) {
                element.getHeaderEvents().Subscribe(element.headerLabel.Id());
                element.getHeaderEvents().Subscribe(element.descriptionLabel.Id());
            }
        });

        this.getHeaderEvents().setOnClick(
            ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                thisClass.toggleOpenHide(<BasePanelHolder>$eventArgs.Owner(), $manager);
            });

        this.getHeaderEvents().setOnMouseOver(
            ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                thisClass.onMouseOverEventHandler(<BasePanelHolder>$eventArgs.Owner(), $manager, $reflection);
            });

        this.getHeaderEvents().setOnMouseOut(
            ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                thisClass.onMouseOutEventHandler(<BasePanelHolder>$eventArgs.Owner(), $manager, $reflection);
            });

        const resizeHandler : any = ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
            const element : BasePanel = <BasePanel>$eventArgs.Owner();
            const holder : BasePanelHolder =
                <BasePanelHolder>($reflection.IsMemberOf(element, BasePanelHolder) ? element : element.Parent());
            if (holder.IsCompleted() && !$manager.IsActive(holder)) {
                if ($eventArgs.Type() === EventType.ON_COMPLETE) {
                    holder.strategy.ResizeHolder(holder, holder.getTotalSize(), true);
                    if (holder.IsOpened()) {
                        if (!holder.IsResizeDelegated()) {
                            thisClass.open(holder);
                        }
                    } else {
                        holder.getBody().Visible(false);
                    }
                    ElementManager.setOpacity(holder.Id(), 100);
                } else {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if (holder.IsOpened() && ObjectValidator.IsEmptyOrNull(holder.PrioritySize()) &&
                            holder.getBodySize() !== 0 && $eventArgs.Owner() === holder.getBody()) {
                            const targetSize = holder.strategy.getContentBasedSize(holder);
                            holder.ScaledOpenedSize(targetSize);
                            holder.strategy.ResizeHolder(holder, targetSize);
                        } else if (!holder.IsResizeDelegated()) {
                            const targetSize : number = holder.IsOpened() && !ObjectValidator.IsEmptyOrNull(holder.ScaledOpenedSize()) ?
                                holder.ScaledOpenedSize() : holder.getTotalSize();
                            holder.strategy.ResizeHolder(holder, targetSize);
                        }
                    });
                }
            }
        };

        this.getBody().getEvents().setOnResize(($eventArgs : EventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void => {
            if (!$manager.IsActive(this)) {
                resizeHandler($eventArgs, $manager, $reflection);
            }
        });
        this.getEvents().setOnResize(resizeHandler);
        this.getEvents().setOnComplete(resizeHandler);

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type")
            .StyleClassName(this.getCssStatus(this.IsOpened()))
            .GuiTypeTag(this.strategy.getName())
            .Add(this.addElement(this.Id() + "_Header")
                .StyleClassName("Header")
                .Add(this.addElement(this.Id() + "_HeaderLeft")
                    .StyleClassName(GeneralCssNames.LEFT)
                    .Add(this.arrowButton)
                )
                .Add(this.addElement(this.Id() + "_HeaderCenter")
                    .StyleClassName(GeneralCssNames.CENTER)
                    .Add(this.headerLabel)
                )
                .Add(this.addElement(this.Id() + "_HeaderRight")
                    .StyleClassName(GeneralCssNames.RIGHT)
                    .Add(this.descriptionLabel)
                )
            )
            .Add(this.getBody());
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getLabelClass() : any {
        return Label;
    }

    /**
     * @returns {IImageButton} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IImageButton
     */
    protected getImageButtonClass() : any {
        return ImageButton;
    }

    protected guiTypeValueSetter($value : any) : any {
        return "";
    }

    protected getCssStatus($opened : boolean) : string {
        if ($opened) {
            return "Opened";
        } else {
            return "Closed";
        }
    }

    protected cssInterfaceName() : string {
        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
        if (this.getClassNameWithoutNamespace() !== thisClass.ClassNameWithoutNamespace()) {
            return thisClass.ClassNameWithoutNamespace() + " " + this.getClassNameWithoutNamespace();
        }
        return thisClass.NamespaceName();
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "headerEvents"
        );
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = super.excludeCacheData();
        exclude.push(
            "headerEvents"
        );
        return exclude;
    }
}
