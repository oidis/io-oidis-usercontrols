/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IllegalArgumentException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/IllegalArgumentException.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { ISelectBox } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/ISelectBox.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";

/**
 * SelectBoxOption should be used as container for SelectBox option content.
 */
export class SelectBoxOption extends BaseObject {
    private index : number;
    private id : string;
    private owner : ISelectBox;
    private text : string;
    private value : string;
    private styleClassName : string;
    private selectedFlag : boolean;
    private showSeparator : boolean;
    private readonly events : ElementEventsManager;
    private isCompleted : boolean;

    /**
     * Change option status to OFF
     * @param {SelectBoxOption} $element Specify element, which should be handled
     * @returns {void}
     */
    public static TurnOff($element : SelectBoxOption) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            ElementManager.TurnOff($element.Owner() + "_Option_" + $element.Index() + "_Status");
        }
    }

    /**
     * Change option status to ON
     * @param {SelectBoxOption} $element Specify element, which should be handled
     * @returns {void}
     */
    public static TurnOn($element : SelectBoxOption) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            ElementManager.TurnOn($element.Owner() + "_Option_" + $element.Index() + "_Status");
        }
    }

    /**
     * Change option status to ACTIVE
     * @param {SelectBoxOption} $element Specify element, which should be handled
     * @returns {void}
     */
    public static TurnActive($element : SelectBoxOption) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            ElementManager.TurnOn($element.Owner() + "_Option_" + $element.Index() + "_Status");
        }
    }

    /**
     * Resize option for ability to display multiline option text
     * @param {SelectBoxOption} $element Specify element, which should be handled
     * @returns {void}
     */
    public static Resize($element : SelectBoxOption) : void {
        if (!ObjectValidator.IsEmptyOrNull($element)) {
            const id : string = $element.Owner();
            const index : string = $element.Index().toString();
            const textElement : HTMLElement = ElementManager.getElement(id + "_Option_" + index + "_Text", true);
            if (ElementManager.Exists(textElement)) {
                const itemHeight : number = textElement.offsetHeight + 2 * textElement.offsetTop;
                if (itemHeight > 0) {
                    ElementManager.setCssProperty(id + "_Option_" + index + "_Status", "height", itemHeight);
                    ElementManager.CleanElementCache(id + "_Option_" + index + "_Envelop");
                    ElementManager.setCssProperty(id + "_Option_" + index + "_Envelop", "height", itemHeight);
                }
            }
        }
    }

    /**
     * @param {string} $text Specify visible text value of the item.
     * @param {ISelectBox} $owner Set item's owner.
     * @param {number} $index Set index of the option in the SelectBox options array.
     */
    constructor($text : string, $owner : ISelectBox, $index : number) {
        super();
        this.Owner($owner.Id());
        this.Index($index);
        this.Text($text);
        this.Value(null);
        this.owner = $owner;
        this.styleClassName = "";
        this.selectedFlag = false;
        this.showSeparator = true;
        this.events = new ElementEventsManager($owner, this.Owner() + "_Option_" + this.Index() + "_Status");
        this.isCompleted = false;
    }

    /**
     * @returns {ElementEventsManager} Returns events manager subscribed to the item.
     */
    public getEvents() : ElementEventsManager {
        return this.events;
    }

    /**
     * @param {string} [$value] Set item's owner id.
     * @returns {string} Returns item's owner id.
     */
    public Owner($value? : string) : string {
        return this.id = Property.String(this.id, $value);
    }

    /**
     * @returns {string} Returns item's GUI id.
     */
    public Id() : string {
        return this.Owner() + "_Option_" + this.Index();
    }

    /**
     * @param {number} [$value] Set index of the item in the SelectBox options array.
     * @returns {number} Returns item index in the SelectBox options array.
     */
    public Index($value? : number) : number {
        return this.index = Property.Integer(this.index, $value);
    }

    /**
     * @param {string} [$value] Set text value of the item.
     * @returns {string} Returns text value of the item.
     */
    public Text($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            if (ObjectValidator.IsEmptyOrNull($value)) {
                ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException("Text attribute can not be null."));
            }
            this.text = Property.String(this.text, $value);
        }

        return this.text;
    }

    /**
     * @param {string|number} [$value] Set value connected with the visible text value.
     * @returns {string} Returns item's value connected with the visible text value.
     */
    public Value($value? : string | number) : string {
        if (ObjectValidator.IsSet($value)) {
            this.value = Property.String(this.value, ObjectValidator.IsEmptyOrNull($value) ? this.text : $value.toString());
        }

        return this.value;
    }

    /**
     * @param {string} [$value] Set type of css class name connected with the object instance.
     * @returns {string} Returns css class name connected with the object instance.
     */
    public StyleClassName($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            this.styleClassName = $value;
        }

        return this.styleClassName;
    }

    /**
     * @param {boolean} [$value] Specify selection flag.
     * @returns {boolean} Returns true, if element has been selected, otherwise false.
     */
    public IsSelected($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.selectedFlag = Property.Boolean(this.selectedFlag, $value);
        }

        return this.selectedFlag;
    }

    /**
     * @param {boolean} [$value] Specify, if rendered item should contains separator element.
     * @returns {boolean} Returns true, if element's separator is visible, otherwise false.
     */
    public SeparatorVisible($value? : boolean) : boolean {
        this.showSeparator = Property.Boolean(this.showSeparator, $value);
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.id)) {
            if (this.showSeparator) {
                ElementManager.Show(this.id + "_Option_" + this.index + "Separator");
            } else {
                ElementManager.Hide(this.id + "_Option_" + this.index + "Separator");
            }
        }
        return this.showSeparator;
    }

    /**
     * @returns {string} Returns HTML representing SelectBox item.
     */
    public getInnerHtml() : IGuiElement {
        const guiElementClass : any = this.owner.getGuiElementClass();
        const addElement : ($id? : string) => IGuiElement = ($id? : string) : IGuiElement => {
            return new guiElementClass().Id($id);
        };
        const itemIndex : string = this.Index().toString();
        const output : IGuiElement = addElement(this.id + "_Option_" + itemIndex).StyleClassName(this.styleClassName);
        let cssStyle : string = GeneralCssNames.OFF;
        if (this.selectedFlag) {
            cssStyle = GeneralCssNames.ACTIVE;
        }
        output
            .Add(addElement(this.id + "_Option_" + itemIndex + "_Envelop")
                .StyleClassName("Option")
                .Add(addElement(this.id + "_Option_" + itemIndex + "_Status")
                    .StyleClassName(cssStyle)
                    .Add(addElement(this.id + "_Option_" + itemIndex + "_Icon").StyleClassName(GeneralCssNames.ICON))
                    .Add(addElement(this.id + "_Option_" + itemIndex + "_Text").StyleClassName(GeneralCssNames.TEXT).Add(this.text))
                )
            )
            .Add(addElement(this.id + "_Option_" + itemIndex + "_Separator").StyleClassName("Separator").Visible(this.showSeparator));
        return output;
    }

    /**
     * @param {boolean} [$value] Specify element's complete flag.
     * @returns {boolean} Returns true, if element has been processed, otherwise false.
     */
    public IsCompleted($value? : boolean) : boolean {
        return this.isCompleted = Property.Boolean(this.isCompleted, $value);
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        return $prefix + "[" + this.getHash() + "] " + this.text + StringUtils.NewLine($htmlTag) +
            $prefix + StringUtils.Tab(1, $htmlTag) + "index: " + this.index + StringUtils.NewLine($htmlTag) +
            $prefix + StringUtils.Tab(1, $htmlTag) + "value: " + this.value + StringUtils.NewLine($htmlTag) +
            $prefix + StringUtils.Tab(1, $htmlTag) + "style: " + this.styleClassName + StringUtils.NewLine($htmlTag) +
            $prefix + StringUtils.Tab(1, $htmlTag) + "withSeparator: " + Convert.BooleanToString(this.showSeparator) +
            StringUtils.NewLine($htmlTag) +
            $prefix + StringUtils.Tab(1, $htmlTag) + "isSelected: " + Convert.BooleanToString(this.selectedFlag);
    }

    /**
     * @returns {number} Returns CRC calculated from data, which represents current object.
     */
    public getHash() : number {
        return StringUtils.getCrc(this.Id() + this.value + this.styleClassName);
    }
}
