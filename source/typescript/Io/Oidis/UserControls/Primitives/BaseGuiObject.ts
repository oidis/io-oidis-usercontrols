/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseGuiObject } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IBaseGuiObject.js";
import { BaseGuiObject as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseGuiObject.js";
import { ToolTip } from "../BaseInterface/Components/ToolTip.js";

/**
 * BaseGuiObject should be used as abstract class for extending to the GUI objects
 * and it is providing base GUI object methods.
 */
export abstract class BaseGuiObject extends Parent implements IBaseGuiObject {

    /**
     * @returns {IToolTip} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IToolTip
     */
    protected getTitleClass() : any {
        return ToolTip;
    }
}
