/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IBasePanelHolderViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IBasePanelHolderViewerArgs.js";
import { IPanelHolderStrategy } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Strategies/IPanelHolderStrategy.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { PropagableNumber } from "@io-oidis-gui/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { BasePanelHolderViewer } from "./BasePanelHolderViewer.js";

/**
 * BasePanelHolderViewerArgs is structure handling content of BasePanelHolder.
 */
export class BasePanelHolderViewerArgs extends BasePanelViewerArgs implements IBasePanelHolderViewerArgs {

    private holderViewerClass : any;
    private bodyViewerClass : any;
    private headerText : string;
    private descriptionText : string;
    private isOpened : boolean;
    private bodyArgs : BasePanelViewerArgs;
    private isExpandable : boolean;
    private prioritySize : PropagableNumber;
    private strategy : IPanelHolderStrategy;

    constructor() {
        super();

        this.isOpened = true;
        this.isExpandable = false;
        this.bodyArgs = new BasePanelViewerArgs();
        this.holderViewerClass = BasePanelHolderViewer;
        this.bodyViewerClass = null;
        this.prioritySize = null;
    }

    /**
     * @param {string} [$value] Set value for text of holder's header.
     * @returns {string} Returns value of holder's header text.
     */
    public HeaderText($value? : string) : string {
        return this.headerText = Property.String(this.headerText, $value);
    }

    /**
     * @param {string} [$value] Set value for text of holder's description text/tooltip.
     * @returns {string} Returns value of holder's description text/tooltip.
     */
    public DescriptionText($value? : string) : string {
        return this.descriptionText = Property.String(this.descriptionText, $value);
    }

    /**
     * @param {boolean} [$value] Set if panel holder should be in opened mode.
     * @returns {boolean} Returns true, if panel holder should be in opened mode, otherwise false.
     */
    public IsOpened($value? : boolean) : boolean {
        return this.isOpened = Property.Boolean(this.isOpened, $value);
    }

    /**
     * @param {PropagableNumber} [$value] Specify size of holder that should be normalized to parent and prioritized.
     * @returns {PropagableNumber} Return size of holder that should be normalized to parent and prioritized.
     */
    public PrioritySize($value? : PropagableNumber) : PropagableNumber {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.prioritySize = $value;
        }
        return this.prioritySize;
    }

    /**
     * @param {BasePanelViewerArgs} [$value] Set arguments for panel held by the panel holder.
     * @returns {BasePanelViewerArgs} Returns arguments for panel held by the panel holder.
     */
    public BodyArgs($value? : BasePanelViewerArgs) : BasePanelViewerArgs {
        if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsMemberOf(BasePanelViewerArgs)) {
            this.bodyArgs = $value;
        }
        return this.bodyArgs;
    }

    /**
     * @param {BasePanelHolderViewer} [$value] Specify viewer class, which should be used for creation of holder instance.
     * @returns {BasePanelHolderViewer} Returns class with type of BasePanelHolderViewer.
     */
    public HolderViewerClass($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.holderViewerClass = $value;
        }
        return this.holderViewerClass;
    }

    /**
     * @param {BasePanelViewer} [$value] Specify viewer class, which should be used for creation of held body instance.
     * @returns {BasePanelViewer} Returns class with type of BasePanelViewer.
     */
    public BodyViewerClass($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.bodyViewerClass = $value;
        }
        return this.bodyViewerClass;
    }

    /**
     * @param {BasePanelHolderViewer} [$value] Specify viewer class, which should be used for creation of holder instance.
     * @returns {BasePanelHolderViewer} Returns class with type of BasePanelHolderViewer.
     */
    public Strategy($value? : IPanelHolderStrategy) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.strategy = $value;
        }
        return this.strategy;
    }
}
