/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IBasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IBasePanel.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BasePanel as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ScrollBar } from "../BaseInterface/Components/ScrollBar.js";
import { ToolTip } from "../BaseInterface/Components/ToolTip.js";
import { IconType } from "../BaseInterface/Enums/UserControls/IconType.js";
import { Dialog } from "../BaseInterface/UserControls/Dialog.js";
import { Icon } from "../BaseInterface/UserControls/Icon.js";
import { Label } from "../BaseInterface/UserControls/Label.js";

/**
 * BasePanel should be used as abstract class for extending to the GUI panels and
 * it is providing base methods connected with panel implementation and behaviour.
 */
export class BasePanel extends Parent implements IBasePanel {

    /**
     * Provide panel resize as reaction on parent element resizing.
     * @param {ResizeEventArgs} $eventArgs Resize event args, which should be processed.
     * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
     * @param {Reflection} [$reflection] Provide reflection instance.
     * @returns {void}
     */
    public static ResizeEventHandler($eventArgs : ResizeEventArgs, $manager : GuiObjectManager,
                                     $reflection : Reflection) : void {
        const element : BasePanel = <BasePanel>$eventArgs.Owner();
        if ($reflection.IsMemberOf(element, Dialog)) {
            if (!ObjectValidator.IsEmptyOrNull((<any>element).PanelViewer())) {
                const child : BasePanel = (<any>element).PanelViewer().getInstance();
                if (!ObjectValidator.IsEmptyOrNull(child) && child.IsLoaded()) {
                    child.Width($eventArgs.AvailableWidth());
                    child.Height($eventArgs.AvailableHeight());
                }
            }
        } else if ($reflection.IsMemberOf(element, BasePanel)) {
            element.Width($eventArgs.AvailableWidth());
            element.Height($eventArgs.AvailableHeight());
        }
    }

    protected static onUnhoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                           $reflection? : Reflection) : void {
        $manager.setHovered($eventArgs.Owner(), false);
        const parent : BasePanel = <BasePanel>$eventArgs.Owner().Parent();
        if (!ObjectValidator.IsEmptyOrNull(parent)) {
            const hoverParent : any = ($parent : BasePanel) : void => {
                if (!ObjectValidator.IsEmptyOrNull($parent)) {
                    if ($reflection.IsMemberOf($parent, Dialog)) {
                        if ($parent.Visible()) {
                            $manager.setActive($parent, true);
                        }
                    } else {
                        if ($reflection.IsMemberOf($parent, BasePanel) && $parent.Scrollable() && (
                            (ObjectValidator.IsSet($parent.verticalScrollBar) &&
                                $parent.verticalScrollBar.Visible() && ElementManager.IsVisible($parent.verticalScrollBar.Id())) ||
                            (ObjectValidator.IsSet($parent.horizontalScrollBar) &&
                                $parent.horizontalScrollBar.Visible() && ElementManager.IsVisible($parent.horizontalScrollBar.Id()))
                        )) {
                            $manager.setHovered($parent, true);
                        } else {
                            hoverParent(<BasePanel>$parent.Parent());
                        }
                    }
                }
            };
            hoverParent(parent);
        }
    }

    private static onScrollBarMoveEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                               $reflection : Reflection) : void {
        if ($manager.IsActive(<IClassName>ScrollBar)) {
            let element : any = <ScrollBar>$eventArgs.Owner();
            if (Reflection.getInstance().IsMemberOf(element, ScrollBar) &&
                ElementManager.Exists(element.Parent().Id())) {
                element = <BasePanel>element.Parent();
                if ($reflection.IsMemberOf(element, BasePanel)) {
                    if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                        BasePanel.scrollTop(element, $eventArgs.Position());
                    } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                        BasePanel.scrollLeft(element, $eventArgs.Position());
                    }
                }
            }
        }
    }

    private static scrollEventHandler($eventArgs : ScrollEventArgs, $manager : GuiObjectManager,
                                      $reflection : Reflection) : void {
        const parent : BasePanel = <BasePanel>$eventArgs.Owner();
        if ($reflection.IsMemberOf(parent, BasePanel)) {
            let child : ScrollBar;
            if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                child = <ScrollBar>parent.verticalScrollBar;
            } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                child = <ScrollBar>parent.horizontalScrollBar;
            }
            if (ObjectValidator.IsSet(child) && !$manager.IsActive(child) && ElementManager.IsVisible(child.Id())) {
                ScrollBar.MoveTo(child, $eventArgs.Position());
            }
        }
    }

    /**
     * @param {number} [$value] Specify panel's width.
     * @returns {number} Returns panel's width.
     */
    public Width($value? : number) : number {
        if (ObjectValidator.IsSet($value) && ObjectValidator.IsSet(this.horizontalScrollBar)) {
            this.horizontalScrollBar.Size(Math.ceil($value * 0.95));
        }
        return super.Width($value);
    }

    /**
     * @param {number} [$value] Specify panel's height.
     * @returns {number} Returns panel's height.
     */
    public Height($value? : number) : number {
        if (ObjectValidator.IsSet($value) && ObjectValidator.IsSet(this.verticalScrollBar)) {
            this.verticalScrollBar.Size(Math.ceil($value * 0.95));
        }
        return super.Height($value);
    }

    /**
     * @returns {IToolTip} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IToolTip
     */
    protected getTitleClass() : any {
        return ToolTip;
    }

    /**
     * @returns {INotification} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.INotification
     */
    protected getNotificationClass() : any {
        return Notification;
    }

    /**
     * @returns {IIcon} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
     */
    protected getLoaderIconClass() : any {
        return Icon;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getLoaderTextClass() : any {
        return Label;
    }

    /**
     * @returns {IScrollBar} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IScrollBar
     */
    protected getScrollBarClass() : any {
        return ScrollBar;
    }

    protected cssInterfaceName() : string {
        return BasePanel.ClassName();
    }

    protected innerCode() : IGuiElement {
        if (ObjectValidator.IsSet(this.loaderIcon)) {
            this.loaderIcon.IconType(IconType.SPINNER_BIG);
        }

        this.verticalScrollBar.getEvents().setOnButton(BasePanel.onScrollBarMoveEventHandler);
        this.verticalScrollBar.getEvents().setOnChange(BasePanel.onScrollBarMoveEventHandler);
        this.horizontalScrollBar.getEvents().setOnButton(BasePanel.onScrollBarMoveEventHandler);
        this.horizontalScrollBar.getEvents().setOnChange(BasePanel.onScrollBarMoveEventHandler);

        this.getEvents().setOnResize(
            ($eventArgs : ResizeEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                const parent : BasePanel = $eventArgs.Owner();
                if (parent.Scrollable()) {
                    ScrollBar.ResizeEventHandler($eventArgs, $manager, $reflection);
                }
            });
        this.getEvents().setOnScroll(BasePanel.scrollEventHandler);

        return super.innerCode();
    }
}
