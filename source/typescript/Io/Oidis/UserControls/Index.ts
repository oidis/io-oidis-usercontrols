/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncBaseHttpController } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/AsyncBaseHttpController.js";
import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { CropBoxViewer } from "./BaseInterface/Viewers/Components/CropBoxViewer.js";
import { DragBarViewer } from "./BaseInterface/Viewers/Components/DragBarViewer.js";
import { FileUploadViewer } from "./BaseInterface/Viewers/Components/FileUploadViewer.js";
import { NotificationViewer } from "./BaseInterface/Viewers/Components/NotificationViewer.js";
import { ResizeBarViewer } from "./BaseInterface/Viewers/Components/ResizeBarViewer.js";
import { ScrollBarViewer } from "./BaseInterface/Viewers/Components/ScrollBarViewer.js";
import { SelectBoxViewer } from "./BaseInterface/Viewers/Components/SelectBoxViewer.js";
import { ToolTipViewer } from "./BaseInterface/Viewers/Components/ToolTipViewer.js";
import { AccordionViewer } from "./BaseInterface/Viewers/UserControls/AccordionViewer.js";
import { AudioViewer } from "./BaseInterface/Viewers/UserControls/AudioViewer.js";
import { ButtonViewer } from "./BaseInterface/Viewers/UserControls/ButtonViewer.js";
import { CheckBoxViewer } from "./BaseInterface/Viewers/UserControls/CheckBoxViewer.js";
import { DialogViewer } from "./BaseInterface/Viewers/UserControls/DialogViewer.js";
import { DirectoryBrowserViewer } from "./BaseInterface/Viewers/UserControls/DirectoryBrowserViewer.js";
import { DropDownListViewer } from "./BaseInterface/Viewers/UserControls/DropDownListViewer.js";
import { FormInputViewer } from "./BaseInterface/Viewers/UserControls/FormInputViewer.js";
import { HorizontalPanelHolderViewer } from "./BaseInterface/Viewers/UserControls/HorizontalPanelHolderViewer.js";
import { IconViewer } from "./BaseInterface/Viewers/UserControls/IconViewer.js";
import { ImageButtonViewer } from "./BaseInterface/Viewers/UserControls/ImageButtonViewer.js";
import { ImageViewer } from "./BaseInterface/Viewers/UserControls/ImageViewer.js";
import { InputLabelViewer } from "./BaseInterface/Viewers/UserControls/InputLabelViewer.js";
import { LabelListViewer } from "./BaseInterface/Viewers/UserControls/LabelListViewer.js";
import { LabelViewer } from "./BaseInterface/Viewers/UserControls/LabelViewer.js";
import { LinkViewer } from "./BaseInterface/Viewers/UserControls/LinkViewer.js";
import { NumberPickerViewer } from "./BaseInterface/Viewers/UserControls/NumberPickerViewer.js";
import { ProgressBarViewer } from "./BaseInterface/Viewers/UserControls/ProgressBarViewer.js";
import { RadioBoxViewer } from "./BaseInterface/Viewers/UserControls/RadioBoxViewer.js";
import { TabsViewer } from "./BaseInterface/Viewers/UserControls/TabsViewer.js";
import { TextAreaViewer } from "./BaseInterface/Viewers/UserControls/TextAreaViewer.js";
import { TextFieldViewer } from "./BaseInterface/Viewers/UserControls/TextFieldViewer.js";
import { VerticalPanelHolderViewer } from "./BaseInterface/Viewers/UserControls/VerticalPanelHolderViewer.js";
import { VideoViewer } from "./BaseInterface/Viewers/UserControls/VideoViewer.js";
import { AnimationTestPanelViewer } from "./RuntimeTests/AnimationTestPanelViewer.js";
import { HorizontalTestPanelHolderViewer } from "./RuntimeTests/HorizontalTestPanelHolderViewer.js";
import { MultiAccordionTestPanelViewer } from "./RuntimeTests/MultiAccordionTestPanelViewer.js";
import { MultiPanelTestPanelViewer } from "./RuntimeTests/MultiPanelTestPanelViewer.js";
import { PerformanceTestPanelViewer } from "./RuntimeTests/PerformanceTestPanelViewer.js";
import { UserControlsTestDialogViewer } from "./RuntimeTests/UserControlsTestDialogViewer.js";
import { UserControlsTestPanelResponsiveSimpleViewer } from "./RuntimeTests/UserControlsTestPanelResponsiveSimpleViewer.js";
import { UserControlsTestPanelResponsiveViewer } from "./RuntimeTests/UserControlsTestPanelResponsiveViewer.js";
import { UserControlsTestPanelViewer } from "./RuntimeTests/UserControlsTestPanelViewer.js";
import { VerticalTestPanelHolderViewer } from "./RuntimeTests/VerticalTestPanelHolderViewer.js";

/**
 * Index request resolver class provides handling of web index page.
 */
export class Index extends AsyncBaseHttpController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Framework User Controls Library");
        $instance.headerInfo.Content("Basic GUI User Controls mainly focused on implementation of business logic and " +
            "extendability to graphical user controls.");

        let content : string = "";
        content += `
            <H3>Components</H3>
            <a href="${ToolTipViewer.CallbackLink(true)}">ToolTip</a>
            <a href="${NotificationViewer.CallbackLink(true)}">Notification</a>
            <a href="${ScrollBarViewer.CallbackLink(true)}">ScrollBar</a>
            <a href="${DragBarViewer.CallbackLink(true)}">DragBar</a>
            <a href="${ResizeBarViewer.CallbackLink(true)}">ResizeBar</a>
            <a href="${SelectBoxViewer.CallbackLink(true)}">SelectBox</a>
            <a href="${CropBoxViewer.CallbackLink(true)}">CropBox</a>
            <a href="${FileUploadViewer.CallbackLink(true)}">FileUpload</a>

            <H3>User Controls</H3>
            <a href="${LabelViewer.CallbackLink(true)}">Label</a>
            <a href="${LabelListViewer.CallbackLink(true)}">Labels list</a>
            <a href="${InputLabelViewer.CallbackLink(true)}">InputLabel</a>
            <a href="${LinkViewer.CallbackLink(true)}">Link</a>

            <a href="${CheckBoxViewer.CallbackLink(true)}">CheckBox</a>
            <a href="${RadioBoxViewer.CallbackLink(true)}">RadioBox</a>

            <a href="${IconViewer.CallbackLink(true)}">Icon</a>
            <a href="${ImageViewer.CallbackLink(true)}">Image</a>
            <a href="${VideoViewer.CallbackLink(true)}">Video</a>
            <a href="${AudioViewer.CallbackLink(true)}">Audio</a>
            <a href="${ImageButtonViewer.CallbackLink(true)}">ImageButton</a>
            <a href="${ButtonViewer.CallbackLink(true)}">Button</a>
            <a href="${TabsViewer.CallbackLink(true)}">Tabs</a>

            <a href="${TextFieldViewer.CallbackLink(true)}">TextField</a>
            <a href="${TextAreaViewer.CallbackLink(true)}">TextArea</a>
            <a href="${DropDownListViewer.CallbackLink(true)}">DropDownList</a>

            <a href="${ProgressBarViewer.CallbackLink(true)}">ProgressBar</a>
            <a href="${NumberPickerViewer.CallbackLink(true)}">NumberPicker</a>

            <a href="${FormInputViewer.CallbackLink(true)}">FormInput</a>

            <a href="${VerticalPanelHolderViewer.CallbackLink(true)}">Vertical panel holder</a>
            <a href="${HorizontalPanelHolderViewer.CallbackLink(true)}">Horizontal panel holder</a>
            <a href="${AccordionViewer.CallbackLink(true)}">Accordion</a>
            <a href="${DialogViewer.CallbackLink(true)}">Dialog</a>
            <a href="${DirectoryBrowserViewer.CallbackLink(true)}">DirectoryBrowser</a>
        `;
        /* dev:start */
        content += `
            <h3>Runtime tests</h3>
            <a href="${PerformanceTestPanelViewer.CallbackLink()}">Performance test</a>
            <a href="${UserControlsTestPanelViewer.CallbackLink()}">User Controls test panel</a>
            <a href="${UserControlsTestPanelResponsiveViewer.CallbackLink()}">User Controls test panel responsive</a>
            <a href="${UserControlsTestPanelResponsiveSimpleViewer.CallbackLink()}">User Controls test panel responsive simple</a>
            <a href="${UserControlsTestDialogViewer.CallbackLink(true)}">User Controls test dialog</a>
            <a href="${MultiPanelTestPanelViewer.CallbackLink()}">Multi panel test</a>
            <a href="${VerticalTestPanelHolderViewer.CallbackLink(true)}">Vertical panel holder test</a>
            <a href="${HorizontalTestPanelHolderViewer.CallbackLink(true)}">Horizontal panel holder test</a>
            <a href="${MultiAccordionTestPanelViewer.CallbackLink()}">Multi accordion test</a>
            <a href="${AnimationTestPanelViewer.CallbackLink()}">Animation test panel</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}
