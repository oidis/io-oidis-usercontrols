# io-oidis-usercontrols

> Oidis Framework library focused on basic User Controls

## Requirements

This library does not have any special requirements, but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2022.2.0
Dependency version update.
### v2022.1.1
Added support for pagination and toast notifications.
### v2022.1.0
Several minor bugfixes for dialogs handling.
### v2022.0.0
Deep integration of Bootstrap.
### v2021.2.0
Added support for SASS reference file. Fixed IntelliJ indexing. Stability bug fixes for several user controls.
### v2021.0.0
Enhancements and fixes connected mostly with external projects. Added audio user control.
### v2020.1.0
Just release with changes inside dependencies.
### v2020.0.0
Identity update. Change of configuration files format. Bug fixes for Image load.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Usage of new animation engine. Responsive rendering performance update. Created user controls for video and camera media streams.
### v2019.1.0
Responsive design enhancements. Fixed Chromium performance issues. Enable to set base64 encoded image source.
### v2019.0.2
Bug fix for text area hint positioning. Usage of new app loader.
### v2019.0.1
Fixed active state for dialogs. Fixed enablement of blur for not focused elements. Added ability to disable resize bars.
### v2019.0.0
Fixed responsive design for DropDownList, ScrollBar init and TextArea. Added better control over DropDownList and TextArea disable mode 
and ResizeBar cursor. Added more Dialog events.
### v2018.3.0
Updates based on usage in WUI Builder. Conversion of String to StringUtils. Updated tests. User controls performance enhancements.
### v2018.2.0
Updates required by better code sharing at back-end projects. Refactoring of Accordion for better flexibility, 
performance and extensibility. Performance updates mostly focused on responsive design needs. 
Added new runtime tests focused on automated functional testing of user controls. Convert of docs to TypeDoc.
### v2018.1.3
Added initial implementation of functional tests.
### v2018.1.2
Fixed resize of Image loader. Test clean up.
### v2018.1.1
Fixed tests connected with updated core. API Clean up. Fixed handling of scroll propagation. Bug fixes for DropDownList and TextArea.
### v2018.1.0
Update of WUI Core for ability to support multithreading model. API clean up.
### v2018.0.4
Stability bug fixes for DropDownList and NumberPicker. Added ability to modify readonly mode for TextField on the fly.
### v2018.0.3
Added ability to control scrollbar size animation. Fixed stability bug for number picker notification.
### v2018.0.2
Stability bug fixes for content scrolling. Code coverage increase.
### v2018.0.1
RuntimeTests for Components and UserControls converted to UnitTests. Fixed linting issues.
### v2018.0.0
Unit tests clean up. Minor UI bug fixes. Change version format.
### v2.2.1
Updated dialog autorezise and autocenter behavior.
### v2.2.0
Bug fixes for NumberPicker and DirectoryBrowser. Update of SCR and change of history ordering.
### v2.1.1
Added ability to disable usage of HTML5 canvas.
### v2.1.0
Update of Commons and GUI libraries.
### v2.0.3
Added fixed version for dependencies.
### v2.0.2
Update of unit tests required for compatibility with jsdom.
### v2.0.1
Update of TypeScript syntax. Modification of DirectoryBrowser for better support of updated back-end.
### v2.0.0
Namespaces refactoring.
### v1.1.0
Changed the licence from proprietary to BSD-3-Clause.
### v1.0.4
Usage of new version compiler features—mainly the protected, lambda expression, and multi-type arguments.
### v1.0.3
Usage of a stand-alone builder.
### v1.0.2
Package renamed from com-freescale-mvc-gui-baseusercontrols to com-freescale-wui-usercontrols and minor bug fixes.
### v1.0.1
Enhancements of all general Primitives, Components, and UserControls for an easy extension to the graphical UserControls libraries.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Copyright 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2025 [Oidis](https://www.oidis.io/)
