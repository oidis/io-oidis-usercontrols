/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class EventsManagerTest extends UnitTestRunner {

    public testisOnLoadKey() : void {
        const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true};
        assert.equal((<any>EventsManager.getInstanceSingleton()).isOnLoadKey(event), false);
    }

    public testisElementMovedAllowed() : void {
        assert.equal((<any>EventsManager.getInstanceSingleton()).isElementMoveAllowed(), true);
    }
}
