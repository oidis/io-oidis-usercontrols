/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseGuiObject } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BaseGuiObject.js";

class MockBaseGuiObject extends BaseGuiObject {
}

export class BaseGuiObjectTest extends UnitTestRunner {

    public testBaseGuiObject() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("id103");
        basegui.StyleClassName("testStyleClass");
        basegui.DisableAsynchronousDraw();
        assert.equal(basegui.Draw(),
            "\r\n<div class=\"IoOidisUserControlsPrimitives\">" +
            "\r\n   <div id=\"id103_GuiWrapper\" guiType=\"GuiWrapper\" class=\"testStyleClass\">" +
            "\r\n      <div id=\"id103\" class=\"BaseGuiObject\" style=\"display: block;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        this.initSendBox();
    }
}
