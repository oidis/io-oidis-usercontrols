/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BasePanelHolder } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolder.js";
import { BasePanelHolderViewer } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewer.js";
import {
    BasePanelHolderViewerArgs
} from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewerArgs.js";

class MockHolder extends BasePanelHolderViewer {
    public getInstance() : MockBasePanelHolder {
        return <MockBasePanelHolder>super.getInstance();
    }
}

class MockHolderViewerArgs extends BasePanelHolderViewerArgs {
}

class MockBasePanelViewer extends BasePanelViewer {
}

class MockBasePanelViewerArgs extends BasePanelViewerArgs {
}

class MockBasePanelHolder extends BasePanelHolder {
    private basepanelviewer : MockBasePanelViewer;
    private basepanelviewerArgs : MockBasePanelViewerArgs;
    private test : string = "";
    private id : string = "";

    constructor($basepanelviewer : MockBasePanelViewer, $basepanelviewerArgs : MockBasePanelViewerArgs, $test = "TestHolderType",
                $id = "id666") {
        super(new MockBasePanelViewer());
        this.basepanelviewerArgs = $basepanelviewerArgs;
        this.basepanelviewer = $basepanelviewer;
        this.test = $test;
        this.id = $id;
    }
}

export class BasePanelHolderViewerTest extends UnitTestRunner {
    public testgetInstance() : void {
        const args : MockHolderViewerArgs = new MockHolderViewerArgs();
        const holderViewer : MockHolder = new MockHolder(args);
        assert.equal(holderViewer.getInstance().getClassName(), "Io.Oidis.UserControls.Primitives.BasePanelHolder");
        Echo.PrintCode(JSON.stringify(holderViewer.getInstance().Draw()));
    }

    public testViewerArgs() : void {
        const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        args.DescriptionText("testViewerArgs");
        args.HeaderText("includeHeader");
        const holderViewer : BasePanelHolderViewer = new BasePanelHolderViewer();
        assert.equal(holderViewer.ViewerArgs(args), args);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
