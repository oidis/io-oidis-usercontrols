/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BasePanelHolderEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/BasePanelHolderEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { GuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiElement.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import {
    TextFieldType
} from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/TextFieldType.js";
import { TextField } from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/TextField.js";
import { BaseField } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BaseField.js";

class MockBaseField extends BaseField {
}

export class BaseFieldTest extends UnitTestRunner {

    public testTurnActive() : void {
        const textField : TextField = new TextField(BasePanelHolderEventType.ON_MESSAGE);
        const guiobjectManager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        textField.DisableAsynchronousDraw();
        Echo.Print(textField.Draw());
        BaseField.TurnActive(textField, guiobjectManager, reflection);
        assert.equal(ElementManager.getClassName(textField.Id() + "_Status"), GeneralCssNames.ACTIVE);
    }

    public testFocus() : void {
        const basefield : BaseField = new MockBaseField(BasePanelHolderEventType.BEFORE_OPEN, "id121");
        basefield.DisableAsynchronousDraw();
        Echo.Print(basefield.Draw());
        const manager : GuiObjectManager = new GuiObjectManager();
        BaseField.Focus(basefield);
        assert.equal(basefield.Draw(),
            "\r\n<div class=\"IoOidisUserControlsPrimitives\">" +
            "\r\n   <div id=\"id121_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id121\" class=\"BaseField\" style=\"display: block;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testBlur() : void {
        const basefield : BaseField = new MockBaseField(BasePanelHolderEventType.ON_MESSAGE, "id636");
        basefield.DisableAsynchronousDraw();
        Echo.Print(basefield.Draw());
        BaseField.Blur();
        assert.equal(basefield.Draw(),
            "\r\n<div class=\"IoOidisUserControlsPrimitives\">" +
            "\r\n   <div id=\"id636_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id636\" class=\"BaseField\" style=\"display: block;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testGuiType() : void {
        const basefield : BaseField = new MockBaseField();
        const guielement : GuiElement = new GuiElement();
        assert.equal(basefield.GuiType(guielement), guielement);
    }

    public testValue() : void {
        const basefield : BaseField = new MockBaseField();
        assert.equal(basefield.Value(20), 20);
    }

    public testEnabled() : void {
        const basefield : BaseField = new MockBaseField();
        assert.equal(basefield.Enabled(true), true);
    }

    public testWidth() : void {
        const basefield : BaseField = new MockBaseField();
        assert.equal(basefield.Width(40), 40);
    }

    public testHint() : void {
        const basefield : BaseField = new MockBaseField();
        assert.equal(basefield.Hint("testHint"), "testHint");
    }

    public testsetArg() : void {
        const basefield : BaseField = new MockBaseField(TextFieldType.BLUE, "id111");
        basefield.setArg(<IGuiCommonsArg>{
            name : "Test",
            type : GuiCommonsArgType.TEXT,
            value: 56
        }, true);
        Echo.PrintCode(basefield.Draw());
        Echo.PrintCode(JSON.stringify(basefield.getArgs()));

        assert.equal(
            ArrayList.ToArrayList(basefield.getArgs()).Equal(ArrayList.ToArrayList([
                {name: "Id", type: "Text", value: "id111"},
                {name: "StyleClassName", type: "Text", value: ""},
                {name: "Enabled", type: "Bool", value: true},
                {name: "Visible", type: "Bool", value: true},
                {name: "Width", type: "Number", value: 0},
                {name: "Height", type: "Number", value: 0},
                {name: "Top", type: "Number", value: 0},
                {name: "Left", type: "Number", value: 0},
                {name: "Title", type: "Text", value: ""},
                {name: "Value", type: "Text", value: ""},
                {name: "Error", type: "Bool", value: false},
                {name: "TabIndex", type: "Number", value: null}
            ])), false);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
