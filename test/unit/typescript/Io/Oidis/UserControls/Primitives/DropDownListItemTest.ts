/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import {
    DropDownListType
} from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/DropDownListType.js";
import { DropDownList } from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/DropDownList.js";
import { DropDownListItem } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/DropDownListItem.js";

export class DropDownListItemTest extends UnitTestRunner {

    public testgetEvents() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GENERAL);
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 3);

        const handler : any = () : void => {
            // test event handler
        };
        dropdownlistItem.getEvents().setEvent("test", handler);
        assert.equal(dropdownlistItem.getEvents().Exists("test"), true);
    }

    public testOwner() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GENERAL, "owner");
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 2);
        assert.equal(dropdownlistItem.Owner(), "owner");

        const dropdownlist2 : DropDownList = new DropDownList(DropDownListType.GENERAL);
        const dropdownlistItem2 : DropDownListItem = new DropDownListItem("test", dropdownlist2, 4);
        assert.equal(dropdownlistItem2.Owner("id323"), "id323");
    }

    public testIndex() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GENERAL);
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 2);
        assert.equal(dropdownlistItem.Index(), 2);
    }

    public testText() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GENERAL);
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 1);
        assert.equal(dropdownlistItem.Text("Content of the Dropdownlist"), "Content of the Dropdownlist");
    }

    public testValue() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GENERAL);
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 1);
        assert.equal(dropdownlistItem.Value(), "dropDownList");
    }

    public testStyleClassName() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GREEN);
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 0);
        assert.equal(dropdownlistItem.StyleClassName("testClassName"), "testClassName");
    }

    public testgetInnerElement() : void {
        const dropdownlist : DropDownList = new DropDownList(DropDownListType.GREEN, "id939");
        const dropdownlistItem : DropDownListItem = new DropDownListItem("dropDownList", dropdownlist, 6);
        Echo.PrintCode(dropdownlistItem.getInnerHtml().Draw("id939"));
        assert.equal(dropdownlistItem.getInnerHtml().Draw("id939"),
            "id939<div id=\"id939_Item_6\">id939" +
            "   <div id=\"id939_Item_6_Envelop\" class=\"Item\">id939" +
            "      <div id=\"id939_Item_6_Status\" class=\"Off\">id939" +
            "         <div id=\"id939_Item_6_Icon\" class=\"Icon\"></div>id939" +
            "         <div id=\"id939_Item_6_Text\" class=\"Text\">dropDownList</div>id939" +
            "      </div>id939   </div>id939" +
            "   <div id=\"id939_Item_6_Separator\" class=\"Separator\" style=\"display: block;\">" +
            "</div>id939</div>");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
