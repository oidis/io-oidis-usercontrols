/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { AbstractGuiObject } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/AbstractGuiObject.js";

export class AbstractGuiObjectTest extends UnitTestRunner {

    public testTitle() : void {
        const abstractgui : AbstractGuiObject = new AbstractGuiObject("id999");
        abstractgui.Enabled(true);
        abstractgui.DisableAsynchronousDraw();
        assert.deepEqual(abstractgui.Title().Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id999_ToolTip_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id999_ToolTip\" class=\"ToolTip\" style=\"display: none;\">" +
            "\r\n         <div id=\"id999_ToolTip_Type\" guiType=\"ToolTip\">" +
            "\r\n            <div id=\"id999_ToolTip_Border\" class=\"Background\">" +
            "\r\n               <div id=\"id999_ToolTip_Top\" class=\"Top\">" +
            "\r\n                  <div id=\"id999_ToolTip_TopLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id999_ToolTip_TopMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id999_ToolTip_TopRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id999_ToolTip_Middle\" class=\"Middle\">" +
            "\r\n                  <div id=\"id999_ToolTip_MiddleLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id999_ToolTip_MiddleMiddle\" class=\"Center\">" +
            "\r\n                     <div id=\"id999_ToolTip_Text\" class=\"Text\"></div>" +
            "\r\n                  </div>" +
            "\r\n                  <div id=\"id999_ToolTip_MiddleRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id999_ToolTip_Bottom\" class=\"Bottom\">" +
            "\r\n                  <div id=\"id999_ToolTip_BottomLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id999_ToolTip_BottomMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id999_ToolTip_BottomRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n            </div>" +
            "\r\n         </div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testNotification() : void {
        const abstractgui : AbstractGuiObject = new AbstractGuiObject("id111");
        assert.deepEqual(abstractgui.Notification().Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id111_Notification_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id111_Notification\" class=\"Notification\" style=\"display: none;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
