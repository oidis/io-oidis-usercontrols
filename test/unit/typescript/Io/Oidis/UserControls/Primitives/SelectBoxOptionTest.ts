/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { SelectBox } from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/SelectBox.js";
import {
    SelectBoxType
} from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/Components/SelectBoxType.js";
import { SelectBoxOption } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/SelectBoxOption.js";

export class SelectBoxOptionTest extends UnitTestRunner {
    public testgetEvents() : void {
        const selectbox : SelectBox = new SelectBox();

        const handler : any = () : void => {
            // test event handler
        };
        selectbox.getEvents().setEvent("test", handler);
        assert.equal(selectbox.getEvents().Exists("test"), true);
    }

    public testOwner() : void {
        const selectbox : SelectBox = new SelectBox("id333");
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 1);
        assert.equal(boxoption.Owner("id333"), "id333");
    }

    public testId() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.RED, "id444");
        const boxoption : SelectBoxOption = new SelectBoxOption("testSlectBoxOption", selectbox, 2);
        assert.equal(boxoption.Id(), "id444_Option_2");
    }

    public testIndex() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GREEN);
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 3);
        assert.equal(boxoption.Index(), 3);
    }

    public testValue() : void {
        const selectbox : SelectBox = new SelectBox();
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 4);
        assert.equal(boxoption.Value(), "test");
    }

    public testStyleClassName() : void {
        Echo.Println("[-597087868] test<br/>   index: 5<br/>   value: test<br/>   style: testClassName<br/>" +
            "   withSeparator: true<br/>  isSelected: false");
        const selectbox : SelectBox = new SelectBox(SelectBoxType.BLUE);
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 5);
        assert.equal(boxoption.StyleClassName("testClassName"), "testClassName");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
