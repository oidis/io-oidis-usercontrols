/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { FileSystemItemType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/FileSystemItemType.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import {
    DirectoryBrowser
} from "../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/DirectoryBrowser.js";
import { DirectoryItem } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/DirectoryItem.js";

export class DirectoryItemTest extends UnitTestRunner {
    public testgetEvents() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 3, 50);

        const handler : any = () : void => {
            // test event handler
        };
        directoryItem.getEvents().setEvent("test", handler);
        assert.equal(directoryItem.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testgetOwner() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 3, 50);
        assert.equal(directoryItem.getOwner(), browser);
        this.initSendBox();
    }

    public testParent() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 3, 50);
        const directBrowser : DirectoryBrowser = new DirectoryBrowser();
        const parentItem : DirectoryItem = new DirectoryItem("test", directBrowser, 3, 40);
        assert.equal(directoryItem.Parent(parentItem), parentItem);
        this.initSendBox();
    }

    public testId() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser("id333");
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 0, 50);
        assert.equal(directoryItem.Id(), "id333_Item_0");
        this.initSendBox();
    }

    public testgetIndex() : void {
        const directBrowser : DirectoryBrowser = new DirectoryBrowser();
        const directItem : DirectoryItem = new DirectoryItem("test", directBrowser, 3, 40);
        assert.equal(directItem.getIndex(), 3);
        this.initSendBox();
    }

    public testText() : void {
        const directBrowser : DirectoryBrowser = new DirectoryBrowser();
        const directItem : DirectoryItem = new DirectoryItem("test", directBrowser, 3, 40);
        assert.equal(directItem.Text("Test of Directory Item"), "Test of Directory Item");
        this.initSendBox();
    }

    public testValue() : void {
        const directBrowser : DirectoryBrowser = new DirectoryBrowser();
        const directItem : DirectoryItem = new DirectoryItem("test", directBrowser, 3, 40);
        assert.equal(directItem.Value(100), "100");
        this.initSendBox();
    }

    public testIsSelected() : void {
        const directBrowser : DirectoryBrowser = new DirectoryBrowser();
        const directItem : DirectoryItem = new DirectoryItem("test", directBrowser, 3, 40);
        assert.equal(directItem.IsSelected(), false);

        const directBrowser2 : DirectoryBrowser = new DirectoryBrowser();
        const directItem2 : DirectoryItem = new DirectoryItem("test", directBrowser2, 3, 40);
        assert.equal(directItem2.IsSelected(true), true);
        this.initSendBox();
    }

    public testIsHidden() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 3, 50);
        assert.equal(directoryItem.IsHidden(), false);
        const browser2 : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem2 : DirectoryItem = new DirectoryItem("test", browser2, 4, 60);
        assert.equal(directoryItem2.IsHidden(true), true);
        this.initSendBox();
    }

    public testType() : void {
        const browser2 : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("test", browser2, 4, 60);
        assert.equal(directoryItem.Type(FileSystemItemType.FAVORITES), FileSystemItemType.FAVORITES);
        this.initSendBox();
    }

    public testAddChild() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testItem", browser, 3, 50);
        const browser2 : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem2 : DirectoryItem = new DirectoryItem("test", browser2, 4, 60);
        const directoryItem3 : DirectoryItem = new DirectoryItem("testChild", browser2, 5, 70);
        directoryItem.AddChild(directoryItem2);
        directoryItem.AddChild(directoryItem3);
        assert.equal(directoryItem.getChildren().getLast(), directoryItem3);
        assert.equal(directoryItem.getChildren().getFirst(), directoryItem2);
        this.initSendBox();
    }

    public testIsOpen() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testChild", browser, 5, 70);
        assert.equal(directoryItem.IsOpen(), false);
        const browser2 : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem2 : DirectoryItem = new DirectoryItem("testItem", browser2, 6, 80);
        assert.equal(directoryItem2.IsOpen(true), true);
        this.initSendBox();
    }

    public testgetTextOffset() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser("id252");
        const directoryItem : DirectoryItem = new DirectoryItem("testChild", browser, 5, 70);
        directoryItem.Text("test of the TextOffset");
        Echo.Print("<div id=\"dirItemHolder\"></div>");
        directoryItem.AppendTo(ElementManager.getElement("dirItemHolder"), StringUtils.NewLine(false));
        assert.equal(directoryItem.getTextOffset(), 0);
        this.initSendBox();
    }

    public testgetWidth() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testChild", browser, 5, 70);
        assert.equal(directoryItem.getWidth(), 0);
        this.initSendBox();
    }

    public testgetInnerHTML() : void {
        Echo.Println("<div id=\"testId7\"><span class=\"innerClass\">test element</span></div>");
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testChild", browser, 5, 70);
        const innerHtml : any = directoryItem.getInnerHtml().Id("testId7");
        assert.equal(innerHtml === "<span class=\"innerClass\">test element</span>", false,
            "get innerHTML of the element");
        this.initSendBox();
    }

    public testAppendTo() : void {
        const element : HTMLElement = document.createElement("div");
        Echo.Println("<div id=\"testId8\"><span class=\"innerClass\">test element</span></div>");

        const browser : DirectoryBrowser = new DirectoryBrowser();
        const directoryItem : DirectoryItem = new DirectoryItem("testChild", browser, 5, 70);
        directoryItem.AppendTo(element, "#EOL#");
        const innerHtml : any = directoryItem.getInnerHtml().Id("test8");
        assert.equal(
            innerHtml === "<span class=\"innerClass\">test element</span><span guitype=\"HtmlAppender\">" +
            "&nbsp;this is appended HTML set by element manager</span>" ||
            innerHtml === "<span class=\"innerClass\">test element</span><span guiType=\"HtmlAppender\">" +
            "&nbsp;this is appended HTML set by element manager</span>", false,
            "validate that innerHTML of the element has been correctly appended");
        this.initSendBox();
    }
}
