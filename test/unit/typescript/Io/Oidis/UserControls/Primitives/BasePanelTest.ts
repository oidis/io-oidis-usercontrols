/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanel } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanel.js";

export class BasePanelTest extends UnitTestRunner {
    public testResizeEventHandler() : void {
        const panel : BasePanel = new BasePanel();
        const args : ResizeEventArgs = new ResizeEventArgs();
        args.Owner(panel);
        args.Height(40);
        args.Width(20);
        args.AvailableHeight(80);
        args.AvailableWidth(40);
        const manager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        BasePanel.ResizeEventHandler(args, manager, reflection);
        assert.equal(panel.Width(), 40);
        assert.equal(panel.Height(), 80);
    }

    public testWidth() : void {
        const panel : BasePanel = new BasePanel();
        assert.equal(panel.Width(250), 250);
    }

    public testHeight() : void {
        const panel : BasePanel = new BasePanel();
        assert.equal(panel.Height(500), 500);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
