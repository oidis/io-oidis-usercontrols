/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { FormsObject } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/FormsObject.js";

class MockFormsObject extends FormsObject {
}

export class FormsObjectTest extends UnitTestRunner {
    public testTurnOn() : void {
        const form : FormsObject = new MockFormsObject();
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        FormsObject.TurnOn(form, guimanager, reflection);
        assert.equal(guimanager.IsActive(FormsObject), false);
    }

    public testBlur() : void {
        const form : FormsObject = new MockFormsObject();
        FormsObject.Blur();
        assert.equal(form.Error(), false);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
