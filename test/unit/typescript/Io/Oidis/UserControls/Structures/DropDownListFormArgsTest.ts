/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DropDownListFormArgs } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Structures/DropDownListFormArgs.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class DropDownListFormArgsTest extends UnitTestRunner {

    public testValue() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        assert.equal(dropdownlist.Value(40), 40);
        const dropdownlist2 : DropDownListFormArgs = new DropDownListFormArgs();
        assert.equal(dropdownlist2.Value("value"), "value");
        const dropdownlist3 : DropDownListFormArgs = new DropDownListFormArgs();
        assert.equal(dropdownlist3.Value(), null);
    }

    public testAddItem() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        dropdownlist.Value(40);
        dropdownlist.AddItem("testItem", 40);
        assert.equal(dropdownlist.getNames().getFirst(), "testItem");
        const dropdownlist2 : DropDownListFormArgs = new DropDownListFormArgs();
        dropdownlist2.AddItem("", "test");
        assert.equal(dropdownlist2.getNames().getLast(), null);
        dropdownlist2.AddItem("withoutValue");
        assert.equal(dropdownlist2.getValues().getLast(), "withoutValue");
    }

    public testgetNames() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        dropdownlist.Value(50);
        dropdownlist.Value(55);
        dropdownlist.AddItem("testItem", 50);
        dropdownlist.AddItem("secondItem", 55);
        assert.deepEqual(dropdownlist.getNames().getAll(), ["testItem", "secondItem"]);
    }

    public testgetValues() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        dropdownlist.Value(60);
        dropdownlist.Value(70);
        dropdownlist.AddItem("newItem", 60);
        dropdownlist.AddItem("testItem", 70);
        assert.deepEqual(dropdownlist.getValues().getAll(), [60, 70]);
    }

    public testMaxVisibleItemsCount() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        assert.equal(dropdownlist.MaxVisibleItemsCount(), -1);
        const dropdownlist2 : DropDownListFormArgs = new DropDownListFormArgs();
        assert.equal(dropdownlist2.MaxVisibleItemsCount(20), 20);
    }

    public testClear() : void {
        const dropdownlist : DropDownListFormArgs = new DropDownListFormArgs();
        dropdownlist.AddItem("testValue", "100");
        dropdownlist.AddItem("testofNumber", 6);
        assert.equal(dropdownlist.getValues().getFirst(), "100");
        assert.ok(dropdownlist.getNames().Contains("testofNumber"));
        assert.ok(dropdownlist.getValues().Contains(6));
        dropdownlist.Clear();
        assert.ok(dropdownlist.getNames().IsEmpty());
        assert.ok(dropdownlist.getValues().IsEmpty());
    }
}
