/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseFormInputArgs } from "../../../../../../../source/typescript/Io/Oidis/UserControls/Structures/BaseFormInputArgs.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

class MockBaseFormInputArgs extends BaseFormInputArgs {
}

export class BaseFormInputArgsTest extends UnitTestRunner {

    public testFormName() : void {
        const baseform : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform.Name(), "");
        const baseform2 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform2.Name("testName"), "testName");
        const baseform3 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform3.Name(null), "");
    }

    public testIsRequired() : void {
        const baseform : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform.IsRequired(true), true);
        const baseform2 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform2.IsRequired(false), false);
    }

    public testRequiredContent() : void {
        const baseform : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform.RequiredContent(), "*");
        const baseform2 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform2.RequiredContent("element"), "element");
        const baseform3 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform3.RequiredContent(""), "*");
    }

    public testHint() : void {
        const baseform : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform.Hint(), "");
        const baseform2 : BaseFormInputArgs = new MockBaseFormInputArgs();
        assert.equal(baseform2.Hint("test"), "test");
    }
}
