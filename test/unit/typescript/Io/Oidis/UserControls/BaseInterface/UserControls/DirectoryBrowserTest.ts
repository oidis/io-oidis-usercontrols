/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { IFileSystemItemProtocol } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IFileSystemItemProtocol.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { FileSystemFilter } from "@io-oidis-commons/Io/Oidis/Commons/Utils/FileSystemFilter.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import {
    DirectoryBrowser
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/DirectoryBrowser.js";
import { BasePanelHolderViewer } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewer.js";
import {
    BasePanelHolderViewerArgs
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewerArgs.js";

class MockBaseViewer extends BaseViewer {
}

class MockDirectoryBrowser extends DirectoryBrowser {
    public testinnerCode() : IGuiElement {
        return this.innerCode();
    }

    public testguiContent() : IGuiElement {
        return this.guiContent();
    }

    public testinnerHtml() : IGuiElement {
        return this.innerHtml();
    }

    public testinterfaceName() : string {
        return this.cssInterfaceName();
    }
}

export class DirectoryBrowserTest extends UnitTestRunner {
    public testTurnOn() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser("id100");
        const manager : GuiObjectManager = new GuiObjectManager();
        browser.DisableAsynchronousDraw();
        Echo.Print(browser.Draw());
        DirectoryBrowser.TurnOn(browser, manager);
        assert.equal(ElementManager.getClassName(browser.Id() + "_Status"), GeneralCssNames.ON);
    }

    public testTurnOff() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const manager : GuiObjectManager = new GuiObjectManager();
        browser.DisableAsynchronousDraw();
        Echo.Print(browser.Draw());
        DirectoryBrowser.TurnOff(browser, manager);
        assert.equal(ElementManager.getClassName(browser.Id() + "_Status"), GeneralCssNames.OFF);
    }

    public testTurnActive() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser("id101");
        browser.DisableAsynchronousDraw();
        Echo.Print(browser.Draw());
        (<any>DirectoryBrowser).tabPressed = true;
        DirectoryBrowser.TurnActive(browser);
        assert.equal(ElementManager.getClassName(browser.Id() + "_Status"), GeneralCssNames.ACTIVE);
    }

    public testgetEvents() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const handler : any = () : void => {
            // test event handler
        };
        browser.getEvents().setEvent("Event", handler);
        assert.equal(browser.getEvents().Exists("Event"), true);
    }

    public testValue() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        assert.equal(browser.Value("Com/WuiFramework/UserControls/BaseInterface/UserControls"),
            "Com/WuiFramework/UserControls/BaseInterface/UserControls");
    }

    public testPath() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        assert.equal(browser.Path("C://wuiframework//Projects//com-wui-framework-usercontrols" +
                "//test//resource//graphics//Com//Wui//Framework//UserControls//watermark.png"),
            "C://wuiframework//Projects//com-wui-framework-usercontrols//test//resource//graphics" +
            "//Com//Wui//Framework//UserControls//watermark.png");
        const browser2 : DirectoryBrowser = new DirectoryBrowser("id76");
        const viewer : BasePanelHolderViewer = new BasePanelHolderViewer(new BasePanelHolderViewerArgs());
        browser2.InstanceOwner(viewer);
        assert.equal(browser2.Path(), "");
        assert.equal(browser2.Path("C://wuiframework//Projects//com-wui-framework-usercontrols" +
                "//test//resource//graphics//Com//Wui//Framework//UserControls//watermark.png"),
            "C://wuiframework//Projects//com-wui-framework-usercontrols//test//resource//graphics" +
            "//Com//Wui//Framework//UserControls//watermark.png");
        const browser3 : DirectoryBrowser = new DirectoryBrowser("id78");
        const viewer3 : BasePanelHolderViewer = new BasePanelHolderViewer(new BasePanelHolderViewerArgs());
        browser2.InstanceOwner(viewer3);
        assert.equal(browser3.Path(null), "");
        assert.equal(browser3.Path(""), "");
    }

    public __IgnoretestFilter() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        assert.equal(browser.Filter(FileSystemFilter.DIRECTORIES),
            "MyComputer|Favorites|Network|Recent|Pinned|Drive|Directory");
    }

    public testsetStructures() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser("id6");
        browser.setStructure([], "C://wuiframework//Projects//com-wui-framework-usercontrols//test//resource//graphics//Com" +
            "//Wui//Framework//UserControls//watermark.png");
        assert.equal(browser.toLocaleString(), "Io.Oidis.UserControls.BaseInterface.UserControls.DirectoryBrowser (id6)");
    }

    public testsetStructureSecond() : void {
        const protocol : IFileSystemItemProtocol = <any>{};
        const protocol2 : IFileSystemItemProtocol = <any>{};
        const array : IFileSystemItemProtocol[] = [protocol, protocol2];
        const browser : DirectoryBrowser = new DirectoryBrowser();
        const viewer : BaseViewer = new MockBaseViewer();
        browser.setStructure(array, "C://wuiframework//Projects//com-wui-framework-usercontrols//test//resource//graphics//Com" +
            "//Wui//Framework//UserControls//watermark.png");
    }

    public testClear() : void {
        const browser : DirectoryBrowser = new DirectoryBrowser();
        browser.setStructure([],
            "C://wuiframework//Projects//com-wui-framework-usercontrols//test//resource" +
            "//graphics//Com//Wui//Framework//UserControls//watermark.png");
        browser.Clear();
    }

    public testInnerCode() : void {
        const browser : MockDirectoryBrowser = new MockDirectoryBrowser();
        assert.equal(browser.testinnerCode().ToString(), "object type of \'Io.Oidis.Gui.Primitives.GuiElement\'");
    }

    public testGuiContent() : void {
        const browser : MockDirectoryBrowser = new MockDirectoryBrowser();
        assert.equal(browser.testguiContent().ToString(), "object type of \'Io.Oidis.Gui.Primitives.GuiElement\'");
    }

    public testInnerHTML() : void {
        const browser : MockDirectoryBrowser = new MockDirectoryBrowser();
        assert.equal(browser.testinnerHtml().ToString(), "object type of \'Io.Oidis.Gui.Primitives.GuiElement\'");
    }

    public testcssInterfaceName() : void {
        const browser : MockDirectoryBrowser = new MockDirectoryBrowser();
        assert.equal(browser.testinterfaceName(), "Io.Oidis.UserControls.BaseInterface.UserControls");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
