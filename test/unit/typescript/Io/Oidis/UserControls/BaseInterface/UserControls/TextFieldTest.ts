/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { IconType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/IconType.js";
import {
    TextFieldType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/TextFieldType.js";
import { TextField } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/TextField.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { AutocompleteOption } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Structures/AutocompleteOption.js";

class MockBasePanelViewer extends BasePanelViewer {
}

export class TextFieldTest extends UnitTestRunner {

    public testConstructor() : void {
        const field : TextField = new TextField(TextFieldType.GENERAL, "id70");
        assert.equal(field.Id(), "id70");
    }

    public testGuiType() : void {
        const field : TextField = new TextField(TextFieldType.GREEN);
        field.Visible(true);
        assert.equal(field.GuiType(), TextFieldType.GREEN);
        assert.equal(field.GuiType(TextFieldType.BLUE), TextFieldType.BLUE);
    }

    public testIconName() : void {
        const field : TextField = new TextField(TextFieldType.RED);
        assert.equal(field.IconName(), "");
        assert.equal(field.IconName(IconType.BLUE_SQUARE), IconType.BLUE_SQUARE);
    }

    public testValue() : void {
        const field : TextField = new TextField(TextFieldType.GENERAL);
        assert.equal(field.Value(), "");
        assert.equal(field.Value("value"), "value");
        (<any>TextField).lengthLimit = 30;
        assert.equal(field.Value("ttttttttttttttttttttttttttttttttttttttttttttttt"), "ttttttttttttttttttttttttttttttttttttttttttttttt");
        assert.equal(field.Value("8"), 8);
    }

    public __IgnoretestValueAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const field : TextField = new TextField(TextFieldType.GREEN);
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            field.InstanceOwner(viewer);
            assert.onGuiComplete(field,
                () : void => {
                    field.Enabled(true);
                    field.Enabled(true);
                    field.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                        if (ObjectValidator.IsEmptyOrNull(field.Value())) {
                            field.Value("Hello World!");
                        }
                        assert.equal(field.Value(), "HelloWorld!");
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const field : TextField = new TextField();
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            field.InstanceOwner(viewer);
            assert.onGuiComplete(field,
                () : void => {
                    field.Enabled(true);
                    field.Visible(true);
                    field.getEvents().setOnBlur(($eventArgs : EventArgs) : void => {
                        // function
                        assert.equal(EventType, "onblur");
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testWidth() : void {
        const field : TextField = new TextField();
        assert.equal(field.Width(), 100);
        assert.equal(field.Width(750), 750);
    }

    public testReadOnly() : void {
        const field : TextField = new TextField();
        assert.equal(field.ReadOnly(), false);
        assert.equal(field.ReadOnly(true), true);
    }

    public testsetPasswordEnabled() : void {
        const field : TextField = new TextField();
        field.setPasswordEnabled();
        assert.equal(field.setPasswordEnabled(), (<any>TextField).passwordEnabled);
    }

    public testsetAutocompleteDisable() : void {
        const field : TextField = new TextField();
        field.setAutocompleteDisable();
        assert.equal(field.setAutocompleteDisable(), (<any>TextField).autocompleteEnabled);
    }

    public testsetOnlyNumbersAllowed() : void {
        const field : TextField = new TextField();
        field.setOnlyNumbersAllowed();
        assert.equal(field.Value("45"), 45);
    }

    public testLengthLimit() : void {
        const field : TextField = new TextField();
        field.Width(200);
        assert.equal(field.LengthLimit(-1), -1);
        assert.equal(field.LengthLimit(), -1);
        assert.equal(field.LengthLimit(200), 200);
    }

    public __IgnoretestLimitAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const field : TextField = new TextField();
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            field.InstanceOwner(viewer);
            manager.Add(field);
            assert.onGuiComplete(field,
                () : void => {
                    field.Width(200);
                    field.Visible(true);
                    field.Enabled(true);
                    field.setOnlyNumbersAllowed();
                    manager.setActive(field, true);
                },
                () : void => {
                    assert.equal(field.Width(), 200);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testsetAutocompleteData() : void {
        const field : TextField = new TextField();
        const list : ArrayList<AutocompleteOption> = new ArrayList<AutocompleteOption>();
        const autocompleteoption : AutocompleteOption = new AutocompleteOption("test", "100", "lookUpData", "style");
        const autocompleteoption2 : AutocompleteOption = new AutocompleteOption("auto", "200", "takeALook", "class");
        list.Add(autocompleteoption);
        list.Add(autocompleteoption2);
        field.setAutocompleteData(list);
        assert.equal(list.getLast(), autocompleteoption2);
        field.setAutocompleteData(null);
    }

    public testBlur() : void {
        const textfield : TextField = new TextField(TextFieldType.GENERAL, "id50");
        textfield.DisableAsynchronousDraw();
        Echo.Print(textfield.Draw());
        TextField.Blur();
        Echo.PrintCode(JSON.stringify(textfield.Draw()));
        assert.equal(textfield.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceUserControls\">\r\n" +
            "   <div id=\"id50_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id50\" class=\"TextField\" style=\"display: block;\">\r\n" +
            "         <div id=\"id50_Type\">\r\n" +
            "            <div id=\"id50_Status\">\r\n" +
            "               <div id=\"id50_Enabled\" guiType=\"TextField\" style=\"display: block;\">\r\n" +
            "                  <div id=\"id50_Background\" class=\"Background\">\r\n" +
            "                     <div id=\"id50_Left\" class=\"Left\"></div>\r\n" +
            "                     <div id=\"id50_Center\" class=\"Center\" style=\"width: 95px;\">\r\n" +
            "                        <div id=\"id50_Envelop\" class=\"Envelop\">\r\n" +
            "                           <div id=\"id50_IconEnvelop\">\r\n" +
            "                              <div id=\"id50_Icon\" class=\"Icon\"></div>\r\n" +
            "                           </div>\r\n" +
            "                           <div style=\"float: left; position: relative;\">\r\n" +
            "                           <form><input id=\"id50_HintInput\" class=\"Hint\" type=\"text\" style=\"width: 95px;" +
            " display: none;\" readonly=\"\" disabled=\"\">" +
            "<input id=\"id50_Input\" name=\"id50\" style=\"width: 95px;\" type=\"text\"></form></div>\r\n" +
            "                        </div>\r\n" +
            "                     </div>\r\n" +
            "                     <div id=\"id50_Right\" class=\"Right\">\r\n" +
            "                        <div id=\"id50_RightIconEnvelop\" class=\"SpinnerSmall\">\r\n" +
            "                           <div id=\"id50_RightIcon\" class=\"Icon\"></div>\r\n" +
            "                        </div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "                  <div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "                     <div id=\"id50_SelectBox_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "                        <div id=\"id50_SelectBox\" class=\"SelectBox\" style=\"display: none;\"></div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
    }

    public testEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const textfield : TextField = new TextField();
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            textfield.Visible(true);
            textfield.Enabled(true);
            textfield.InstanceOwner(viewer);
            textfield.Hint("Text");
            assert.equal(textfield.getEvents().Exists(EventType.ON_CLICK), false);
            textfield.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(textfield.Draw());
            Echo.Print(textfield.Draw());
            EventsManager.getInstanceSingleton().setEvent(textfield, EventType.ON_CLICK,
                ($eventArgs : EventArgs) : void => {
                    if (textfield.Changed()) {
                        textfield.Hint("Write some text");
                    }
                    assert.equal(textfield.getEvents().Exists(EventType.ON_CLICK), true);
                });
            $done();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
