/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseGuiGroupObjectArgs } from "@io-oidis-gui/Io/Oidis/Gui/Structures/BaseGuiGroupObjectArgs.js";
import { FormInput } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/FormInput.js";
import { BaseFormInputArgs } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Structures/BaseFormInputArgs.js";
import { TextFieldFormArgs } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Structures/TextFieldFormArgs.js";

class MockBaseGuiGroupObjectArgs extends BaseGuiGroupObjectArgs {
}

class MockBaseFormInputArgs extends BaseFormInputArgs {
}

export class FormInputTest extends UnitTestRunner {
    public testConfiguration() : void {
        const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const baseinpuArgs : BaseFormInputArgs = new MockBaseFormInputArgs();
        const forminput : FormInput = new FormInput(baseguigrupArgs);
        assert.equal(forminput.Configuration(baseinpuArgs), baseinpuArgs);
    }

    public testgetWidth() : void {
        const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const forminput : FormInput = new FormInput(baseguigrupArgs);
        assert.equal(forminput.getWidth(), -1);
    }

    public __IgnoretestValue() : void {
        const baseguigrupArgs : TextFieldFormArgs = new TextFieldFormArgs();
        const forminput : FormInput = new FormInput(baseguigrupArgs);
        assert.equal(forminput.Value(10), 10);
        assert.equal(forminput.Value(), 10);
    }

    public testClear() : void {
        const baseguigrupArgs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const forminput : FormInput = new FormInput(baseguigrupArgs, "id135");
        forminput.Value("test");
        forminput.Clear();
        assert.equal(forminput.Value(), null);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
