/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DialogEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/DialogEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { DialogType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/DialogType.js";
import { Dialog } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Dialog.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { BasePanel } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanel.js";
import { BasePanelHolderViewer } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewer.js";

class MockBasePanelHolder extends BasePanel {
}

class MockBasePanelViewer extends BasePanelViewer {
    constructor() {
        super();
        this.setInstance(new MockBasePanelHolder(null));
    }
}

class MockGuiCommons extends GuiCommons {
}

class MockBaseViewer extends BaseViewer {
}

export class DialogTest extends UnitTestRunner {
    public testConstructor() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN, "id1000");
        assert.equal(dialog.Id(), "id1000");
        this.initSendBox();
    }

    public testgetEventsSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        const handler : any = () : void => {
            // test event handler
        };
        dialog.getEvents().setEvent("test", handler);
        assert.equal(dialog.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testWidthSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.RED);
        assert.equal(dialog.Width(600), 600);
        assert.equal(dialog.Width(), 600);
        this.initSendBox();
    }

    public __IgnoretestWidthAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dialog : Dialog = new Dialog(DialogType.GENERAL);
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockBaseViewer();
            dialog.InstanceOwner(viewer);
            manager.Add(dialog);
            dialog.Visible(true);
            dialog.Enabled(true);
            dialog.Width(600);
            assert.onGuiComplete(dialog,
                () : void => {
                    assert.equal(dialog.Width(), 600);
                    assert.equal(dialog.GuiType(), DialogType.GENERAL);
                    assert.equal(dialog.Visible(), true);
                    assert.equal(manager.Exists(dialog), true);
                    manager.Clear();
                }, $done, viewer);
        };
    }

    public testHeightSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.RED);
        assert.equal(dialog.Height(500), 500);
        assert.equal(dialog.Height(), 500);
        this.initSendBox();
    }

    public testTopOffsetSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.TopOffset(-1), -1);
        assert.equal(dialog.TopOffset(20), 20);
        this.initSendBox();
    }

    public testMaxWidthSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.MaxWidth(-1), -1);
        assert.equal(dialog.MaxWidth(1000), 1000);
        this.initSendBox();
    }

    public testMaxHeightSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.MaxHeight(-1), -1);
        assert.equal(dialog.MaxHeight(1000), 1000);
        this.initSendBox();
    }

    public testAutoResizeSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        assert.equal(dialog.AutoResize(), true);
        assert.equal(dialog.AutoResize(false), false);
        this.initSendBox();
    }

    public testAutoCenter() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        assert.equal(dialog.AutoCenter(), true);
        assert.equal(dialog.AutoCenter(false), false);
        this.initSendBox();
    }

    public testResizeableType() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN);
        assert.equal(dialog.ResizeableType(), "none");
        assert.equal(dialog.ResizeableType(ResizeableType.HORIZONTAL), ResizeableType.HORIZONTAL);
        assert.equal(dialog.ResizeableType(ResizeableType.VERTICAL), ResizeableType.VERTICAL);
        assert.equal(dialog.ResizeableType(ResizeableType.NONE), ResizeableType.NONE);
        assert.equal(dialog.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL), ResizeableType.HORIZONTAL_AND_VERTICAL);
        this.initSendBox();
    }

    public testModal() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        assert.equal(dialog.Modal(), true);
        assert.equal(dialog.Modal(false), false);
        this.initSendBox();
    }

    public testDraggable() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.Draggable(), false);
        assert.equal(dialog.Draggable(true), false);
        this.initSendBox();
    }

    public testGuiType() : void {
        const dialog : Dialog = new Dialog(DialogType.RED);
        dialog.Visible(true);
        assert.equal(dialog.GuiType(DialogType.GENERAL), DialogType.GENERAL);
        assert.equal(dialog.GuiType(), "");
        this.initSendBox();
    }

    public testValue() : void {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        const dialog : Dialog = new Dialog(DialogType.RED);
        dialog.PanelViewer(new MockBasePanelViewer());
        dialog.PanelViewerArgs(args);
        assert.equal(dialog.Value(), args);
        this.initSendBox();
    }

    public testPanelViewer() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.PanelViewer(), (<any>Dialog).panelViewerInstance);
        this.initSendBox();
    }

    public testCenter() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        const panelviewer : BasePanelHolderViewer = new BasePanelHolderViewer();
        dialog.InstanceOwner(panelviewer);
        dialog.PanelViewer(panelviewer);
        const gui : GuiCommons = new MockGuiCommons("id4");
        const gui2 : GuiCommons = new MockGuiCommons("id6");
        dialog.getChildElements().Add(gui);
        dialog.getChildElements().Add(gui2);
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        dialog.Visible(true);
        dialog.Modal(false);
        (<any>Dialog).id = null;
        Dialog.Center(dialog);
        assert.equal(dialog.TopOffset(), 384);
        this.initSendBox();
    }

    public __IgnoretestOpen() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dialog : Dialog = new Dialog(DialogType.BLUE, "id100");

            const panelviewer : BasePanelHolderViewer = new BasePanelHolderViewer();
            dialog.InstanceOwner(panelviewer);

            assert.onGuiComplete(dialog,
                () : void => {
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setActive(dialog, false);
                    dialog.DisableAsynchronousDraw();
                    Echo.Print(dialog.Draw());
                    (<any>Dialog).modalOpened = false;
                    dialog.Visible(true);
                    dialog.Enabled(true);
                    Dialog.Hide(dialog);
                    // dialog.PanelViewer().getInstance();
                    manager.setHovered(dialog, true);
                    Dialog.Open(dialog);
                    const args : EventArgs = new EventArgs();
                    args.Owner(Dialog);
                    this.getEventsManager().FireEvent(Dialog.ClassName(), DialogEventType.ON_OPEN, args);
                },
                () : void => {
                    $done();
                },
                panelviewer);
        };
    }

    public __IgnoretestOpenSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dialog : Dialog = new Dialog(DialogType.BLUE, "id51");
            const gui : GuiCommons = new MockGuiCommons("id7");
            const viewer : BasePanelHolderViewer = new BasePanelHolderViewer();
            dialog.InstanceOwner(viewer);
            dialog.PanelViewer(viewer);

            assert.onGuiComplete(dialog,
                () : void => {
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setActive(dialog, false);
                    dialog.DisableAsynchronousDraw();
                    Echo.Print(dialog.Draw());
                    dialog.Parent(gui);
                    dialog.Visible(true);
                    (<any>Dialog).modalOpened = true;
                    dialog.Enabled(false);
                    Dialog.Hide(dialog);
                    // dialog.PanelViewer().getInstance();
                    Dialog.Open(dialog);
                    const args : EventArgs = new EventArgs();
                    args.Owner(Dialog);
                    this.getEventsManager().FireEvent(Dialog.ClassName(), DialogEventType.ON_OPEN, args);
                },
                () : void => {
                    $done();
                },
                viewer);
        };
    }

    public __IgnoretestOpenThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dialog : Dialog = new Dialog(DialogType.BLUE, "id666");
            // const viewer : BasePanelHolderViewer = new BasePanelHolderViewer();
            // dialog.InstanceOwner(viewer);
            assert.onGuiComplete(dialog,
                () : void => {
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.Add(dialog);
                    manager.setActive(dialog, false);
                    dialog.DisableAsynchronousDraw();
                    Echo.Print(dialog.Draw());
                    dialog.Visible(true);
                    (<any>Dialog).modalOpened = false;
                    dialog.Enabled(true);
                    Dialog.Hide(dialog);
                    Dialog.Open(dialog);
                    const args : EventArgs = new EventArgs();
                    args.Owner(Dialog);
                    this.getEventsManager().FireEvent(Dialog.ClassName(), DialogEventType.ON_OPEN, args);
                    manager.Clear();
                    EventsManager.getInstanceSingleton().Clear(Dialog.ClassName(), DialogEventType.ON_OPEN);
                }, $done);
        };
    }

    public __IgnoretestOpenForth() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE, "id91");
        Dialog.Open(dialog);
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        assert.equal(manager.IsActive(<IClassName>dialog), false);
        assert.equal(dialog.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceUserControls\">" +
            "\r\n   <div id=\"id91_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id91\" class=\"Dialog\" style=\"display: none;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testClose() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        Dialog.Close(dialog);
        assert.equal(ElementManager.getCssValue(dialog.Id(), "display"), "none");
        Dialog.Close();
        Dialog.Close();
    }

    public testCloseSecond() : void {
        const gui : GuiCommons = new MockGuiCommons();
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        const viewer : BasePanelHolderViewer = new BasePanelHolderViewer();
        dialog.InstanceOwner(viewer);
        dialog.PanelViewer(viewer);
        dialog.Parent(gui);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        dialog.Modal();
        Dialog.Close(dialog);
    }

    public testShow() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        Dialog.Show(dialog);
        assert.equal(ElementManager.getCssValue(dialog.Id() + "_Envelop", "top"), "-768px");
        assert.equal(ElementManager.getCssValue(dialog.Id() + "_Envelop", "left"), "-1024px");
    }

    public testShowSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL, "id78");
        const gui : GuiCommons = new MockGuiCommons("id7");
        const viewer : BasePanelHolderViewer = new BasePanelHolderViewer();
        dialog.InstanceOwner(viewer);
        dialog.PanelViewer(viewer);
        GuiObjectManager.getInstanceSingleton().setActive(dialog, true);
        dialog.DisableAsynchronousDraw();
        const dialogdraw : string = dialog.Draw().toString();

        dialog.Visible(true);
        dialog.Enabled(true);
        // dialog.DisableAsynchronousDraw();
        dialog.Draggable(true);
        dialog.Width(450);
        dialog.Height(890);
        dialog.TopOffset(50);
        dialog.MaxHeight(1000);
        dialog.MaxWidth(500);
        Dialog.Show(dialog);
        assert.equal(dialog.Height(), 890);
        assert.equal(dialog.Width(), 450);
    }

    public testHide() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        Dialog.Hide(dialog);
        const draw : string = dialog.Draw();
        assert.equal(dialog.Draw(), draw);
    }

    public testTurnOn() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        Dialog.TurnOn(dialog, guimanager, reflection);
        assert.equal(ElementManager.getClassName(dialog.Id() + "_Status"), GeneralCssNames.ON);
    }

    public testTurnOff() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        Dialog.TurnOff(dialog, guimanager, reflection);
        assert.equal(ElementManager.getClassName(dialog.Id() + "_Status"), GeneralCssNames.OFF);
    }

    public testTurnActive() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        Dialog.TurnActive(dialog);
        assert.equal(ElementManager.getClassName(dialog.Id() + "_Status"), GeneralCssNames.OFF);
    }

    public testFocus() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        const manager : GuiObjectManager = new GuiObjectManager();
        Dialog.Focus(dialog);
        assert.equal(ElementManager.getCssIntegerValue(dialog.Id(), "z-index"), 99999);
    }

    public testBlur() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN);
        dialog.DisableAsynchronousDraw();
        Echo.Print(dialog.Draw());
        const manager : GuiObjectManager = new GuiObjectManager();
        Dialog.Blur();
        const blur : string = dialog.Draw();
        assert.equal(dialog.Draw(), blur);
        assert.equal(ElementManager.getCssValue(dialog.Id(), "position"), null);
        assert.equal(ElementManager.getCssValue(dialog.Id(), "z-index"), null);
    }

    public testgetEvents() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);

        const handler : any = () : void => {
            // test event handler
        };
        dialog.getEvents().setEvent("TestEvent", handler);
        assert.equal(dialog.getEvents().Exists("TestEvent"), true);
    }

    public testWidth() : void {
        const dialog : Dialog = new Dialog(DialogType.RED);
        assert.equal(dialog.Width(300), 300);
    }

    public testHeight() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.Height(800), 800);
    }

    public testTopOffset() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN);
        assert.equal(dialog.TopOffset(), -1);
        assert.equal(dialog.TopOffset(20), 20);
    }

    public testMaxWidth() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        assert.equal(dialog.MaxWidth(), -1);
        assert.equal(dialog.MaxWidth(400), 400);
    }

    public testMaxHeight() : void {
        const dialog : Dialog = new Dialog(DialogType.GREEN);
        assert.equal(dialog.MaxHeight(), -1);
        assert.equal(dialog.MaxHeight(1000), 1000);
    }

    public testAutoResize() : void {
        const dialog : Dialog = new Dialog(DialogType.RED);
        assert.equal(dialog.AutoResize(), true);
        assert.equal(dialog.AutoResize(true), true);
        assert.equal(dialog.AutoResize(false), false);

        const dialog2 : Dialog = new Dialog(DialogType.RED);
        assert.equal(dialog.AutoResize(false), false);
    }

    public testAutoCenterSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.AutoCenter(true), true);
    }

    public testResizeableTypeSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.ResizeableType(ResizeableType.HORIZONTAL), ResizeableType.HORIZONTAL);
    }

    public testModalSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        assert.equal(dialog.Modal(true), true);
    }

    public testDraggableSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL);
        dialog.Modal(false);
        assert.equal(dialog.Draggable(true), true);
    }

    public __IgnoretestValueSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL, "id109");
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        args.AsyncEnabled(true);
        args.Visible(true);
        assert.equal(dialog.Value(args), args);
    }

    public testPanelViewerSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL, "id110");
        const panelviewer : BasePanelViewer = new MockBasePanelViewer();
        assert.equal(dialog.PanelViewer(panelviewer), panelviewer);
    }

    public __IgnoretestPanelViewerArgs() : void {
        const dialog : Dialog = new Dialog(DialogType.GENERAL, "id112");
        const panelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelviewerArgs.AsyncEnabled(true);
        panelviewerArgs.Visible(true);
        panelviewerArgs.getClassName();
        assert.equal(dialog.PanelViewerArgs(panelviewerArgs), panelviewerArgs);
    }

    public testGuiTypeSecond() : void {
        const dialog : Dialog = new Dialog(DialogType.BLUE);
        assert.equal(dialog.GuiType(), DialogType.BLUE);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
