/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { ImageOutputArgs } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageOutputArgs.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ImageType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/ImageType.js";
import { Image } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Image.js";

class MockViewer extends BaseViewer {
}

export class ImageTest extends UnitTestRunner {
    public testConstructor() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL, "id3");
        assert.equal(image.Id(), "id3");
    }

    public testGuiType() : void {
        const image3 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        assert.equal(image3.GuiType(), ImageType.GALLERY_PHOTO);
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        assert.equal(image.GuiType(), ImageType.GENERAL);
        const image2 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        image2.Visible(true);
        assert.equal(image.GuiType(ImageType.GALLERY_PHOTO), ImageType.GALLERY_PHOTO);
    }

    public __IgnoretestGuiTypeAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const image4 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
            const viewer : BaseViewer = new MockViewer();
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.Add(image4);
            image4.InstanceOwner(viewer);
            assert.onGuiComplete(image4,
                () : void => {
                    manager.setActive(image4, true);
                    image4.Visible(true);
                    image4.Enabled(true);
                    image4.IsSelected(true);
                },
                () : void => {
                    assert.equal(image4.IsSelected(), true);
                    assert.equal(image4.GuiType(), ImageType.GALLERY_PHOTO);
                    assert.equal(image4.Source(), "test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
                    manager.Clear();
                    $done();
                }, viewer);
        };
    }

    public testIsSelected() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg", ImageType.GENERAL);
        assert.equal(image.IsSelected(), false);
        const image2 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg", ImageType.GENERAL);
        image2.Visible(true);
        assert.equal(image2.IsSelected(true), true);
    }

    public testSource() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg",
            ImageType.GALLERY_PHOTO);
        assert.equal(image.Source("test/resource/graphics/Io/Oidis/UserControls/img2.jpg"),
            "test/resource/graphics/Io/Oidis/UserControls/img2.jpg");
        const image2 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        assert.equal(image2.Source("C:/wuiframework/Projects/com-wui-framework-usercontrols/test" +
                "/resource/graphics/Com/Wui/Framework/UserControls/img1.jpg"),
            "C:/wuiframework/Projects/com-wui-framework-usercontrols/test/resource" +
            "/graphics/Com/Wui/Framework/UserControls/img1.jpg");
        const image3 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg", ImageType.GENERAL);
        const input : HTMLImageElement = document.createElement("img");
        input.src = "test/resource/graphics/Com/Freescale/Gui/RuntimeTests/ImageProcessor/ChessBoard.png";
        input.className = "myClass";
        assert.equal(image3.Source("myClass"), "myClass");
        const image4 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img2.jpg", ImageType.GENERAL);
        const image5 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        assert.equal(image5.Source("C:/wuiframework/Projects/com-wui-framework-usercontrols/test/resource" +
                "/graphics/Com/Wui/Framework/UserControls/img1.jpg"),
            "C:/wuiframework/Projects/com-wui-framework-usercontrols/test" +
            "/resource/graphics/Com/Wui/Framework/UserControls/img1.jpg");
    }

    public testLink() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        assert.equal(image.Link(), undefined);
    }

    public testLoadingSpinnerEnabled() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        assert.equal(image.LoadingSpinnerEnabled(), true);
        assert.equal(image.LoadingSpinnerEnabled(false), false);
    }

    public testOpacityShowEnabled() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        assert.equal(image.OpacityShowEnabled(), true);
        assert.equal(image.OpacityShowEnabled(false), false);
    }

    public testsetSize() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        image.setSize(400, 200);
        assert.equal(image.getWidth(), 400);
        assert.equal(image.getHeight(), 200);
        image.setSize(0, 0);
        assert.equal(image.getWidth(), 0);
        assert.equal(image.getHeight(), 0);
        const image2 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        image2.setSize(null, null);
        assert.equal(image2.getWidth(), null);
        assert.equal(image2.getHeight(), null);
    }

    public testgetStream() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        assert.equal(image.getStream(), null);
    }

    public testTabIndex() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        assert.equal(image.TabIndex(), undefined);
        assert.equal(image.TabIndex(3), 3);
    }

    public testOutputArgs() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        const imageoutputargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(image.OutputArgs(imageoutputargs), imageoutputargs);
        const image2 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        assert.equal(image2.OutputArgs(), undefined);
        const image3 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        const imageoutputargs2 : ImageOutputArgs = new ImageOutputArgs();
        imageoutputargs2.setSize(600, 400);
        assert.equal(image3.OutputArgs(imageoutputargs2), imageoutputargs2);
        const image4 : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg",
            ImageType.GALLERY_PHOTO, "id17");
        assert.equal(image4.OutputArgs(), undefined);
    }

    public testTurnOn() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        image.DisableAsynchronousDraw();
        Echo.Print(image.Draw());
        Image.TurnOn(image);
        Echo.PrintCode(JSON.stringify(image.Draw()));
        assert.equal(ElementManager.getClassName(image.Id() + "_Status"), GeneralCssNames.OFF);
    }

    public testTurnOff() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        image.DisableAsynchronousDraw();
        Echo.Print(image.Draw());
        Image.TurnOff(image);
        assert.equal(ElementManager.getClassName(image.Id() + "_Status"), GeneralCssNames.OFF);
    }

    public testTurnActive() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        image.DisableAsynchronousDraw();
        Echo.Print(image.Draw());
        Image.TurnActive(image);
        assert.equal(ElementManager.getClassName(image.Id() + "_Status"), GeneralCssNames.ACTIVE);
    }

    public testShow() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        image.DisableAsynchronousDraw();
        Echo.Print(image.Draw());
        Image.Show(image);
        (<any>Image).sourceLoaded = false;
        assert.equal(image.OpacityShowEnabled(), true);
    }

    public testFocus() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        const manager : GuiObjectManager = new GuiObjectManager();
        Image.Focus(image, true);
        assert.equal(ElementManager.getClassName(image.Id() + "_Enabled"), null);
    }

    public testBlur() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg",
            ImageType.GALLERY_PHOTO, "id19");
        image.DisableAsynchronousDraw();
        Echo.Print(image.Draw());
        Image.Blur();
        assert.equal(image.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceUserControls\">" +
            "\r\n   <div id=\"id19_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id19\" class=\"Image\" style=\"display: block;\">" +
            "\r\n         <div guiType=\"Image\" class=\"GalleryPhoto\">" +
            "\r\n            <div id=\"id19_Status\" class=\"Off\">" +
            "\r\n               <div id=\"id19_Background\" class=\"Background\">" +
            "\r\n                  <div id=\"id19_Loading\" class=\"Loading\" style=\"display: block; height: 800px; width: 800px;\">" +
            "\r\n                     <div id=\"id19_LoadingContent\" class=\"Content\">" +
            "\r\n                        <div class=\"IoOidisUserControlsBaseInterfaceUserControls\">" +
            "\r\n                           <div id=\"id19_LoadingSpinner_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n                              <div id=\"id19_LoadingSpinner\" class=\"Icons\" style=\"display: block;\">" +
            "\r\n                                 <div id=\"id19_LoadingSpinner_Status\">" +
            "\r\n                                    <div id=\"id19_LoadingSpinner_Type\" guiType=\"Icon\" class=\"SpinnerSmall\">" +
            "\r\n                                       <div id=\"id19_LoadingSpinner_Icon\" class=\"Icon\"></div>" +
            "\r\n                                    </div>" +
            "\r\n                                 </div>" +
            "\r\n                              </div>" +
            "\r\n                           </div>" +
            "\r\n                        </div>" +
            "\r\n                        <div class=\"IoOidisUserControlsBaseInterfaceUserControls\">" +
            "\r\n                           <div id=\"id19_LoadingText_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n                              <div id=\"id19_LoadingText\" class=\"Label\" style=\"display: block;\">" +
            "\r\n                                 <div id=\"id19_LoadingText_Enabled\" style=\"display: block;\">" +
            "\r\n                                    <div id=\"id19_LoadingText_Text\" class=\"Text\">Loading ...</div>" +
            "\r\n                                 </div>" +
            "\r\n                              </div>" +
            "\r\n                           </div>" +
            "\r\n                        </div>" +
            "\r\n                     </div>" +
            "\r\n                  </div>" +
            "\r\n                  <div id=\"id19_Loaded\" class=\"Loaded\" style=\"display: none;\">" +
            "\r\n                  <img id=\"id19_Image\" class=\"Hidden  \" alt=\"test/resource/graphics/Io/Oidis/" +
            "UserControls/img1.jpg\" crossorigin=\"Anonymous\"></div>" +
            "\r\n                  <div id=\"id19_Foreground\" class=\"Foreground\"></div>" +
            "\r\n               </div>" +
            "\r\n            </div>" +
            "\r\n         </div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testEnabled() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
        assert.equal(image.Enabled(), true);
        assert.equal(image.Enabled(false), false);
    }

    public testVersion() : void {
        const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GALLERY_PHOTO);
        assert.equal(image.Version(1), 1);
    }

    public __IgnoretestStyleClassNameAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
            const viewer : BaseViewer = new MockViewer();
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.Add(image);
            image.InstanceOwner(viewer);
            assert.onGuiComplete(image,
                () : void => {
                    manager.setActive(image, true);
                    image.Visible(true);
                    image.Enabled(true);
                    image.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                        image.StyleClassName("imageBack");
                        assert.equal(image.StyleClassName(), "imageBack");
                    });
                },
                () : void => {
                    assert.equal(image.GuiType(), ImageType.GENERAL);
                    assert.equal(image.Source(), "test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
                    manager.Clear();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
            const viewer : BaseViewer = new MockViewer();
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.Add(image);
            image.InstanceOwner(viewer);
            image.Width(800);
            assert.onGuiComplete(image,
                () : void => {
                    manager.setActive(image, true);
                    image.Visible(true);
                    image.Enabled(true);
                    image.StyleClassName("imageBack");
                    image.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
                        image.getStream();
                        image.LoadingSpinnerEnabled(true);
                        const rotation : number = image.OutputArgs().Rotation();
                        ElementManager.setCssProperty(image.Source(), "transform", "rotate(" + rotation + "deg)");
                        ElementManager.setCssProperty(image.Source(), "-webkit-transform", "rotate(" + rotation + "deg)");
                        ElementManager.setCssProperty(image.Source(), "-moz-transform", "rotate(" + rotation + "deg)");
                        ElementManager.setCssProperty(image.Source(), "-o-transform", "rotate(" + rotation + "deg)");
                        ElementManager.setCssProperty(image.Source(), "-ms-transform", "rotate(" + rotation + "deg)");
                        ElementManager.setSize(image.Id(), 700, 700);
                        assert.equal(image.getEvents().Exists("onload"), true);
                    });
                },
                () : void => {
                    assert.equal(image.StyleClassName(), "imageBack");
                    assert.equal(image.GuiType(), ImageType.GENERAL);
                    assert.equal(image.Source(), "test/resource/graphics/Io/Oidis/UserControls/img1.jpg");
                    manager.Clear();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestHideAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const image : Image = new Image("test/resource/graphics/Io/Oidis/UserControls/img1.jpg", ImageType.GENERAL);
            const viewer : BaseViewer = new MockViewer();
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.Add(image);
            image.InstanceOwner(viewer);
            assert.onGuiComplete(image,
                () : void => {
                    manager.setActive(image, true);
                    image.Visible(true);
                    image.Enabled(true);
                    image.getEvents().setOnHide(($eventArgs : EventArgs) : void => {
                        image.Enabled(false);
                        assert.equal(image.getEvents().Exists(EventType.ON_HIDE), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
