/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import {
    HorizontalPanelHolderViewer
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Viewers/UserControls/HorizontalPanelHolderViewer.js";

export class HorizontalPanelHolderTest extends UnitTestRunner {
    public testConstructor() : void {
        const basepanelviewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        basepanelviewerArgs.AsyncEnabled(true);
        basepanelviewerArgs.Visible(true);
        const basepanelviewer : HorizontalPanelHolderViewer = new HorizontalPanelHolderViewer();
        // const horizontalpanel : HorizontalPanelHolderStrategy = new HorizontalPanelHolderStrategy(basepanelviewer,
        //     basepanelviewerArgs, BasePanelHolderEventType.ON_MESSAGE, "id9999");
        // assert.equal(horizontalpanel.Id(), "id9999");
        this.initSendBox();
    }
}
