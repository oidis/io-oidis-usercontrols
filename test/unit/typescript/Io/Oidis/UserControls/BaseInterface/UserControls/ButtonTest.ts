/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ButtonType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/ButtonType.js";
import { Button } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Button.js";
import { ImageButton } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/ImageButton.js";

class MockBaseViewer extends BaseViewer {
}

class MockButton extends Button {
    public testexcludeSerializationData() : string[] {
        return this.excludeSerializationData();
    }
}

export class ButtonTest extends UnitTestRunner {

    public testConstructor() : void {
        const button : Button = new Button(ButtonType.GENERAL, "id110");
        assert.equal(button.Id(), "id110");
        this.initSendBox();
    }

    public testgetEvents() : void {
        const button : Button = new Button(ButtonType.BLUE);
        const handler : any = ($eventArgs : MouseEventArgs) : void => {
            button.getEvents().FireAsynchronousMethod(() : void => {
                const button : ImageButton = $eventArgs.Owner();
            }, 4000);
        };
        button.getEvents().setEvent("Event", handler);
        assert.equal(button.getEvents().Exists("Event"), true);
        this.initSendBox();
    }

    public testGuiType() : void {
        const button : Button = new Button(ButtonType.GREEN);
        assert.equal(button.GuiType(), ButtonType.GREEN);
        assert.equal(button.GuiType(ButtonType.BLUE), ButtonType.BLUE);
        this.initSendBox();
    }

    public testText() : void {
        const button : Button = new Button();
        const viewer : BaseViewer = new MockBaseViewer();
        button.InstanceOwner(viewer);
        assert.equal(button.Text("PutTitle"), "PutTitle");
        this.initSendBox();
    }

    public __IgnoretestWidthAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const button : Button = new Button(ButtonType.GENERAL);
            const viewer : BaseViewer = new BaseViewer();
            button.InstanceOwner(viewer);
            assert.onGuiComplete(button,
                () : void => {
                    assert.equal(button.Width(500), 500);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testWidth() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        const viewer : BaseViewer = new BaseViewer();
        button.InstanceOwner(viewer);
        button.Width(200);
        assert.equal(button.Width(200), 200);
        this.initSendBox();
    }

    public __IgnoretestWidthThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const button : Button = new Button(ButtonType.GENERAL);
            const viewer : BaseViewer = new BaseViewer();
            button.InstanceOwner(viewer);
            button.getGuiOptions().Add(GuiOptionType.SELECTED);
            assert.onGuiComplete(button,
                () : void => {
                    assert.equal(button.Width(null), 1);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestWidthSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const button : Button = new Button(ButtonType.GENERAL);
            const viewer : BaseViewer = new BaseViewer();
            button.InstanceOwner(viewer);
            assert.onGuiComplete(button,
                () : void => {
                    assert.equal(button.Width(), 1);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testEnabledElse() : void {
        const button : Button = new Button(ButtonType.GREEN);
        const viewer : BaseViewer = new BaseViewer();
        button.InstanceOwner(viewer);
        button.Visible(true);
        button.Enabled(true);
        assert.equal(button.Enabled(), true);
        this.initSendBox();
    }

    public __IgnoretestgetSizeSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const button : Button = new Button();
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockBaseViewer();
            button.InstanceOwner(viewer);
            manager.Add(button);
            assert.onGuiComplete(button,
                () : void => {
                    manager.setActive(button, true);
                    button.Visible(true);
                    button.Enabled(true);
                    button.Text("This is test of button");
                    button.Width(200);
                },
                () : void => {
                    assert.equal(button.Enabled(), true);
                    assert.equal(button.getSize().Width(), 200);
                    manager.Clear();
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testgetSize() : void {
        const button : Button = new Button(ButtonType.BLUE);
        button.Width(500);
        assert.equal(button.getSize().Width(), 500);
        assert.equal(button.getSize().Height(), 0);
        this.initSendBox();
    }

    public testTurnOn() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        Button.TurnOn(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), GeneralCssNames.ON);
        this.initSendBox();
    }

    public testTurnOff() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        Button.TurnOff(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), GeneralCssNames.OFF);
        this.initSendBox();
    }

    public testTurnOffElse() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        const guimanager : GuiObjectManager = new GuiObjectManager();
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        Button.TurnOff(button, guimanager, Reflection.getInstance());
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), "Off");
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), "Off");
        Button.TurnOn(button, guimanager, Reflection.getInstance());
        Button.TurnActive(button, guimanager, Reflection.getInstance());
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), "Active");
        Button.TurnOff(button, guimanager, Reflection.getInstance());
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), "Off");
        this.initSendBox();
    }

    public testTurnActive() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        Button.TurnActive(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
        this.initSendBox();
    }

    public testTurnSelected() : void {
        const button : Button = new Button(ButtonType.GENERAL);
        Button.TurnSelected(button, true);
        assert.equal(button.IsSelected(true), true);
        this.initSendBox();
    }

    public testgetArgs() : void {
        const button : Button = new Button(ButtonType.RED, "id46");
        button.Enabled(true);
        button.Width(500);
        button.GuiType(ButtonType.RED);
        button.Text("testOfArgs");
        button.setArg(<IGuiCommonsArg>{
            name : "testName",
            type : "testType",
            value: "testValue"
        }, true);
        assert.equal(button.getArgs().toString(),
            [
                {name: "Id", type: "Text", value: "id46"},
                {name: "StyleClassName", type: "Text", value: ""},
                {name: "Enabled", type: "Bool", value: true},
                {name: "Visible", type: "Bool", value: true},
                {name: "Width", type: "Number", value: 500},
                {name: "Height", type: "Number", value: 0},
                {name: "Top", type: "Number", value: 0},
                {name: "Left", type: "Number", value: 0},
                {name: "Title", type: "Text", value: ""},
                {name: "Value", type: "Text", value: "testOfArgs"},
                {name: "Error", type: "Bool", value: false},
                {name: "TabIndex", type: "Number", value: null},
                {name: "Text", type: "Text", value: "testOfArgs"},
                {name: "Width", type: "Number", value: 500},
                {items: ["GENERAL", "RED", "BLUE", "GREEN"], name: "GuiType", type: "List", value: "RED"}
            ]);
        this.initSendBox();
    }

    public testsetArgs() : void {
        const button : Button = new Button();
        button.setArg(<IGuiCommonsArg>{
            name : "Value",
            type : GuiCommonsArgType.NUMBER,
            value: 100
        }, true);
        assert.equal(button.getArgs().length, 15);
        this.initSendBox();
    }

    public testsetArgsSecond() : void {
        const button : Button = new Button();
        button.setArg(<IGuiCommonsArg>{
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: "Testing of Text"
        }, true);
        assert.equal(button.getArgs().length, 15);
        this.initSendBox();
    }

    public testsetArgsWidth() : void {
        const button : Button = new Button();
        button.setArg(<IGuiCommonsArg>{
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: 50
        }, true);
        assert.equal(button.getArgs().length, 15);
        this.initSendBox();
    }

    public testsetArgsHeight() : void {
        const button : Button = new Button();
        button.setArg(<IGuiCommonsArg>{
            name : "Height",
            type : GuiCommonsArgType.NUMBER,
            value: 100
        }, false);
        assert.equal(button.getArgs().length, 15);
        this.initSendBox();
    }

    public testsetArgsGuiType() : void {
        const button : Button = new Button();
        button.setArg(<IGuiCommonsArg>{
            name : "GuiType",
            type : GuiCommonsArgType.TEXT,
            value: ButtonType.GENERAL
        }, true);
        assert.equal(button.getArgs().length, 15);
        this.initSendBox();
    }

    public testexcludSerializationData() : void {
        const button : MockButton = new MockButton();
        assert.deepEqual(button.testexcludeSerializationData().toString(),
            "objectNamespace,objectClassName,options,availableOptionsList,parent,owner,guiPath,visible,enabled," +
            "prepared,completed,interfaceClassName,styleClassName,containerClassName,loaded,asyncDrawEnabled,contentLoaded," +
            "waitFor,outputEndOfLine,innerHtmlMap,events,changed,error,tabIndex,autofillPersistence,valuesPersistence," +
            "errorFlags,isPersistent,isSelected,iconType,text,width");
        this.initSendBox();
    }

    public __IgnoretestgetGuiOptionsSec() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const button : Button = new Button(ButtonType.GREEN, "id45");
            const viewer : BaseViewer = new BaseViewer();
            button.getGuiOptions().Add(GuiOptionType.DISABLE);
            button.InstanceOwner(viewer);
            assert.onGuiComplete(button,
                () : void => {
                    button.Visible(true);
                    button.Enabled(false);
                    button.Text("Option");
                },
                () : void => {
                    assert.equal(button.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testgetGuiOptions() : void {
        const button : Button = new Button(ButtonType.GREEN, "id45");
        const viewer : BaseViewer = new BaseViewer();
        button.getGuiOptions().Add(GuiOptionType.DISABLE);
        button.InstanceOwner(viewer);
        assert.equal(button.getGuiOptions().Contains(GuiOptionType.DISABLE), true);
        this.initSendBox();
    }
}
