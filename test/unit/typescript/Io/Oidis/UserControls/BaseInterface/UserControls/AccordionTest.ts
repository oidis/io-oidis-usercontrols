/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import {
    AccordionType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/AccordionType.js";
import { Accordion } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Accordion.js";
import { BasePanelHolder } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolder.js";
import {
    BasePanelHolderViewerArgs
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/BasePanelHolderViewerArgs.js";

class MockBasePanelHolder extends BasePanelHolder {
    constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
        super($bodyClass, $args, $holderType, $id);
    }
}

export class AccordionTest extends UnitTestRunner {

    public testConstructor() : void {
        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const panelViewerArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        const panelViewerArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        listOfArgs.Add(panelViewerArgs);
        listOfArgs.Add(panelViewerArgs2);
        const acordion : Accordion = new Accordion(listOfArgs, AccordionType.VERTICAL, "id300");

        const listOfArgs2 : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const acordion2 : Accordion = new Accordion(listOfArgs2);
        const acordion3 : Accordion = new Accordion(null);
        this.initSendBox();
    }

    public testGuiType() : void {
        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const acordion : Accordion = new Accordion(listOfArgs, AccordionType.VERTICAL, "id302");
        assert.equal(acordion.GuiType(), AccordionType.VERTICAL);
        assert.equal(acordion.GuiType(AccordionType.HORIZONTAL), AccordionType.HORIZONTAL);
        this.initSendBox();
    }

    public testsetPanelHolderArgs() : void {
        const acordion : Accordion = new Accordion(null, AccordionType.VERTICAL, "id303");

        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const panelViewerArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        const panelViewerArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        listOfArgs.Add(panelViewerArgs);
        listOfArgs.Add(panelViewerArgs2);
        acordion.setPanelHoldersArgs(listOfArgs);
        assert.deepEqual(acordion.getChildPanelList().getFirst(), null);
        const acordion2 : Accordion = new Accordion(null, AccordionType.VERTICAL, "id304");
        acordion2.setPanelHoldersArgs(null);
        this.initSendBox();
    }

    public testClear() : void {
        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const panelViewerArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        panelViewerArgs.BodyViewerClass("BodyClass");
        panelViewerArgs.HolderViewerClass("HolderViewer");
        panelViewerArgs.IsOpened(true);
        const panelViewerArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        listOfArgs.Add(panelViewerArgs);
        listOfArgs.Add(panelViewerArgs2);

        const acordion : Accordion = new Accordion(listOfArgs, AccordionType.HORIZONTAL);
        const viewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        const panelviewer : BasePanelViewer = new BasePanelViewer(viewerArgs);
        acordion.getChildPanelList().Add(panelviewer);
        assert.equal(acordion.getChildPanelList().getFirst(), panelviewer);
        acordion.Clear();
        this.initSendBox();
    }

    public testExpandSingle() : void {
        const acordion : Accordion = new Accordion(null, AccordionType.HORIZONTAL);
        const panelviewer : BasePanelViewer = new BasePanelViewer();
        const panelholder : BasePanelHolder = new MockBasePanelHolder(panelviewer);
        acordion.ExpandExclusive(panelholder);
        this.initSendBox();
    }

    public testGuiTypeSecond() : void {
        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const acordion : Accordion = new Accordion(listOfArgs, AccordionType.VERTICAL);
        assert.equal(acordion.GuiType(), AccordionType.VERTICAL);
        assert.equal(acordion.GuiType(AccordionType.HORIZONTAL), AccordionType.HORIZONTAL);
        this.initSendBox();
    }

    public testsetPanelHolderArgsSecond() : void {
        const acordion : Accordion = new Accordion(null, AccordionType.VERTICAL, "id303");

        const listOfArgs : ArrayList<BasePanelHolderViewerArgs> = new ArrayList<BasePanelHolderViewerArgs>();
        const panelViewerArgs : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.IsOpened(true);
        panelViewerArgs.BodyArgs(args);
        const panelViewerArgs2 : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
        listOfArgs.Add(panelViewerArgs);
        listOfArgs.Add(panelViewerArgs2);
        acordion.setPanelHoldersArgs(listOfArgs);

        assert.equal(acordion.Draw(),
            "\r\n<div id=\"id303_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">" +
            "\r\n    <div class=\"IoOidisUserControlsBaseInterfaceUserControls\">" +
            "\r\n       <div id=\"id303_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n          <div id=\"id303\" class=\"Accordion\" style=\"display: none;\"></div>" +
            "\r\n       </div>" +
            "\r\n    </div>" +
            "\r\n</div>"
        );
        this.initSendBox();
    }
}
