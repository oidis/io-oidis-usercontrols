/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { RadioBox } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/RadioBox.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";

class MockBasePanelViewer extends BasePanelViewer {
}

export class RadioBoxTest extends UnitTestRunner {

    public testConstructor() : void {
        const radiobox : RadioBox = new RadioBox("GuiGroup");
        assert.equal(radiobox.GroupName(), "GuiGroup");
    }

    public testGroupName() : void {
        const radiobox : RadioBox = new RadioBox("GroupTest");
        assert.equal(radiobox.GroupName("GroupName"), "GroupName");
    }

    public testgetGroup() : void {
        const radiobox : RadioBox = new RadioBox("group");
        assert.deepEqual(radiobox.getGroup().Length(), 1);
    }

    public testChecked() : void {
        const radiobox : RadioBox = new RadioBox("group");
        assert.equal(radiobox.Checked(false), false);
        assert.equal(radiobox.Checked(true), true);
        const radiobox2 : RadioBox = new RadioBox("GuiTest", "id34");
        (<any>GuiCommons).completed = true;
        ElementManager.getElement("id34");
        assert.equal(radiobox2.Checked(), null);
    }

    public testToggleChecked() : void {
        const radiobox : RadioBox = new RadioBox("GroupName");
        assert.equal(radiobox.Checked(), null);
        RadioBox.ToggleChecked(radiobox);
        assert.equal(radiobox.Checked(), true);
    }

    public testEnabledGroup() : void {
        const radiobox : RadioBox = new RadioBox("Name");
        assert.equal(radiobox.Enabled(), true);
        RadioBox.EnabledGroup("Name", false);
        assert.equal(radiobox.Enabled(), false);
        const radiobox2 : RadioBox = new RadioBox("group");
        RadioBox.EnabledGroup("group", true);
        assert.equal(radiobox2.Checked(true), true);
    }

    public testErrorGroup() : void {
        const radiobox : RadioBox = new RadioBox("Name");
        assert.equal(radiobox.Error(), false);
        RadioBox.ErrorGroup("Name", true);
        assert.equal(radiobox.Error(), true);
    }

    public testError() : void {
        const radiobox : RadioBox = new RadioBox("test");
        RadioBox.ErrorGroup("test", true);
        assert.equal(radiobox.Error(), true);
    }

    public __IgnoretestOnChangeEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const radiobox : RadioBox = new RadioBox("id46");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            const args : EventArgs = new EventArgs();
            radiobox.InstanceOwner(viewer);
            assert.onGuiComplete(radiobox,
                () : void => {
                    assert.equal(radiobox.getEvents().Exists(EventType.ON_CHANGE), false);
                    radiobox.Visible(true);
                    radiobox.Enabled(true);
                    radiobox.DisableAsynchronousDraw();
                    Echo.Print(radiobox.Draw());
                    radiobox.getEvents().setEvent(EventType.ON_CHANGE, ($eventArgs : EventArgs) : void => {
                        LogIt.Debug("this is event OnChange");
                        assert.equal($eventArgs.Owner(), radiobox);
                        assert.equal(radiobox.getEvents().Exists(EventType.ON_CHANGE), true);
                        assert.equal(radiobox.Checked(), true);
                    });
                    EventsManager.getInstanceSingleton().FireEvent(radiobox, EventType.ON_CHANGE, args, true);
                },
                () : void => {
                    assert.equal(radiobox.getEvents().Exists(EventType.ON_CHANGE), true);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestOnBlurEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const radiobox : RadioBox = new RadioBox("id58");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            radiobox.InstanceOwner(viewer);
            assert.onGuiComplete(radiobox,
                () : void => {
                    radiobox.Visible(true);
                    radiobox.Enabled(true);
                    radiobox.getEvents().setOnBlur(($eventArgs : EventArgs) : void => {
                        // empty function
                    });
                    assert.equal(radiobox.getEvents().Exists(EventType.ON_BLUR), true);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testOnBlurEventNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const radiobox : RadioBox = new RadioBox("ïd666");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            radiobox.InstanceOwner(viewer);
            radiobox.Visible(true);
            radiobox.Enabled(true);
            radiobox.DisableAsynchronousDraw();
            radiobox.Draw();
            radiobox.getEvents().setOnBlur(($eventArgs : EventArgs) : void => {
                LogIt.Debug("this is event onBlur");
            });
            assert.equal(radiobox.getEvents().Exists(EventType.ON_BLUR), true);
            EventsManager.getInstanceSingleton().FireEvent(radiobox, EventType.ON_BLUR, true);
            this.initSendBox();
            $done();
        };
    }

    public testOnChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const radiobox : RadioBox = new RadioBox("id101");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            const args : EventArgs = new EventArgs();
            radiobox.InstanceOwner(viewer);
            assert.equal(radiobox.getEvents().Exists(EventType.ON_CHANGE), false);
            radiobox.Checked(false);
            radiobox.Visible(true);
            radiobox.Enabled(true);
            radiobox.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(radiobox.Draw());
            radiobox.Draw();
            EventsManager.getInstanceSingleton().setEvent(radiobox, EventType.ON_CHANGE, ($eventArgs : EventArgs) : void => {
                LogIt.Debug("this is event OnChange");
                assert.equal($eventArgs.Owner(), radiobox);
                assert.equal(radiobox.getEvents().Exists(EventType.ON_CHANGE), true);
                assert.equal(radiobox.Checked(), true);

            });
            EventsManager.getInstanceSingleton().FireEvent(radiobox, EventType.ON_CHANGE, args, true);
            this.initSendBox();
            $done();
        };
    }

    public testOnFocusEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const radiobox : RadioBox = new RadioBox("ïd666");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            radiobox.InstanceOwner(viewer);
            radiobox.Visible(true);
            radiobox.Enabled(true);
            assert.equal(radiobox.getEvents().Exists(EventType.ON_FOCUS), false);
            radiobox.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(radiobox.Draw());
            radiobox.Draw();
            radiobox.getEvents().setOnFocus(($eventArgs : EventArgs) : void => {
                LogIt.Debug("this is event OnFocus");
            });
            assert.equal(radiobox.getEvents().Exists(EventType.ON_FOCUS), true);
            this.initSendBox();
            $done();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
        console.clear(); // eslint-disable-line no-console
    }
}
