/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import {
    ImageButtonType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/ImageButtonType.js";
import { ImageButton } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/ImageButton.js";

class MockBaseViewer extends BaseViewer {
}

export class ImageButtonTest extends UnitTestRunner {
    public testTurnOn() : void {
        const button : ImageButton = new ImageButton(ImageButtonType.GENERAL);
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        ImageButton.TurnOn(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.ON);
    }

    public testTurnOff() : void {
        const button : ImageButton = new ImageButton(ImageButtonType.GREEN);
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        ImageButton.TurnOff(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.OFF);
    }

    public testTurnActive() : void {
        const button : ImageButton = new ImageButton(ImageButtonType.GENERAL);
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        const guimanager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        ImageButton.TurnActive(button, guimanager, reflection);
        assert.equal(ElementManager.getClassName(button.Id() + "_Active"), GeneralCssNames.ACTIVE);
    }

    public testTurnSelected() : void {
        const button : ImageButton = new ImageButton(ImageButtonType.BLUE, "id43");
        ImageButton.TurnSelected(button, true);
        button.DisableAsynchronousDraw();
        Echo.Print(button.Draw());
        assert.equal(button.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceUserControls\">\r\n" +
            "   <div id=\"id43_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id43\" class=\"ImageButton\" style=\"display: block;\">\r\n" +
            "         <div id=\"id43_Type\" guiType=\"ImageButton\" class=\"Blue\">\r\n" +
            "            <div id=\"id43_Status\">\r\n" +
            "               <div id=\"id43_Enabled\" style=\"display: block;\">\r\n" +
            "                  <div id=\"id43_Active\" class=\"Off\" style=\"display: block;\">\r\n" +
            "                     <button id=\"id43_Input\" name=\"id43\" style=\"opacity: 0;\" class=\"GuiSelector\"></button>\r\n" +
            "                     <div id=\"id43_IconEnvelop\">\r\n" +
            "                        <div id=\"id43_Icon\" class=\"Icon\"></div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>"
        );
    }

    public testTurnSelected2() : void {
        const button2 : ImageButton = new ImageButton(ImageButtonType.BLUE, "id44");
        ImageButton.TurnSelected(button2, false);
        button2.DisableAsynchronousDraw();
        Echo.Print(button2.Draw());
        assert.equal(button2.Draw(), "\r\n" +
            "<div class=\"IoOidisUserControlsBaseInterfaceUserControls\">\r\n" +
            "   <div id=\"id44_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id44\" class=\"ImageButton\" style=\"display: block;\">\r\n" +
            "         <div id=\"id44_Type\" guiType=\"ImageButton\" class=\"Blue\">\r\n" +
            "            <div id=\"id44_Status\">\r\n" +
            "               <div id=\"id44_Enabled\" style=\"display: block;\">\r\n" +
            "                  <div id=\"id44_Active\" class=\"Off\" style=\"display: block;\">\r\n" +
            "                     <button id=\"id44_Input\" name=\"id44\" style=\"opacity: 0;\" class=\"GuiSelector\"></button>\r\n" +
            "                     <div id=\"id44_IconEnvelop\">\r\n" +
            "                        <div id=\"id44_Icon\" class=\"Icon\"></div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>"
        );
    }

    public __IgnoretestAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const imagebutton : ImageButton = new ImageButton();
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockBaseViewer();
            imagebutton.InstanceOwner(viewer);
            manager.Add(imagebutton);
            assert.onGuiComplete(imagebutton,
                () : void => {
                    imagebutton.GuiType(ImageButtonType.BLUE);
                    imagebutton.Visible(true);
                    imagebutton.Enabled(true);
                    imagebutton.IsSelected(true);
                    manager.setActive(imagebutton, true);
                    imagebutton.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                        imagebutton.StyleClassName("test");
                        assert.equal(imagebutton.getEvents().Exists("onchange"), true);
                    });
                },
                () : void => {
                    assert.equal(imagebutton.IsSelected(), true);
                    $done();
                }, viewer);
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
