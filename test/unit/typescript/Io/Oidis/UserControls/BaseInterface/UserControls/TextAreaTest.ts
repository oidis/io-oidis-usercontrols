/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import {
    TextAreaType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/TextAreaType.js";
import {
    TextFieldType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/TextFieldType.js";
import { TextArea } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/TextArea.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";

class MockBasePanelViewer extends BasePanelViewer {
}

export class TextAreaTest extends UnitTestRunner {

    public testConstructor() : void {
        const area : TextArea = new TextArea(TextAreaType.BLUE, "id56");
        assert.equal(area.Id(), "id56");
    }

    public testGuiType() : void {
        const area : TextArea = new TextArea(TextAreaType.GENERAL);
        assert.equal(area.GuiType(TextAreaType.BLUE), TextAreaType.BLUE);
    }

    public testHeight() : void {
        const area : TextArea = new TextArea(TextAreaType.GENERAL);
        assert.equal(area.Height(500), 500);
    }

    public testMaxWidth() : void {
        const area : TextArea = new TextArea(TextAreaType.GREEN);
        assert.equal(area.MaxWidth(), -1);
        assert.equal(area.MaxWidth(400), 400);
        const area2 : TextArea = new TextArea(TextAreaType.BLUE);
        assert.equal(area2.MaxWidth(-1), -1);
    }

    public testMaxHeight() : void {
        const area : TextArea = new TextArea(TextAreaType.BLUE);
        assert.equal(area.MaxHeight(), -1);
        assert.equal(area.MaxHeight(1000), 1000);
    }

    public testResizeableType() : void {
        const area : TextArea = new TextArea(TextAreaType.GREEN);
        assert.equal(area.ResizeableType(ResizeableType.HORIZONTAL), ResizeableType.HORIZONTAL);
    }

    public testReadOnly() : void {
        const area : TextArea = new TextArea(TextAreaType.GENERAL);
        assert.equal(area.ReadOnly(), false);
        assert.equal(area.ReadOnly(true), true);
    }

    public testLengthLimit() : void {
        const area : TextArea = new TextArea(TextAreaType.BLUE);
        assert.equal(area.LengthLimit(), -1);
        assert.equal(area.LengthLimit(-1), -1);
        assert.equal(area.LengthLimit(1000), 1000);
    }

    public testValue() : void {
        const area : TextArea = new TextArea(TextAreaType.RED);
        assert.equal(area.Value(), "");
        assert.equal(area.Value("test"), "test");
        assert.equal(area.Value("6"), 6);
        assert.equal(area.Value("111111111122222222223333333333"), "111111111122222222223333333333");
    }

    public testFocus() : void {
        const textarea : TextArea = new TextArea(TextFieldType.GREEN, "id44");
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        textarea.DisableAsynchronousDraw();
        Echo.Print(textarea.Draw());
        TextArea.Focus(textarea);
        assert.equal(manager.IsActive(textarea), true);
        assert.deepEqual(ElementManager.getElement(textarea.Id() + "_Input").outerHTML,
            "<textarea id=\"id44_Input\" name=\"id44\" style=\"width: 100px; height: 100px;\"></textarea>");
    }

    public __IgnoretestEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const textarea : TextArea = new TextArea(TextAreaType.GENERAL, "id55");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            textarea.InstanceOwner(viewer);
            textarea.MaxHeight(400);
            textarea.MaxWidth(400);
            assert.onGuiComplete(textarea,
                () : void => {
                    const args : ResizeEventArgs = new ResizeEventArgs();
                    EventsManager.getInstanceSingleton().setEvent(textarea, EventType.ON_RESIZE,
                        ($eventArgs : ResizeEventArgs) : void => {
                            args.Width(400);
                            args.Height(400);
                            assert.equal(textarea.getEvents().Exists(EventType.ON_RESIZE), true);
                        });
                },
                () : void => {
                    EventsManager.getInstanceSingleton().FireEvent(textarea, EventType.ON_RESIZE, true);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testEventOnClick() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const textarea : TextArea = new TextArea(TextAreaType.GENERAL, "id55");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            textarea.InstanceOwner(viewer);
            textarea.MaxHeight(400);
            textarea.MaxWidth(400);
            assert.equal(textarea.getEvents().Exists(EventType.ON_CLICK), false);
            textarea.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(textarea.Draw());
            Echo.Print(textarea.Draw());
            const args : ResizeEventArgs = new ResizeEventArgs();
            EventsManager.getInstanceSingleton().setEvent(textarea, EventType.ON_CLICK,
                ($eventArgs : ResizeEventArgs) : void => {
                    textarea.Value("Write some text");
                    assert.equal(textarea.getEvents().Exists(EventType.ON_CLICK), true);
                });
            $done();
        };
    }

    public etestEventOnScroll() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const textarea : TextArea = new TextArea(TextAreaType.GENERAL, "id55");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            textarea.InstanceOwner(viewer);
            textarea.MaxHeight(400);
            textarea.MaxWidth(400);
            assert.equal(textarea.getEvents().Exists(EventType.ON_SCROLL), false);
            textarea.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(textarea.Draw());
            Echo.Print(textarea.Draw());
            const args : ScrollEventArgs = new ScrollEventArgs();
            args.Position(200);
            EventsManager.getInstanceSingleton().setEvent(textarea, EventType.ON_SCROLL,
                ($eventArgs : ScrollEventArgs) : void => {
                    textarea.Value("Write some text");
                    assert.equal(textarea.getEvents().Exists(EventType.ON_SCROLL), true);
                });
            $done();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
