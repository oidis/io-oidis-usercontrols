/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { CheckBox } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/CheckBox.js";

class MockBaseViewer extends BaseViewer {
}

class MockCheckBox extends CheckBox {
    public testexcludeSerializationData() : string[] {
        return this.excludeSerializationData();
    }

    public testexcludCacheData() : string[] {
        return this.excludeCacheData();
    }
}

export class CheckBoxTest extends UnitTestRunner {

    public __IgnoretestConstructor() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const checkbox : CheckBox = new CheckBox("id2000");
            const viewer : BaseViewer = new MockBaseViewer();
            checkbox.InstanceOwner(viewer);
            assert.equal(checkbox.Id(), "id2000");
            assert.onGuiComplete(checkbox,
                () : void => {
                    checkbox.Text("Testing of CheckBox");
                    checkbox.Value(true);
                    checkbox.Checked(false);
                    checkbox.DisableAsynchronousDraw();
                    Echo.Print(checkbox.Draw());
                    assert.equal(checkbox.Text(), "Testing of CheckBox");
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testTextSecond() : void {
        const checkbox : CheckBox = new CheckBox("id2001");
        assert.equal(checkbox.Text("testOfCheckBox"), "testOfCheckBox");
        const checkbox2 : CheckBox = new CheckBox("id2002");
        assert.equal(checkbox2.Text(), "id2002");
        this.initSendBox();
    }

    public testCheckedSecond() : void {
        const checkbox : CheckBox = new CheckBox();
        assert.equal(checkbox.Checked(), null);
        assert.equal(checkbox.Checked(false), false);
        assert.equal(checkbox.Checked(true), true);
        this.initSendBox();
    }

    public testValueSecond() : void {
        const checkbox : CheckBox = new CheckBox();
        assert.equal(checkbox.Value(), null);
        assert.equal(checkbox.Value(false), false);
        assert.equal(checkbox.Value(false), false);
        assert.equal(checkbox.Value(true), true);
        assert.equal(checkbox.Value(true), true);
        this.initSendBox();
    }

    public testgetArgsSecond() : void {
        const checkbox : CheckBox = new CheckBox();
        assert.equal(checkbox.getArgs().length, 13);
        this.initSendBox();
    }

    public testsetArg() : void {
        const checkbox : CheckBox = new CheckBox("id2007");
        checkbox.setArg(<IGuiCommonsArg>{
            name : "newAge",
            type : GuiCommonsArgType.TEXT,
            value: ""
        }, true);
        assert.equal(checkbox.getArgs().toString(), [
            {name: "Id", type: "Text", value: "id2007"},
            {name: "StyleClassName", type: "Text", value: ""},
            {name: "Enabled", type: "Bool", value: true},
            {name: "Visible", type: "Bool", value: true},
            {name: "Width", type: "Number", value: 0},
            {name: "Height", type: "Number", value: 0},
            {name: "Top", type: "Number", value: 0},
            {name: "Left", type: "Number", value: 0},
            {name: "Title", type: "Text", value: ""},
            {name: "Value", type: "Text", value: null},
            {name: "Error", type: "Bool", value: false},
            {name: "TabIndex", type: "Number", value: null},
            {name: "Text", type: "Text", value: "id2007"}
        ]);

        const checkbox2 : CheckBox = new CheckBox("id2008");
        checkbox2.Text("TextValue");
        checkbox2.Value(true);
        checkbox2.TabIndex(4);
        checkbox2.StyleClassName("style");
        checkbox2.setArg(<IGuiCommonsArg>{
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: "TextValue"
        }, false);
        assert.equal(checkbox2.getArgs().toString(), [
            {name: "Id", type: "Text", value: "id2008"},
            {name: "StyleClassName", type: "Text", value: "style"},
            {name: "Enabled", type: "Bool", value: true},
            {name: "Visible", type: "Bool", value: true},
            {name: "Width", type: "Number", value: 0},
            {name: "Height", type: "Number", value: 0},
            {name: "Top", type: "Number", value: 0},
            {name: "Left", type: "Number", value: 0},
            {name: "Title", type: "Text", value: ""},
            {name: "Value", type: "Text", value: true},
            {name: "Error", type: "Bool", value: false},
            {name: "TabIndex", type: "Number", value: 4},
            {name: "Text", type: "Text", value: "TextValue"}
        ]);
        this.initSendBox();
    }

    public testToggleChecked() : void {
        const box : CheckBox = new CheckBox("id80");
        box.DisableAsynchronousDraw();
        Echo.Print(box.Draw());
        CheckBox.ToggleChecked(box);
        assert.equal(ElementManager.getClassName(box.Id() + "_Check"), "Checked");
        assert.equal(box.Checked(), true);
        this.initSendBox();
    }

    public testText() : void {
        const box : CheckBox = new CheckBox("id81");
        assert.equal(box.Text(), "id81");
        assert.equal(box.Text("TestOfCheckBox"), "TestOfCheckBox");
        this.initSendBox();
    }

    public testValue() : void {
        const box : CheckBox = new CheckBox();
        assert.equal(box.Value(true), true);
        this.initSendBox();
    }

    public testgetArgs() : void {
        const box : CheckBox = new CheckBox("id84");
        box.setArg(<IGuiCommonsArg>{
            name : "Text",
            type : "testType",
            value: "testValue"
        }, true);
        box.Checked(true);
        box.Text("Text");
        box.Value(true);
        box.getSize().Width(1000);
        box.getSize().Height(600);
        box.TabIndex(10);
        box.StyleClassName("Class");
        assert.equal(box.getArgs().toString(),
            [
                {name: "Id", type: "Text", value: "id84"},
                {name: "StyleClassName", type: "Text", value: "Class"},
                {name: "Enabled", type: "Bool", value: true},
                {name: "Visible", type: "Bool", value: true},
                {name: "Width", type: "Number", value: 0},
                {name: "Height", type: "Number", value: 0},
                {name: "Top", type: "Number", value: 0},
                {name: "Left", type: "Number", value: 0},
                {name: "Title", type: "Text", value: ""},
                {name: "Value", type: "Text", value: true},
                {name: "Error", type: "Bool", value: false},
                {name: "TabIndex", type: "Number", value: 10},
                {name: "Text", type: "Text", value: "Text"}
            ]);
        this.initSendBox();
    }

    public testserializationData() : void {
        const checkbox : MockCheckBox = new MockCheckBox("id33");
        assert.equal(checkbox.testexcludeSerializationData().toString(),
            "objectNamespace,objectClassName,options,availableOptionsList,parent,owner,guiPath,visible,enabled," +
            "prepared,completed,interfaceClassName,styleClassName,containerClassName,loaded,asyncDrawEnabled,contentLoaded," +
            "waitFor,outputEndOfLine,innerHtmlMap,events,changed,error,tabIndex,autofillPersistence,valuesPersistence," +
            "errorFlags,isPersistent,text,checked");
        this.initSendBox();
    }

    public testexcludeData() : void {
        const checkbox : MockCheckBox = new MockCheckBox("id44");
        assert.equal(checkbox.testexcludCacheData().toString(),
            "options,availableOptionsList,events,childElements,waitFor,cached,prepared,completed,parent,owner," +
            "guiPath,interfaceClassName,styleClassName,containerClassName,innerHtmlMap,loaded,title,changed," +
            "notification,selectorEvents,tabIndex");
        this.initSendBox();
    }

    public __IgnoretestChecked() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const checkbox : CheckBox = new CheckBox("id44");
            const viewer : BaseViewer = new BaseViewer();
            checkbox.InstanceOwner(viewer);
            assert.onGuiComplete(checkbox,
                () : void => {
                    checkbox.getEvents().setOnChange(() : void => {
                        // empty handler
                    });
                    checkbox.Checked(true);
                    checkbox.Visible(true);
                    checkbox.Enabled(true);
                    assert.equal(checkbox.Checked(), true);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestBlurEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const checkbox : CheckBox = new CheckBox("id45");
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new BaseViewer();
            checkbox.InstanceOwner(viewer);
            manager.Add(checkbox);
            assert.onGuiComplete(checkbox,
                () : void => {
                    manager.setActive(checkbox, true);
                    checkbox.Visible(true);
                    checkbox.Enabled(true);
                    checkbox.getEvents().setOnBlur(() : void => {
                        LogIt.Debug("this is event test for blur");
                    });
                    assert.equal(checkbox.Enabled(), true);
                    assert.equal(checkbox.getEvents().getAll().Length(), 8);
                },
                () : void => {
                    manager.Clear();
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestFocusEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const checkbox : CheckBox = new CheckBox("id46");
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new BaseViewer();
            checkbox.InstanceOwner(viewer);
            manager.Add(checkbox);
            assert.onGuiComplete(checkbox,
                () : void => {
                    manager.setActive(checkbox, true);
                    checkbox.Visible(true);
                    checkbox.Enabled(true);
                    checkbox.Value(true);
                    checkbox.getEvents().setOnFocus(() : void => {
                        LogIt.Debug("this is event test for focus");
                        assert.equal(checkbox.getEvents().Exists(EventType.ON_FOCUS), true);
                    });
                },
                () : void => {
                    assert.equal(checkbox.Value(), true);
                    assert.equal(checkbox.Enabled(), true);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }
}
