/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";

class MockBasePanelViewer extends BasePanelViewer {
}

export class VerticalPanelHolderTest extends UnitTestRunner {

    public testConstructor() : void {
        const viewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        const basepanelviewer : BasePanelViewer = new BasePanelViewer(viewerArgs);
        // const verticalpanel : VerticalPanelHolderStrategy = new VerticalPanelHolderStrategy(basepanelviewer, viewerArgs,
        //     BasePanelHolderEventType.ON_CHANGE, "id80");
        // assert.equal(verticalpanel.Id(), "id80");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
