/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { IconType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/IconType.js";
import { Icon } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Icon.js";

class MockIcon extends Icon {
    public testSetterValidator() : boolean {
        return this.styleClassNameSetterValidator("Icon");
    }
}

class MockViewer extends BaseViewer {
}

export class IconTest extends UnitTestRunner {
    public testConstructor() : void {
        const icon : Icon = new Icon(IconType.GENERAL, "id500");
        assert.equal(icon.Id(), "id500");
        this.initSendBox();
    }

    public testIconType() : void {
        const icon : Icon = new Icon(IconType.BLUE_SQUARE);
        assert.equal(icon.IconType(), IconType.BLUE_SQUARE);

        const icon2 : Icon = new Icon(IconType.GENERAL);
        assert.equal(icon2.IconType(IconType.GENERAL), IconType.GENERAL);

        const icon3 : Icon = new Icon(IconType.BLACK_SQUARE, "id700");
        icon3.Visible(true);
        assert.equal(icon3.ToString("", true),
            "Io.Oidis.UserControls.BaseInterface.UserControls.Icon (id700)");
        assert.equal(icon3.IconType(), IconType.BLACK_SQUARE);
        this.initSendBox();
    }

    public teststyleClassNameSetterValidator() : void {
        const icon : MockIcon = new MockIcon(IconType.GENERAL);
        assert.equal(icon.testSetterValidator(), true);
        this.initSendBox();
    }

    public __IgnoretestIconAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const icon : Icon = new Icon(IconType.GENERAL);
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockViewer();
            icon.InstanceOwner(viewer);
            manager.Add(icon);
            icon.Visible(true);
            icon.Enabled(true);
            icon.Value("picture");
            assert.onGuiComplete(icon,
                () : void => {
                    assert.equal(icon.IconType(), IconType.GENERAL);
                    assert.equal(icon.Visible(), true);
                    assert.equal(manager.Exists(icon), true);
                    this.initSendBox();
                }, $done, viewer);
        };
    }
}
