/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { Link } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Link.js";

class MockBasePanelViewer extends BasePanelViewer {
}

class MockLink extends Link {
    public testExcludSerializationData() : string[] {
        return this.excludeSerializationData();
    }

    public testExcludCacheData() : string[] {
        return this.excludeCacheData();
    }
}

export class LinkTest extends UnitTestRunner {

    public testConstructor() : void {
        const link : Link = new Link("id1");
        assert.equal(link.Id(), "id1");
    }

    public testEnabled() : void {
        const link : Link = new Link();
        assert.equal(link.Enabled(), true);
        assert.equal(link.Enabled(false), false);
    }

    public testReloadTo() : void {
        const link : Link = new Link();
        assert.equal(link.ReloadTo("www.wuiframework.com"), "http://www.wuiframework.com");
    }

    public testOpenInNewWindow() : void {
        const link : Link = new Link();
        assert.equal(link.OpenInNewWindow(), false);
        assert.equal(link.OpenInNewWindow(true), true);
    }

    public testTabIndex() : void {
        const link : Link = new Link();
        assert.equal(link.TabIndex(5), 5);
    }

    public testFocus() : void {
        const link : Link = new Link();
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        link.DisableAsynchronousDraw();
        Echo.Print(link.Draw());
        Link.Focus(link, true);
        assert.equal(manager.IsActive(link), true);
    }

    public testBlur() : void {
        const link : Link = new Link();
        link.DisableAsynchronousDraw();
        Echo.Print(link.Draw());
        Link.Blur();
        assert.equal(ElementManager.getClassName(link.Id() + "_Enabled"), GeneralCssNames.OFF);
    }

    public testEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const link : Link = new Link("id3");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            const args : EventArgs = new EventArgs();
            link.InstanceOwner(viewer);
            link.OpenInNewWindow(true);
            link.Visible(true);
            link.Enabled(true);
            link.Text("www.wuiframework.com");
            assert.equal(link.getEvents().Exists(EventType.ON_CHANGE), false);
            link.DisableAsynchronousDraw();
            StaticPageContentManager.BodyAppend(link.Draw());
            Echo.Print(link.Draw());

            link.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                if (link.Changed()) {
                    link.ReloadTo("www.wuiframework.com");
                }
                assert.equal(link.Changed(), true);
                assert.equal(link.ReloadTo(), "www.wuiframework.com");
                assert.equal(link.getEvents().Exists(EventType.ON_CHANGE), true);
            });
            this.initSendBox();
            $done();
        };
    }

    public testexcludSerializationData() : void {
        const link : MockLink = new MockLink();
        assert.deepEqual(link.testExcludSerializationData(),
            [
                "objectNamespace", "objectClassName", "options", "availableOptionsList", "parent", "owner", "guiPath", "visible",
                "enabled", "prepared", "completed", "interfaceClassName", "styleClassName", "containerClassName", "loaded",
                "asyncDrawEnabled", "contentLoaded", "waitFor", "outputEndOfLine", "innerHtmlMap", "events", "changed", "selector"
            ]);
    }

    public testexcludChacheData() : void {
        const link : MockLink = new MockLink();
        assert.deepEqual(link.testExcludCacheData(),
            [
                "options", "availableOptionsList", "events", "childElements", "waitFor", "cached", "prepared", "completed", "parent",
                "owner", "guiPath", "interfaceClassName", "styleClassName", "containerClassName", "innerHtmlMap", "loaded", "title",
                "changed", "text", "selector"
            ]);
    }

    protected tearDown() : void {
        this.initSendBox();
        console.clear(); // eslint-disable-line no-console
    }
}
