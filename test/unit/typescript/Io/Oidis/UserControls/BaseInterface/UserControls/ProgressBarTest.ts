/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import {
    ProgressBarType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/ProgressBarType.js";
import { ProgressBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/ProgressBar.js";

class MockViewer extends BaseViewer {
}

export class ProgressBarTest extends UnitTestRunner {

    public testConstructor() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL, "id20");
        assert.equal(progressbar.Id(), "id20");
    }

    public testgetEvents() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        const handler : any = () : void => {
            // test event handler
        };
        progressbar.getEvents().setEvent("testOfProgressBar", handler);
        assert.equal(progressbar.getEvents().Exists("testOfProgressBar"), true);
    }

    public __IgnoretestEventsAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GREEN);
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockViewer();
            progressbar.InstanceOwner(viewer);
            assert.onGuiComplete(progressbar,
                () : void => {
                    progressbar.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
                        progressbar.StyleClassName("BackStyle");
                        assert.equal($eventArgs.Owner(), progressbar);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testGuiType() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        assert.equal(progressbar.GuiType(), "");
        const progressbar2 : ProgressBar = new ProgressBar();
        assert.equal(progressbar2.GuiType(ProgressBarType.RED), "Red");
    }

    public testValue() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        assert.equal(progressbar.Value(), 0);
        assert.equal(progressbar.Value(1000), 100);
        assert.equal(progressbar.Value(50), 50);
        assert.equal(progressbar.Value(undefined), 50);
    }

    public testRangeStart() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        assert.equal(progressbar.RangeStart(), 0);
        assert.equal(progressbar.RangeStart(250), 250);
    }

    public testRangeEnd() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        assert.equal(progressbar.RangeEnd(), 100);
        assert.equal(progressbar.RangeEnd(250), 250);
    }

    public testWidth() : void {
        const progressbar : ProgressBar = new ProgressBar(ProgressBarType.GENERAL);
        assert.equal(progressbar.Width(), 250);
        assert.equal(progressbar.Width(50), 50);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
