/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { Label } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Label.js";

class MockBasePanelViewer extends BasePanelViewer {
}

class MockLabel extends Label {
    public testExcludeCacheData() : string[] {
        return this.excludeCacheData();
    }
}

export class LabelTest extends UnitTestRunner {
    public testConstructor() : void {
        const label : Label = new Label("testOfLabel");
        assert.equal(label.Text(), "testOfLabel");
    }

    public testgetEvents() : void {
        const label : Label = new Label("Label");

        const handler : any = () : void => {
            // test event handler
        };
        label.getEvents().setEvent("test", handler);
        assert.equal(label.getEvents().Exists("test"), true);
    }

    public testText() : void {
        const label : Label = new Label("Label");
        (<any>GuiCommons).loaded = true;
        assert.equal(label.Text("ContentLabelElement"), "ContentLabelElement");
    }

    public testValue() : void {
        const label : Label = new Label();
        const value : any = () : void => {
            const text : string = "Hello!!!";
        };
        assert.equal(label.Value("value"), null);
    }

    public testEnabled() : void {
        const label : Label = new Label("test");
        label.getGuiOptions().Add(GuiOptionType.DISABLE);
        label.Visible(true);
        assert.equal(label.Enabled(true), true);
        const label2 : Label = new Label();
        assert.equal(label2.Enabled(false), false);
    }

    public testgetArgs() : void {
        const label : Label = new Label();
        label.setArg(<IGuiCommonsArg>{
            name : "Labeltest",
            type : GuiCommonsArgType.TEXT,
            value: "Content"
        }, true);
        assert.equal(label.getArgs().length, 10);
    }

    public __IgnoretestLabelEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const label : Label = new Label("354");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            label.InstanceOwner(viewer);
            label.Visible(true);
            label.Enabled(true);
            const args : MouseEventArgs = new MouseEventArgs();
            assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), false);
            label.DisableAsynchronousDraw();
            Echo.Print(label.Draw());
            assert.onGuiComplete(label,
                () : void => {
                    label.getEvents().setOnChange(() : void => {
                        label.Changed();
                        assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), true);
                        assert.equal(label.Changed(), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestLabelEventSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const label : Label = new Label("354");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            label.InstanceOwner(viewer);
            label.Visible(true);
            label.Enabled(true);
            const args : MouseEventArgs = new MouseEventArgs();
            assert.equal(label.getEvents().Exists(EventType.ON_LOAD), false);

            assert.onGuiComplete(label,
                () : void => {
                    label.getEvents().setOnLoad(() : void => {
                        label.Changed();
                        label.Text("ChangeText");
                        assert.equal(label.getEvents().Exists(EventType.ON_LOAD), true);
                        assert.equal(label.Changed(), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestLabelEventThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const label : Label = new Label("Name:");
            const label2 : Label = new Label("Last name:");
            const viewer : BasePanelViewer = new MockBasePanelViewer();
            label.InstanceOwner(viewer);
            label.Visible(true);
            label.Enabled(true);
            label2.Value("value");
            label.DisableAsynchronousDraw();
            Echo.Print(label.Draw());
            assert.equal(label.Value(), "value");
            assert.equal(label.getEvents().Exists(EventType.ON_CHANGE), false);
            this.initSendBox();
            $done();
        };
    }

    public testexcludeCacheData() : void {
        const label : MockLabel = new MockLabel();
        assert.deepEqual(label.testExcludeCacheData(), [
            "options", "availableOptionsList", "events",
            "childElements", "waitFor", "cached", "prepared", "completed",
            "parent", "owner", "guiPath", "interfaceClassName",
            "styleClassName", "containerClassName", "innerHtmlMap", "loaded",
            "title", "changed", "text"
        ]);
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
