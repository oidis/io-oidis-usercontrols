/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileUpload } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/FileUpload.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class FileUploadTest extends UnitTestRunner {
    public testgetEvents() : void {
        const fileupload : FileUpload = new FileUpload();
        const handler : any = () : void => {
            // test event handler
        };
        fileupload.getEvents().setEvent("test", handler);
        assert.equal(fileupload.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testsetOpenElement() : void {
        const fileupload : FileUpload = new FileUpload();
        fileupload.setOpenElement("500");
        this.initSendBox();
    }

    public setDropZone() : void {
        const fileupload : FileUpload = new FileUpload();
        fileupload.setDropZone("id");
    }
}
