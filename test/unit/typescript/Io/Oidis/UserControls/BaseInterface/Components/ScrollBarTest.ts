/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { ScrollBarEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/ScrollBarEventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { PositionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PositionType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ScrollBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ScrollBar.js";
import { Button } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Button.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockGuiCommons extends GuiCommons {
}

class MockBaseViewer extends BaseViewer {
}

class MockScrollBar extends ScrollBar {
    public testexcludeSerializationData() : string[] {
        return this.excludeSerializationData();
    }

    public testexcludeCacheData() : string[] {
        return this.excludeCacheData();
    }
}

export class ScrollBarTest extends UnitTestRunner {
    public testgetEvents() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
        const handler : any = () : void => {
            // test event handler
        };
        scrollbar.getEvents().setEvent("test", handler);
        assert.equal(scrollbar.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testGuiType() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
        assert.equal(scrollbar.GuiType(), OrientationType.VERTICAL);
        this.initSendBox();
    }

    public testOrientationType() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        assert.equal(scrollbar.OrientationType(), OrientationType.HORIZONTAL);
        this.initSendBox();
    }

    public __IgnoretestSize() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id5544");
            const viewer : BaseViewer = new MockBaseViewer();
            const manager : GuiObjectManager = new GuiObjectManager();
            scrollbar.InstanceOwner(viewer);
            assert.onGuiComplete(scrollbar,
                () : void => {
                    scrollbar.Size(300);
                    scrollbar.Visible(true);
                    scrollbar.Enabled(true);
                    scrollbar.DisableAsynchronousDraw();
                    Echo.Print(scrollbar.Draw());
                    assert.equal(ElementManager.IsVisible("id5544"), true);
                    assert.equal(scrollbar.Size(), 300);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testsetArgsDefault() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        scrollbar.setArg(<IGuiCommonsArg>{
            name : "testName",
            type : "testType",
            value: "testValue"
        }, true);
        assert.equal(scrollbar.getArgs().length, 11);
        this.initSendBox();

    }

    public testsetArg() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id381");
        scrollbar.setArg(<IGuiCommonsArg>{
            name : "Width",
            type : "Number",
            value: 500
        }, true);
        assert.equal(scrollbar.getArgs().length, 11);
        this.initSendBox();
    }

    public testsetArgsSecond() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
        scrollbar.setArg(<IGuiCommonsArg>{
            name : "GuiType",
            type : "Text",
            value: OrientationType.VERTICAL
        });
        assert.deepEqual(scrollbar.getArgs().length, 11);
        this.initSendBox();
    }

    public testsetArgsThird() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        scrollbar.setArg(<IGuiCommonsArg>{
            name : "Height",
            type : "Number",
            value: 800
        });
        assert.deepEqual(scrollbar.getArgs().length, 11);
        this.initSendBox();
    }

    public testsetArgsFourth() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        const size : Size = new Size();
        size.Width(45);
        size.Height(20);
        scrollbar.setArg(<IGuiCommonsArg>{
            name : "Size",
            type : "Number",
            value: 200
        });
        assert.deepEqual(scrollbar.getArgs().length, 11);
        this.initSendBox();
    }

    public testArrowOn() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id300");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.ArrowOn(scrollbar, "id300");
        assert.equal(ElementManager.getClassName(scrollbar.Id()), GeneralCssNames.ON);
        assert.equal(ElementManager.getClassName(scrollbar.Id()), GeneralCssNames.ON);
        this.initSendBox();
    }

    public testArrowOnElse() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id400");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        GuiObjectManager.getInstanceSingleton().setActive(scrollbar, true);
        ScrollBar.ArrowOn(scrollbar, "id400");
        assert.equal(ElementManager.getClassName(scrollbar.Id()), "ScrollBar");
        this.initSendBox();
    }

    public testArrowOff() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id301");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.ArrowOff(scrollbar, "id301");
        assert.equal(ElementManager.getClassName(scrollbar.Id()), GeneralCssNames.OFF);
        this.initSendBox();
    }

    public testArrowOffElse() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id302");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        GuiObjectManager.getInstanceSingleton().setActive(scrollbar, true);
        ScrollBar.ArrowOff(scrollbar, "id302");
        assert.equal(ElementManager.getClassName(scrollbar.Id()), GeneralCssNames.OFF);
        this.initSendBox();
    }

    public testButtonOn() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.ButtonOn(scrollbar);
        assert.equal(ElementManager.getClassName(scrollbar.Id() + "_Button"), GeneralCssNames.ON);
        this.initSendBox();
    }

    public testButtonOnElse() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        GuiObjectManager.getInstanceSingleton().setActive(scrollbar, true);
        ScrollBar.ButtonOn(scrollbar);
        assert.equal(ElementManager.getClassName(scrollbar.Id() + "_Button"), "Off");
        this.initSendBox();
    }

    public __IgnoretestButtonOff() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.ButtonOff(scrollbar);
        assert.equal(ElementManager.getClassName(scrollbar.Id() + "_Button"), GeneralCssNames.OFF);
        this.initSendBox();
    }

    public testButtonOffElse() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        GuiObjectManager.getInstanceSingleton().setActive(scrollbar, true);
        ScrollBar.ButtonOff(scrollbar);
        assert.equal(ElementManager.getClassName(scrollbar.Id() + "_Button"), "Off");
        this.initSendBox();
    }

    public __IgnoretestMoveMinusNext() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id304");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.MoveTo(scrollbar, 100);
        ScrollBar.MoveMinus(scrollbar, 50);
        assert.equal(scrollbar.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "   <div id=\"id304_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id304\" class=\"ScrollBar\" style=\"display: block;\">\r\n" +
            "         <div id=\"id304_Type\" guiType=\"ScrollBar\" class=\"Horizontal\">\r\n" +
            "            <div class=\"UpArrow\">\r\n" +
            "               <div id=\"id304_UpArrow\" class=\"Off\">\r\n" +
            "                  <div class=\"Icon\"></div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "            <div id=\"id304_Tracker\" class=\"Tracker\" style=\"width: 300px;\">\r\n" +
            "               <div id=\"id304_ButtonEnvelop\" class=\"Button\">\r\n" +
            "                  <div id=\"id304_Button\" class=\"Off\">\r\n" +
            "                     <div id=\"id304_ButtonTop\" class=\"Top\"></div>\r\n" +
            "                     <div id=\"id304_ButtonCenter\" class=\"Center\">\r\n" +
            "                        <div id=\"id304_ButtonIcon\" class=\"Icon\"></div>\r\n" +
            "                     </div>\r\n" +
            "                     <div id=\"id304_ButtonBottom\" class=\"Bottom\"></div>\r\n" +
            "                  </div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "            <div class=\"DownArrow\">\r\n" +
            "               <div id=\"id304_DownArrow\" class=\"Off\">\r\n" +
            "                  <div class=\"Icon\"></div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        (<any>ScrollBar).moveStop();
        assert.equal(ElementManager.getCssValue(scrollbar.Id() + "_ButtonEnvelop", "left"), "127px");
        this.initSendBox();
    }

    public testMovePlusNext() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id305");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.MovePlus(scrollbar, 50);
        assert.equal(ElementManager.getCssValue(scrollbar.Id() + "_ButtonEnvelop", "top"), "0px");
        this.initSendBox();
    }

    public testMoveToNext() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id306");
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw() + "<div style=\"clear: both;\"></div>");
        ScrollBar.MoveTo(scrollbar, 50);
        assert.equal(ElementManager.getCssValue(scrollbar.Id() + "_ButtonEnvelop", "left"), "0px");
        this.initSendBox();
    }

    public __IgnoretestScrollEventHandlerNext() : void {
        const args : ScrollEventArgs = new ScrollEventArgs();
        args.Position(30);
        const manager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        ScrollBar.ScrollEventHandler(args, manager, reflection);
    }

    public __IgnoretestEventOnStart() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id66");
            //   scrollbar.getEvents().setOnButton(($eventArgs : ScrollEventArgs) : void => {
            //      assert.equal($eventArgs.OrientationType(), scrollbar.OrientationType());
            //     $done();
            //  });

            const viewer : BaseViewer = new MockBaseViewer();
            assert.onGuiComplete(scrollbar, () : void => {
                scrollbar.Visible(true);
                scrollbar.Enabled(true);
                scrollbar.setPosition(40, 40, PositionType.ABSOLUTE);
                const manager : GuiObjectManager = new GuiObjectManager();
                manager.setActive(scrollbar, true);
                assert.equal(manager.IsActive(scrollbar), true);
                const event : any = {altKey: true, button: 2};
                const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
                mouseEventArgs.Owner(scrollbar);

                (<any>ScrollBar).moveInit(mouseEventArgs, manager, Reflection.getInstance());
                EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                    mouseEventArgs);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestEventOnChangeSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id8889");

            const viewer : BaseViewer = new MockBaseViewer();
            assert.onGuiComplete(scrollbar, () : void => {
                scrollbar.Visible(true);
                scrollbar.Enabled(true);
                scrollbar.setPosition(40, 40, PositionType.ABSOLUTE);
                const manager : GuiObjectManager = new GuiObjectManager();
                // manager.setActive(scrollbar, true);
                const event : any = {altKey: true, button: 2};
                const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                moveEventArgs.Owner(scrollbar);

                (<any>ScrollBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                assert.equal(manager.IsActive(scrollbar), true);
                EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                    moveEventArgs);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestEventOnTracker() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id1006");

            const viewer : BaseViewer = new MockBaseViewer();
            assert.onGuiComplete(scrollbar, () : void => {
                scrollbar.Visible(true);
                scrollbar.Enabled(true);
                scrollbar.OrientationType(OrientationType.VERTICAL);
                scrollbar.setPosition(40, 40, PositionType.ABSOLUTE);

                const manager : GuiObjectManager = new GuiObjectManager();
                const event : any = {altKey: true, button: 2};
                const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
                mouseEventArgs.Owner(scrollbar);

                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(scrollbar);
                eventArgs.OrientationType(OrientationType.VERTICAL);

                (<any>ScrollBar).buttonMove(mouseEventArgs, manager, Reflection.getInstance());
                EventsManager.getInstanceSingleton().FireEvent(ScrollBar.ClassName(), ScrollBarEventType.ON_TRACKER, eventArgs);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestEventOnTrackerSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id2323");

            const viewer : BaseViewer = new MockBaseViewer();
            assert.onGuiComplete(scrollbar, () : void => {
                scrollbar.Visible(true);
                scrollbar.Enabled(true);
                scrollbar.OrientationType(OrientationType.HORIZONTAL);
                scrollbar.setPosition(40, 40, PositionType.ABSOLUTE);

                const manager : GuiObjectManager = new GuiObjectManager();
                const event : any = {altKey: true, button: 2, clientX: 20, clientY: 20, offsetX: 20, offsetY: 20};
                const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
                mouseEventArgs.Owner(scrollbar);

                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(scrollbar);
                eventArgs.OrientationType(OrientationType.HORIZONTAL);

                (<any>ScrollBar).buttonMove(mouseEventArgs, manager, Reflection.getInstance());
                EventsManager.getInstanceSingleton().FireEvent(scrollbar, ScrollBarEventType.ON_TRACKER, eventArgs);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestEventOnTrackerNotActive() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id2244");

            const viewer : BaseViewer = new MockBaseViewer();
            assert.onGuiComplete(scrollbar, () : void => {
                scrollbar.Visible(true);
                scrollbar.Enabled(true);
                scrollbar.OrientationType(OrientationType.HORIZONTAL);
                scrollbar.setPosition(40, 40, PositionType.ABSOLUTE);

                const manager : GuiObjectManager = new GuiObjectManager();
                manager.setActive(scrollbar, false);
                const event : any = {altKey: true, button: 2, clientX: 20, clientY: 20, offsetX: 20, offsetY: 20};
                const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
                mouseEventArgs.Owner(scrollbar);

                const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                eventArgs.Owner(scrollbar);
                eventArgs.OrientationType(OrientationType.HORIZONTAL);

                (<any>ScrollBar).buttonMove(mouseEventArgs, manager, Reflection.getInstance());
                EventsManager.getInstanceSingleton().FireEvent(scrollbar, ScrollBarEventType.ON_TRACKER, eventArgs);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestEventOnchange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id77");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);

            scrollbar.Visible(true);
            scrollbar.Enabled(true);
            scrollbar.DisableAsynchronousDraw();
            Echo.Print(scrollbar.Draw());

            const button : Button = new Button();
            button.setPosition(20, 20, PositionType.ABSOLUTE);
            button.Visible(true);
            button.Enabled(true);
            const button2 : Button = new Button();
            button2.setPosition(40, 15, PositionType.ABSOLUTE);
            button2.Visible(true);
            button2.Enabled(true);

            scrollbar.getChildElements().Add(button);
            scrollbar.getChildElements().Add(button2);

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(scrollbar, true);
            // ElementManager.TurnActive(scrollbar.Id() + "_Button");
            assert.equal(manager.IsActive(scrollbar), true);

            const event : any = {altKey: true, button: 2};
            const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
            mouseEventArgs.Owner(scrollbar);

            document.body.style.cursor = "default";

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                ($args : MoveEventArgs, manager) : void => {
                    assert.equal($args.getClassName(), "Io.Oidis.Gui.Events.Args.MouseEventArgs");
                    assert.equal($args.Owner(), scrollbar);
                    this.initSendBox();
                    $done();
                });

            (<any>ScrollBar).moveInit(mouseEventArgs, manager, Reflection.getInstance());
            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                mouseEventArgs);
        };
    }

    public __IgnoretestEventOncomplete() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id88");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);
            scrollbar.Visible(true);
            scrollbar.Enabled(true);
            scrollbar.DisableAsynchronousDraw();
            Echo.Print(scrollbar.Draw());

            const button : Button = new Button();
            button.setPosition(20, 20, PositionType.ABSOLUTE);
            button.Visible(true);
            button.Enabled(true);
            const button2 : Button = new Button();
            button2.setPosition(40, 15, PositionType.ABSOLUTE);
            button2.Visible(true);
            button2.Enabled(true);

            scrollbar.getChildElements().Add(button);
            scrollbar.getChildElements().Add(button2);
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(scrollbar, true);
            assert.equal(manager.IsActive(scrollbar), true);

            const event : any = {altKey: true, button: 2};
            const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
            mouseEventArgs.Owner(scrollbar);

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                ($args : MoveEventArgs, manager) : void => {
                    assert.equal($args.getClassName(), "Io.Oidis.Gui.Events.Args.MouseEventArgs");
                    assert.equal($args.Owner(), scrollbar);
                    this.initSendBox();
                    $done();
                });

            (<any>ScrollBar).moveInit(mouseEventArgs, manager, Reflection.getInstance());
            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                mouseEventArgs);

        };
    }

    public testResizeEventHandler() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id99");
        const gui : MockGuiCommons = new MockGuiCommons("66");
        const gui2 : MockGuiCommons = new MockGuiCommons("77");

        scrollbar.getChildElements().Add(gui);
        scrollbar.getChildElements().Add(gui2);
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        assert.equal(ElementManager.IsVisible("id99"), true);
        manager.setActive(scrollbar, true);
        assert.equal(manager.IsActive(scrollbar), true);

        const args : ResizeEventArgs = new ResizeEventArgs();
        args.Owner(scrollbar);
        args.AvailableHeight(200);
        args.AvailableWidth(200);
        args.Height(200);
        args.ScrollBarHeight(400);
        args.ScrollBarWidth(400);
        args.Width(200);
        ScrollBar.ResizeEventHandler(args, manager, Reflection.getInstance());
        this.initSendBox();
    }

    public __IgnoretestResizeEventHandlerSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id33");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);
            assert.onGuiComplete(scrollbar,
                () : void => {
                    scrollbar.Visible(false);
                    const gui : MockGuiCommons = new MockGuiCommons("66");
                    gui.Visible(false);
                    const gui2 : MockGuiCommons = new MockGuiCommons("77");
                    gui2.Visible(false);
                    scrollbar.getChildElements().Add(gui);
                    scrollbar.getChildElements().Add(gui2);
                    // scrollbar.Enabled(true);
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(scrollbar, true);
                    assert.equal(manager.IsActive(scrollbar), true);

                    const args : ResizeEventArgs = new ResizeEventArgs();
                    args.Owner(<any>new MockGuiCommons());
                    args.Owner(scrollbar);
                    args.AvailableHeight(200);
                    args.AvailableWidth(200);
                    args.Height(200);
                    args.ScrollBarHeight(400);
                    args.ScrollBarWidth(400);
                    args.Width(200);

                    ScrollBar.ResizeEventHandler(args, null, undefined);

                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestMoveInit() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollBar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
            const viewer : BaseViewer = new MockBaseViewer();
            scrollBar.InstanceOwner(viewer);
            assert.onGuiComplete(scrollBar,
                () : void => {
                    const mouseArgs : MouseEventArgs = new MouseEventArgs();
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(scrollBar, true);
                    new ElementEventsManager(scrollBar, scrollBar.Id() + "_Button");
                    // (<any>ScrollBar).MoveInit(MouseEventArgs, manager, Reflection.getInstance());
                    scrollBar.getEvents().setOnMouseMove(($eventArgs : MouseEventArgs) : void => {
                        $eventArgs.Owner(scrollBar);
                        $eventArgs.Type(EventType.ON_CHANGE);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestMoveInitSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollBar : ScrollBar = new ScrollBar(OrientationType.VERTICAL);
            const viewer : BaseViewer = new MockBaseViewer();
            scrollBar.InstanceOwner(viewer);
            assert.onGuiComplete(scrollBar,
                () : void => {
                    const mouseArgs : MouseEventArgs = new MouseEventArgs();
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(scrollBar, true);
                    new ElementEventsManager(scrollBar, scrollBar.Id() + "_Button");
                    // (<any>ScrollBar).MoveInit(MouseEventArgs, manager, Reflection.getInstance());
                    scrollBar.getEvents().setOnArrow(($eventArgs : ScrollEventArgs) : void => {
                        $eventArgs.OrientationType(OrientationType.VERTICAL);
                        $eventArgs.Type(EventType.ON_SCROLL);
                        $eventArgs.Position(400);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testMoveMinus() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id85");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MoveMinus(scrollbar, 250);
        this.initSendBox();
    }

    public testMoveMinusSecond() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id87");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MoveMinus(scrollbar, 50);
        this.initSendBox();
    }

    public testMoveMinusThird() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id82");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MoveMinus(scrollbar, -100);
        ScrollBar.MoveMinus(<any>(new Button()), 1); // -> refelection E
        this.initSendBox();
    }

    public __IgnoretestMoveMinusAsynchronous() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id89");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);

            scrollbar.Visible(true);
            scrollbar.Enabled(true);
            scrollbar.DisableAsynchronousDraw();
            Echo.Print(scrollbar.Draw());

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(scrollbar, true);
            ElementManager.TurnActive(scrollbar.Id() + "_Button");
            assert.equal(manager.IsActive(scrollbar), true);
            ScrollBar.MoveMinus(scrollbar, 1);

            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.initSendBox();
                $done();
            }, 500);
        };
    }

    public testMovePlus() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id810");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MovePlus(scrollbar, 250);
        ScrollBar.MovePlus(<any>(new Button()), 5);
        this.initSendBox();
    }

    public testMovePlusSecond() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id820");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MovePlus(scrollbar, 50);
        this.initSendBox();
    }

    public __IgnoretestMovePlusThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id820");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);
            scrollbar.Visible(true);
            scrollbar.Enabled(true);
            assert.onGuiComplete(scrollbar,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    manager.setActive(scrollbar, true);
                    ElementManager.TurnActive(scrollbar.Id() + "_Button");
                    assert.equal(manager.IsActive(scrollbar), true);
                    scrollbar.getEvents().setOnArrow(($eventArgs : ScrollEventArgs) : void => {
                        $eventArgs.Position(750);
                        $eventArgs.DirectionType(DirectionType.RIGHT);
                        $eventArgs.OrientationType(OrientationType.HORIZONTAL);
                        ScrollBar.MovePlus(scrollbar, 200);
                        assert.equal(scrollbar.getEvents().Exists("onarrow"), true);
                    });
                },
                () : void => {
                    // ScrollBar.MovePlus(scrollbar, -100);
                    assert.equal(scrollbar.Size().toString(), "300");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestMovePlusAsynchronous() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id22");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);

            scrollbar.Visible(true);
            scrollbar.Enabled(true);
            scrollbar.DisableAsynchronousDraw();
            Echo.Print(scrollbar.Draw());

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(scrollbar, true);
            ElementManager.TurnActive(scrollbar.Id() + "_Button");
            assert.equal(manager.IsActive(scrollbar), true);
            ScrollBar.MovePlus(scrollbar, 5);

            this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.initSendBox();
                $done();
            }, 800);
        };
    }

    public testMovePlusNoReflection() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id820");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        ScrollBar.MovePlus(scrollbar, 5);
        this.initSendBox();
    }

    public testMoveTo() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id85");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(false);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        ScrollBar.MoveTo(scrollbar, 250);
        ScrollBar.MoveTo(<any>(new Button()), 5);
        this.initSendBox();
    }

    public testMoveToSecond() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id87");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MoveTo(scrollbar, 50);
        this.initSendBox();
    }

    public testMoveToThird() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id82");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        Reflection.getInstance();
        ScrollBar.MoveTo(scrollbar, -100);
        this.initSendBox();
    }

    public testScrollEventHandler() : void {
        const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id31");
        const viewer : BaseViewer = new MockBaseViewer();
        scrollbar.InstanceOwner(viewer);

        const button : Button = new Button("id1");
        const button2 : Button = new Button("id2");
        button.Visible(true);
        button.Enabled(true);
        ElementManager.TurnOff(button.Id());
        button2.Visible(true);
        button2.Enabled(true);
        ElementManager.TurnOff(button2.Id());
        scrollbar.getChildElements().Add(button);
        scrollbar.getChildElements().Add(button2);

        scrollbar.Visible(true);
        scrollbar.Enabled(true);
        scrollbar.DisableAsynchronousDraw();
        Echo.Print(scrollbar.Draw());

        const args : ScrollEventArgs = new ScrollEventArgs();
        args.Owner(scrollbar);

        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(scrollbar, true);
        // ElementManager.TurnActive(scrollbar.Id() + "_Button");
        assert.equal(manager.IsActive(scrollbar), true);
        ScrollBar.ScrollEventHandler(args, manager, Reflection.getInstance());
        this.initSendBox();
    }

    public __IgnoretestScrollEventHandlerSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id44");
            const scrollbar : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "id31");
            const viewer : BaseViewer = new MockBaseViewer();
            scrollbar.InstanceOwner(viewer);
            assert.onGuiComplete(scrollbar,
                () : void => {
                    scrollbar.Visible(true);
                    const gui : MockGuiCommons = new MockGuiCommons("6");
                    gui.Visible(true);
                    const gui2 : MockGuiCommons = new MockGuiCommons("7");
                    gui2.Visible(true);
                    scrollbar.getChildElements().Add(gui);
                    scrollbar.getChildElements().Add(gui2);

                    scrollbar.DisableAsynchronousDraw();
                    Echo.Print(scrollbar.Draw());

                    const args : ScrollEventArgs = new ScrollEventArgs();
                    args.Owner(scrollbar);
                    args.Owner(gui);
                    args.Owner(gui2);
                    args.OrientationType(OrientationType.HORIZONTAL);

                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(scrollbar, true);
                    // ElementManager.TurnActive(scrollbar.Id() + "_Button");
                    assert.equal(manager.IsActive(scrollbar), true);
                    ScrollBar.ScrollEventHandler(args, manager, Reflection.getInstance());
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testScrollEventHandlerAsync() : void {
        const scroll : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL);
        const manager : GuiObjectManager = new GuiObjectManager();
        const args : ScrollEventArgs = new ScrollEventArgs();
        Reflection.getInstance();
        args.OrientationType(OrientationType.HORIZONTAL);
        scroll.Visible(true);
        manager.Add(scroll);
        const viewer : BaseViewer = new MockBaseViewer();
        scroll.InstanceOwner(viewer);
        scroll.DisableAsynchronousDraw();
        Echo.Printf(scroll.Draw());
        scroll.getEvents().setOnChange(($eventArgs : ScrollEventArgs) : void => {
            // ScrollBar.ScrollEventHandler(args, manager, Reflection.getInstance());
            Reflection.getInstance();
            $eventArgs.OrientationType(OrientationType.HORIZONTAL);
            assert.equal(scroll.Visible(), true);
            this.initSendBox();
        });
    }

    public testexcludeSerialization() : void {
        const scrollbar : MockScrollBar = new MockScrollBar(OrientationType.VERTICAL, "id7");
        assert.deepEqual(scrollbar.testexcludeSerializationData().toString(),
            "objectNamespace,objectClassName,options,availableOptionsList,parent,owner,guiPath,visible,enabled," +
            "prepared,completed,interfaceClassName,styleClassName,containerClassName,loaded,asyncDrawEnabled,contentLoaded," +
            "waitFor,outputEndOfLine,innerHtmlMap,events,size,upArrowEvents,downArrowEvents,trackerEvents,trackerButtonEvents," +
            "isInnerTriggeredState,isOutsideTriggeredState,elementPosition,buttonPositionValue");
        this.initSendBox();
    }

    public testexcludeCacheData() : void {
        const scrollbar : MockScrollBar = new MockScrollBar(OrientationType.HORIZONTAL, "id4");
        assert.deepEqual(scrollbar.testexcludeCacheData().toString(),
            "options,availableOptionsList,events,childElements,waitFor,cached,prepared,completed,parent,owner,guiPath," +
            "interfaceClassName,styleClassName,containerClassName,innerHtmlMap,loaded,upArrowEvents,downArrowEvents,trackerEvents," +
            "trackerButtonEvents,isInnerTriggeredState,isOutsideTriggeredState,elementPosition,buttonPositionValue");
        this.initSendBox();
    }
}
