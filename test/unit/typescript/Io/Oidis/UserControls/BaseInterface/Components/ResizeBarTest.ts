/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { CursorType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/CursorType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ResizeBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ResizeBar.js";
import { ScrollBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ScrollBar.js";
import { Button } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/Button.js";
import { TextArea } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/UserControls/TextArea.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseViewer extends BaseViewer {
}

export class ResizeBarTest extends UnitTestRunner {
    public testgetEvents() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
        const handler : any = () : void => {
            // test event handler
        };
        resizebar.getEvents().setEvent("test", handler);
        assert.equal(resizebar.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public __IgnoretestResizeAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
            const textarea : TextArea = new TextArea();
            const viewer : BaseViewer = new MockBaseViewer();
            textarea.InstanceOwner(viewer);
            eventArgs.Owner(textarea);
            assert.onGuiComplete(textarea,
                () : void => {
                    textarea.Visible(true);
                    textarea.Enabled(true);
                    eventArgs.DistanceX(1000);
                    eventArgs.DistanceY(1000);
                    textarea.getEvents().setOnResize(() : void => {
                        assert.equal(eventArgs.getDistanceX(), 1000);
                        assert.equal(eventArgs.getDistanceY(), 1000);
                        assert.equal(textarea.getEvents().Exists("onresize"), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer
            );
        };
    }

    public __IgnoretestResizeableType() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
            const args : BaseViewerArgs = new BaseViewerArgs();
            const viewer : BaseViewer = new BaseViewer(args);
            resizebar.InstanceOwner(viewer);
            resizebar.Visible(true);
            resizebar.Enabled(true);
            resizebar.DisableAsynchronousDraw();
            Echo.Print(resizebar.Draw());
            assert.equal(resizebar.ResizeableType(), ResizeableType.HORIZONTAL);

            const resizebar2 : ResizeBar = new ResizeBar(ResizeableType.NONE, "id183");
            const viewer2 : BaseViewer = new BaseViewer(args);
            resizebar2.InstanceOwner(viewer2);
            resizebar2.Visible(false);
            ResizeBar.TurnActive(resizebar2);
            resizebar2.DisableAsynchronousDraw();
            Echo.Print(resizebar.Draw());
            assert.equal(ElementManager.Exists("id183"), false);
            assert.equal(resizebar2.ResizeableType(), ResizeableType.NONE);
            this.initSendBox();
            $done();
        };
    }

    public testResizeEventHandler2() : void {
        const resize : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL, "id33");
        const viewer : BaseViewer = new MockBaseViewer();
        resize.InstanceOwner(viewer);
        resize.Visible(true);
        resize.Enabled(true);

        const button : Button = new Button("66");
        const button2 : Button = new Button("77");
        button.Visible(true);
        button.Enabled(true);
        button2.Visible(true);
        button2.Enabled(true);
        resize.getChildElements().Add(button);
        resize.getChildElements().Add(button2);
        resize.DisableAsynchronousDraw();
        Echo.Print(resize.Draw());
        const manager : GuiObjectManager = new GuiObjectManager();
        manager.setActive(resize, true);
        // ElementManager.TurnActive(resize.Id() + "_Button");
        // assert.equal(manager.IsActive(resize), true);
        const args : ResizeEventArgs = new ResizeEventArgs();
        args.Owner(resize);
        args.AvailableHeight(200);
        args.AvailableWidth(200);
        args.Height(200);
        args.ScrollBarHeight(400);
        args.ScrollBarWidth(400);
        args.Width(200);
        ResizeBar.ResizeEventHandler(args, manager, Reflection.getInstance());
        this.initSendBox();
    }

    public testsetArg() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.setArg(<IGuiCommonsArg>{
            name : "Width",
            type : "Number",
            value: 500
        }, true);
        assert.equal(resizebar.getArgs().length, 9);
        this.initSendBox();
    }

    public testsetArgsSecond() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.setArg(<IGuiCommonsArg>{
            name : "ResizeableType",
            type : "Text",
            value: ResizeableType.VERTICAL
        });
        assert.deepEqual(resizebar.getArgs().length, 9);
        this.initSendBox();
    }

    public testsetArgsThird() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.setArg(<IGuiCommonsArg>{
            name : "Height",
            type : "Number",
            value: 800
        });
        assert.deepEqual(resizebar.getArgs().length, 9);
        this.initSendBox();
    }

    public testsetArgDefault() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.setArg(<IGuiCommonsArg>{
            name : "testName",
            type : "testType",
            value: "testValue"
        }, true);
        assert.equal(resizebar.getArgs().length, 9);
        this.initSendBox();
    }

    public testTurnOn() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        ResizeBar.TurnOn(resizebar);
        assert.equal(ElementManager.getClassName(resizebar.Id() + "_Status"), GeneralCssNames.ON);
        this.initSendBox();
    }

    public testTurnOff() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        ResizeBar.TurnOff(resizebar);
        assert.equal(ElementManager.getClassName(resizebar.Id() + "_Status"), "");
        this.initSendBox();
    }

    public testActive() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        ResizeBar.TurnActive(resizebar);
        assert.equal(ElementManager.getClassName(resizebar.Id() + "_Status"), GeneralCssNames.ACTIVE);
        this.initSendBox();
    }

    public testsetWidth() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        ResizeBar.setWidth(resizebar, 400);
        /// TODO: cross-browser issue
        assert.equal(ElementManager.getCssValue(resizebar.Id(), "width"), null);
        this.initSendBox();
    }

    public testsetHeight() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        ResizeBar.setHeight(resizebar, 200);
        /// TODO: cross-browser issue
        assert.equal(ElementManager.getCssValue(resizebar.Id(), "height"), null);
        this.initSendBox();
    }

    public testResizeTo() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        resizebar.getEvents().setOnResize(($eventArgs : ResizeBarEventArgs) : void => {
            assert.equal($eventArgs.DistanceX(), 300);
            assert.equal($eventArgs.DistanceY(), 170);
        });
        ResizeBar.ResizeTo(resizebar, 300, 170);
        this.initSendBox();
    }

    public __IgnoretestResizeEventHandler() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL_AND_VERTICAL);
        const viewerargs : BaseViewerArgs = new BaseViewerArgs();
        const viewer : BaseViewer = new BaseViewer(viewerargs);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        const args : ResizeEventArgs = new ResizeEventArgs();
        args.Width(300);
        args.Height(200);
        const manager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        manager.getActive(ResizeBar);
        ResizeBar.ResizeEventHandler(args, manager, reflection);
        assert.patternEqual(resizebar.Draw(), "");
        assert.equal(ElementManager.getCssValue(resizebar.Id(), "width"), "300");
        assert.equal(ElementManager.getCssValue(resizebar.Id(), "height"), "200px");
    }

    public testResizeBarSecond() : void {
        const resizebar : ResizeBar = new ResizeBar(ResizeableType.NONE, CursorType.NS_RESIZE, "id87");
        const viewerargs : BaseViewerArgs = new BaseViewerArgs();
        const viewer : BaseViewer = new BaseViewer(viewerargs);
        resizebar.InstanceOwner(viewer);
        resizebar.Visible(true);
        resizebar.Enabled(true);
        ResizeBar.TurnActive(resizebar);
        resizebar.DisableAsynchronousDraw();
        Echo.Print(resizebar.Draw());
        assert.equal(ElementManager.Exists("id87"), true);
        this.initSendBox();
    }

    public __IgnoretestResizeBarAsynchronous() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL);
            const viewerargs : BaseViewerArgs = new BaseViewerArgs();
            const viewer : BaseViewer = new MockBaseViewer(viewerargs);
            resizebar.InstanceOwner(viewer);
            assert.onGuiComplete(resizebar,
                () : void => {
                    resizebar.Visible(true);
                    resizebar.Enabled(true);
                    resizebar.getEvents().setOnResize(($eventArgs : ResizeBarEventArgs) : void => {
                        assert.equal($eventArgs.ResizeableType(), ResizeableType.HORIZONTAL);
                        assert.equal($eventArgs.Owner(), resizebar);
                        assert.equal(resizebar.getEvents().Exists("onresize"), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestResizeBarNextAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.VERTICAL);
            const viewerargs : BaseViewerArgs = new BaseViewerArgs();
            const viewer : BaseViewer = new MockBaseViewer(viewerargs);
            resizebar.InstanceOwner(viewer);
            assert.onGuiComplete(resizebar,
                () : void => {
                    resizebar.Visible(true);
                    resizebar.Enabled(true);
                    const args : MouseEventArgs = new MouseEventArgs();
                    resizebar.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.Owner(), resizebar);
                        assert.equal(resizebar.getEvents().Exists("onclick"), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestEventOnStart() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const resize : ResizeBar = new ResizeBar(ResizeableType.HORIZONTAL, "id66");
            const viewer : BaseViewer = new MockBaseViewer();
            resize.InstanceOwner(viewer);
            resize.Visible(true);
            resize.Enabled(true);
            resize.DisableAsynchronousDraw();
            Echo.Print(resize.Draw());

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(resize, true);
            ElementManager.TurnActive(resize.Id() + "_Button");
            assert.equal(manager.IsActive(resize), true);
            const event : any = {altKey: true, button: 2};
            const mouseEventArgs : MouseEventArgs = new MouseEventArgs(event);
            mouseEventArgs.Owner(resize);

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                ($args : MoveEventArgs, manager) : void => {
                    assert.equal($args.getClassName(), "Io.Oidis.Gui.Events.Args.MouseEventArgs");
                    assert.equal($args.Owner(), resize);
                    assert.equal($args.getPositionY(), 0);
                    this.initSendBox();
                    $done();
                });
            (<any>ScrollBar).moveInit(mouseEventArgs, manager, Reflection.getInstance());
        };
    }
}
