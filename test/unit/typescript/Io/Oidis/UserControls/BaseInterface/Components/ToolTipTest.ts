/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { OpacityEventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/OpacityEventType.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { MouseEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { GuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiElement.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { ToolTip } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ToolTip.js";
import { ToolTipType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/Components/ToolTipType.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseViewer extends BaseViewer {
}

class MockToolTip extends ToolTip {
    public testData() : string[] {
        return this.excludeSerializationData();
    }

    public testStyleClassNameSetterValidator() : boolean {
        return this.styleClassNameSetterValidator("test");
    }
}

class MockGuiCommons extends GuiCommons {
}

export class ToolTipTest extends UnitTestRunner {

    public testGuiType() : void {
        const tooltip : ToolTip = new ToolTip("testTool", ToolTipType.GENERAL);
        assert.equal(tooltip.GuiType(), ToolTipType.GENERAL);
        const tooltip2 : ToolTip = new ToolTip();
        assert.equal(tooltip2.GuiType(), ToolTipType.GENERAL);
        const tooltip3 : ToolTip = new ToolTip("Tool", ToolTipType.GENERAL, "id927");
        assert.equal(tooltip3.GuiType(), ToolTipType.GENERAL);
        const tooltip4 : ToolTip = new ToolTip("Tool", "", "id927");
        tooltip4.Visible(true);
        assert.equal(tooltip3.GuiType(ToolTipType.GENERAL), ToolTipType.GENERAL);
        const tooltip5 : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id501");
        ElementManager.setCssProperty("id501", "Hide", 100);
        ElementManager.setCssProperty("id501", "Enable", "true");
        const viewer : BaseViewer = new MockBaseViewer();
        tooltip5.InstanceOwner(viewer);
        tooltip5.Visible(true);
        tooltip5.DisableAsynchronousDraw();
        Echo.Print(tooltip5.Draw());
        assert.equal(ElementManager.getClassName(tooltip5.Id() + "_Type"), tooltip5.GuiType());
        this.initSendBox();
    }

    public __IgnoretestGuiType2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tip : ToolTip = new ToolTip("tip", ToolTipType.GENERAL, "id5");
            const viewer : BaseViewer = new MockBaseViewer();
            tip.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();

            assert.onGuiComplete(tip,
                () : void => {
                    tip.Visible(true);
                    tip.Enabled(true);
                    tip.DisableAsynchronousDraw();
                    Echo.Print(tip.Draw());
                    Reflection.getInstance();
                    assert.equal(tip.GuiType(ToolTipType.GENERAL), ToolTipType.GENERAL);
                },
                $done, viewer);
        };
    }

    public __IgnoretestHideSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tip : ToolTip = new ToolTip("tip", ToolTipType.GENERAL, "id8");
            const viewer : BaseViewer = new MockBaseViewer();
            tip.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();
            assert.onGuiComplete(tip,
                () : void => {
                    tip.Visible(false);
                    tip.Enabled(true);
                    tip.DisableAsynchronousDraw();
                    Echo.Print(tip.Draw());

                    const args : ValueProgressEventArgs = new ValueProgressEventArgs("id58");
                    args.Step(9);
                    args.Owner(tip);
                    args.RangeStart(0);
                    args.RangeEnd(30);
                    args.DirectionType(DirectionType.DOWN);

                    // Loader.getHttpResolver().getEvents().setEvent(tip, OpacityEventType.COMPLETE,
                    //     ($eventArgs : ValueProgressEventArgs) : void => {
                    //         assert.equal($eventArgs.Step(), 9);
                    //         assert.equal($eventArgs.Owner(), tip);
                    //         this.initSendBox();
                    //          $done();
                    //   });
                    (<any>ToolTip).Hide();
                    EventsManager.getInstanceSingleton().FireEvent(tip, OpacityEventType.COMPLETE, args);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                },
                viewer);
        };
    }

    public __IgnoretestText() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tip : ToolTip = new ToolTip("tip", ToolTipType.GENERAL, "id5");
            const viewer : BaseViewer = new MockBaseViewer();
            tip.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();

            assert.onGuiComplete(tip,
                () : void => {
                    tip.Visible(true);
                    tip.Enabled(true);
                    tip.Text("tip");
                    tip.DisableAsynchronousDraw();
                    Echo.Print(tip.Draw());
                    Reflection.getInstance();
                    assert.equal(tip.Text(), "tip");
                },
                $done, viewer);
        };
    }

    public __IgnoretestTextSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id602");
            const gui : GuiCommons = new MockGuiCommons();
            tip.setWrappingElement(new GuiElement());
            tip.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            tip.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();
            assert.onGuiComplete(tip,
                () : void => {
                    tip.Visible(true);
                    tip.Enabled(true);
                    // tip.Text("Tooltip of Gui");
                    Reflection.getInstance();
                    tip.getEvents().FireAsynchronousMethod(() : void => {
                        tip.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                            tip.Text("Tooltip of Gui");
                            assert.equal(tip.getEvents().Exists("onclick"), true);
                            this.initSendBox();
                        });
                    }, 200);
                },
                $done, viewer);
        };
    }

    public __IgnoretestHideAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id602");
            const gui : GuiCommons = new MockGuiCommons();
            ElementManager.IsVisible("id602");
            ElementManager.setClassName("id602Type", "general");
            tip.setWrappingElement(new GuiElement());
            tip.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            tip.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();
            assert.onGuiComplete(tip,
                () : void => {
                    tip.Visible(true);
                    tip.Enabled(true);
                    Reflection.getInstance();
                    const widthManipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(tip.Id() + "Width");
                    widthManipulatorArgs.Owner(tip);
                    widthManipulatorArgs.DirectionType(DirectionType.DOWN);
                    tip.getEvents().FireAsynchronousMethod(() : void => {
                        tip.getEvents().setOnLoad(($eventArgs : EventArgs) : void => {
                            // ToolTip.Hide(tip);
                            // tip.Text("Tooltip of Gui");
                            assert.equal(tip.getEvents().Exists("onload"), true);
                            this.initSendBox();
                        });
                    }, 200);
                },
                $done, viewer);
        };
    }

    public testsetArg() : void {
        const tooltip : ToolTip = new ToolTip("testTool", ToolTipType.GENERAL);
        tooltip.setArg(<IGuiCommonsArg>{
            name : "Text",
            type : GuiCommonsArgType.TEXT,
            value: "testValue"
        }, true);
        assert.equal(tooltip.getArgs().length, 10);
        this.initSendBox();
    }

    public testsetArgSecond() : void {
        const tooltip : ToolTip = new ToolTip("testTool", ToolTipType.GENERAL);
        tooltip.setArg(<IGuiCommonsArg>{
            name : "GuiType",
            type : "Text",
            value: ToolTipType.GENERAL
        }, true);
        assert.equal(tooltip.getArgs().length, 10);
        this.initSendBox();
    }

    public testsetArgThird() : void {
        const tooltip : ToolTip = new ToolTip("testTool", ToolTipType.GENERAL);
        tooltip.setArg(<IGuiCommonsArg>{
            name : "StyleClassName",
            type : "Text",
            value: "ToolTip"
        }, true);
        assert.equal(tooltip.getArgs().length, 10);
        this.initSendBox();
    }

    public testsetArgDefault() : void {
        const tooltip : ToolTip = new ToolTip("testTool", ToolTipType.GENERAL);
        tooltip.setArg(<IGuiCommonsArg>{
            name : "testName",
            type : "testType",
            value: "testValue"
        }, true);
        assert.equal(tooltip.getArgs().length, 10);
        this.initSendBox();
    }

    public testShow() : void {
        const tooltip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id500");
        tooltip.DisableAsynchronousDraw();
        Echo.Print(tooltip.Draw());
        const event : any = {altKey: true, button: 2};
        const args : MouseEventArgs = new MouseEventArgs();
        args.NativeEventArgs(event);
        ToolTip.Show(tooltip, args);
        assert.equal(tooltip.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id500_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id500\" class=\"ToolTip\" style=\"display: none;\">" +
            "\r\n         <div id=\"id500_Type\" guiType=\"ToolTip\">" +
            "\r\n            <div id=\"id500_Border\" class=\"Background\">" +
            "\r\n               <div id=\"id500_Top\" class=\"Top\">" +
            "\r\n                  <div id=\"id500_TopLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id500_TopMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id500_TopRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id500_Middle\" class=\"Middle\">" +
            "\r\n                  <div id=\"id500_MiddleLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id500_MiddleMiddle\" class=\"Center\">" +
            "\r\n                     <div id=\"id500_Text\" class=\"Text\">TextOfTooltip</div>" +
            "\r\n                  </div>" +
            "\r\n                  <div id=\"id500_MiddleRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id500_Bottom\" class=\"Bottom\">" +
            "\r\n                  <div id=\"id500_BottomLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id500_BottomMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id500_BottomRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n            </div>" +
            "\r\n         </div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        this.initSendBox();
    }

    public testHide() : void {
        const tooltip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id501");
        ElementManager.setCssProperty("id501", "z-index", -1000);
        ElementManager.StopOpacityChange(tooltip);
        ElementManager.ChangeOpacity(tooltip, DirectionType.DOWN, 10);
        const progressArgs : ValueProgressEventArgs = new ValueProgressEventArgs("id95");
        progressArgs.DirectionType(DirectionType.DOWN);

        const viewer : BaseViewer = new MockBaseViewer();
        tooltip.InstanceOwner(viewer);

        tooltip.Visible(false);
        tooltip.Enabled(true);

        tooltip.DisableAsynchronousDraw();
        Echo.Print(tooltip.Draw());

        EventsManager.getInstanceSingleton().setEvent(tooltip, OpacityEventType.COMPLETE,
            ($eventArgs : ValueProgressEventArgs) : void => {
                assert.equal($eventArgs.DirectionType(), DirectionType.DOWN);
                assert.equal(ElementManager.IsVisible("id501"), true);
                assert.equal(ElementManager.getCssValue("id501", "z-index"), "-1000");
            });
        ToolTip.Hide(tooltip);
        this.initSendBox();
    }

    public testMove() : void {
        const tooltip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id502");
        tooltip.DisableAsynchronousDraw();
        Echo.Print(tooltip.Draw());
        const event : any = {altKey: true, button: 2};
        const args : MouseEventArgs = new MouseEventArgs();
        args.NativeEventArgs(event);
        ToolTip.Move(tooltip, args);
        assert.equal(ElementManager.getCssValue(tooltip.Id(), "top"), "0px");
        assert.equal(ElementManager.getCssValue(tooltip.Id(), "left"), "0px");
        // assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "top"), "auto");
        // assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "left"), "auto");
        assert.equal(tooltip.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id502_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id502\" class=\"ToolTip\" style=\"display: none;\">" +
            "\r\n         <div id=\"id502_Type\" guiType=\"ToolTip\">" +
            "\r\n            <div id=\"id502_Border\" class=\"Background\">" +
            "\r\n               <div id=\"id502_Top\" class=\"Top\">" +
            "\r\n                  <div id=\"id502_TopLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id502_TopMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id502_TopRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id502_Middle\" class=\"Middle\">" +
            "\r\n                  <div id=\"id502_MiddleLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id502_MiddleMiddle\" class=\"Center\">" +
            "\r\n                     <div id=\"id502_Text\" class=\"Text\">TextOfTooltip</div>" +
            "\r\n                  </div>" +
            "\r\n                  <div id=\"id502_MiddleRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id502_Bottom\" class=\"Bottom\">" +
            "\r\n                  <div id=\"id502_BottomLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id502_BottomMiddle\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id502_BottomRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n            </div>" +
            "\r\n         </div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        this.initSendBox();
    }

    public __IgnoretestMoveSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tooltip : ToolTip = new ToolTip("TextOfTooltip", ToolTipType.GENERAL, "id602");
            const gui : GuiCommons = new MockGuiCommons();
            tooltip.setWrappingElement(new GuiElement());
            tooltip.Parent(gui);

            tooltip.Text(null);
            const viewer : BaseViewer = new MockBaseViewer();
            tooltip.InstanceOwner(viewer);
            assert.onGuiComplete(tooltip,
                () : void => {
                    tooltip.Enabled(true);
                    tooltip.Visible(true);
                    tooltip.DisableAsynchronousDraw();
                    Echo.Print(tooltip.Draw());
                    const event : any = {altKey: true, button: 2};
                    const args : MouseEventArgs = new MouseEventArgs(event);
                    args.NativeEventArgs(event);

                    ElementManager.setCssProperty("id602", "top", -1000);
                    ElementManager.setCssProperty("id602", "left", -1000);
                    ToolTip.Move(tooltip, args);

                    assert.equal(ElementManager.getCssValue(tooltip.Id(), "top"), "-1000px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id(), "left"), "-1000px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "top"), "15px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "left"), "20px");
                }, $done, viewer);
        };
    }

    public __IgnoretestMoveThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const tooltip : ToolTip = new ToolTip(null, ToolTipType.GENERAL, "id26");
            const gui : GuiCommons = new MockGuiCommons("id3");
            tooltip.setWrappingElement(new GuiElement());
            tooltip.Parent(gui);
            ElementManager.setCssProperty("id3", "top", 10);
            ElementManager.setCssProperty("id3", "left", 5);

            const viewer : BaseViewer = new MockBaseViewer();
            tooltip.InstanceOwner(viewer);
            assert.onGuiComplete(tooltip,
                () : void => {
                    tooltip.Enabled(true);
                    tooltip.Visible(true);
                    tooltip.DisableAsynchronousDraw();
                    Echo.Print(tooltip.Draw());
                    const event : any = {altKey: true, button: 2};
                    const args : MouseEventArgs = new MouseEventArgs(event);
                    args.NativeEventArgs(event);
                    ElementManager.setCssProperty("id26", "top", -1000);
                    ElementManager.setCssProperty("id26", "left", -1000);
                    ToolTip.Move(tooltip, args);
                    assert.equal(ElementManager.getCssValue(tooltip.Id(), "top"), "-1000px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id(), "left"), "-1000px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "top"), "15px");
                    assert.equal(ElementManager.getCssValue(tooltip.Id() + "_Text", "left"), "20px");
                }, $done, viewer);
        };
    }

    public teststyleClassNameSetterValidator() : void {
        const tip : MockToolTip = new MockToolTip("test");
        assert.equal(tip.testStyleClassNameSetterValidator(), true);
        this.initSendBox();
    }

    public testexcludeSerializationData() : void {
        const form : MockToolTip = new MockToolTip("Tip", ToolTipType.GENERAL, "id957");
        assert.deepEqual(form.testData().toString(), "objectNamespace,objectClassName,options,availableOptionsList," +
            "parent,owner,guiPath,visible,enabled,prepared,completed,interfaceClassName,styleClassName,containerClassName,loaded," +
            "asyncDrawEnabled,contentLoaded,waitFor,outputEndOfLine,innerHtmlMap,events,elementPosition,elementBorders"
        );
        this.initSendBox();
    }
}
