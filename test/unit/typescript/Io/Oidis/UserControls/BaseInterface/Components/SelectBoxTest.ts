/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/DirectionType.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { OrientationType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/OrientationType.js";
import { PositionType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PositionType.js";
import { ProgressType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ProgressType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ScrollEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { ValueProgressEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { ValueProgressManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ValueProgressManager.js";
import { ScrollBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ScrollBar.js";
import { SelectBox } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/SelectBox.js";
import {
    SelectBoxType
} from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/Components/SelectBoxType.js";
import { IconType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/UserControls/IconType.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { SelectBoxOption } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Primitives/SelectBoxOption.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseViewer extends BaseViewer {
}

class MockGuiCommons extends GuiCommons {
}

class MockSelectBox extends SelectBox {
    public testexcludeSerializationData() : string[] {
        return this.excludeSerializationData();
    }

    public testexcludeCacheData() : string[] {
        return this.excludeCacheData();
    }
}

export class SelectBoxTest extends UnitTestRunner {
    public testgetEvents() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        const handler : any = () : void => {
            // test event handler
        };
        selectbox.getEvents().setEvent("test", handler);
        assert.equal(selectbox.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testValue() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id777");
        assert.equal(selectbox.Value("id777"), "id777");
        selectbox.Select("id777");
        assert.equal(selectbox.Value("id777"), "id777");
        const selectbox2 : SelectBox = new SelectBox(SelectBoxType.GENERAL, "555");
        assert.equal(selectbox2.Value(555), 555);
        selectbox2.Select(555);
        assert.equal(selectbox2.Value(555), 555);
        this.initSendBox();
    }

    public testWidth() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        assert.equal(selectbox.Width(60), 60);
        this.initSendBox();
    }

    public testHeight() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        assert.equal(selectbox.Height(80), 80);
        this.initSendBox();
    }

    public testMaxVisibleItemsCount() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        assert.equal(selectbox.MaxVisibleItemsCount(6), 6);
        this.initSendBox();
    }

    public __IgnoretestAdd() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "262");
            const viewer : BaseViewer = new MockBaseViewer();
            selectbox.InstanceOwner(viewer);
            assert.onGuiComplete(selectbox,
                () : void => {
                    GuiObjectManager.getInstanceSingleton().setActive(selectbox, true);
                    selectbox.MaxVisibleItemsCount(20);
                    selectbox.Add("add", 4, "style");
                    selectbox.Select("add");
                    selectbox.Add("test", 5, "style");
                    selectbox.Select("test");
                    selectbox.Select(1);
                    selectbox.Add("add", 1, "style");
                    selectbox.Add("18", 1, IconType.BLUE_SQUARE);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testClear() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        selectbox.Add("test", 4, "new item");
        assert.equal((<any>selectbox).getItems().Length(), 1);
        selectbox.Clear();
        assert.equal((<any>selectbox).getItems().Length(), 0);
        this.initSendBox();
    }

    public testDisableCharacterNavigation() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox.InstanceOwner(viewer);
        selectbox.setPosition(50, 50, PositionType.ABSOLUTE);
        selectbox.DisableAdvancedSelection();
        this.initSendBox();
    }

    public __IgnoretestClearSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "222");
            const viewer : BaseViewer = new MockBaseViewer();
            selectbox.InstanceOwner(viewer);
            assert.onGuiComplete(selectbox,
                () : void => {
                    GuiObjectManager.getInstanceSingleton().setActive(selectbox, true);
                    selectbox.Add("add", 4, "style");
                    selectbox.Select("add");
                    selectbox.Clear();
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testOptionsCount() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        assert.equal(selectbox.OptionsCount(), 0);
        this.initSendBox();
    }

    public testSelect() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        selectbox.Select(1);
        assert.equal(selectbox.getChildElements().getLast(), null);
        this.initSendBox();
    }

    public testGuiType() : void {
        const selectbox3 : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id54");
        const manager : GuiObjectManager = new GuiObjectManager();
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox3.InstanceOwner(viewer);
        selectbox3.Visible(true);
        selectbox3.DisableAsynchronousDraw();
        Echo.Print(selectbox3.Draw());
        assert.equal(ElementManager.IsVisible("id54"), true);
        selectbox3.GuiType(SelectBoxType.GENERAL);
        assert.equal(selectbox3.GuiType(), SelectBoxType.GENERAL);
        this.initSendBox();
    }

    public testGuiTypeElse() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id515");
        const manager : GuiObjectManager = new GuiObjectManager();
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox.InstanceOwner(viewer);
        assert.equal(selectbox.GuiType(SelectBoxType.BLUE), "Blue");
        assert.equal(selectbox.GuiType(), SelectBoxType.BLUE);
        this.initSendBox();
    }

    public testInitSize() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id54");
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox.InstanceOwner(viewer);
        selectbox.Visible(true);
        selectbox.DisableAsynchronousDraw();
        Echo.Print(selectbox.Draw());
        assert.equal(ElementManager.IsVisible("id54"), true);
        selectbox.setInitSize(null, null);
        assert.equal(selectbox.Width(), null);
        this.initSendBox();
    }

    public testValueSecond() : void {
        const selectbox : SelectBox = new SelectBox();
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 4);
        assert.equal(boxoption.Value(), "test");
        this.initSendBox();
    }

    public testStyleClassName() : void {
        Echo.Println("[-597087868] test<br/>   index: 5<br/>   value: test<br/>   style: testClassName<br/>" +
            "   withSeparator: true<br/>  isSelected: false");
        const selectbox : SelectBox = new SelectBox(SelectBoxType.BLUE);
        const boxoption : SelectBoxOption = new SelectBoxOption("test", selectbox, 5);
        assert.equal(boxoption.StyleClassName("testClassName"), "testClassName");
        this.initSendBox();
    }

    public testShow() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id400");
        selectbox.DisableAsynchronousDraw();
        Echo.Print(selectbox.Draw());
        SelectBox.Show(selectbox);
        assert.equal(selectbox.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id400_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id400\" class=\"SelectBox\" style=\"display: block;\">" +
            "\r\n         <div id=\"id400_Type\" guiType=\"SelectBox\">" +
            "\r\n            <div id=\"id400_Envelop\" class=\"Background\">" +
            "\r\n               <div id=\"id400_Top\" class=\"Top\">" +
            "\r\n                  <div id=\"id400_TopLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id400_TopCenter\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id400_TopRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id400_Middle\" class=\"Middle\">" +
            "\r\n                  <div id=\"id400_MiddleLeft\" class=\"Left\" style=\"height: 1px;\"></div>" +
            "\r\n                  <div id=\"id400_MiddleCenter\" class=\"Center\" style=\"height: 1px;\">" +
            "\r\n                     <div id=\"id400_Content\" class=\"Content\" style=\"height: 1px;\">" +
            "\r\n                        <div id=\"id400_Options\" class=\"Options\" style=\"height: 0;\"></div>" +
            "\r\n                        <div id=\"id400_ScrollBarEnvelop\" class=\"ScrollBarEnvelop\" style=\"display: none;\">" +
            "\r\n                           <div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n                              <div id=\"id400_ScrollBar_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n                                 <div id=\"id400_ScrollBar\" class=\"ScrollBar\" style=\"display: none;\"></div>" +
            "\r\n                              </div>" +
            "\r\n                           </div>" +
            "\r\n                        </div>" +
            "\r\n                     </div>" +
            "\r\n                  </div>" +
            "\r\n                  <div id=\"id400_MiddleRight\" class=\"Right\" style=\"height: 1px;\"></div>" +
            "\r\n               </div>" +
            "\r\n               <div id=\"id400_Bottom\" class=\"Bottom\">" +
            "\r\n                  <div id=\"id400_BottomLeft\" class=\"Left\"></div>" +
            "\r\n                  <div id=\"id400_BottomCenter\" class=\"Center\"></div>" +
            "\r\n                  <div id=\"id400_BottomRight\" class=\"Right\"></div>" +
            "\r\n               </div>" +
            "\r\n            </div>" +
            "\r\n         </div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        this.initSendBox();
    }

    public __IgnoretestHide() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id401");
            selectbox.Width(200);
            selectbox.Height(400);

            const manager : GuiObjectManager = new GuiObjectManager();
            const argsEvent : EventArgs = new EventArgs();
            argsEvent.Owner(selectbox);

            const args : ValueProgressEventArgs = new ValueProgressEventArgs("id401Width");
            args.Owner(selectbox);
            args.DirectionType(DirectionType.DOWN);
            args.Step(8);
            args.CompleteEventType(EventType.ON_COMPLETE + "Hide");
            ValueProgressManager.Execute(args);
            SelectBox.Hide(selectbox);
            this.initSendBox();
            $done();
        };
    }

    public __IgnoretestHideSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id501");
            selectbox.Width(250);
            selectbox.Height(450);

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(selectbox, false);
            const argsEvent : EventArgs = new EventArgs();
            argsEvent.Owner(selectbox);

            const args : ValueProgressEventArgs = new ValueProgressEventArgs("id501Height");
            args.Owner(selectbox);
            args.DirectionType(DirectionType.DOWN);
            args.Step(8);
            args.CompleteEventType(EventType.ON_COMPLETE + "Hide");
            ValueProgressManager.Execute(args);

            this.getEventsManager().setEvent(selectbox.Id(), EventType.ON_HIDE, () : void => {
                assert.equal(selectbox.Visible(false), false);
                assert.equal(selectbox.Height(), 450);
                this.initSendBox();
                $done();
            });
            ElementManager.SendToBack(selectbox.Id());
            SelectBox.Hide(selectbox);
        };
    }

    public testShowThird() : void {
        const selectbox3 : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id153");
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox3.InstanceOwner(viewer);
        selectbox3.Visible(true);
        selectbox3.Enabled(true);
        const args : ValueProgressEventArgs = new ValueProgressEventArgs("id801");
        args.RangeStart(200);
        args.RangeEnd(20);
        args.Step(10);
        selectbox3.DisableAdvancedSelection();
        selectbox3.Width(800);
        selectbox3.Height(800);
        selectbox3.MaxVisibleItemsCount(-1);
        selectbox3.DisableAsynchronousDraw();
        selectbox3.setOpenDirection(DirectionType.RIGHT, DirectionType.DOWN);
        Echo.Print(selectbox3.Draw());
        SelectBox.Show(selectbox3);
        assert.equal(selectbox3.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "   <div id=\"id153_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id153\" class=\"SelectBox\" style=\"display: block;\">\r\n" +
            "         <div id=\"id153_Type\" guiType=\"SelectBox\">\r\n" +
            "            <div id=\"id153_Envelop\" class=\"Background\">\r\n" +
            "               <div id=\"id153_Top\" class=\"Top\">\r\n" +
            "                  <div id=\"id153_TopLeft\" class=\"Left\"></div>\r\n" +
            "                  <div id=\"id153_TopCenter\" class=\"Center\" style=\"width: 800px;\"></div>\r\n" +
            "                  <div id=\"id153_TopRight\" class=\"Right\"></div>\r\n" +
            "               </div>\r\n" +
            "               <div id=\"id153_Middle\" class=\"Middle\">\r\n" +
            "                  <div id=\"id153_MiddleLeft\" class=\"Left\" style=\"height: 1px;\"></div>\r\n" +
            "                  <div id=\"id153_MiddleCenter\" class=\"Center\" style=\"height: 1px; width: 800px;\">\r\n" +
            "                     <div id=\"id153_Content\" class=\"Content\" style=\"height: 1px; width: 800px;\">\r\n" +
            "                        <div id=\"id153_Options\" class=\"Options\" style=\"height: 0;\"></div>\r\n" +
            "                        <div id=\"id153_ScrollBarEnvelop\" class=\"ScrollBarEnvelop\" style=\"display: none;\">\r\n" +
            "                           <div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "                              <div id=\"id153_ScrollBar_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "                                 <div id=\"id153_ScrollBar\" class=\"ScrollBar\" style=\"display: none;\"></div>\r\n" +
            "                              </div>\r\n" +
            "                           </div>\r\n" +
            "                        </div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "                  <div id=\"id153_MiddleRight\" class=\"Right\" style=\"height: 1px;\"></div>\r\n" +
            "               </div>\r\n" +
            "               <div id=\"id153_Bottom\" class=\"Bottom\">\r\n" +
            "                  <div id=\"id153_BottomLeft\" class=\"Left\"></div>\r\n" +
            "                  <div id=\"id153_BottomCenter\" class=\"Center\" style=\"width: 800px;\"></div>\r\n" +
            "                  <div id=\"id153_BottomRight\" class=\"Right\"></div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        this.initSendBox();
    }

    public testShowSecond() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id348");
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox.InstanceOwner(viewer);
        selectbox.Visible(true);
        selectbox.Enabled(true);
        selectbox.Height(300);
        selectbox.Width(400);
        selectbox.Add("_TopCenter", 50);
        selectbox.Add("_MiddleCenter", 30);
        selectbox.Add("_Content", 200);
        selectbox.DisableAsynchronousDraw();
        Echo.Print(selectbox.Draw());
        SelectBox.Show(selectbox);
        assert.equal(selectbox.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "   <div id=\"id348_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id348\" class=\"SelectBox\" style=\"display: block;\">\r\n" +
            "         <div id=\"id348_Type\" guiType=\"SelectBox\">\r\n" +
            "            <div id=\"id348_Envelop\" class=\"Background\">\r\n" +
            "               <div id=\"id348_Top\" class=\"Top\">\r\n" +
            "                  <div id=\"id348_TopLeft\" class=\"Left\"></div>\r\n" +
            "                  <div id=\"id348_TopCenter\" class=\"Center\" style=\"width: 400px;\"></div>\r\n" +
            "                  <div id=\"id348_TopRight\" class=\"Right\"></div>\r\n" +
            "               </div>\r\n" +
            "               <div id=\"id348_Middle\" class=\"Middle\">\r\n" +
            "                  <div id=\"id348_MiddleLeft\" class=\"Left\" style=\"height: 1px;\"></div>\r\n" +
            "                  <div id=\"id348_MiddleCenter\" class=\"Center\" style=\"height: 1px; width: 400px;\">\r\n" +
            "                     <div id=\"id348_Content\" class=\"Content\" style=\"height: 1px; width: 400px;\">\r\n" +
            "                        <div id=\"id348_Options\" class=\"Options\" style=\"height: 0;\"></div>\r\n" +
            "                        <div id=\"id348_ScrollBarEnvelop\" class=\"ScrollBarEnvelop\" style=\"display: none;\">\r\n" +
            "                           <div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
            "                              <div id=\"id348_ScrollBar_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "                                 <div id=\"id348_ScrollBar\" class=\"ScrollBar\" style=\"display: none;\"></div>\r\n" +
            "                              </div>\r\n" +
            "                           </div>\r\n" +
            "                        </div>\r\n" +
            "                     </div>\r\n" +
            "                  </div>\r\n" +
            "                  <div id=\"id348_MiddleRight\" class=\"Right\" style=\"height: 1px;\"></div>\r\n" +
            "               </div>\r\n" +
            "               <div id=\"id348_Bottom\" class=\"Bottom\">\r\n" +
            "                  <div id=\"id348_BottomLeft\" class=\"Left\"></div>\r\n" +
            "                  <div id=\"id348_BottomCenter\" class=\"Center\" style=\"width: 400px;\"></div>\r\n" +
            "                  <div id=\"id348_BottomRight\" class=\"Right\"></div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        this.initSendBox();
    }

    public __IgnoretestEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("id55");
            args.Step(10);
            args.CurrentValue(30);
            args.StartEventType(EventType.ON_START);

            const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id65");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            box.Height(300);
            box.Width(242);
            box.Visible(true);
            box.Enabled(true);

            box.DisableAsynchronousDraw();
            Echo.Print(box.Draw());

            this.getEventsManager().setEvent(box.Id() + "Width", EventType.ON_CHANGE,
                ($eventArgs : ValueProgressEventArgs) : void => {
                    assert.equal($eventArgs.CurrentValue(), 242);
                    assert.equal($eventArgs.Step(), 12);
                    $done();
                    this.initSendBox();
                });
            SelectBox.Show(box);
            // Loader.getHttpResolver().getEvents().FireEvent(box.Id() + "Width", EventType.ON_CHANGE, args);
        };
    }

    public __IgnoretestShowAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id992");
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    manager.setActive(box, true);
                    box.MaxVisibleItemsCount(20);
                    box.Select(200);
                    box.Add("id348_TopLeft", 200, "style");
                    box.DisableCharacterNavigation();
                    box.GuiType(SelectBoxType.GENERAL);
                    box.Visible(true);
                    box.Enabled(true);
                    box.setInitSize(500, 150);
                    const args : ValueProgressEventArgs = new ValueProgressEventArgs("id442");
                    args.Step(20);
                    args.CompleteEventType("Onchange");
                    args.DirectionType(DirectionType.UP);
                    args.ProgressType(ProgressType.LINEAR);
                    args.RangeStart(100);
                    args.RangeEnd(10);
                    ValueProgressManager.Execute(args);
                    SelectBox.Show(box);
                    // Loader.getHttpResolver().getEvents().FireEvent("id992Height", EventType.ON_COMPLETE + "Show", args);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestShowEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id901");
            const viewer : BaseViewer = new MockBaseViewer();

            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(selectbox, false);
            manager.setHovered(selectbox, true);
            const argsEvent : EventArgs = new EventArgs();
            argsEvent.Owner(selectbox);

            selectbox.InstanceOwner(viewer);
            selectbox.Add("test1", 60);
            selectbox.Add("test2", 40);
            selectbox.Width(250);
            selectbox.Height(450);
            selectbox.Visible(true);
            selectbox.Enabled(true);
            selectbox.MaxVisibleItemsCount(20);
            selectbox.OptionsCount();

            const scrollArgs : ScrollEventArgs = new ScrollEventArgs();
            scrollArgs.DirectionType(DirectionType.DOWN);
            scrollArgs.OrientationType(OrientationType.VERTICAL);
            scrollArgs.Owner(selectbox);

            EventsManager.getInstanceSingleton().setEvent(selectbox, EventType.ON_SCROLL, ($eventArgs : EventArgs) : void => {
                assert.equal($eventArgs.Owner(), selectbox);
                this.initSendBox();
                $done();
            });
            (<any>SelectBox).onBodyScrollEventHandler(scrollArgs, manager, Reflection.getInstance());
            EventsManager.getInstanceSingleton().FireEvent(selectbox, EventType.ON_SCROLL, argsEvent);
        };
    }

    public testexcludeData() : void {
        const box : MockSelectBox = new MockSelectBox(SelectBoxType.GENERAL);
        assert.equal(box.testexcludeCacheData().toString(),
            "options,availableOptionsList,events,childElements,waitFor,cached,prepared,completed,parent,owner," +
            "guiPath,interfaceClassName,styleClassName,containerClassName,innerHtmlMap,loaded,initWidth,initHeight,items," +
            "itemOptions,selectedItem,lastActiveItem,height,visibleItemsCount,selectedIndex,openDirectionVertical," +
            "openDirectionHorizontal,characterNavigationEnabled,advancedSelectionEnabled");
        this.initSendBox();
    }

    public testHeightSecond() : void {
        const selectbox : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id601");
        const viewer : BaseViewer = new MockBaseViewer();
        selectbox.InstanceOwner(viewer);
        selectbox.Visible(true);
        selectbox.Enabled(true);
        selectbox.Height(-1);
        assert.equal(selectbox.Height(), -1);
        this.initSendBox();
    }

    public __IgnoretestfinalizeShow() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id777");
            box.Add("contentWidth", 300);
            box.Add("height", 500);
            ElementManager.setCssProperty(box.Id(), "contentWidth", 300);
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);

            assert.onGuiComplete(box, () : void => {
                    box.OptionsCount();
                    box.MaxVisibleItemsCount(10);
                    box.OptionsCount();
                    box.Width(300);
                    box.Height(500);
                    box.setInitSize(300, 500);
                    // ElementManager.getElement(SelectBoxOption.Id()).offsetHeight;

                    const args : ResizeEventArgs = new ResizeEventArgs();
                    args.Width(200);
                    args.AvailableHeight(500);
                    args.AvailableWidth(300);
                    args.ScrollBarWidth(300);
                    args.ScrollBarHeight(500);
                    args.Height(500);
                    args.Owner(box);

                    const info : any = {
                        bottomHeight     : 0,
                        contentHeight    : 0,
                        contentOffsetLeft: 0,
                        contentOffsetTop : 0,
                        contentWidth     : 0,
                        height           : 0,
                        leftWidth        : 0,
                        rightWidth       : 0,
                        topHeight        : 0,
                        width            : 0
                    };
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    (<any>SelectBox).finalizeShow(box, info);
                    this.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_RESIZE, args);

                    const args2 : ResizeEventArgs = new ResizeEventArgs();
                    args2.Width(200);
                    args2.AvailableHeight(400);
                    args2.AvailableWidth(150);
                    args2.ScrollBarWidth(150);
                    args2.ScrollBarHeight(400);
                    args2.Height(400);
                    args2.Owner(box);

                    const infonext : any = {
                        bottomHeight     : 50,
                        contentHeight    : 40,
                        contentOffsetLeft: 30,
                        contentOffsetTop : 30,
                        contentWidth     : 30,
                        height           : 30,
                        leftWidth        : 40,
                        rightWidth       : 50,
                        topHeight        : 10,
                        width            : 30
                    };
                    (<any>SelectBox).finalizeShow(box, infonext);
                    this.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_RESIZE, args);

                    const args3 : ResizeEventArgs = new ResizeEventArgs();
                    args3.Width(100);
                    args3.AvailableHeight(300);
                    args3.AvailableWidth(100);
                    args3.ScrollBarWidth(100);
                    args3.ScrollBarHeight(300);
                    args3.Height(300);
                    args3.Owner(box);

                    const infothird : any = {
                        bottomHeight     : 60,
                        contentHeight    : 60,
                        contentOffsetLeft: 80,
                        contentOffsetTop : 80,
                        contentWidth     : 40,
                        height           : 40,
                        leftWidth        : 50,
                        rightWidth       : 50,
                        topHeight        : 60,
                        width            : 70
                    };
                    (<any>SelectBox).finalizeShow(box, infothird);
                    this.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_RESIZE, args);

                    const args4 : ResizeEventArgs = new ResizeEventArgs();
                    const info4 : any = {
                        bottomHeight     : 40,
                        contentHeight    : 40,
                        contentOffsetLeft: 70,
                        contentOffsetTop : 70,
                        contentWidth     : 30,
                        height           : 50,
                        leftWidth        : 20,
                        rightWidth       : 50,
                        topHeight        : 60,
                        width            : 70
                    };
                    (<any>SelectBox).finalizeShow(box, info4);
                    //  Loader.getHttpResolver().getEvents().FireEvent(box, EventType.ON_RESIZE, args4);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testsetOpenDirection2() : void {
        const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id801");
        box.setOpenDirection(DirectionType.LEFT, DirectionType.UP);
        box.setOpenDirection(DirectionType.UP, DirectionType.LEFT);
        assert.equal(box.getClassName(), "Io.Oidis.UserControls.BaseInterface.Components.SelectBox");
        this.initSendBox();
    }

    public testEventSecond() : void {
        const box : SelectBox = new SelectBox(SelectBoxType.GENERAL);
        const viewer : BaseViewer = new MockBaseViewer();
        const manager : GuiObjectManager = new GuiObjectManager();
        box.InstanceOwner(viewer);
        box.setOpenDirection(DirectionType.LEFT, DirectionType.UP);
        box.setInitSize(600, 700);
        manager.Add(box);
        box.DisableAsynchronousDraw();
        Echo.Print(box.Draw());
        manager.setActive(box, true);
        const args : EventArgs = new EventArgs();
        args.PreventDefault();
        args.StopAllPropagation();
        Reflection.getInstance();
        assert.equal(box.getEvents().Exists(EventType.ON_CHANGE), false);
        box.getEvents().setOnChange(($eventArgs : EventArgs) : void => {
            box.Value("Add");
            assert.equal(box.Height(), 700);
            assert.equal(box.getEvents().Exists("onchange"), true);
            this.initSendBox();
            manager.Clear();
        });
    }

    public testShowinfo() : void {
        const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id811");
        const viewer : BaseViewer = new MockBaseViewer();
        box.InstanceOwner(viewer);
        box.setOpenDirection(DirectionType.LEFT, DirectionType.UP);
        box.setInitSize(600, 700);
        box.DisableAdvancedSelection();
        box.Add("_Envelop", "left");
        box.Add("width", 300);
        box.Add("height", 500, "_Options");
        box.Select("_Envelop");
        box.Select("width");
        box.Select("height");
        box.MaxVisibleItemsCount(10);
        box.setInitSize(300, 500);
        assert.equal(box.getSize().Width(), 0);
        this.initSendBox();
    }

    public __IgnoretestSelectBoxAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(SelectBoxType.GENERAL, "id811");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            // box.InstanceOwner(viewer);
            box.setOpenDirection(DirectionType.LEFT, DirectionType.UP);
            // box.setInitSize(600, 700);
            assert.onGuiComplete(box,
                () : void => {
                    box.getEvents().setOnSelect(($eventArgs : EventArgs) : void => {
                        box.Select("Volvo");
                        // box.Select("BMW");
                        // box.Select("Jaguar");
                        assert.equal(box.getEvents().Exists("onselect"), true);
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandlerSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id33");
            const manager : GuiObjectManager = new GuiObjectManager();
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            manager.Add(box);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    // box.DisableAsynchronousDraw();
                    // Echo.Print(box.Draw());

                    manager.setActive(box, true);

                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 38};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);

                    (<any>SelectBox).onKeyDownEventHandler(args, manager, Reflection.getInstance());
                    assert.equal(manager.IsActive(box), true);
                    manager.Clear();
                }, $done, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandlerSixth() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id65421");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(box, true);
                    assert.equal(manager.IsActive(box), true);
                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 1};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);
                    (<any>SelectBox).onKeyDownEventHandler(args, manager, Reflection.getInstance());
                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id9911");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(box, true);
                    assert.equal(manager.IsActive(box), true);
                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 40};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);
                    (<any>SelectBox).onKeyDownEventHandler(args, manager, Reflection.getInstance());
                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandlerFive() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id7711");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    box.MaxVisibleItemsCount(40);
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(box, true);
                    assert.equal(manager.IsActive(box), true);
                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 37, ctrlKey: true};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);
                    (<any>SelectBox).onKeyDownEventHandler(args, manager, Reflection.getInstance());
                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandlerThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id4411");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(box, true);
                    assert.equal(manager.IsActive(box), true);
                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 32};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);

                    (<any>SelectBox).onKeyDownEventHandler(args, manager);

                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestKeyDownEventHandlerForth() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id5511");
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    box.Enabled(true);
                    box.DisableAsynchronousDraw();
                    Echo.Print(box.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(box, true);
                    assert.equal(manager.IsActive(box), true);
                    const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 39};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.Owner(box);

                    (<any>SelectBox).onKeyDownEventHandler(args, manager);

                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestonScrollBarMoveEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id6611");
            const bar : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "id3311");
            const viewer : BaseViewer = new MockBaseViewer();
            bar.InstanceOwner(viewer);
            assert.onGuiComplete(bar,
                () : void => {
                    bar.Visible(true);
                    bar.Enabled(true);
                    bar.DisableAsynchronousDraw();
                    Echo.Print(bar.Draw());
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(bar, true);
                    assert.equal(manager.IsActive(bar), true);
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    args.Owner(bar);
                    args.Position(50);

                    (<any>SelectBox).onScrollBarMoveEventHandler(args, manager, Reflection.getInstance());

                }, () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestonBodyScrollEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.HORIZONTAL, "id663");

            // scrollbar.getEvents().setOnButton(($eventArgs : ScrollEventArgs) : void => {
            //     assert.equal($eventArgs.OrientationType(), scrollbar.OrientationType());
            //     $done();
            // });
            const viewer : BaseViewer = new MockBaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box, () : void => {
                const gui : GuiCommons = new MockGuiCommons("id44444");
                gui.Visible(true);
                gui.Enabled(true);
                box.Parent(gui);
                box.Visible(true);
                box.Enabled(true);
                box.Add("width", 50);
                box.setPosition(40, 40, PositionType.ABSOLUTE);
                box.DisableAsynchronousDraw();
                Echo.Print(box.Draw());
                const manager : GuiObjectManager = new GuiObjectManager();
                manager.setActive(box, true);
                manager.setHovered(box, true);
                assert.equal(manager.IsActive(box), true);
                const args : ScrollEventArgs = new ScrollEventArgs();
                // args.Owner(box.Id());
                args.Owner(gui);
                args.Owner(box);

                args.OrientationType(OrientationType.VERTICAL);
                args.Position((<any>SelectBox).scrollTop(box));
                // args.Position(200);

                (<any>SelectBox).onBodyScrollEventHandler(args, manager, Reflection.getInstance());
                this.getEventsManager().FireEvent(SelectBox.ClassName(), EventType.ON_SCROLL, args);
            }, () : void => {
                // TODO: waite for fire of internal event
                $done();
            }, viewer);
        };
    }

    public __IgnoretestscrollToOption() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const box : SelectBox = new SelectBox(OrientationType.VERTICAL, "id311");
            const selectbox : SelectBoxOption = new SelectBoxOption("id311_Content", box, 0);
            const viewer : BaseViewer = new BaseViewer();
            box.InstanceOwner(viewer);
            assert.onGuiComplete(box,
                () : void => {
                    box.Visible(true);
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    args.Owner(box);
                    args.OrientationType(OrientationType.VERTICAL);
                    args.Position(300);

                    (<any>SelectBox).scrollToOption(box, selectbox);
                    EventsManager.getInstanceSingleton().FireEvent(box, EventType.ON_SCROLL, args);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    protected tearDown() : void {
        //      this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
