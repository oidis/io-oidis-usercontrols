/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { DragBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/DragBar.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockBaseViewer extends BaseViewer {
}

class MockGuiCommons extends GuiCommons {
}

export class DragBarTest extends UnitTestRunner {
    public testConstructor() : void {
        const dragbar : DragBar = new DragBar("id558");
        assert.equal(dragbar.Id(), "id558");
        this.initSendBox();
    }

    public testgetEvents() : void {
        const dragbar : DragBar = new DragBar();
        const handler : any = () : void => {
            // test event handler
        };
        dragbar.getEvents().setEvent("Complete", handler);
        assert.equal(dragbar.getEvents().Exists("Complete"), true);
        this.initSendBox();
    }

    public testEventOnStart() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = new DragBar("id5");
            const viewer : BaseViewer = new MockBaseViewer();
            dragbar.InstanceOwner(viewer);
            const gui1 : GuiCommons = new MockGuiCommons("id87");
            dragbar.Parent(gui1);
            assert.onGuiComplete(dragbar,
                () : void => {
                    dragbar.Width(300);
                    dragbar.Height(500);
                    dragbar.Visible(true);
                    dragbar.Enabled(true);

                    const manager : GuiObjectManager = new GuiObjectManager();
                    // manager.setActive(gui1, true);

                    const event : any = {altKey: true, button: 2};
                    const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                    moveEventArgs.Owner(dragbar);
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        manager.setActive(dragbar, true);
                    }, 500);
                    manager.setActive(dragbar, true);
                    assert.equal(manager.IsActive(<IClassName>DragBar), true);
                    (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_START, moveEventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testEventOnChangeAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = new DragBar("id2");
            const gui1 : GuiCommons = new MockGuiCommons("id23");
            dragbar.Parent(gui1);
            const viewer : BaseViewer = new MockBaseViewer();
            dragbar.InstanceOwner(viewer);

            assert.onGuiComplete(dragbar,
                () : void => {
                    dragbar.getEvents().setOnDragChange(() : void => {
                        const args : DragEvent = new DragEvent("drag", null);
                        dragbar.IsCached();
                    });
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public testEventOnChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = new DragBar("id2");
            const gui1 : GuiCommons = new MockGuiCommons("id23");
            dragbar.Parent(gui1);
            const viewer : BaseViewer = new MockBaseViewer();
            dragbar.InstanceOwner(viewer);
            assert.onGuiComplete(dragbar,
                () : void => {
                    dragbar.Visible(true);
                    dragbar.Enabled(true);
                    const manager : GuiObjectManager = new GuiObjectManager();

                    // manager.setActive(gui1, true);
                    // manager.setActive(dragbar, true);

                    const event : any = {altKey: true, button: 2};
                    const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                    moveEventArgs.Owner(dragbar);

                    this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                        ($args : MoveEventArgs, manager) : void => {
                            assert.equal($args.getClassName(), "Io.Oidis.Gui.Events.Args.MoveEventArgs");
                            this.initSendBox();
                            $done();
                        });

                    manager.setActive(gui1, true);
                    manager.setActive(dragbar, true);
                    (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_CHANGE, moveEventArgs);
                    // this.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_DRAG_CHANGE, moveEventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testEventOnComplete() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = new DragBar("id21");
            const gui1 : GuiCommons = new MockGuiCommons("id231");
            dragbar.Parent(gui1);
            const viewer : BaseViewer = new MockBaseViewer();
            dragbar.InstanceOwner(viewer);
            assert.onGuiComplete(dragbar,
                () : void => {
                    dragbar.Visible(true);
                    dragbar.Enabled(true);
                    const manager : GuiObjectManager = new GuiObjectManager();
                    // manager.setActive(gui1, true);
                    const event : any = {altKey: true, button: 2};
                    const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);
                    moveEventArgs.Owner(dragbar);
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        manager.setActive(dragbar, true);
                    }, 400);
                    (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                    this.getEventsManager().FireEvent(DragBar.ClassName(), EventType.ON_COMPLETE, moveEventArgs, true);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public testEventOnCompleteSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dragbar : DragBar = new DragBar("id21");
            const gui1 : GuiCommons = new MockGuiCommons("id231");
            dragbar.Parent(gui1);
            const viewer : BaseViewer = new MockBaseViewer();
            dragbar.InstanceOwner(viewer);

            dragbar.Visible(true);
            dragbar.Enabled(true);
            // dragbar.DisableAsynchronousDraw();
            // Echo.Print(dragbar.Draw());
            assert.onGuiComplete(dragbar,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();
                    manager.setActive(dragbar, true);
                    // ElementManager.TurnActive(dragbar.Id() + "_Button");
                    assert.equal(manager.IsActive(dragbar), true);
                    const event : any = {altKey: true, button: 2};
                    const moveEventArgs : MoveEventArgs = new MoveEventArgs(event);

                    this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                        ($args : MoveEventArgs, manager) : void => {
                            this.getEventsManager().FireAsynchronousMethod(() : void => {
                                assert.equal($args.getClassName(), "Io.Oidis.Gui.Events.Args.MouseEventArgs");
                                // assert.equal($args.Owner(), scrollbar);
                                manager.setActive(dragbar, true);
                                this.initSendBox();
                                $done();
                            }, 400);
                        });

                    (<any>DragBar).moveInit(moveEventArgs, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(DragBar.ClassName(), EventType.ON_COMPLETE, moveEventArgs, true);
                    EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                        // empty handler
                    }, 800);
                },
                () : void => {
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }
}
