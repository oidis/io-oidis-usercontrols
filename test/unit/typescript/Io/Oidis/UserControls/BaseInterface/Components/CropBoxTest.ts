/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiCommonsArgType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { CropBoxEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/CropBoxEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IGuiCommonsArg } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { CropBox } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/CropBox.js";
import { ResizeBar } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Components/ResizeBar.js";
import { CropBoxType } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/BaseInterface/Enums/Components/CropBoxType.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/UserControls/Events/EventsManager.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

class MockGuiCommons extends GuiCommons {
}

class MockBaseViewer extends BaseViewer {
}

export class CropBoxTest extends UnitTestRunner {
    public testgetEvents() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "owner");
        const handler : any = () : void => {
            // test event handler
        };
        cropbox.getEvents().setEvent("test", handler);
        assert.equal(cropbox.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testGuiType() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "owner");
        assert.equal(cropbox.GuiType(), "");
        assert.equal(cropbox.GuiType(CropBoxType.GENERAL), CropBoxType.GENERAL);
        this.initSendBox();
    }

    public testEnvelopOwner() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        assert.equal(cropbox.EnvelopOwner("EnvelopOwnerTest"), "EnvelopOwnerTest");
        this.initSendBox();
    }

    public __IgnoretestsetDimension() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "owner", "id73");
        cropbox.setDimensions(1, 10, 30, 1);
        assert.equal((<any>cropbox).topOffset.Top(), 10);
        assert.equal((<any>cropbox).topOffset.Left(), 1);
        assert.equal((<any>cropbox).bottomOffset.Top(), 1);
        assert.equal((<any>cropbox).bottomOffset.Left(), 30);
        assert.deepEqual(cropbox.getProperties().toString(),
            "guiId,parent,owner,prepared,completed,loaded,innerHtmlMap,guiType,size,startOffset,topOffset,bottomOffset," +
            "autoCenter,topResizeBar,bottomResizeBar,envelopOwner,objectNamespace,objectClassName");
        this.initSendBox();
    }

    public testsetDimensionSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id6");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui, "id8");
            cropbox.Visible(true);
            cropbox.Enabled(true);
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(cropbox,
                () : void => {
                    cropbox.InstanceOwner(viewer);
                    cropbox.setDimensions(1, 100, 100, 1);
                    cropbox.setDimensions(1, 200, 200, 1);
                    assert.equal((<any>cropbox).topOffset.Top(), 200);
                    assert.equal((<any>cropbox).topOffset.Left(), 1);
                    assert.equal((<any>cropbox).bottomOffset.Top(), 1);
                    assert.equal((<any>cropbox).bottomOffset.Left(), 200);
                }, $done, viewer);
        };
    }

    public testsetDimensionThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui1 : GuiCommons = new MockGuiCommons("id6");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui1, "id32");
            cropbox.Parent(gui1);
            const viewer : BaseViewer = new BaseViewer();
            cropbox.InstanceOwner(viewer);

            assert.onGuiComplete(cropbox,
                () : void => {
                    cropbox.setDimensions(100, 5, 5, 100);
                    assert.equal((<any>cropbox).topOffset.Top(), 5);
                    assert.equal((<any>cropbox).topOffset.Left(), 100);
                    assert.equal((<any>cropbox).bottomOffset.Top(), 100);
                    assert.equal((<any>cropbox).bottomOffset.Left(), 5);
                }, $done, viewer);
        };
    }

    public __IgnoretestsetSizeSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui1 : GuiCommons = new MockGuiCommons("id4");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui1, "id44");
            cropbox.Parent(gui1);
            const viewer : BaseViewer = new BaseViewer();
            cropbox.InstanceOwner(viewer);
            cropbox.Enabled(true);
            cropbox.EnvelopOwner(gui1);
            cropbox.Visible(true);

            assert.onGuiComplete(cropbox,
                () : void => {
                    cropbox.setSize(200, 100);
                    cropbox.GuiType("General");
                    cropbox.DisableAsynchronousDraw();
                    assert.patternEqual(ElementManager.getElement(cropbox).parentElement.parentElement.outerHTML,
                        "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
                        "   <div id=\"id44_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "      <div id=\"id44\" class=\"CropBox\" style=\"display: block;\">\r\n" +
                        "         <div id=\"id44_Type\" guiType=\"CropBox\">\r\n" +
                        "            <div id=\"id44_BorderTop\" class=\"Top\"></div>\r\n" +
                        "            <div id=\"id44_BorderMiddle\" class=\"Middle\">\r\n" +
                        "               <div id=\"id44_BorderMiddleLeft\" class=\"Left\"></div>\r\n" +
                        "               <div id=\"id44_BorderMiddleCenter\" class=\"Center\">\r\n" +
                        "                  <div id=\"id44_Top\" class=\"Top\">\r\n" +
                        "                     <div id=\"id44_TopLeft\" class=\"Left\">\r\n" +
                        "                        <div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
                        "                           <div id=\"ResizeBar*_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "                              <div id=\"ResizeBar*\" class=\"ResizeBar\" style=\"display: none;\"></div>\r\n" +
                        "                           </div>\r\n" +
                        "                        </div>\r\n" +
                        "                     </div>\r\n" +
                        "                     <div id=\"id44_TopCenter\" class=\"Center\"></div>\r\n" +
                        "                     <div id=\"id44_TopRight\" class=\"Right\"></div>\r\n" +
                        "                  </div>\r\n" +
                        "                  <div id=\"id44_Middle\" class=\"Middle\">\r\n" +
                        "                     <div id=\"id44_MiddleLeft\" class=\"Left\"></div>\r\n" +
                        "                     <div id=\"id44_MiddleCenter\" class=\"Center\"></div>\r\n" +
                        "                     <div id=\"id44_MiddleRight\" class=\"Right\"></div>\r\n" +
                        "                  </div>\r\n" +
                        "                  <div id=\"id44_Bottom\" class=\"Bottom\">\r\n" +
                        "                     <div id=\"id44_BottomLeft\" class=\"Left\"></div>\r\n" +
                        "                     <div id=\"id44_BottomCenter\" class=\"Center\"></div>\r\n" +
                        "                     <div id=\"id44_BottomRight\" class=\"Right\">\r\n" +
                        "                        <div class=\"IoOidisUserControlsBaseInterfaceComponents\">\r\n" +
                        "                           <div id=\"ResizeBar*_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "                              <div id=\"ResizeBar*\" class=\"ResizeBar\" style=\"display: none;\"></div>\r\n" +
                        "                           </div>\r\n" +
                        "                        </div>\r\n" +
                        "                     </div>\r\n" +
                        "                  </div>\r\n" +
                        "               </div>\r\n" +
                        "               <div id=\"id44_BorderMiddleRight\" class=\"Right\"></div>\r\n" +
                        "            </div>\r\n" +
                        "            <div id=\"id44_BorderBottom\" class=\"Bottom\"></div>\r\n" +
                        "         </div>\r\n" +
                        "      </div>\r\n" +
                        "   </div>\r\n" +
                        "</div>");
                    assert.equal(cropbox.getSize().Height(), 0);
                    assert.equal(cropbox.getSize().Width(), 0);
                }, $done, viewer);
        };
    }

    public __IgnoretestsetSize() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "owner", "id73");
        cropbox.setSize(100, 200);
        /// TODO: duplicated Width and Height records
        assert.deepEqual(cropbox.getArgs(), [
            {
                name : "Id",
                type : "Text",
                value: "id73"
            },
            {
                name : "StyleClassName",
                type : "Text",
                value: ""
            },
            {
                name : "Enabled",
                type : "Bool",
                value: true
            },
            {
                name : "Visible",
                type : "Bool",
                value: true
            },
            {
                name : "Top",
                type : "Number",
                value: 0
            },
            {
                name : "Left",
                type : "Number",
                value: 0
            },
            {
                name : "Width",
                type : "Number",
                value: 100
            },
            {
                name : "Height",
                type : "Number",
                value: 200
            },
            {
                items: ["GENERAL"],
                name : "GuiType",
                type : "List",
                value: "GENERAL"
            }
        ]);
    }

    public __IgnoretestEnabled() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        assert.equal(cropbox.Enabled(), true);
        assert.equal(cropbox.Enabled(false), false);
    }

    public __IgnoretestsetArgs() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id75");
        cropbox.Enabled(true);
        cropbox.StyleClassName("style");
        cropbox.setArg(<IGuiCommonsArg>{
            name : "Height",
            type : GuiCommonsArgType.NUMBER,
            value: 50
        }, true);
        assert.deepEqual(cropbox.getArgs().length, 11);
    }

    public __IgnoretestsetArgsThird() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id75");
        cropbox.Enabled(true);
        cropbox.StyleClassName("style");
        cropbox.setArg(<IGuiCommonsArg>{
            name : "GuiType",
            type : GuiCommonsArgType.TEXT,
            value: CropBoxType.GENERAL
        }, true);
        assert.deepEqual(cropbox.getArgs().length, 11);
    }

    public __IgnoretestsetArgsDefault() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        cropbox.setArg(<IGuiCommonsArg>{
            name : "testName",
            type : "testType",
            value: "testValue"
        }, true);
        assert.equal(cropbox.getArgs().length, 11);
    }

    public __IgnoretestConstructor() : void {
        const gui : GuiCommons = new MockGuiCommons("id454");
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui, "id454");
        assert.equal(cropbox.Id(), "id454");
    }

    public testgetEventsSecond() : void {
        const cropbox : CropBox = new CropBox();
        const handler : any = () : void => {
            // test event handler
        };
        cropbox.getEvents().setEvent("test", handler);
        assert.equal(cropbox.getEvents().Exists("test"), true);
        this.initSendBox();
    }

    public testGuiTypeSecond() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        assert.equal(cropbox.GuiType(CropBoxType.GENERAL), CropBoxType.GENERAL);
        this.initSendBox();
    }

    public testEnvelopOwnerSecond() : void {
        const gui : GuiCommons = new MockGuiCommons("id989");
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui, "id989");
        assert.equal(cropbox.EnvelopOwner(), "id989");
        this.initSendBox();
    }

    public testsetDimensions() : void {
        const gui : GuiCommons = new MockGuiCommons("id656");
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui, "id656");
        cropbox.setDimensions(100, 100, 600, 600);
        Echo.PrintCode(JSON.stringify(gui.Draw()));
        assert.equal(cropbox.Draw(),
            "\r\n<div class=\"IoOidisUserControlsBaseInterfaceComponents\">" +
            "\r\n   <div id=\"id656_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"id656\" class=\"CropBox\" style=\"display: none;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        this.initSendBox();
    }

    public __IgnoretestsetSizeNext() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        cropbox.setSize(50, 100);
        assert.equal(cropbox.getSize().Width(50), 50);
        assert.equal(cropbox.getSize().Height(100), 100);
    }

    public testEnabledSecond() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        assert.equal(cropbox.Enabled(), true);
        assert.equal(cropbox.Enabled(false), false);
        this.initSendBox();
    }

    public testsetArgsSecond() : void {
        const cropbox : CropBox = new CropBox(CropBoxType.GENERAL);
        cropbox.setArg(<IGuiCommonsArg>{
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: 100
        }, true);
        assert.equal(cropbox.getArgs().length, 11);
        this.initSendBox();
    }

    public testCropBoxAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id88");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui, "id52");
            cropbox.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            cropbox.InstanceOwner(viewer);
            const args : CropBoxEventArgs = new CropBoxEventArgs();
            assert.onGuiComplete(cropbox,
                () : void => {
                    cropbox.Enabled(true);
                    cropbox.EnvelopOwner(gui);
                    cropbox.Visible(true);
                    cropbox.getEvents().setOnResizeChange(($eventArgs : CropBoxEventArgs) : void => {
                        $eventArgs.OffsetLeft(40);
                        $eventArgs.OffsetTop(40);
                        assert.equal(cropbox.getEvents().Exists("onresizeChange"), true);
                    });
                },
                () : void => {
                    assert.equal(cropbox.Visible(), true);
                    this.initSendBox();
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui1 : GuiCommons = new MockGuiCommons("id9");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, gui1, "id90");
            cropbox.Parent(gui1);
            const viewer : BaseViewer = new BaseViewer();
            cropbox.InstanceOwner(viewer);
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.setActive(gui1, true);
            const reflection : Reflection = new Reflection();
            const args : CropBoxEventArgs = new CropBoxEventArgs();
            args.OffsetLeft(100);
            args.OffsetLeft(50);
            ElementManager.setCssProperty(gui1.Id(), "_BorderTop", "20px");
            ElementManager.setCssProperty(gui1.Id(), "_BorderMiddleLeft", "50px");
            assert.onGuiComplete(cropbox,
                () : void => {
                    const handler : any = () : void => {
                        // EventHandler
                    };
                    EventsManager.getInstanceSingleton().setEvent(gui1, EventType.ON_RESIZE_START, handler, args);
                    EventsManager.getInstanceSingleton().FireEvent(gui1, EventType.ON_RESIZE_START, args);
                    EventsManager.getInstanceSingleton().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE_START, args);
                    assert.equal(ElementManager.getElement(gui1.Id() + "_BorderTop"), null);
                }, $done, viewer);
        };
    }

    public __IgnoretestonResizeStartEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id66");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id6", gui.Id());
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.VERTICAL);
            cropbox.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            cropbox.InstanceOwner(viewer);
            assert.onGuiComplete(cropbox,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();

                    manager.setActive(cropbox, true);
                    manager.setActive(resizebar, true);

                    cropbox.EnvelopOwner(gui);
                    cropbox.setDimensions(60, 10, 60, 10);
                    cropbox.Enabled(true);
                    cropbox.Visible(true);

                    const event : any = {altKey: true, button: 2};
                    const args : ResizeBarEventArgs = new ResizeBarEventArgs(event);
                    args.Owner(cropbox);
                    args.Owner(resizebar);
                    // args.Owner(gui);
                    manager.setActive(gui, true);
                    args.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
                    const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                    eventArgs.Owner(gui);
                    eventArgs.Owner(cropbox);
                    eventArgs.Height(gui.Height(60));
                    eventArgs.Width(gui.Width(30));

                    assert.equal(manager.IsActive(gui), true);
                    (<any>CropBox).onResizeStartEventHandler(args, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(gui, EventType.ON_RESIZE_START, eventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestonResizeCompleteEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id77");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id7", gui.Id());
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.VERTICAL);
            cropbox.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            cropbox.InstanceOwner(viewer);
            assert.onGuiComplete(cropbox,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();

                    // manager.setActive(cropbox, true);
                    // manager.setActive(resizebar, true);

                    cropbox.EnvelopOwner(gui);
                    cropbox.setDimensions(60, 10, 60, 10);
                    cropbox.Enabled(true);
                    cropbox.Visible(true);

                    const event : any = {altKey: true, button: 2};
                    const args : ResizeBarEventArgs = new ResizeBarEventArgs(event);
                    args.Owner(cropbox);
                    args.Owner(gui);
                    args.Owner(resizebar);

                    // args.Owner(cropbox.Parent(gui));
                    manager.setActive(gui, true);
                    args.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
                    const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                    eventArgs.Owner(gui);
                    eventArgs.Owner(cropbox);
                    eventArgs.Height(gui.Height(60));
                    eventArgs.Width(gui.Width(30));

                    assert.equal(manager.IsActive(gui), true);
                    (<any>CropBox).onResizeCompleteEventHandler(args, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(cropbox, EventType.ON_RESIZE_COMPLETE, eventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public __IgnoreonResizeChangeEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id88");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id8", gui.Id());
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.VERTICAL);
            cropbox.Parent(gui);
            const viewer : BaseViewer = new MockBaseViewer();
            cropbox.InstanceOwner(viewer);
            assert.onGuiComplete(cropbox,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();
                    // ResizeBar.TurnOn(resizebar);
                    // ResizeBar.TurnActive(resizebar);
                    manager.setActive(cropbox, true);
                    manager.setActive(resizebar, true);

                    cropbox.EnvelopOwner(gui);
                    cropbox.setDimensions(60, 10, 60, 10);
                    cropbox.Enabled(true);
                    cropbox.Visible(true);

                    const event : any = {altKey: true, button: 2};
                    const args : ResizeBarEventArgs = new ResizeBarEventArgs(event);
                    args.Owner(cropbox);
                    args.Owner(gui);
                    args.Owner(resizebar);

                    // args.Owner(cropbox.Parent(gui));
                    manager.setActive(gui, true);
                    args.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
                    const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                    eventArgs.Owner(gui);
                    eventArgs.Owner(cropbox);
                    eventArgs.Height(gui.Height(60));
                    eventArgs.Width(gui.Width(30));

                    assert.equal(manager.IsActive(gui), true);
                    (<any>CropBox).onResizeChangeEventHandler(args, manager, Reflection.getInstance());
                    EventsManager.getInstanceSingleton().FireEvent(CropBox.ClassName(), EventType.ON_RESIZE_CHANGE, eventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    public __IgnoreonResize() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id88");
            const cropbox : CropBox = new CropBox(CropBoxType.GENERAL, "id8", gui.Id());
            const resizebar : ResizeBar = new ResizeBar(ResizeableType.VERTICAL);
            cropbox.Parent(gui);
            gui.Height(100);
            gui.Width(200);
            const viewer : BaseViewer = new MockBaseViewer();
            cropbox.InstanceOwner(viewer);

            assert.onGuiComplete(cropbox,
                () : void => {
                    const manager : GuiObjectManager = new GuiObjectManager();
                    // ResizeBar.TurnOn(resizebar);
                    // ResizeBar.TurnActive(resizebar);
                    manager.setActive(cropbox, true);
                    manager.setActive(resizebar, true);

                    cropbox.EnvelopOwner(gui);
                    cropbox.setDimensions(60, 10, 60, 10);
                    cropbox.Enabled(true);
                    cropbox.Visible(true);

                    const event : any = {altKey: true, button: 2};
                    const args : ResizeBarEventArgs = new ResizeBarEventArgs(event);
                    args.Owner(cropbox);
                    args.Owner(gui);
                    args.Owner(resizebar);

                    // args.Owner(cropbox.Parent(gui));
                    manager.setActive(gui, true);
                    args.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
                    const eventArgs : CropBoxEventArgs = new CropBoxEventArgs();
                    eventArgs.Owner(gui);
                    eventArgs.Owner(cropbox);
                    eventArgs.Height(gui.Height(60));
                    eventArgs.Width(gui.Width(30));

                    assert.equal(manager.IsActive(gui), true);
                    (<any>CropBox).resize(cropbox, false);
                    EventsManager.getInstanceSingleton().FireEvent(cropbox, EventType.ON_RESIZE, eventArgs);
                },
                () : void => {
                    $done();
                }, viewer);
        };
    }

    protected tearDown() : void {
        //  this.initSendBox();
        console.clear(); // eslint-disable-line no-console
    }
}
