/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../../../../reference.d.ts" />
// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Io.Oidis.UserControls.RuntimeTests {
    "use strict";
    import SeleniumTestRunner = Io.Oidis.Commons.SeleniumTestRunner;

    export class ExecuteRuntimeTest extends SeleniumTestRunner {

        public testToolTip() : void {
            this.driver.findElement(this.by.linkText("ToolTip")).click().then(() : void => {
                this.validate();
            });
        }

        public testNotification() : void {
            this.driver.findElement(this.by.linkText("Notification")).click().then(() : void => {
                this.validate();
            });
        }

        public testScrollBar() : void {
            this.driver.findElement(this.by.linkText("ScrollBar")).click().then(() : void => {
                this.validate();
            });
        }

        public testDragBar() : void {
            this.driver.findElement(this.by.linkText("DragBar")).click().then(() : void => {
                this.validate();
            });
        }

        public testResizeBar() : void {
            this.driver.findElement(this.by.linkText("ResizeBar")).click().then(() : void => {
                this.validate();
            });
        }

        protected setUp() : void {
            this.driver.get("file:///" + this.getAbsoluteRoot() + "/build/target/index.html");
        }

        protected after() : void {
            this.driver.quit();
        }

        private validate() : void {
            this.driver.findElement(this.by.className("Result")).getText().then(($value : string) : void => {
                assert.equal($value, "SUCCESS");
            });
        }
    }
}
